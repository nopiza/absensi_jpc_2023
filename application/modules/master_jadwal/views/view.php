  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Master Jadwal</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Master Jadwal</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Master Jadwal</h3>

              <div class="card-tools">
                <a href="#" class="btn btn-info btn-sm" onclick="add()"><i class="fa fa-plus"></i> Tambah Jadwal</a>&nbsp;
                <button class="btn btn-default btn-sm" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="3%">No</th>
                    <th width="20%">Nama Jam Kerja</th>
                    <th width="20%">Keterangan</th>
                    <th width="10%">Jam Masuk</th>
                    <th width="10%">Jam Pulang</th>
                    <th width="12%">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>






<div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Large Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id" id="id"/>

          <div class="form-group row">
            <label class="col-sm-3 control-label">Nama Jam Kerja</label>
            <div class="col-md-9">
              <input name="nama_jamker" type="text" class="form-control" id="nama_jamker">
            </div>
          </div>


          <table class="table table-bordered">
            <tr>
              <th width="40%">Hari</th>
              <th width="20%">Masuk</th>
              <th width="20%">Batas</th>
              <th width="20%">Pulang</th>
            </tr>

            <tr>
              <td>Senin</td>
              <td><input name="senin_m" type="time" class="form-control" id="senin_m" ></td>
              <td><input name="senin_batas" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="senin_p" type="time" class="form-control" id="nama_mesin" ></td>
            </tr>

            <tr>
              <td>Selasa</td>
              <td><input name="selasa_m" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="selasa_batas" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="selasa_p" type="time" class="form-control" id="nama_mesin" ></td>
            </tr>

            <tr>
              <td>Rabu</td>
              <td><input name="rabu_m" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="rabu_batas" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="rabu_p" type="time" class="form-control" id="nama_mesin" ></td>
            </tr>

            <tr>
              <td>Kamis</td>
              <td><input name="kamis_m" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="kamis_batas" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="kamis_p" type="time" class="form-control" id="nama_mesin" ></td>
            </tr>

            <tr>
              <td>Jumat</td>
              <td><input name="jumat_m" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="jumat_batas" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="jumat_p" type="time" class="form-control" id="nama_mesin" ></td>
            </tr>

            <tr>
              <td>Sabtu</td>
              <td><input name="sabtu_m" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="sabtu_batas" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="sabtu_p" type="time" class="form-control" id="nama_mesin" ></td>
            </tr>

            <tr>
              <td>Minggu</td>
              <td><input name="minggu_m" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="minggu_batas" type="time" class="form-control" id="nama_mesin" ></td>
              <td><input name="minggu_p" type="time" class="form-control" id="nama_mesin" ></td>
            </tr>
          </table>

                                                  
          </form>

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<?php  $this->load->view('template/footer'); ?>



<script src="<?php echo base_url('assets/admin/plugins/select2/select2.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2-bootstrap.css') ?>">

<script type="text/javascript">

var url_apps = '<?=base_url();?>';


$(document).ready(function () {
//----->
//Ambil semua data customer untuk select 2
  $("#lokasi").select2({
    ajax: {
      url: url_apps+'master_jadwal/ajax_select',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params, // search term
        };
      },
      results: function (data, params) {
        console.log(data);
        return {
            results: $.map(data, function (item) {
                return {
                    text: item.LOKASI,
                    id: item.LOKASI
                }
            })
        };
      },
      cache: true
    },
    minimumInputLength: 1,
  });  


});


$('#lokasi').on('change', function() {
  var idSiswa = $(this).val();
  $.ajax({
    url: url_apps + 'adm_mesin/get/' + $(this).val(),
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    //alert(data.ALAMAT);
    $('#kolok').val(data.KOLOK);
    
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
});

</script>


<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });

      //datepicker
      $('.datepicker').datepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
          todayHighlight: true,
          orientation: "top auto",
          todayBtn: true,
          todayHighlight: true,  
      });
      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });

   function add()
   {
      
      // alert("asd");
       save_method = 'add';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
       $('#btnSave').text('Simpan'); //change button text
       $('#btnSave').attr('disabled',false); //set button enable 
       $('#modal-xl').modal('show'); // show bootstrap modal
       $('.modal-title').text('Tambah Jadwal'); // Set Title to Bootstrap modal title
   }

   function edit(id)
   {
       save_method = 'update';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
       $('#btnSave').text('Simpan'); //change button text
       $('#btnSave').attr('disabled',false); //set button enable 
    
       //Ajax Load data from ajax
       $.ajax({
           url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_edit/')?>/" + id,
           type: "GET",
           dataType: "JSON",
           success: function(data)
           {
               $('[name="id"]').val(data.id_jamker);
               $('[name="kolok"]').val(data.kolok);
               $('[name="nama_jamker"]').val(data.nama_jamker);
               $('[name="senin_m"]').val(data.senin_m);
               $('[name="senin_batas"]').val(data.senin_batas);
               $('[name="senin_p"]').val(data.senin_p);

               $('[name="selasa_m"]').val(data.selasa_m);
               $('[name="selasa_batas"]').val(data.selasa_batas);
               $('[name="selasa_p"]').val(data.selasa_p);
               
               $('[name="rabu_m"]').val(data.rabu_m);
               $('[name="rabu_batas"]').val(data.rabu_batas);
               $('[name="rabu_p"]').val(data.rabu_p);
               
               $('[name="kamis_m"]').val(data.kamis_m);
               $('[name="kamis_batas"]').val(data.kamis_batas);
               $('[name="kamis_p"]').val(data.kamis_p);

               $('[name="jumat_m"]').val(data.jumat_m);
               $('[name="jumat_batas"]').val(data.jumat_batas);
               $('[name="jumat_p"]').val(data.jumat_p);
               
               $('[name="sabtu_m"]').val(data.sabtu_m);
               $('[name="sabtu_batas"]').val(data.sabtu_batas);
               $('[name="sabtu_p"]').val(data.sabtu_p);

               
               $('[name="minggu_m"]').val(data.minggu_m);
               $('[name="minggu_batas"]').val(data.minggu_batas);
               $('[name="minggu_p"]').val(data.minggu_p);
               
               $('#modal-xl').modal('show'); // show bootstrap modal when complete loaded
               $('.modal-title').text('Edit Jadwal'); // Set title to Bootstrap modal title
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error get data from ajax');
           }
       });
   }

   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
   }

   function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
       }
    
       // ajax adding data to database
       $.ajax({
           url : url,
           type: "POST",
           data: $('#form').serialize(),
           dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   $('#modal-xl').modal('hide');
                   reload_table();
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }

   function hapus(id){
    $.confirm({
      title: 'Confirm!',
      content: 'Apakah anda yakin menghapus data ini ?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_delete/" + id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  reload_table();
                  Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Dihapus'
                   });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }

</script>





