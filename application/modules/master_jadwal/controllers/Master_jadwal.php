<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_jadwal extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'master_jadwal');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Master_jadwal_model','master_jadwal');
		check_login();
	}

	public function index()
	{

		$user_data['data_ref'] = $this->data_ref;
		$user_data['sub_menu_active'] = 'Menu';
     	
     	$this->load->view('template/header');
		$this->load->view('view',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->master_jadwal->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_jamker."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_jamker."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

			$no++;
			$row = array();
         	$row[] = $no;
			
         	$row[] = $post->nama_jamker;
         	$row[] = $post->keterangan;
         	$row[] = $post->senin_m;
         	$row[] = $post->senin_p;

			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->master_jadwal->count_all(),
						"recordsFiltered" => $this->master_jadwal->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}



	public function ajax_edit($id)
	{
		$data = $this->master_jadwal->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_edit_solution($sn)
	{
		$data = $this->db->query("SELECT *, tsnallow.id as idallow, tipmesin.id as idtip FROM tsnallow, tipmesin WHERE tsnallow.sn=tipmesin.sn AND tsnallow.sn='$sn'")->row_array();
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'nama_jamker' 			=> $this->input->post('nama_jamker'),
			'senin_m' 			=> $this->input->post('senin_m'),
        	'senin_batas' 			=> $this->input->post('senin_batas'),
        	'senin_p' 			=> $this->input->post('senin_p'),
			'selasa_m' 			=> $this->input->post('selasa_m'),
        	'selasa_batas' 			=> $this->input->post('selasa_batas'),
        	'selasa_p' 			=> $this->input->post('selasa_p'),
			'rabu_m' 			=> $this->input->post('rabu_m'),
        	'rabu_batas' 			=> $this->input->post('rabu_batas'),
        	'rabu_p' 			=> $this->input->post('rabu_p'),
			'kamis_m' 			=> $this->input->post('kamis_m'),
        	'kamis_batas' 			=> $this->input->post('kamis_batas'),
        	'kamis_p' 			=> $this->input->post('kamis_p'),
			'jumat_m' 			=> $this->input->post('jumat_m'),
        	'jumat_batas' 			=> $this->input->post('jumat_batas'),
        	'jumat_p' 			=> $this->input->post('jumat_p'),
			'sabtu_m' 			=> $this->input->post('sabtu_m'),
        	'sabtu_batas' 			=> $this->input->post('sabtu_batas'),
        	'sabtu_p' 			=> $this->input->post('sabtu_p'),
			'minggu_m' 			=> $this->input->post('minggu_m'),
        	'minggu_batas' 			=> $this->input->post('minggu_batas'),
        	'minggu_p' 			=> $this->input->post('minggu_p')
		);

		$insert = $this->master_jadwal->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'nama_jamker' 			=> $this->input->post('nama_jamker'),
			'senin_m' 			=> $this->input->post('senin_m'),
        	'senin_batas' 			=> $this->input->post('senin_batas'),
        	'senin_p' 			=> $this->input->post('senin_p'),
			'selasa_m' 			=> $this->input->post('selasa_m'),
        	'selasa_batas' 			=> $this->input->post('selasa_batas'),
        	'selasa_p' 			=> $this->input->post('selasa_p'),
			'rabu_m' 			=> $this->input->post('rabu_m'),
        	'rabu_batas' 			=> $this->input->post('rabu_batas'),
        	'rabu_p' 			=> $this->input->post('rabu_p'),
			'kamis_m' 			=> $this->input->post('kamis_m'),
        	'kamis_batas' 			=> $this->input->post('kamis_batas'),
        	'kamis_p' 			=> $this->input->post('kamis_p'),
			'jumat_m' 			=> $this->input->post('jumat_m'),
        	'jumat_batas' 			=> $this->input->post('jumat_batas'),
        	'jumat_p' 			=> $this->input->post('jumat_p'),
			'sabtu_m' 			=> $this->input->post('sabtu_m'),
        	'sabtu_batas' 			=> $this->input->post('sabtu_batas'),
        	'sabtu_p' 			=> $this->input->post('sabtu_p'),
			'minggu_m' 			=> $this->input->post('minggu_m'),
        	'minggu_batas' 			=> $this->input->post('minggu_batas'),
        	'minggu_p' 			=> $this->input->post('minggu_p')
		);
		
		
		$this->master_jadwal->update(array('id_jamker' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}



	public function ajax_delete($id)
	{
		$this->master_jadwal->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}




}
