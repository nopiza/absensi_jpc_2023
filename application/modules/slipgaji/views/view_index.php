  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Laporan IKM</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">

        <!-- /.card-header -->
        <div class="card-body">

          <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="3%">No</th>
                    <th width="85%">Jenis Pelayanan</th>
                    <th width="12%">Action</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              $pelayanan = $this->db->query("SELECT * FROM pelayanan")->result();
              foreach ($pelayanan as $ply) { ?>
              <tr>
                <td><?=$no++;?></td>
                <td><?=$ply->nama_pelayanan;?> </td>
                <td><a href="<?=base_url('pelaporan/detail/'.$ply->id_pelayanan);?>" class="btn btn-info btn-sm">Lihat Detail</a></td>
              </tr>

            <?php } ?>

              

            </tbody>
          </table>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>





<?php  $this->load->view('template/footer'); ?>




