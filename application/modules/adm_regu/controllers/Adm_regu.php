<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_regu extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'adm_regu');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Adm_regu_model','regu');
		// $this->load->model('Group/Group_model','group');
		// check_login_opd();
	}

	public function index()
	{

		$user_data['data_ref'] = $this->data_ref;
	
     	$this->load->view('template/header');
		$this->load->view('view',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->regu->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {


				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_regu."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_regu."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
				$link_anggota = ' <a class="btn btn-xs btn-info" href="'.base_url('adm_regu_anggota/index/'.$post->id_regu).'" ><i class="glyphicon glyphicon-trash"></i> Data Anggota Regu</a>';
				
				$ang = $this->db->query("SELECT * FROM regu_anggota WHERE id_regu='$post->id_regu'")->num_rows();
				

			$no++;
			$row = array();
         	$row[] = $no;
			$row[] = $post->nama_regu;;
         	$row[] = $ang.' | '.$link_anggota;
			$row[] = $link_edit.$link_hapus;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->regu->count_all(),
						"recordsFiltered" => $this->regu->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->regu->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
			$data = array(
            	'nama_regu' 			=> $this->input->post('nama_regu'),
				'stt_regu' 			=> '1'
			);
		
		$this->db->insert('regu', $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'nama_regu' 			=> $this->input->post('nama_regu'),
			'stt_regu' 			=> '1'
		);
		$this->regu->update(array('id_regu' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{
		$this->db->query("DELETE FROM regu WHERE id_regu='$id'");
		$this->db->query("DELETE FROM regu_anggota WHERE id_regu='$id'");
		echo json_encode(array("status" => TRUE));
	}



}
