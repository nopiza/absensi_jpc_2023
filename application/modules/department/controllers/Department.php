<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'department');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Department_model','department');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->department->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_department."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_department."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_department;
         	$row[] = $post->keterangan;

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->department->count_all(),
						"recordsFiltered" => $this->department->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->department->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'nama_department' 		=> $this->input->post('department'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->department->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'nama_department' 		=> $this->input->post('department'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->department->update(array('id_department' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('department',array('id_department'=>$id));
		echo json_encode(array("status" => TRUE));
	}



}
