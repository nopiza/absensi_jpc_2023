  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Update Data Potongan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Kavling</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-body">
        <form action="<?=base_url('potongan/proses_upload');?>" id="form" class="form-horizontal"  method="POST" enctype="multipart/form-data">    
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <div class="form-group row">
                            <label class="control-label col-md-2">Pilih data Excel</label>
                            <div class="col-md-9">
                                <input name="datakavling" type="file">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-2">
                            <button type="submit" id="btnSave" class="btn btn-info">Update Data Potongan</button>
                            </div>
                        </div>

                    </div>
                </form>
           

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

</body>
</html>

<?php  $this->load->view('template/footer'); ?>