<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_lembur extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'data_lembur');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_lembur_model','data_lembur');
		$this->load->model('Data_lembur_detail_model','data_lembur_detail');
		check_login_karyawan();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;    	
      	$this->load->view('template/header_marketing',$user_data);
		$this->load->view('view',$user_data);
	}

	public function detail($id)
	{
      	$user_data['data_ref'] = $this->data_ref;    	
      	$user_data['idPengajuan'] = $id;    	
      	$this->load->view('template/header_marketing',$user_data);
		$this->load->view('detail',$user_data);
	}

	public function ajax_list()
	{
		$list = $this->data_lembur->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_pengajuan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_pengajuan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->tanggal;
         	$row[] = $post->jam_mulai;
         	$row[] = $post->keterangan;
			$jum = $this->db->query("SELECT COUNT(id_pengajuan_detail) as jumlah FROM pengajuan_lembur_detail WHERE id_pengajuan='$post->id_pengajuan'")->row_array();
         	$row[] = $jum['jumlah'].' - '.'<a class="btn btn-xs btn-primary" href="'.base_url('data_lembur/detail/'.$post->id_pengajuan).'"> Data Karyawan</a>';

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->data_lembur->count_all(),
						"recordsFiltered" => $this->data_lembur->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_list_detail($id)
	{
		$list = $this->data_lembur_detail->get_datatables($id);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_pengajuan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_pengajuan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->jabatan;
         	$row[] = $post->jam_masuk;
         	$row[] = $post->jam_pulang;
			//add html for action
			$row[] = $link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->data_lembur_detail->count_all($id),
						"recordsFiltered" => $this->data_lembur_detail->count_filtered($id),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->data_lembur->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'id_kordinator' 	=> $this->encryption->decrypt($this->session->userdata('id_karyawan')),
			'tanggal' 			=> $this->input->post('tgl_lembur'),
			'jam_mulai' 		=> $this->input->post('jam_mulai'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->db->insert('pengajuan_lembur', $data);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_add_lembur()
	{
		$id = $this->input->post('idPengajuan');
		$dt = $this->db->query("SELECT * FROM pengajuan_lembur WHERE id_pengajuan='$id'")->row_array();
		$data = array(
			'tanggal'		=> $dt['tanggal'],
			'id_pengajuan' 	=> $id,
			'id_karyawan' 	=> $this->input->post('nama_pegawai')
		);

		$this->db->insert('pengajuan_lembur_detail', $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'nama_data_lembur' 		=> $this->input->post('data_lembur'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->data_lembur->update(array('id_data_lembur' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('pengajuan_lembur',array('id_pengajuan'=>$id));
		$this->db->delete('pengajuan_lembur_detail',array('id_pengajuan'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function delete_karyawan($id, $idPengajuan)
	{
		$this->db->delete('pengajuan_lembur_detail',array('id_pengajuan_detail'=>$id));
		redirect('data_lembur/detail/'	.$idPengajuan);
	}

	
}
