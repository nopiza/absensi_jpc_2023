  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Upload Excel - Data Cuti</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Upload</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

        <form action="<?=base_url('cuti/proses_upload');?>" id="form" class="form-horizontal" method="POST" enctype="multipart/form-data">         
          <div class="form-group row">
            <label class="col-sm-2 control-label">Upload File Excel </label>
            <div class="col-md-2">
              <input name="berkas_excel" type="file" id="berkas_excel">
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 control-label"></label>
            <div class="col-md-5">
            <button type="submit" id="btnSave"  class="btn btn-primary">Upload Data</button>
              <span class="help-block"></span>
            </div>
          </div>
                            
          </form>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

<?php  $this->load->view('template/footer'); ?>