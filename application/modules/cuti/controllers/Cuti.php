<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuti extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'cuti');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cuti_model','cuti');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;    	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function upload()
	{
		$this->load->view('template/header');
		$this->load->view('upload_form');
	}

	public function ajax_list()
	{

		$list = $this->cuti->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_detail = ' <a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Detail" onclick="detail('."'".$post->id_karyawan."'".')">Detail Cuti</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
			 $row[] = $post->nama_lengkap.'<br><small class="badge badge-success">'.$post->no_reg.'</small>';
         	$row[] = $post->nama_lokasi;
         	$row[] = $post->nama_department;
         	$row[] = $post->nama_jabatan;
			$cuti = $post->cuti_tahunan;
			$query = "SELECT SUM(durasi) as Jum FROM pengajuan_izin WHERE id_karyawan = '$post->id_karyawan' AND jenis_izin = 'Cuti'";
			$cutiTerpakai = $this->db->query($query)->row_array();
         	$row[] = $cuti - $cutiTerpakai['Jum'];

			//add html for action
			$row[] = $link_detail;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->cuti->count_all(),
						"recordsFiltered" => $this->cuti->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	private function _do_upload(){

	      $config['upload_path']          = './assets/lampiran_cuti/';
	      $config['allowed_types']        = 'gif|jpg|png|pdf';
	      $config['max_size']             = 1000; //set max size allowed in Kilobyte
	      $config['max_width']            = 3000; // set max width image allowed
	      $config['max_height']           = 3000; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('lampiran')) //upload and validate
	        {
	            $data['inputerror'][] = 'lampiran';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	}

	public function ajax_edit($id)
	{
		$data = $this->cuti->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		
		$data = array(
			'id_karyawan' 		=> $this->input->post('karyawan'),
			'tanggal_awal' 		=> $this->input->post('tanggal_awal'),
			'tanggal_akhir' 	=> $this->input->post('tanggal_akhir'),
			'jumlah_hari' 		=> $this->input->post('jumlah_hari'),
			'alamat_cuti' 		=> $this->input->post('alamat_cuti'),
			'keterangan' 		=> $this->input->post('catatan')
		);

		if(!empty($_FILES['lampiran']['name']))
		{
			$upload = $this->_do_upload();
			$data['lampiran'] = $upload;
		}

		$this->db->insert('cuti',$data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'nik' 				=> $this->input->post('nik'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			'tempat_lahir' 		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'alamat' 			=> $this->input->post('alamat'),
			'no_hp' 			=> $this->input->post('no_hp'),
			'agama' 			=> $this->input->post('agama'),
			'status_perkawinan' 	=> $this->input->post('status_perkawinan'),
			'bpjs_kesehatan' 		=> $this->input->post('bpjs_kesehatan'),
			'bpjs_ketenagakerjaan' 	=> $this->input->post('bpjs_ketenagakerjaan'),
			'insentif' 				=> $this->input->post('insentif'),
			'tunjangan_jabatan' 	=> $this->input->post('tunjangan_jabatan'),
			'tunjangan_site' 		=> $this->input->post('tunjangan_site'),
			'tanggal_masuk' 		=> $this->input->post('tanggal_masuk'),
			'tanggal_phk' 			=> $this->input->post('tanggal_phk'),
			'stt_cuti' 			=> $this->input->post('stt_cuti')
		);


		// if($this->input->post('ktp')) // if remove photo checked
  //     {
  //        if(file_exists('./assets/lampiran_cuti/'.$this->input->post('ktp')) && $this->input->post('ktp'))
  //           unlink('./assets/lampiran_cuti/'.$this->input->post('ktp'));
  //        $data['ktp'] = '';
  //     }
 
      if(!empty($_FILES['ktp']['name']))
      {
         $upload = $this->_do_upload();
             
         //delete file
         $cuti = $this->cuti->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_cuti/'.$cuti->ktp) && $cuti->ktp)
             unlink('./assets/lampiran_cuti/'.$cuti->ktp);
 
         $data['ktp'] = $upload;
      }


      if(!empty($_FILES['kk']['name']))
      {
         $upload = $this->_do_upload_kk();
             
         //delete file
         $cuti = $this->cuti->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_cuti/'.$cuti->kk) && $cuti->kk)
             unlink('./assets/lampiran_cuti/'.$cuti->kk);
 
         $data['kk'] = $upload;
      }

		$this->cuti->update(array('id_cuti' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('cuti',array('id_cuti'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function detailcuti($id)
	{
		$query = "SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id'" ;
		$kyr = $this->db->query($query)->row_array();
		$a = '<div class="form-group row">
		<label class="control-label col-md-3">Nama Lengkap</label>
		<div class="col-md-5">
		<input name="karyawan" value="'.$kyr['nama_lengkap'].'" class="form-control" type="text"  readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Lokasi</label>
		<div class="col-md-5">
			<input name="lokasi" id="lokasi"  value="'.$kyr['nama_lokasi'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Department</label>
		<div class="col-md-5">
			<input name="department" id="department"  value="'.$kyr['nama_department'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Jabatan</label>
		<div class="col-md-5">
			<input name="jabatan" id="jabatan"  value="'.$kyr['nama_jabatan'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>
	
	<table class="table table-bordered" >
              
		<thead>
				<tr>
					<th width="5%">No</th>
					<th width="15%">Tanggal</th>
					<th width="5%">Jumlah</th>
					<th width="30%">Alamat Cuti</th>
					<th width="20%">Keterangan</th>
			</tr>
			</thead>
			<tbody>';
		$b = '';
		$no=1;
		$query = "SELECT * FROM pengajuan_izin WHERE id_karyawan='$id' AND jenis_izin = 'Cuti' AND YEAR(tanggal) = '2023'";
		$komisi = $this->db->query($query)->result();
		foreach($komisi as $kms){ 
		
		$b .= '<tr>
				
			<td>'.$no++.'</td>
			<td>'.tgl_indo($kms->tanggal).'</td>
			<td>'.$kms->durasi.'</td>
			<td>'.$kms->deskripsi.'</td>
			<td>'.$kms->catatan_kordinator.'</td>
		</tr>';
		}

		echo $a.$b.'</tbody>
		</table>';

	}


	public function ajax_select_karyawan(){
        $this->db->select('id_karyawan,nama_lengkap');
        $this->db->like('nama_lengkap',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('karyawan')->result_array();
        //output to json format
        echo json_encode($items);
    }

	public function get($id_karyawan){
        $item=$this->db->query("SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id_karyawan' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


	public function proses_upload()
	{
		$file_mimes = array('application/octet-stream', 
		'application/vnd.ms-excel', 
		'application/x-csv', 
		'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
 
			if(isset($_FILES['berkas_excel']['name']) && in_array($_FILES['berkas_excel']['type'], $file_mimes)) {
				$arr_file = explode('.', $_FILES['berkas_excel']['name']);
				$extension = end($arr_file);
				if('csv' == $extension) {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
				} else {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				}
				$spreadsheet = $reader->load($_FILES['berkas_excel']['tmp_name']);
				
				$sheetData = $spreadsheet->getActiveSheet()->toArray();
				for($i = 2;$i < count($sheetData);$i++){
					$no_ktp           		= $sheetData[$i]['1'];
					$nik           			= $sheetData[$i]['2'];
					$nama_lengkap        	= addslashes($sheetData[$i]['3']);
					$tgl_awal      			= $sheetData[$i]['4'];
					$tgl_akhir      		= $sheetData[$i]['5'];
					$jumHari        		= $sheetData[$i]['6'];
					$alamat_cuti        	= $sheetData[$i]['7'];
					$keterangan        		= $sheetData[$i]['8'];

					// Cari ID karyawan 
					$kary = $this->db->query("SELECT id_karyawan FROM karyawan WHERE no_ktp='$no_ktp'")->row_array();
					$id_karyawan = $kary['id_karyawan'];
					$param = [
						'id_karyawan'		=> $id_karyawan,
						'tanggal_awal'		=> $tgl_awal,
						'tanggal_akhir'		=> $tgl_akhir ,
						'jumlah_hari'		=> $jumHari,
						'alamat_cuti'		=> $alamat_cuti,
						'keterangan'		=> $keterangan
					];
					$this->db->insert('cuti', $param);

				}
				// header("Location: index.php"); 
			}else{
				echo 'Tidak Proses';
			}
		redirect('cuti');
	}





}
