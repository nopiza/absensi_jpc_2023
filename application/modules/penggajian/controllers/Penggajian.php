<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggajian extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'penggajian');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Penggajian_model','penggajian');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
		$user_data['data_ref'] = $this->data_ref;   	
		$this->load->view('template/header',$user_data);
		$this->load->view('form-periode',$user_data);
	}

	public function rekap()
	{
		$user_data['data_ref'] = $this->data_ref;   	
		$this->load->view('template/header',$user_data);
		$this->load->view('form-rekap',$user_data);
	}

	public function tampil_rekap()
	{
		$user_data['data_ref'] = $this->data_ref;   	
		$user_data['bulan'] = $this->input->post('bulan');   	
		$user_data['tahun'] = $this->input->post('tahun');   	

		$this->load->view('template/header',$user_data);
		$this->load->view('rekap',$user_data);
	}

	public function pencarian()
	{
		$user_data['data_ref'] = $this->data_ref;   	
		$this->load->view('template/header',$user_data);
		$this->load->view('form',$user_data);
	}

	public function hitung()
	{
		$user_data['data_ref'] = $this->data_ref;   	
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$bln = $bulan * 1;
		$lokasi = $this->input->post('lokasi');

		// Kosongkan dulu data yang sudah ada
		// cari kelompok yang akan dohapus
		$paramHapus = [
			'bulan' 			=> $this->input->post('bulan'),
			'tahun' 			=> $this->input->post('tahun'),
			'jenis_karyawan' 	=> $this->input->post('jenis_karyawan'),
			'lokasi' 			=> $this->input->post('lokasi')
		];
		$cari = $this->db->get_where('payroll', $paramHapus)->row_array();

		// var_dump($cari);
		if(@$cari['id_payroll']){
			$this->db->delete('payroll', ['id_payroll' => $cari['id_payroll']]);
			$this->db->delete('payroll_'.$bln.'_'.$tahun, ['id_pay' => $cari['id_payroll']]);
		}

		// masukkan ke tabel hitung - sebagai penanda pernah dihitung
		if($this->input->post('thr') != null ){
			$param = [
				'tgl_awal' 			=> $this->input->post('tanggal_awal'),
				'tgl_akhir' 		=> $this->input->post('tanggal_akhir'),
				'bulan' 			=> $bulan,
				'tahun' 			=> $tahun,
				'jenis_karyawan' 	=> $this->input->post('jenis_karyawan'),
				'include_thr' 		=> $this->input->post('thr'),
				'lokasi' 			=> $lokasi,
				'tanggal_hitung'	=> date('Y-m-d')
			];
		}else{
			$param = [
				'tgl_awal' 			=> $this->input->post('tanggal_awal'),
				'tgl_akhir' 		=> $this->input->post('tanggal_akhir'),
				'bulan' 			=> $bulan,
				'tahun' 			=> $tahun,
				'jenis_karyawan' 	=> $this->input->post('jenis_karyawan'),
				'lokasi' 			=> $lokasi,
				'tanggal_hitung'	=> date('Y-m-d')
			];
		}
			
			$this->db->insert('payroll', $param);
			$idPay = $this->db->insert_id();

		// input ke penggajian
		if($this->input->post('jenis_karyawan') == '1'){
			$jenisKaryawan = 'PWT | PWTT';
		}else{
			$jenisKaryawan = 'PKHL';
		}
		$query = "SELECT * FROM karyawan k
		LEFT JOIN lokasi l ON k.id_lokasi = l.id_lokasi 
		LEFT JOIN jabatan jab ON k.id_jabatan = jab.id_jabatan 
		LEFT JOIN department d ON k.id_department = d.id_department 
		WHERE k.id_lokasi='$lokasi' AND sistem_penggajian='$jenisKaryawan'";
		$pay = $this->db->query($query)->result();

		$jterima = 0;
		foreach($pay as $p){
			// Tanggal Periode penghitungan Gaji
			$tanggalAwal = $this->input->post('tanggal_awal');
			$tanggalAkhir = $this->input->post('tanggal_akhir');

			// Hitung masa kerja karyawan
			if($p->tanggal_mulai_kerja == '0000-00-00'){
				$masaHariKerja = 0;
			}else{
				$diff  = date_diff( date_create($p->tanggal_mulai_kerja), date_create() );
				$masaHariKerja = $diff->format('%a'); 
			}
			

			// cek apakah ada THR
			if($this->input->post('thr') != null ){
				// bulan gajian ada penambahan THR
				//cek apakah tanggal mulai kerja sudah diisi
				if($p->tanggal_mulai_kerja == '0000-00-00'){
					// belum ada tanggal mulai kerja
					$jum_THR = 0;
				}else{
					if($masaHariKerja < 365){
						// jika masa kerja belum 1 tahun
						$thrSementara = ($p->gaji_pokok + $p->tunjangan) / 365;
						$jum_THR = $thrSementara * $masaHariKerja ;
					}else{
						// jika masa kerja lebih dari 1 tahun THR Penuh
						$jum_THR = $p->gaji_pokok + $p->tunjangan;
					}
				}
				
				
			}else{
				// bulan gajian tidak ada penambahan THR
				$jum_THR = 0;
			}

			

			// Hitung Jumlah Kehadiran Absen (Berdasarkan Periode Tanggal)
			$jHadir = $this->db->query("SELECT COUNT(*) as jum FROM absensi 
			WHERE id_account = '$p->id_karyawan' AND type_absensi = 'MASUK' 
			AND tanggal_absensi >='$tanggalAwal' AND tanggal_absensi <='$tanggalAkhir' ")->row_array();

			// Hitung Jumlah Kehadiran Absen (Berdasarkan Bulan Gajian)
			// $jkehadiranAbsen = $this->db->query("SELECT COUNT(*) as jum FROM absensi 
			// WHERE id_account = '$p->id_karyawan' AND type_absensi = 'MASUK' AND MONTH(tanggal_absensi) ='$bulan'")->row_array();
			// hitung jumlah kehadiran
			// $jHadir = $this->db->query("SELECT COUNT(*) as jum FROM absensi 
            //         WHERE id_account='$p->id_karyawan' AND MONTH(tanggal_absensi) = '$bulan' AND type_absensi='MASUK'")->row_array();

			// hitung jumlah Perjalanan Dinas
			$jDinass = $this->db->query("SELECT COUNT(*) as jum FROM perjalanan_dinas 
					WHERE id_karyawan='$p->id_karyawan' AND MONTH(tanggal) = '$bulan'")->row_array();

			// Hitung Uang makan dan uang saku perjalanan dinas
			$jumUangMakan = ($jHadir['jum'] - $jDinass['jum']) * $p->t_meal;
			$jumUangSaku = $jDinass['jum'] *  $p->t_uang_saku;

			// Ambil data Hours Meter
			
			$qry = "SELECT * FROM hours_meter WHERE id_karyawan='$p->id_karyawan' AND bulan='$bulan'";
			$cekk = $this->db->query($qry)->row_array();
			if($cekk){
				$hm = $this->db->query($qry)->row_array();
				$hoursMeter = $hm['jumlah_nominal'];
			}else{
				$hoursMeter = 0;
			}
			

			//hitung total penerimaan
			$jPenerimaan = $p->gaji_pokok + $p->tunjangan + $p->t_kerajinan + $p->t_transport + $p->t_hp + 
                    $p->t_perumahan + $p->t_kehadiran + $jumUangMakan + $jumUangSaku + $jum_THR + $hoursMeter;

			// ambil data potongan
			$query2 = "SELECT * FROM potongan WHERE id_karyawan = '$p->id_karyawan' AND MONTH(tanggal) = '$bulan'";
			$potongan = $this->db->query($query2)->row_array();

			// Potongan BPJS 4%
			$bpjs = $p->gaji_pokok * (4/100);

			// Hitung Jumlah Potongan
			$jPotongan = @$potongan['zakat'] + 
			@$potongan['koperasi'] + 
			@$potongan['kasbon'] + 
			@$potongan['kas'] + 
			@$potongan['lain_lain'] + 
			@$potongan['pinjaman'];
			$jterima = $jPenerimaan - $jPotongan;

			$paramGaji = [
				'id_pay' 			=> $idPay,
				'tanggal_hitung' 	=> date('Y-m-d'),
				'id_karyawan' 		=> $p->id_karyawan,
				'no_reg' 			=> $p->no_reg,
				'nama_karyawan' 	=> $p->nama_lengkap,
				'lokasi' 			=> $p->nama_lokasi,
				'department' 		=> '',
				'jabatan' 			=> $p->nama_jabatan,
				'masa_kerja_hari' 	=> $masaHariKerja,
				'gaji_pokok' 		=> $p->gaji_pokok,
				't_operasional' 	=> $p->tunjangan,
				't_kerajinan' 		=> $p->t_kerajinan,
				'jum_thr' 			=> $jum_THR,
				't_meal' 			=> $p->t_meal, 
				't_transport' 		=> $p->t_transport,
				'uang_saku_dl' 		=> $p->t_uang_saku, 
				't_hp' 				=> $p->t_hp,
				't_perumahan' 		=> $p->t_perumahan,
				't_hari_raya' 		=> '', 
				't_kehadiran' 		=> $p->t_kehadiran, 
				'j_hari_hadir' 		=> $jHadir['jum'],
				'j_hari_dl' 		=> $jDinass['jum'],
				'jum_uangSaku' 		=> $jumUangSaku,
				'jum_uangMakan' 	=> $jumUangMakan,
				'jum_gaji_lembur' 	=> '',
				'hours_meter' 		=> $hoursMeter,
				'p_zakat' 			=> @$potongan['zakat'],
				'p_pinjaman' 		=> @$potongan['pinjaman'],
				'p_koperasi' 		=> @$potongan['koperasi'],
				'p_kasbon' 			=> @$potongan['kasbon'],
				'p_kas' 			=> @$potongan['kas'],
				'p_lain_lain' 		=> @$potongan['lain_lain'],
				'p_bpjs' 			=> $bpjs,
				'jum_pendapatan' 	=> $jPenerimaan,
				'jum_potongan' 		=> $jPotongan,
				'jum_terima' 		=> $jterima
			];
			
			$this->db->insert('payroll_'.$bln.'_'.$tahun, $paramGaji);
		}
		
		redirect('penggajian');
	}

	public function proses($jenis, $lokasi)
	{
		if($jenis == '1'){
			$jenisKaryawan = 'PWT | PWTT';
		}else{
			$jenisKaryawan = 'PKHL';
		}
		$no = 1;
		$query = "SELECT * FROM karyawan WHERE id_lokasi='$lokasi' AND sistem_penggajian='$jenisKaryawan'";
		$pay = $this->db->query($query)->result();
		$a = '<table class="table table-bordered">
					<thead>
					<th width="5%">No</th>
					<th width="25%">Nama Karyawan</th>
					<th width="8%">Pokok</th>
					<th width="8%">Tunjangan</th>
					<th width="8%">Kerajinan</th>
					<th width="8%" align="center">Meal</th>
					<th width="8%">Transport</th>
					<th width="8%">Lembur</th>
					<th width="8%">Total</th>
					<th width="8%"></th>
					<th width="8%"></th>
					</thead>
				<tbody>';
		foreach($pay as $p){
			$a .= '<tr>
				<td>'.$no++.'</td>
				<td>'.$p->nama_lengkap.'</td>
				<td align="right">'.rupiah($p->gaji_pokok).'</td>
				<td align="right">'.rupiah($p->tunjangan).'</td>
				<td align="right">'.rupiah($p->t_kerajinan).'</td>
				<td align="right">'.rupiah($p->t_meal).'</td>
				<td align="right">'.rupiah($p->t_transport).'</td>
				<td></td>
				<td></td>
				<td></td>
				<td><a href="'.base_url('penggajian/cetak/'.$p->id_karyawan).'" target="_blank" class="btn btn-success btn-sm">Cetak</a></td>
			</tr>';
		}

		$a .= '</tbody>
		</table>';

		echo $a;
	}



	function cetak($id_karyawan){

        $tahun = date('Y');
        $bulan = date('m');
		
		$kry = $this->db->query("SELECT * FROM karyawan WHERE id_karyawan = '$id_karyawan'")->row_array();

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Slip Gaji');
        $this->mypdf->SetFont('Arial','B',10);

		//Master Desain background Kwitansi Pembayaran
		// $this->mypdf->Image(base_url().'assets/aplikasi/kwitansi.jpg',10,10,190);
		// LOgo Kavling
		// $this->mypdf->Image(base_url().'assets/aplikasi/'.$konfig['logo'],18,10,15);
		$this->mypdf->Cell(190,4,'PT. JAKARTA PRIMA CRANES ',0,1,'L');
		$this->mypdf->Cell(190,4,'Balikpapan ',0,1,'L');
		$this->mypdf->SetFont('Times','B',10);  
		$this->mypdf->SetTextColor(218,0,0);
		$this->mypdf->Cell(190,6,'PAYMENT SLIP',0,1,'C');
		$this->mypdf->SetTextColor(0,0,0);

		$this->mypdf->Line(10,24,190,24);		
		$this->mypdf->SetLineWidth(0.1);
		$this->mypdf->SetFont('Times','',9);  


		$this->mypdf->Cell(21,5,'Nama ',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, $kry['nama_lengkap'],0,0,'L');

		$this->mypdf->Cell(21,5,'Departemen',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, '',0,0,'L');

		$this->mypdf->Cell(15,5,'Bulan',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(25,5, bulan($bulan),0,1,'L');

		// ===============================================================

		$this->mypdf->Cell(21,5,'Jabatan',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, $kry['id_jabatan'],0,0,'L');

		$this->mypdf->Cell(21,5,'Masuk Kerja',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, '',0,0,'L');

		$this->mypdf->Cell(15,5,'Tahun',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(25,5, $tahun,0,1,'L');

		// ===============================================================

		// $this->mypdf->Line(10,47,190,47);	
		$this->mypdf->Ln(3);    
		$this->mypdf->SetFont('Times','B',9);  
		$this->mypdf->setFillColor(204,255,204); 
		$this->mypdf->Cell(60,5,'Gaji Tetap : ',1,0,'L', 1);
		$this->mypdf->setFillColor(204,255,255);
		$this->mypdf->Cell(60,5,'Tunjangan Tidak Tetap',1,0,'L',1);
		$this->mypdf->setFillColor(255,204,229); 
		$this->mypdf->Cell(60,5, 'Pemotongan',1,1,'L',1);

		$this->mypdf->SetFont('Times','',9);  
		$this->mypdf->Ln(1);    

		$this->mypdf->Line(70,42,70,83);	
		$this->mypdf->Line(130,42,130,83);	
		$this->mypdf->Line(10,83,190,83);			

		$this->mypdf->Cell(30,5,'Gaji ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['gaji_pokok']),0,0,'R');
		$this->mypdf->Cell(30,5,'Overtime / Lembur : ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, 'Pinjaman',0,0,'L');
		$this->mypdf->Cell(30,5, '0',0,1,'R');

		$this->mypdf->Cell(30,5,'Tunj. Operasional  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['tunjangan']),0,0,'R');
		$this->mypdf->Cell(30,5,'Meal  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, 'Kasbon  ',0,0,'L');
		$this->mypdf->Cell(30,5, '0',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Kerajinan  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_kerajinan']),0,0,'R');
		$this->mypdf->Cell(30,5, 'BPJS',0,0,'L');
		$this->mypdf->Cell(30,5, '0',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Transport',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_transport']),0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Lembur  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. Jabatan  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. HP ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. Kehadiran  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		// $this->mypdf->Ln(1);    
		$this->mypdf->setFillColor(204,255,204); 
		$this->mypdf->Cell(30,5,'Total : ',0,0,'R');
		$this->mypdf->Cell(30,5,rupiah($kry['gaji_pokok'] + $kry['tunjangan']),1,0,'R', 1);

		$this->mypdf->setFillColor(204,255,255); 
		$this->mypdf->Cell(30,5,'Total : ',0,0,'R');
		$this->mypdf->Cell(30,5, rupiah($kry['t_kerajinan'] + $kry['t_transport']) ,1,0,'R', 1);

		$this->mypdf->setFillColor(255,204,229); 
		$this->mypdf->Cell(30,5, 'Total :',0,0,'R');
		$this->mypdf->Cell(30,5, '',1,1,'R', 1);

		$this->mypdf->Ln(1);    
		$this->mypdf->setFillColor(224,224,224); 
		$this->mypdf->Cell(120,5, '',0,0,'R');
		$this->mypdf->Cell(30,5, 'Gaji Bersih:',0,0,'R');
		$this->mypdf->Cell(30,5, '',1,1,'R', 1);




		
		

		//tabel
		$this->mypdf->Ln(3);    
		$this->mypdf->SetFont('Times','B',7);  
		$this->mypdf->setFillColor(211,236,230); 
		$this->mypdf->Ln(5);    
		$this->mypdf->Cell(5,5,'No.',1,0,'C', 1);
		$this->mypdf->Cell(35,5,'Tanggal',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Masuk',1,0,'C', 1);
		$this->mypdf->Cell(20,5, 'Pulang',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Jam Kerja',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Jam Kantor',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Lama Lembur',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'1,5',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'2',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'3',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'4',1,1,'C', 1);

		$no =1;
		$this->mypdf->setFillColor(255,255,255); 
		$this->mypdf->SetFont('Times','',7);  
		for($i=1 ; $i <= 31; $i++){
			$tgl = $tahun.'-'.$bulan.'-'.$i;
			$this->mypdf->Cell(5,5,$no++,1,0,'C', 1);
			$this->mypdf->Cell(35,5, longdate_indo($tgl),1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5, '',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,1,'C', 1);
		}

		
        
        $this->mypdf->SetFont('Times','B',16);
        // $this->mypdf->text(37,74, rupiah($kavling['jumlah_bayar']));



        $this->mypdf->SetFont('Times','',11);

		$this->mypdf->SetY(70);
		$this->mypdf->Cell(120,4,'',0,0,'C');
        // $this->mypdf->Cell(60,4,'Palangka Raya, '.tgl_indo($kavling['tanggal']),0,0,'C');
		$this->mypdf->ln(21);
		$this->mypdf->Cell(120,4,'',0,0,'C');
        // $this->mypdf->Cell(60,4,$konfig['nama_penandatangan'],0,0,'C');

		//Tanda Tangan
		// $this->mypdf->Image(base_url().'assets/aplikasi/'.$konfig['file_ttd'],135,57,50);



		// $namaFile = str_replace('/','-', '0123/KNG/2022');
		// $noFile = explode('-', $namaFile);
		

		// if(file_exists('./kwitansi/'.$namaFile.'.pdf')){
		// 	echo '';
		// }else{
		// 	$this->mypdf->Output('F', './kwitansi/kwitansi-'.$noFile[0].'.pdf', true);
		// }
        

        $this->mypdf->Output();
    }




	public function ajax_list()
	{

		$list = $this->penggajian->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$linkDetail = ' <a class="btn btn-xs btn-info" target="_blank" href="'.base_url('penggajian/detail/'.$post->id_payroll.'/'.$post->bulan.'/'.$post->tahun).'")">Detail</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = getbln($post->bulan).' '.$post->tahun;
			 	
			
			$namaTabelBulanan = 'payroll_'.$post->bulan.'_'.$post->tahun;
         	// Hitung Jumlah Karyawan
			$jkar = $this->db->query("SELECT COUNT(*) jum FROM $namaTabelBulanan WHERE id_pay = '$post->id_payroll'")->row_array();

			 	if($post->jenis_karyawan == '1'){
					$j = '<span class="badge badge-info">PKWT | PKWTT</span>';
				}else{
					$j = '<span class="badge badge-info">PKHL</span>';
				}
			$row[] = $post->nama_lokasi.' # '.$jkar['jum'].' Karyawan <br>'.$j;

			// Hitung Jumlah Pendapatan Kotor
			$pendapatan = $this->db->query("SELECT SUM(jum_pendapatan) jum FROM $namaTabelBulanan WHERE id_pay = '$post->id_payroll'")->row_array();
         	$row[] = rupiah($pendapatan['jum']);

			// Hitung Jumlah Potongan
			$potongan = $this->db->query("SELECT SUM(jum_potongan) jum FROM $namaTabelBulanan WHERE id_pay = '$post->id_payroll'")->row_array();
         	$row[] = rupiah($potongan['jum']);

			// Hitung Jumlah Terima
			$terima = $this->db->query("SELECT SUM(jum_terima) jum FROM $namaTabelBulanan WHERE id_pay = '$post->id_payroll'")->row_array();
         	$row[] = rupiah($terima['jum']);

			if($post->include_thr == '1'){
				$row[] = '<span class="badge badge-primary">YA</span>';
			}else{
				$row[] = '<span class="badge badge-secondary">TIDAK</span>';
			}

			if($post->stt_publish == '1'){
				$unpublish = '<a href="'.base_url('penggajian/unpublish/'.$post->id_payroll).'" class="btn btn-xs btn-primary" 
onclick="return confirm(\'Anda yakin akan meng Unpublish?\')">  Publish</a>';
				$row[] = $unpublish;
			}else{
				$row[] = '<span class="badge badge-secondary">Belum di Publish</span>';
			}

			//add html for action
			$row[] = $linkDetail;

			$data[] = $row;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->penggajian->count_all(),
					"recordsFiltered" => $this->penggajian->count_filtered(),
					"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function detail($id, $bulan, $tahun)
	{
		$user_data = array();
		$user_data['idPay'] = $id;
		$user_data['bulan'] = $bulan;
		$user_data['tahun'] = $tahun;
		$cekTHR = $this->db->query("SELECT * FROM payroll WHERE id_payroll='$id'")->row_array();
		$user_data['thr'] = $cekTHR['include_thr'];

		$this->load->view('template/header_kosong',$user_data);
		$this->load->view('detail',$user_data);

	}

	public function detail_rekap($id_lokasi, $bulan, $tahun)
	{
		$user_data = array();
		$user_data['bulan'] = $bulan;
		$user_data['tahun'] = $tahun;
		$cekTHR = $this->db->query("SELECT * FROM payroll WHERE lokasi='$id_lokasi' AND bulan='$bulan' AND tahun='$tahun'")->row_array();
		$user_data['thr'] = @$cekTHR['include_thr'];
		$user_data['idPay'] = @$cekTHR['id_payroll'];

		$this->load->view('template/header_kosong',$user_data);
		$this->load->view('detail_rekap',$user_data);

	}

	public function publish($id)
	{
		$this->db->query("UPDATE payroll SET stt_publish='1' WHERE id_payroll='$id'");
		redirect('penggajian');
	}

	public function unpublish($id)
	{
		$this->db->query("UPDATE payroll SET stt_publish='0' WHERE id_payroll='$id'");
		redirect('penggajian');
	}




	function pre_cetak($id){
		date_default_timezone_set('Asia/Jakarta');// Set timezone
		$bulan = date('m');
		$tahun = date('Y');
		$namaTabel = 'payroll_1_2023';
		$kry = $this->db->query("SELECT * FROM $namaTabel p 
		LEFT JOIN payroll r ON p.id_pay = r.id_payroll 
		LEFT JOIN karyawan k ON k.id_karyawan = p.id_karyawan 
		WHERE p.id = '$id'")->row_array();

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Slip Gaji');
        $this->mypdf->SetFont('Arial','B',10);

		//Master Desain background Kwitansi Pembayaran
		// $this->mypdf->Image(base_url().'assets/aplikasi/kwitansi.jpg',10,10,190);
		// LOgo Kavling
		// $this->mypdf->Image(base_url().'assets/aplikasi/'.$konfig['logo'],18,10,15);
		$this->mypdf->Cell(190,4,'PT. JAKARTA PRIMA CRANES ',0,1,'L');
		$this->mypdf->Cell(190,4,'Balikpapan ',0,1,'L');
		$this->mypdf->SetFont('Times','B',10);  
		$this->mypdf->SetTextColor(218,0,0);
		$this->mypdf->Cell(190,6,'PAYMENT SLIP',0,1,'C');
		$this->mypdf->SetTextColor(0,0,0);

		$this->mypdf->Line(10,24,190,24);		
		$this->mypdf->SetLineWidth(0.1);
		$this->mypdf->SetFont('Times','',9);  


		$this->mypdf->Cell(21,5,'Nama ',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, $kry['nama_karyawan'],0,0,'L');

		$this->mypdf->Cell(21,5,'Departemen',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, '',0,0,'L');

		$this->mypdf->Cell(15,5,'Bulan',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(25,5, getbln($kry['bulan']),0,1,'L');

		// ===============================================================

		$this->mypdf->Cell(21,5,'Jabatan',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, $kry['jabatan'],0,0,'L');

		$this->mypdf->Cell(21,5,'Masuk Kerja',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, tgl_indo($kry['tanggal_mulai_kerja']),0,0,'L');

		$this->mypdf->Cell(15,5,'Tahun',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(25,5, $kry['tahun'],0,1,'L');

		// ===============================================================

		// $this->mypdf->Line(10,47,190,47);	
		$this->mypdf->Ln(3);    
		$this->mypdf->SetFont('Times','B',9);  
		$this->mypdf->setFillColor(204,255,204); 
		$this->mypdf->Cell(60,5,'Gaji Tetap : ',1,0,'L', 1);
		$this->mypdf->setFillColor(204,255,255);
		$this->mypdf->Cell(60,5,'Tunjangan Tidak Tetap',1,0,'L',1);
		$this->mypdf->setFillColor(255,204,229); 
		$this->mypdf->Cell(60,5, 'Pemotongan',1,1,'L',1);

		$this->mypdf->SetFont('Times','',9);  
		$this->mypdf->Ln(1);    

		$this->mypdf->Line(70,42,70,88);	
		$this->mypdf->Line(130,42,130,88);	
		$this->mypdf->Line(10,88,190,88);			

		$this->mypdf->Cell(30,5,'Gaji ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['gaji_pokok']),0,0,'R');
		$this->mypdf->Cell(30,5,'Overtime / Lembur : ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['jum_gaji_lembur']),0,0,'R');
		$this->mypdf->Cell(30,5, 'BPJS (-4%)',0,0,'L');
		$this->mypdf->Cell(30,5, rupiah($kry['p_bpjs']),0,1,'R');

		$this->mypdf->Cell(30,5,'Tunj. Operasional  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_operasional']),0,0,'R');
		$this->mypdf->Cell(30,5,'Meal  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['jum_uangMakan']),0,0,'R');
		$this->mypdf->Cell(30,5, 'Zakat  ',0,0,'L');
		$this->mypdf->Cell(30,5, rupiah($kry['p_zakat']),0,1,'R');

		if($kry['include_thr'] == '1'){
			$this->mypdf->Cell(30,5,'THR',0,0,'L');
			$this->mypdf->Cell(30,5,rupiah($kry['jum_thr']),0,0,'R');
		}else{
			$this->mypdf->Cell(30,5,'',0,0,'L');
			$this->mypdf->Cell(30,5,'',0,0,'R');
		}
		
		$this->mypdf->Cell(30,5,'Kerajinan  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_kerajinan']),0,0,'R');
		$this->mypdf->Cell(30,5, 'Koperasi',0,0,'L');
		$this->mypdf->Cell(30,5, rupiah($kry['p_koperasi']),0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Transport',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_transport']),0,0,'R');
		$this->mypdf->Cell(30,5, 'Pinjaman',0,0,'L');
		$this->mypdf->Cell(30,5, rupiah($kry['p_pinjaman']),0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Uang Saku Dinas  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['jum_uangSaku']),0,0,'R');
		$this->mypdf->Cell(30,5, 'Kasbon',0,0,'L');
		$this->mypdf->Cell(30,5, rupiah($kry['p_kasbon']),0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. Perumahan  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_perumahan']),0,0,'R');
		$this->mypdf->Cell(30,5, 'Uang Kas',0,0,'L');
		$this->mypdf->Cell(30,5, rupiah($kry['p_kas']),0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. HP ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_hp']),0,0,'R');
		$this->mypdf->Cell(30,5, 'Lain-lain',0,0,'L');
		$this->mypdf->Cell(30,5, rupiah($kry['p_lain_lain']),0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. Kehadiran  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_kehadiran']),0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Hours Meter  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['hours_meter']),0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		

		// $this->mypdf->Ln(1);    
		$gajiTetap = $kry['gaji_pokok'] + $kry['t_operasional'] + $kry['jum_thr'];
		$gajiTidakTetap = $kry['jum_pendapatan'] - $gajiTetap;
		$this->mypdf->setFillColor(204,255,204); 
		$this->mypdf->Cell(30,5,'Total : ',0,0,'R');
		$this->mypdf->Cell(30,5,rupiah($gajiTetap),1,0,'R', 1);

		$this->mypdf->setFillColor(204,255,255); 
		$this->mypdf->Cell(30,5,'Total : ',0,0,'R');
		$this->mypdf->Cell(30,5, rupiah($gajiTidakTetap) ,1,0,'R', 1);

		$this->mypdf->setFillColor(255,204,229); 
		$this->mypdf->Cell(30,5, 'Total :',0,0,'R');
		$this->mypdf->Cell(30,5, rupiah($kry['jum_potongan']),1,1,'R', 1);

		$this->mypdf->Ln(1);    
		$this->mypdf->setFillColor(224,224,224); 
		$this->mypdf->Cell(120,5, '',0,0,'R');
		$this->mypdf->Cell(30,5, 'Gaji Bersih:',0,0,'R');
		$this->mypdf->Cell(30,5, rupiah($kry['jum_terima']),1,1,'R', 1);


		//tabel
		$this->mypdf->Ln(3);    
		$this->mypdf->SetFont('Times','B',7);  
		$this->mypdf->Cell(25,4, 'Periode Absen',0,0,'L', 0);
		$this->mypdf->Cell(100,4, ': '.tgl_indo($kry['tgl_awal']).' - '.tgl_indo($kry['tgl_akhir']) ,0,1,'L', 0);
		$this->mypdf->Cell(25,4, 'Jumlah Kehadiran',0,0,'L', 0);
		$this->mypdf->Cell(100,4, ': '.$kry['j_hari_hadir'].' Hari Kerja' ,0,1,'L', 0);
		$this->mypdf->setFillColor(211,236,230); 
		$this->mypdf->Cell(5,5,'No.',1,0,'C', 1);
		$this->mypdf->Cell(35,5,'Tanggal',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Masuk',1,0,'C', 1);
		$this->mypdf->Cell(20,5, 'Pulang',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Jam Kerja',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Jam Kantor',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Lama Lembur',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'1,5',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'2',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'3',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'4',1,1,'C', 1);

		$no =1;
		$this->mypdf->setFillColor(255,255,255); 
		$this->mypdf->SetFont('Times','',7);  
		
		// ===========================
		// $bulanTahun_1 = $tahun.'-'.$bulan;
		// $tglAwal = date('Y-m-d', strtotime(date($bulanTahun_1.'-25') . '- 1 month'));
		// $tglAkhir = '2023-01-25';

		$tglAwal	= $kry['tgl_awal'];
		$tglAkhir	= $kry['tgl_akhir'];
		


		while (strtotime($tglAwal) <= strtotime($tglAkhir)) {
		// echo "$tglAwal<br/>";
		
		// }
			// if($kry['bulan'] < 10){
			// 	$bulan = '0'.$kry['bulan'];
			// }else{
			// 	$bulan = $kry['bulan'];
			// }

			// if($i < 10){
			// 	$tang = '0'.$i;
			// }else{
			// 	$tang = $i;
			// }
				$tgl = $tglAwal;
			// $tgl = $kry['tahun'].'-'.$bulan.'-'.$tang;
			// cari absensi 
			$idAccount = $kry['id_karyawan'];
			// Cari absen masuk =============================>>>>>>>
			$abs = $this->db->query("SELECT * FROM absensi 
			WHERE id_account='$idAccount' AND tanggal_absensi LIKE '$tgl%' AND type_absensi ='MASUK'")->row_array();
			// Cari absen pulang =============================>>>>>>>
			$plg = $this->db->query("SELECT * FROM absensi 
			WHERE id_account='$idAccount' AND tanggal_absensi LIKE '$tgl%' AND type_absensi ='PULANG'")->row_array();
			if(@$abs['tanggal_absensi'] == NULL){
				$masuk = '';
			}else{
				$masuk = substr($abs['tanggal_absensi'], 11, 5);
			}

			if(@$plg['tanggal_absensi'] == NULL){
				$pulang = '';
			}else{
				$pulang = substr($plg['tanggal_absensi'], 11, 5);
			}


			$this->mypdf->Cell(5,5,$no++,1,0,'C', 1);
			$this->mypdf->Cell(35,5, longdate_indo($tgl),1,0,'C', 1);
			$this->mypdf->Cell(20,5,$masuk,1,0,'C', 1);
			$this->mypdf->Cell(20,5, $pulang,1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,1,'C', 1);

			$tglAwal = date ("Y-m-d", strtotime("+1 day", strtotime($tglAwal)));//looping tambah 1 date
		}

        
        $this->mypdf->SetFont('Times','B',16);
        // $this->mypdf->text(37,74, rupiah($kavling['jumlah_bayar']));

        $this->mypdf->SetFont('Times','',11);

		$this->mypdf->SetY(70);
		$this->mypdf->Cell(120,4,'',0,0,'C');
        // $this->mypdf->Cell(60,4,'Palangka Raya, '.tgl_indo($kavling['tanggal']),0,0,'C');
		$this->mypdf->ln(21);
		$this->mypdf->Cell(120,4,'',0,0,'C');

        

        $this->mypdf->Output();
    }

	function tanggal(){
		$bulan = '01';
		$tahun = '2023';
		$bulanTahun_1 = $tahun.'-'.$bulan;
		$tglAwal = date('Y-m-d', strtotime(date($bulanTahun_1.'-25') . '- 1 month'));
		$tglAkhir = '2023-01-25';
		date_default_timezone_set('Asia/Jakarta');// Set timezone


		while (strtotime($tglAwal) < strtotime($tglAkhir)) {
		echo "$tglAwal<br/>";
		$tglAwal = date ("Y-m-d", strtotime("+1 day", strtotime($tglAwal)));//looping tambah 1 date
		}
		// echo $tglAwal;
	}


	public function ajax_proses_individu_v2($bulan, $tahun)
	{
        $warnaJadwal = 'style="background-color:#faf8e6"';
		$data = ' <a href="'.base_url('penggajian/cetakrekap/'.$bulan.'/'.$tahun).'" class="btn btn-warning btn-sm" target="_blank"> Cetak Rekap Bulanan</a> 
		<a href="'.base_url('penggajian/cetakdetail/'.$bulan.'/'.$tahun).'" class="btn btn-info btn-sm" target="_blank"> Cetak Detail Bulanan</a>';
        $data .= '
        <br>
        <br>
        <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="25%">Lokasi Karyawan</th>
                    <th width="10%">Jum Karyawan</th>
                    <th width="10%" class="table-warning">Gaji Kotor</th>
                    <th width="10%">THR</th>
                    <th width="10%" class="table-danger">Jum Potongan</th>
                    <th width="10%" class="table-success">Total Bersih</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>';
			$no = 1;
			$bln = $bulan * 1;
			$namaTabel = 'payroll_'.$bln.'_'.$tahun;
			// $query = "SELECT * FROM lokasi l 
			// LEFT JOIN payroll p ON l.id_lokasi = p.lokasi 
			// LEFT JOIN $namaTabel t ON p.id_payroll = t.id_pay 
			// WHERE p.bulan='$bln' AND p.tahun='$tahun'";

			$query = "SELECT *, (SELECT COUNT(*) as jum FROM karyawan WHERE id_lokasi=l.id_lokasi) as jumlah, 
			(SELECT SUM(jum_potongan) as pot FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as potongan,   
			(SELECT SUM(jum_thr) as hariraya FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as thr,   
			(SELECT SUM(jum_pendapatan) as penda FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as pendapatan,     
			(SELECT SUM(jum_terima) as penda FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as terima     
			FROM lokasi l";
			$rekap = $this->db->query($query)->result();
				$totPendapatan 	= 0;
			  	$totTHR			= 0;
			  	$totPotongan 	= 0;
			  	$totTERIMA 		= 0;
				$totKaryawan 	= 0;
			
			foreach($rekap as $r){
				$pendapatan = $r->pendapatan - $r->thr;
				$data .= '<tr>
				<td>'.$no++.'</td>
				<td>'.$r->nama_lokasi.'</td>
				<td align="center">'.$r->jumlah.'</td>
				<td align="right" class="table-warning">'.rupiah($pendapatan).'</td>
				<td align="right">'.rupiah($r->thr).'</td>
				<td align="right" class="table-danger">'.rupiah($r->potongan).'</td>
				<td align="right" class="table-success">'.rupiah($r->terima).'</td>
				<td><a class="btn btn-xs btn-info" target="_blank" href="'.base_url('penggajian/detail_rekap/'.$r->id_lokasi.'/'.$bulan.'/'.$tahun).'")">Detail</a></td>
			  </tr>';

			  $totKaryawan 		= $totKaryawan + $r->jumlah;
			  $totPendapatan 	= $totPendapatan + $pendapatan;
			  $totTHR			= $totTHR + $r->thr;
			  $totPotongan 		= $totPotongan + $r->potongan;
			  $totTERIMA 		= $totTERIMA + $r->terima;
			}

			$data .= '<tr>
            <td colspan="2"><b>TOTAL</b></td>
            <td align="center"><b>'.rupiah($totKaryawan).'</b></td>
            <td align="right"><b>'.rupiah($totPendapatan).'</b></td>
            <td align="right"><b>'.rupiah($totTHR).'</b></td>
            <td align="right"><b>'.rupiah($totPotongan).'</b></td>
            <td align="right"><b>'.rupiah($totTERIMA).'</b></td>
          </tr>';

        
			$data .= '</tbody>
			</table>';
			echo json_encode($data);

	}



	public function cetakrekap($bulan, $tahun)
	{
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
		$this->load->view('cetak_rekap', $data);

	}

	public function cetakdetail($bulan, $tahun)
	{
        $user_data['bulan'] = $bulan;
		$user_data['tahun'] = $tahun;
		$this->load->view('cetak_detail', $user_data);

	}
	
}
