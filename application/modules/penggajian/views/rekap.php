  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Proses Payroll</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="card">


        <!-- /.card-header -->
        <div class="card-body">

        <form action="<?=base_url('penggajian/hitung');?>" id="form" class="form-horizontal" method="POST">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                    <div class="form-group row">
                            <label class="control-label col-md-2">Periode Payroll</label>
                            <div class="col-md-2">
                                <select name="bulan" id="bulan" class="form-control" >
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jumi</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">Septerber</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <label class="control-label col-md-1">Tahun</label>
                            <div class="col-md-2">
                                <select name="tahun" id="tahun" class="form-control" >
                                    <option value="2023">2023</option>
                                </select>
                            </div>
                        </div>

                    <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal Awal</label>
                            <div class="col-md-2">
                                <input type="date" name="tanggal_awal" class="form-control">
                            </div>
                            <label class="control-label col-md-1">Tanggal Akhir</label>
                            <div class="col-md-2">
                            <input type="date" name="tanggal_akhir" class="form-control">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-2">Lokasi</label>
                            <div class="col-md-2">
                                <select class="form-control" name="lokasi" id="lokasi">
                                    <?php 
                                    $dept = $this->db->query("SELECT * FROM lokasi")->result();
                                    foreach($dept as $dp){
                                        echo '<option value="'.$dp->id_lokasi.'">'.$dp->nama_lokasi.'</option>';
                                    }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <label class="control-label col-md-1">Jenis Karyawan</label>
                            <div class="col-md-2">
                                <select name="jenis_karyawan" id="jenis_karyawan" class="form-control" >
                                    <option value="1">PKWT - PKWTT</option>
                                    <option value="2">PKHL</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Include THR</label>
                            <div class="col-md-3">
                       
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="thr" name="thr" value="1">
                            </div> 
                         
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-3">
                            <!-- <button type="button" id="proses" onclick="add()" class="btn btn-primary">Hitung Payroll</button> -->
                            <button type="submit" id="proses" class="btn btn-primary">Hitung Payroll</button>
                            </div>
                        </div>


                    </div>
                </form>

                <div id="detail">

                <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="10%">Periode Payroll</th>
                    <th width="25%">Lokasi Karyawan</th>
                    <th width="10%">Jum Pendapatan</th>
                    <th width="10%">Jum Potongan</th>
                    <th width="10%">Jum Total</th>
                    <th width="10%"> THR</th>
                    <th width="10%"> Publish</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>


                </div>

         

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

</body>
</html>






<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "penggajian/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });


      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });



function add()
   {
        var jenis = document.getElementById("jenis_karyawan").value;
        var lokasi = document.getElementById("lokasi").value;
        url = "<?=base_url();?>";
        trx = '123';
        $('#detail').load(url + "penggajian/hitung/" + jenis + '/' + lokasi);
          
   }

// function add()
//    {
//        alert('asd');
//        url = "<?php echo site_url($data_ref['uri_controllers'].'/proses')?>";
    
//        // ajax adding data to database
//        var formData = new FormData($('#form')[0]);
//        $.ajax({
//             url : url,
//             type: "POST",
//             data: formData,
//             contentType: false,
//             processData: false,
//             dataType: "JSON",
//            success: function(data)
//            {
//             $('#detail').load(url + "transaksi/detailtrx/" + trx);
//            },
//            error: function (jqXHR, textStatus, errorThrown)
//            {
//                alert('Error adding / update data');
//                $('#btnSave').text('Simpan'); //change button text
//                $('#btnSave').attr('disabled',false); //set button enable 
    
//            }
//        });
//    }




</script>


