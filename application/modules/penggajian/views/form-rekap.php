  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Rekap Payroll</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="card">


        <!-- /.card-header -->
        <div class="card-body">

        <form action="<?=base_url('penggajian/tampil_rekap');?>" id="form" class="form-horizontal" method="POST">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                    <div class="form-group row">
                            <label class="control-label col-md-2">Rekap Payroll</label>
                            <div class="col-md-2">
                                <select name="bulan" id="bulan" class="form-control" >
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jumi</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">Septerber</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <label class="control-label col-md-1">Tahun</label>
                            <div class="col-md-2">
                                <select name="tahun" id="tahun" class="form-control" >
                                    <option value="2023">2023</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-3">
                            <!-- <button type="button" id="proses" onclick="add()" class="btn btn-primary">Hitung Payroll</button> -->
                            <!-- <button type="submit" id="proses" class="btn btn-primary btn-sm">Tampilkan Rekap</button> -->
                            <a href="#" class="btn btn-primary btn-md" onclick="proses()" >Tampilkan Rekap</a>
                            </div>
                        </div>


                    </div>
                </form>
                <hr>
                <div id="laporan"></div>
                

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>



</body>
</html>

<?php  $this->load->view('template/footer'); ?>



<script type="text/javascript">
function proses(){

var bulan   = document.getElementById("bulan").value;
var tahun   = document.getElementById("tahun").value;

// alert(nip + ' - ' + bulan)

url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_proses_individu_v2')?>/" + bulan+ '/' + tahun;

  
     // ajax adding data to database
     $.ajax({
         url : url,
         type: "POST",
         data: $('#form').serialize(),
         dataType: "JSON",
         success: function(data)
         {

                 document.getElementById('laporan').innerHTML = data;

         },
         error: function (jqXHR, textStatus, errorThrown)
         {
             alert('Error adding / update data');
 
         }
     });
 }


</script>