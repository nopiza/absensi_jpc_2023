<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=rekap.xls");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

		$data = '';
        $data .= '
        <br>
        <br>
        <table border="1">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="25%">Lokasi Karyawan</th>
                    <th width="10%">Jum Karyawan</th>
                    <th width="10%" class="table-warning">Gaji Kotor</th>
                    <th width="10%">THR</th>
                    <th width="10%" class="table-danger">Jum Potongan</th>
                    <th width="10%" class="table-success">Total Bersih</th>
                </tr>
            </thead>
            <tbody>';
			$no = 1;
			$bln = $bulan * 1;
			$namaTabel = 'payroll_'.$bln.'_'.$tahun;
			// $query = "SELECT * FROM lokasi l 
			// LEFT JOIN payroll p ON l.id_lokasi = p.lokasi 
			// LEFT JOIN $namaTabel t ON p.id_payroll = t.id_pay 
			// WHERE p.bulan='$bln' AND p.tahun='$tahun'";

			$query = "SELECT *, (SELECT COUNT(*) as jum FROM karyawan WHERE id_lokasi=l.id_lokasi) as jumlah, 
			(SELECT SUM(jum_potongan) as pot FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as potongan,   
			(SELECT SUM(jum_thr) as hariraya FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as thr,   
			(SELECT SUM(jum_pendapatan) as penda FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as pendapatan,     
			(SELECT SUM(jum_terima) as penda FROM $namaTabel t LEFT JOIN payroll p ON p.id_payroll=t.id_pay WHERE p.lokasi=l.id_lokasi ) as terima     
			FROM lokasi l";
			$rekap = $this->db->query($query)->result();
				$totPendapatan 	= 0;
			  	$totTHR			= 0;
			  	$totPotongan 	= 0;
			  	$totTERIMA 		= 0;
				$totKaryawan 	= 0;
			
			foreach($rekap as $r){
				$pendapatan = $r->pendapatan - $r->thr;
				$data .= '<tr>
				<td>'.$no++.'</td>
				<td>'.$r->nama_lokasi.'</td>
				<td align="center">'.$r->jumlah.'</td>
				<td align="right" class="table-warning">'.rupiah($pendapatan).'</td>
				<td align="right">'.rupiah($r->thr).'</td>
				<td align="right" class="table-danger">'.rupiah($r->potongan).'</td>
				<td align="right" class="table-success">'.rupiah($r->terima).'</td>
			  </tr>';

			  $totKaryawan 		= $totKaryawan + $r->jumlah;
			  $totPendapatan 	= $totPendapatan + $pendapatan;
			  $totTHR			= $totTHR + $r->thr;
			  $totPotongan 		= $totPotongan + $r->potongan;
			  $totTERIMA 		= $totTERIMA + $r->terima;
			}

			$data .= '<tr>
            <td colspan="2"><b>TOTAL</b></td>
            <td align="center"><b>'.rupiah($totKaryawan).'</b></td>
            <td align="right"><b>'.rupiah($totPendapatan).'</b></td>
            <td align="right"><b>'.rupiah($totTHR).'</b></td>
            <td align="right"><b>'.rupiah($totPotongan).'</b></td>
            <td align="right"><b>'.rupiah($totTERIMA).'</b></td>
          </tr>';

        
			$data .= '</tbody>
			</table>';


            echo $data;