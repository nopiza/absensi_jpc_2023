<style>
.table-condensed{
  font-size: 13px;
}

.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
  background-color: #ffc3bf;
}
</style>

<br>

<a href="<?=base_url('penggajian/publish/'.$idPay);?>" class="btn btn-sm btn-primary" 
onclick="return confirm('Anda yakin akan mempublish? Pastikan data sudah benar semua...!')"> Setujui dan publish</a>

<br>
<br>



<table class="table-striped table-hover table-bordered table-condensed" width="2500px">
            <thead>
                <tr>
                    <th width="15px">No</th>
                    <th width="120px">No. REG / Lokasi</th>
                    <th width="190px">Nama lengkap</th>
                    <th width="140px">Jabatan</th>
                    <th width="100px"  class="table-warning">Masa Kerja</th>
                    <th width="30px" align="center">Jumlah Kehadiran</th>
                    <th width="30px" align="center">Jumlah Dinas</th>
                    <th width="45px">Gaji Pokok</th>
                    <th width="45px">Tunjangan Operasional</th>
                    <?php if($thr == '1'){
                      echo '<th width="45px"  class="table-info">THR</th>';
                    }
                    ?>
                    <th width="45px">Tunjangan Kerajinan</th>
                    <th width="45px">Tunjangan Transport</th>
                    <th width="45px">Tunjangan Handphone</th>
                    <th width="45px">Tunjangan perumahan</th>
                    <th width="45px">Tunjangan kehadiran</th>
                    <th width="45px" class="table-secondary">Tunjangan Perjalanan Dinas</th>
                    <th width="45px" class="table-secondary">T Meal</th>
                    <th width="45px" class="table-warning">Uang Saku</th>
                    <th width="45px" class="table-warning">Lembur</th>
                    <th width="45px" class="table-warning">Uang Makan</th>
                    <th width="45px" class="table-warning">Hours Meter</th>
                    <th width="45px" class="table-info">Jumlah Penerimaan</th>
                    <th width="45px">Zakat</th>
                    <th width="45px">Koperasi</th>
                    <th width="45px">Kasbon</th>
                    <th width="45px">Uang Kas</th>
                    <th width="45px">Pinjaman</th>
                    <th width="45px">BPJS</th>
                    <th width="45px">Lain-lain </th>
                    <th width="45px" class="table-danger">Jumlah Potongan</th>
                    <th width="65px" class="table-success">Jumlah Diterima</th>
                    <th width="45px">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no =1;

                $namaTabel = 'payroll_'.$bulan.'_'.$tahun;
                $pay = $this->db->query("SELECT * FROM $namaTabel WHERE id_pay = '$idPay'")->result();
                foreach($pay as $p){

                  $thn = floor($p->masa_kerja_hari / 365);
                  $hr = $p->masa_kerja_hari % 365;
                  if($p->masa_kerja_hari < 365){
                    if($hr == 0){
                      $masaKerja = 'Belum diatur';
                    }else{
                      $masaKerja = $hr.' Hari';
                    }
                    
                  }else{
                    $masaKerja = $thn.' Tahun '.$hr.' Hari';
                  }
                  
                    $a = '<tr>
                    <td>'.$no++.'</td>
                    <td>'.$p->lokasi.'-'.$p->no_reg.'</td>
                    <td>'.$p->nama_karyawan.'</td>
                    <td>'.$p->jabatan.'</td>
                    <td align="center" class="table-warning">'.$masaKerja.'</td>
                    <td align="center">'.$p->j_hari_hadir.'</td>
                    <td align="center">'.$p->j_hari_dl.'</td>
                    <td align="right">'.rupiah($p->gaji_pokok).'</td>
                    <td align="right">'.rupiah($p->t_operasional).'</td>';
                    if($thr == '1'){
                      $a .= '<td align="right" class="table-info">'.rupiah($p->jum_thr).'</td>';
                    }
                    
                    $a .= '<td align="right">'.rupiah($p->t_kerajinan).'</td>
                    <td align="right">'.rupiah($p->t_transport).'</td>
                    <td align="right">'.rupiah($p->t_hp).'</td>
                    <td align="right">'.rupiah($p->t_perumahan).'</td>
                    <td align="right">'.rupiah($p->t_kehadiran).'</td>
                    <td align="right" class="table-secondary">'.rupiah($p->uang_saku_dl).'</td>
                    <td align="right" class="table-secondary">'.rupiah($p->t_meal).'</td>
                    <td align="right" class="table-warning">'.rupiah($p->jum_uangSaku).'</td>
                    <td align="right" class="table-warning">'.rupiah($p->jum_gaji_lembur).'</td>
                    <td align="right" class="table-warning">'.rupiah($p->jum_uangMakan).'</td>
                    <td align="right" class="table-warning">'.rupiah($p->hours_meter).'</td>
                    <td align="right" class="table-info">'.rupiah($p->jum_pendapatan).'</td>
                    
                    <td align="right">'.rupiah($p->p_zakat).'</td>
                    <td align="right">'.rupiah($p->p_koperasi).'</td>
                    <td align="right">'.rupiah($p->p_kasbon).'</td>
                    <td align="right">'.rupiah($p->p_kas).'</td>
                    <td align="right">'.rupiah($p->p_pinjaman).'</td>
                    <td align="right">'.rupiah($p->p_bpjs).'</td>
                    <td align="right">'.rupiah($p->p_lain_lain).'</td>

                    <td align="right" class="table-danger">'.rupiah($p->jum_potongan).'</td>
                    <td align="right" class="table-success">'.rupiah($p->jum_terima).'</td>
                    <td><a class="btn btn-xs btn-info" target="_blank" href="'.base_url('penggajian/pre_cetak/'.$p->id).'")">Cetak</a></td>
                </tr>';

                echo $a;
                }
                ?>
            </tbody>
          </table>

