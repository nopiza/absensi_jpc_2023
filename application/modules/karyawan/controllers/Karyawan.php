<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'karyawan');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Karyawan_model','karyawan');
		$this->load->model('Karyawan_pkhl_model','karyawan_pkhl');
		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
	}

	public function pkhl()
	{
      	$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header',$user_data);
		$this->load->view('view_pkhl',$user_data);
	}


	public function detail($id)
	{

      	$user_data['data_ref'] = $this->data_ref;
		$query = "SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id'";
      	$user_data['karyawan'] = $this->db->query($query)->row_array();
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('detail',$user_data);

	}

	public function ajax_list()
	{
		$list = $this->karyawan->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = ' <a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit Karyawan</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$link_detail = ' <a class="btn btn-xs btn-warning" href="'.base_url('karyawan/detail/'.$post->id_karyawan).'" title="Detail" >Detail</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
			if($post->foto == ''){
				if($post->jenis_kelamin == 'Laki-laki'){
					$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/pria.jpg').'"><br>'.$post->no_reg;
				}else if($post->jenis_kelamin == 'Perempuan'){
					$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/wanita.jpg').'"><br>'.$post->no_reg;
				}
			}else{
				$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/'.$post->foto).'"><br>'.$post->no_reg;
			}
			 
			 if($post->jenis_kelamin == 'Perempuan'){
				$jk = '<span class="badge badge-pill badge-warning">Perempuan</span>';
			 }else{
				$jk = '<span class="badge badge-pill badge-info">Laki-laki</span>';
			 }
         	$row[] = $post->nama_lengkap.'<br>'.$jk;
			if($post->stt_admin == '1'){
				$adm = '<span class="badge badge-pill badge-warning">Admin</span>';
			}else{
				$adm = '';
			}
			$row[] = $post->nama_jabatan.'<br><span class="badge badge-pill badge-success">'.$post->golongan.'</span> '.$adm;
			$row[] = $post->nama_lokasi;
			$row[] = $post->alamat;
			$row[] = tgl_indo($post->tanggal_masuk);
			if($post->stt_karyawan == 'Aktif'){
				$row[] = '<span class="btn btn-xs btn-info">Aktif</span>';
			}else{
				$row[] = '<span class="btn btn-xs btn-secondary">Non Aktif</span>';
			}


			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->karyawan->count_all(),
						"recordsFiltered" => $this->karyawan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_list_pkhl()
	{
		$list = $this->karyawan_pkhl->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = ' <a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit Karyawan</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$link_detail = ' <a class="btn btn-xs btn-warning" href="'.base_url('karyawan/detail/'.$post->id_karyawan).'" title="Detail" >Detail</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
			 if($post->foto == ''){
				if($post->jenis_kelamin == 'Laki-laki'){
					$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/pria.jpg').'"><br>'.$post->no_reg;
				}else if($post->jenis_kelamin == 'Perempuan'){
					$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/wanita.jpg').'"><br>'.$post->no_reg;
				}
			}else{
				$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/'.$post->foto).'"><br>'.$post->no_reg;
			}
			 
			 if($post->jenis_kelamin == 'Perempuan'){
				$jk = '<span class="badge badge-pill badge-warning">Perempuan</span>';
			 }else{
				$jk = '<span class="badge badge-pill badge-info">Laki-laki</span>';
			 }
         	$row[] = $post->nama_lengkap.'<br>'.$jk;
			$row[] = $post->nama_jabatan.'<br><span class="badge badge-pill badge-success">'.$post->golongan.'</span>';
			$row[] = $post->nama_lokasi;
			$row[] = $post->alamat;
			$row[] = tgl_indo($post->tanggal_masuk);
			if($post->stt_karyawan == 'Aktif'){
				$row[] = '<span class="btn btn-xs btn-info">Aktif</span>';
			}else{
				$row[] = '<span class="btn btn-xs btn-secondary">Non Aktif</span>';
			}


			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->karyawan_pkhl->count_all(),
						"recordsFiltered" => $this->karyawan_pkhl->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_keluarga()
	{

		$list = $this->keluarga->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

				$link_edit = ' <a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_keluarga."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_keluarga."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->jenis_kelamin;
			//  $row[] = $post->no_hp;
			$row[] = $post->hubungan_keluarga;
			// $row[] = $post->no_hp;
			

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->keluarga->count_all(),
						"recordsFiltered" => $this->keluarga->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	private function _do_upload(){

	      $config['upload_path']          = './assets/lampiran_karyawan/';
	      $config['allowed_types']        = 'gif|jpg|png|pdf';
	      $config['max_size']             = 1000; //set max size allowed in Kilobyte
	      $config['max_width']            = 3000; // set max width image allowed
	      $config['max_height']           = 3000; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('ktp')) //upload and validate
	        {
	            $data['inputerror'][] = 'ktp';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	}


	private function _do_upload_kk(){

	      $config['upload_path']          = './assets/lampiran_karyawan/';
	      $config['allowed_types']        = 'gif|jpg|png|pdf';
	      $config['max_size']             = 1000; //set max size allowed in Kilobyte
	      $config['max_width']            = 3000; // set max width image allowed
	      $config['max_height']           = 3000; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('kk')) //upload and validate
	        {
	            $data['inputerror'][] = 'kk';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	}

	public function ajax_edit($id)
	{
		$data = $this->karyawan->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'no_reg' 				=> $this->input->post('noreg'),
			'no_ktp' 				=> $this->input->post('no_ktp'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			// 'tempat_lahir' 		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'alamat' 			=> $this->input->post('alamat'),
			// 'no_hp' 			=> $this->input->post('no_hp'),
			// 'agama' 			=> $this->input->post('agama'),
			// 'status_perkawinan' 	=> $this->input->post('status_perkawinan'),
			'no_bpjs_kes' 		=> $this->input->post('no_bpjs_kes'),
			'no_bpjs_tk' 		=> $this->input->post('no_bpjs_tk'),
			'gaji_pokok' 				=> $this->input->post('gaji_pokok'),
			't_kerajinan' 		=> $this->input->post('t_kerajinan'),
			't_meal' 		=> $this->input->post('t_kerajinan'),
			't_transport' 		=> $this->input->post('t_kerajinan'),
			'tanggal_masuk' 		=> $this->input->post('tanggal_masuk'),
			'stt_admin' 			=> $this->input->post('is_admin'),
			'stt_karyawan' 			=> $this->input->post('stt_karyawan'),
			'cuti_tahunan' 			=> $this->input->post('cuti_tahunan'),
			'username' 			=> $this->input->post('username'),
			'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'sistem_penggajian' 			=> $this->input->post('sistem_penggajian')
			// 'id_department' 			=> $this->input->post('department'),
			// 'id_jabatan' 			=> $this->input->post('range_mcu'),
			// 'id_lokasi' 			=> $this->input->post('range_mcu')
		);

		if(!empty($_FILES['ktp']['name']))
		{
			$upload = $this->_do_upload();
			$data['ktp'] = $upload;
		}


		if(!empty($_FILES['kk']['name']))
		{
			$upload = $this->_do_upload_kk();
			$data['kk'] = $upload;
		}

		$this->karyawan->save($data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'no_reg' 				=> $this->input->post('noreg'),
			'no_ktp' 				=> $this->input->post('no_ktp'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			// 'tempat_lahir' 		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'alamat' 			=> $this->input->post('alamat'),
			// 'no_hp' 			=> $this->input->post('no_hp'),
			// 'agama' 			=> $this->input->post('agama'),
			// 'status_perkawinan' 	=> $this->input->post('status_perkawinan'),
			'no_bpjs_kes' 		=> $this->input->post('no_bpjs_kes'),
			'no_bpjs_tk' 		=> $this->input->post('no_bpjs_tk'),
			'no_bpjs_jht' 		=> $this->input->post('no_bpjs_jht'),
			'gaji_pokok' 				=> $this->input->post('gaji_pokok'),
			'tunjangan' 	=> $this->input->post('tunjangan'),
			't_kerajinan' 		=> $this->input->post('t_kerajinan'),
			't_meal' 		=> $this->input->post('t_kerajinan'),
			't_transport' 		=> $this->input->post('t_kerajinan'),
			'tanggal_masuk' 		=> $this->input->post('tanggal_masuk'),
			// 'tanggal_phk' 			=> $this->input->post('tanggal_phk'),
			'stt_admin' 			=> $this->input->post('is_admin'),
			'stt_karyawan' 			=> $this->input->post('stt_karyawan'),
			'cuti_tahunan' 			=> $this->input->post('cuti_tahunan'),
			'username' 			=> $this->input->post('username'),
			'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT)
		);


		// if($this->input->post('ktp')) // if remove photo checked
  //     {
  //        if(file_exists('./assets/lampiran_karyawan/'.$this->input->post('ktp')) && $this->input->post('ktp'))
  //           unlink('./assets/lampiran_karyawan/'.$this->input->post('ktp'));
  //        $data['ktp'] = '';
  //     }
 
      if(!empty($_FILES['ktp']['name']))
      {
         $upload = $this->_do_upload();
             
         //delete file
         $karyawan = $this->karyawan->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_karyawan/'.$karyawan->ktp) && $karyawan->ktp)
             unlink('./assets/lampiran_karyawan/'.$karyawan->ktp);
 
         $data['ktp'] = $upload;
      }


      if(!empty($_FILES['kk']['name']))
      {
         $upload = $this->_do_upload_kk();
             
         //delete file
         $karyawan = $this->karyawan->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_karyawan/'.$karyawan->kk) && $karyawan->kk)
             unlink('./assets/lampiran_karyawan/'.$karyawan->kk);
 
         $data['kk'] = $upload;
      }

		$this->karyawan->update(array('id_karyawan' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('karyawan',array('id_karyawan'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function download()
	{
		$data['karyawan_pkwt'] = $this->db->query("SELECT * FROM karyawan 
		LEFT JOIN lokasi ON karyawan.id_lokasi = lokasi.id_lokasi 
		LEFT JOIN jabatan ON karyawan.id_jabatan = jabatan.id_jabatan 
		WHERE sistem_penggajian = 'PWT | PWTT'")->result();
		$this->load->view('download_pkwt', $data);
	}


	public function updatedata($id){
		$user_data['jenis'] = $id;     	
		$this->load->view('template/header',$user_data);
		$this->load->view('update_data',$user_data);
	}


	public function proses_upload()
	{
		$idKavling = $this->input->post('id');
		$file_mimes = array('application/octet-stream', 
		'application/vnd.ms-excel', 
		'application/x-csv', 
		'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		// echo $_FILES['datakavling']['name'];
 
			if(isset($_FILES['datakavling']['name'])) {
				$arr_file = explode('.', $_FILES['datakavling']['name']);
				$extension = end($arr_file);
				if('csv' == $extension) {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
				} else {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				}
				$spreadsheet = $reader->load($_FILES['datakavling']['tmp_name']);
				
				$sheetData = $spreadsheet->getActiveSheet()->toArray();
				for($i = 1;$i < count($sheetData);$i++){
					$id_karyawan           	= $sheetData[$i]['1'];
					$gaji_pokok           	= str_replace('.','', str_replace(',','', $sheetData[$i]['6']));
					$tunjangan          	= str_replace('.','', str_replace(',','', $sheetData[$i]['7']));
					$t_kerajinan        	= str_replace('.','', str_replace(',','', $sheetData[$i]['8']));
					$t_meal      			= str_replace('.','', str_replace(',','', $sheetData[$i]['9']));
					$t_transport      		= str_replace('.','', str_replace(',','', $sheetData[$i]['10']));
					$t_hp        			= str_replace('.','', str_replace(',','', $sheetData[$i]['11']));
					$t_perumahan        	= str_replace('.','', str_replace(',','', $sheetData[$i]['12']));
					$t_kehadiran        	= str_replace('.','', str_replace(',','', $sheetData[$i]['13']));
					$uang_saku        		= str_replace('.','', str_replace(',','', $sheetData[$i]['14']));
					$cuti        		= str_replace('.','', str_replace(',','', $sheetData[$i]['15']));
			
					$param = [
						'gaji_pokok'			=> $gaji_pokok,
						'tunjangan'				=> $tunjangan,
						't_kerajinan'			=> $t_kerajinan,
						't_meal'				=> $t_meal,
						't_transport'			=> $t_transport ,
						't_hp'					=> $t_hp,
						't_perumahan'			=> $t_perumahan,
						't_kehadiran'			=> $t_kehadiran,
						't_uang_saku'			=> $uang_saku,
						'cuti_tahunan'			=> $cuti
					];
					$this->db->update('karyawan', $param, ['id_karyawan' => $id_karyawan]);

				}
				// header("Location: index.php"); 
			}else{
				echo 'Tidak Proses';
			}
		redirect('karyawan');
	}


}
