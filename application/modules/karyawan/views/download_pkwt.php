<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=download_data.xls");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
<table border="1">
    <tr>
        <td>No</td>
        <td>ID</td>
        <td>No. REG</td>
        <td>Nama Lengkap</td>
        <td>Lokasi</td>
        <td>Jabatan</td>
        <td>Gaji Pokok</td>
        <td>Tunjangan</td>
        <td>T Kerajinan</td>
        <td>T Meal</td>
        <td>T Transport</td>
        <td>T HP</td>
        <td>T Perumahan</td>
        <td>T Kehadiran</td>
        <td>T Uang Saku</td>
        <td>Jumlah Cuti</td>
    </tr>
    <?php 
    $no = 1;
    foreach($karyawan_pkwt as $pkwt){
        echo '<tr>
            <td>'.$no++.'</td>
            <td>'.$pkwt->id_karyawan.'</td>
            <td>'.$pkwt->no_reg.'</td>
            <td>'.$pkwt->nama_lengkap.'</td>
            <td>'.$pkwt->nama_lokasi.'</td>
            <td>'.$pkwt->nama_jabatan.'</td>
            <td align="right">'.rupiah($pkwt->gaji_pokok).'</td>
            <td align="right">'.rupiah($pkwt->tunjangan).'</td>
            <td align="right">'.rupiah($pkwt->t_kerajinan).'</td>
            <td align="right">'.rupiah($pkwt->t_meal).'</td>
            <td align="right">'.rupiah($pkwt->t_transport).'</td>
            <td align="right">'.rupiah($pkwt->t_hp).'</td>
            <td align="right">'.rupiah($pkwt->t_perumahan).'</td>
            <td align="right">'.rupiah($pkwt->t_kehadiran).'</td>
            <td align="right">'.rupiah($pkwt->t_uang_saku).'</td>
            <td align="right">'.$pkwt->cuti_tahunan.'</td>
        </tr>';
    }

    ?>
</table>