<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  


  <!-- Main content -->
  <section class="content">
    <div class="card">

      <!-- /.card-header -->
      <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-12">
        <div class="alert alert-warning text-center" role="alert">Untuk menjaga akurasi lokasi dan menghindari kesalahan data, disarankan absen menggunakan HP.</div>
        </div>
      </div>

          <center>
          <?php
          echo '<badge class="badge badge-info">'.$this->encryption->decrypt($this->session->userdata('nama_lengkap')).'</badge>';
          echo '<br>';
          echo '<b>'.$this->encryption->decrypt($this->session->userdata('no_reg')).'</b>';
          $id_marketing = $this->encryption->decrypt($this->session->userdata('id_karyawan'));
          ?>
          <br>
          <br>
          <?php 
          if(infoPegawai($id_marketing)['foto'] == ""){
            echo '<img src="'.base_url('foto/noimage_person.png').'" width="180">';
          }else{
            echo '<img src="'.base_url('foto/'.infoPegawai($id_marketing)['foto']).'" width="180">';
          } 
          ?>
          

          <br>
          <br>
          <a class="btn btn-success btn-md" href="<?=base_url('absen/masuk');?>">Lakukan Absen</a>

          <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" class="form-control" name="lat_long" value="" id="lat_long" readonly>
          <input type="hidden" class="form-control" name="long" value="" id="long">
          <input type="hidden" class="form-control" name="lat" value="" id="lat">
          </form>

          </center>
    </div>
  </section>
</div>









<script type="text/javascript">

  var url = '<?=base_url();?>';
   function masuk(id){
    $.confirm({
      title: 'Perhatian..',
      content: 'Apakah anda akan melakukan absen masuk?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "dashboard/absen_masuk/" + id,
              type: "POST",
              data: $('#form').serialize(),
              dataType: "JSON",
              success: function(data)
              {
                  //if success 
                  window.location = "<?=base_url('rekap');?>";

              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error adding data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }


  function pulang(id){
    $.confirm({
      title: 'Perhatian..',
      content: 'Apakah anda akan melakukan absen pulang?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "dashboard/absen_pulang/" + id,
              type: "POST",
              data: $('#form').serialize(),
              dataType: "JSON",
              success: function(data)
              {
                  //if success 
                  window.location = "<?=base_url('rekap');?>";

              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error adding data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }
</script>



<script src="<?=base_url('assets/js/geo-min.js');?>"></script>

    <script>
        if(geo_position_js.init()){
            geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
        }
        else{
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ="Tidak ada fungsi geolocation";
        }
 
        function success_callback(p)
        {
            latitude=p.coords.latitude ;
            longitude=p.coords.longitude;

            var long = document.getElementById('long'); 
            var lat = document.getElementById('lat'); 
            var lat_long = document.getElementById('lat_long'); 
            long.value = longitude;
            lat.value = latitude;
            lat_long.value = latitude+','+longitude;

        }
        
        function error_callback(p)
        {
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ='error='+p.message;
        }        
    </script>


  <?php  $this->load->view('template/footer'); ?>