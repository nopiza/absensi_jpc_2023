<div class="container">
  <div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-10">
 

   <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Pengaturan Awal</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
          <div class="alert alert-warning" role="alert">
            <b>PERHATIAH : </b>
            <br>Merunut pada kebijakan baru, bahwa absensi hanya bisa dilakukan pada radius yang telah didaftarkan. Pastikan saat ini bapak/Ibu berada pada posisi koordinat yang akan didaftarkan. 
            <br><br>
            Untuk menghindari lupa absen, bapak/ibu dapat mengaktifkan notifikasi WhatsApp, yang akan mengirimkan notifikasi setiap pukul 07.15 dan 15.30 pada setiap jadwal WFH. 
            <br>*-Notifikasi hanya akan dikirim jika belum absen pada jadwal WFH.
          </div>
          <br>
          <br>


                                    <div class="col-sm-12" id="petani"></div>

                                  <br>
  Klik 
  <a href="https://support.google.com/maps/answer/2839911?co=GENIE.Platform%3DAndroid&hl=id" target="_blank">disini </a>jika akurasi lokasi tidak tepat.
  <br>
  <hr>

          

          <form action="<?=base_url('pengguna/update_reg');?>" id="form" class="form-horizontal" method="POST" enctype="multipart/form-data">
          <input type="hidden" value="<?=$id_pegawai;?>" name="id_pegawai" id="id_pegawai"/>

          <div class="form-group row">
              <label class="col-sm-2 control-label">Lokasi</label>
              <div class="col-md-5">
              <input type="text" class="form-control" name="lat_long" value="" id="lat_long" readonly>
              <input type="hidden" class="form-control" name="long" value="" id="long">
              <input type="hidden" class="form-control" name="lat" value="" id="lat">
              </div>
          </div>


          <div class="form-group row">
            <label class="col-sm-2 control-label">Nama Lengkap </label>
            <div class="col-md-5">
              <input name="nama_pegawai" type="text" class="form-control" id="nama_pegawai" value="<?=infoPegawai($id_pegawai)['nama_pegawai'];?>" readonly>
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 control-label">NIP</label>
            <div class="col-md-5">
              <input name="nip" type="text" class="form-control" id="nip" value="<?=infoPegawai($id_pegawai)['nip'];?>" readonly>
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 control-label">Password </label>
            <div class="col-md-5">
              <input name="pass1" type="password" class="form-control" id="pass1" value="">
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-md-5">
              <input name="email" type="text" class="form-control" id="email" value="<?=infoPegawai($id_pegawai)['email'];?>">
            </div>
          </div>


          <div class="form-group row">
            <label class="col-sm-2 control-label">No. WA</label>
            <div class="col-md-5">
              <input name="no_wa" type="text" class="form-control" id="no_wa"  value="<?=infoPegawai($id_pegawai)['no_wa'];?>">
            </div>
          </div>


          <div class="form-group row">
            <label class="col-sm-2 control-label">Notifikasi WA</label>
            <div class="col-md-3">
              <select name="notif" class="form-control" id="notif">
                <option value="0">- pilih -</option>
                <option value="1">Aktifkan</option>
                <option value="0">Non Aktifkan</option>
              </select>
            </div>
          </div>

          <?php 
          if(infoPegawai($id_pegawai)['foto'] != ''){ ?>
          <div class="form-group row">
            <label class="col-sm-2 control-label"></label>
            <div class="col-md-5">
              <img src="<?=base_url('foto/'.infoPegawai($id_pegawai)['foto']);?>">
            </div>
          </div>

        <?php } ?>


          <div class="form-group row">
            <label class="col-sm-2 control-label">Foto Profil</label>
            <div class="col-md-5">
              <input name="foto" type="file"  id="foto"  value="">
            </div>
          </div>


          <div class="form-group row">
            <label class="col-sm-2 control-label"></label>
            <div class="col-md-3">
              <button type="submit" id="btnSave" class="btn btn-primary">Simpan</button>
            </div>
          </div>



                                                  
          </form>

      </div>
         
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</div>
    <div class="col-sm-1">
    </div>
  </div>
</div>





<script src="<?=base_url('assets/js/geo-min.js');?>"></script>

    <script>
        if(geo_position_js.init()){
            geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
        }
        else{
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ="Tidak ada fungsi geolocation";
        }
 
        function success_callback(p)
        {
            latitude=p.coords.latitude ;
            longitude=p.coords.longitude;
            // pesan='posisi:'+latitude+','+longitude;
            // pesan = pesan + "<br/>";
            // pesan = pesan + '<img src="https://maps.googleapis.com/maps/api/staticmap?size=400×400&amp;zoom=13&amp;markers=color:red%7Clabel:C%7C'+latitude +','+longitude+'"/>';
            // div_isi=document.getElementById("div_isi");
            //alert(pesan);
            // div_isi.innerHTML =pesan;
            var long = document.getElementById('long'); 
            var lat = document.getElementById('lat'); 
            var lat_long = document.getElementById('lat_long'); 
            long.value = longitude;
            lat.value = latitude;
            lat_long.value = latitude+','+longitude;
            document.getElementById("petani").innerHTML = '<iframe width="100%" height="300" frameborder="0"   scrolling="no"   marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+latitude+','+longitude+'&hl=id&z=15&amp;output=embed" ></iframe>';
        }
        
        function error_callback(p)
        {
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ='error='+p.message;
        }        
    </script>