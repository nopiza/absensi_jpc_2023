<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perjalanan_dinas extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'perjalanan_dinas');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perjalanan_dinas_model','perjalanan_dinas');
		check_login();
	}

	public function index()
	{
		$user_data['tambah_view'] = 1;
		$user_data['lihat_view'] = 1;
		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';
		$user_data['menu_active'] = 'Data Referensi';
		$user_data['sub_menu_active'] = 'Menu';
     	$this->load->view('template/header');
		$this->load->view('view',$user_data);
	}

	public function ajax_list()
	{
		$list = $this->perjalanan_dinas->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_izin."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_izin."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = tgl_indo($post->tanggal);
			$row[] = '<b>'.$post->nama_lengkap.'</b><br>'.$post->no_reg;
         	$row[] = $post->tujuan_dinas;
         	$row[] = $post->deskripsi;
			//add html for action
			$row[] = $link_hapus;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->perjalanan_dinas->count_all(),
						"recordsFiltered" => $this->perjalanan_dinas->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->perjalanan_dinas->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add(){

        $tanggal = $this->input->post('tanggal');
        $tanggal2 = $this->input->post('tanggal2');

		if(empty($tanggal2)){
			$data = array(
				'id_karyawan' 	=> $this->input->post('nama_lengkap'),
				'tanggal' 		=> $this->input->post('tanggal'),
				'tujuan_dinas' 		=> $this->input->post('tujuan_dinas'),
				'durasi' 		=> '1',
				'deskripsi' 	=> $this->input->post('deskripsi')
			);
			$this->perjalanan_dinas->save($data);

		}else{
			while (strtotime($tanggal) <= strtotime($tanggal2)) {
				$data = array(
					'id_karyawan' 	=> $this->input->post('id_karyawan'),
					'tanggal' 		=> $tanggal,
					'tujuan_dinas' 		=> $this->input->post('tujuan_dinas'),
					'durasi' 		=> '1',
					'deskripsi' 	=> $this->input->post('deskripsi')
				);
				$this->perjalanan_dinas->save($data);
				$tanggal = date ("Y-m-d", strtotime("+1 days", strtotime($tanggal)));
			}
		}
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
        $tanggal = $this->input->post('tanggal');
        $tanggal2 = $this->input->post('tanggal2');

		$data = array(
			'id_karyawan' 	=> $this->input->post('id_karyawan'),
			'tanggal' 		=> $this->input->post('tanggal'),
			'jenis_izin' 	=> $this->input->post('jenis_izin'),
			'durasi' 		=> '1',
			'deskripsi' 	=> $this->input->post('deskripsi')
		);
		$this->perjalanan_dinas->update(array('IDperjalanan_dinas' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->perjalanan_dinas->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_select(){
		$q =$this->input->get('q');
        $items=$this->db->query("SELECT id_karyawan, no_reg, nama_lengkap FROM karyawan WHERE nama_lengkap like '%$q%' OR no_reg like '%$q%'")->result_array();
        echo json_encode($items);
    }


    public function get($id_karyawan){
        $item=$this->db->query("SELECT * FROM karyawan WHERE id_karyawan ='$id_karyawan' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


	



}
