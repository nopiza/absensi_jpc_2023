<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  


  <!-- Main content -->
  <section class="content">
    <div class="card">

      <!-- /.card-header -->
      <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-12">
            <?php 
            date_default_timezone_set("Asia/Jakarta");
            // cek apakah sudah boleh absen lembur
            $sekarang = date('Y-m-d');
            // $jamSekarang = time();
            $idKaryawan = $this->encryption->decrypt($this->session->userdata('id_karyawan'));
            $query = "SELECT * FROM pengajuan_lembur l 
            LEFT JOIN pengajuan_lembur_detail d ON l.id_pengajuan=d.id_pengajuan 
            WHERE d.id_karyawan='$idKaryawan' AND l.tanggal='$sekarang'";
            $cek = $this->db->query($query)->row_array();

            if(strtotime($cek['jam_mulai']) >= time()){
                echo '<div class="alert alert-warning text-center" role="alert">Tombol absen lembur akan aktif pada pukul : '.$cek['jam_mulai'].'</div>';
            }else{
                // cek apakah sudah absen masuk lembur
                if($cek['jam_masuk'] == '00:00:00'){
                    // absen masuk lembur
                    echo '<a class="btn btn-success btn-md" href="'.base_url('absen/absenlembur/masuk-lembur').'">Absen Lembur Masuk</a>';
                }else{
                    // absen pulang lembur
                    // cek apakah sudah absen lembur pulang
                    if($cek['jam_pulang'] == '00:00:00'){ 
                        // Belum Absen Pulang
                        echo '<a class="btn btn-danger btn-md" href="'.base_url('absen/absenlembur/pulang-lembur').'">Absen Lembur Pulang</a>';
                    }else{
                        //Hitung rekapan lembur hari ini
                        $jam   = floor($cek['durasi_lembur'] / (60));
			            $menit = $cek['durasi_lembur'] - ( $jam * (60) );
                        echo 'Hari ini anda telah menjalankan Lembur selama : <br>'. $jam.' jam '.$menit.' menit';
                    }
                }
                
            }
            ?>
        </div>
      </div>
</form>

          </center>
    </div>
  </section>
</div>



  <?php  $this->load->view('template/footer'); ?>