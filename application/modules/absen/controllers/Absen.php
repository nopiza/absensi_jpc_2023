<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'absen');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Absen_model','absen');
		$this->load->library('user_agent');
		// $this->load->model('Group/Group_model','group');
		check_login_karyawan();
	}

	function distance($lat1, $lon1, $lat2, $lon2) { 
        $pi80 = M_PI / 180; 
        $lat1 *= $pi80; 
        $lon1 *= $pi80; 
        $lat2 *= $pi80; 
        $lon2 *= $pi80; 
        $r = 6372000.797; // radius of Earth in km 6371
        $dlat = $lat2 - $lat1; 
        $dlon = $lon2 - $lon1; 
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2); 
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a)); 
        $km = $r * $c; 
        return round($km); 
    }
	

// 	function cek_jarak(){
// 		echo $this-> distance('-7.8744146', '112.5244688', '-7.8750102', '112.5244586');
// 	}

	public function index()
	{
		$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header_marketing');
		$this->load->view('view',$user_data);
	}

	public function lembur()
	{
		$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header_marketing');
		$this->load->view('lembur',$user_data);
	}

	public function absenlembur($jenis)
	{
		$query = "SELECT nama_tempat, latitude, longitude, SQRT(
			POW(69.1 * (latitude - -7.8747331), 2) +
			POW(69.1 * (112.5241237 - longitude) * COS(latitude / 57.3), 2)) * 1000 AS distance
		FROM lokasi_absen HAVING distance < 1000 ORDER BY distance;";
		$user_data['data_ref'] = $this->data_ref;
		$user_data['jenis'] = $jenis;
		$this->load->view('template/header_marketing');
		$this->load->view('kosong_lembur',$user_data);
	}

	public function masuk()
	{
		$query = "SELECT nama_tempat, latitude, longitude, SQRT(
			POW(69.1 * (latitude - -7.8747331), 2) +
			POW(69.1 * (112.5241237 - longitude) * COS(latitude / 57.3), 2)) * 1000 AS distance
		FROM lokasi_absen HAVING distance < 1000 ORDER BY distance;";
		$user_data['data_ref'] = $this->data_ref;
		$this->load->view('template/header_marketing');
		$this->load->view('kosong',$user_data);
	}

	public function cek_lokasi($lat, $long)
	{
		$query = "SELECT nama_tempat, latitude, longitude, SQRT(
			POW(69.1 * (latitude - $lat), 2) +
			POW(69.1 * ($long - longitude) * COS(latitude / 57.3), 2)) * 1000 AS distance, radius_absen 
			FROM lokasi_absen ORDER BY distance ASC LIMIT 0,1";
		$data = $this->db->query($query)->row_array();

		// echo json_encode(array("status" => 'TRUE'));
		echo json_encode($data);

	}


	// public function masuk()
	// {
	// 	// cek radius 
	// 	// Target Lokasi Absen
	// 	$lat_t = '-7.8750102';
	// 	$long_t = '112.5244586';
	// 	// Rumah 
	// 	$lat = '-7.8747331';
	// 	$long = '112.5241237';

	// 	$jarak = $this-> distance($lat_t, $long_t, $lat,$long);
	// 	$user_data['data_ref'] = $this->data_ref;
	// 	$this->load->view('template/header_marketing');
	// 	if($jarak < 40){
	// 		$this->load->view('view_masuk',$user_data);
	// 	}else{
	// 		$this->load->view('radius_jauh',$user_data);
	// 	}
	// }


	public function pulang(){

		$user_data['data_ref'] = $this->data_ref;
		// $user_data['pertanyaan'] = $this->db->get('pertanyaan')->result();

     	$this->load->view('template/header_marketing');
		$this->load->view('view_pulang',$user_data);

	}

	public function simpan()
	{

		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Data user gagal di dapatkan';
		}

		// $regLat = infoPegawai($this->encryption->decrypt($this->session->userdata('id_pegawai')))['reg_lat'];
		// $regLong = infoPegawai($this->encryption->decrypt($this->session->userdata('id_pegawai')))['reg_long'];

		// //cek radius koordinat
		// $a = getDistanceBetweenPointsNew(
		// 	$this->input->post('lat'),
		// 	$regLat,  
		// 	$this->input->post('long'),
		// 	$regLong);

		// echo $a;
 

		$data = array(
				'id_account' 		=> $this->encryption->decrypt($this->session->userdata('id_karyawan')),
				'tanggal_absensi' 	=> tglTime_now(),
            	'longitude_absensi' => $this->input->post('long'),
            	'latitude_absensi' 	=> $this->input->post('lat'),
				'type_absensi' 		=> $this->input->post('type_absensi'),
				'lokasi_absen' 		=> $this->input->post('lokasi_absen'),
				'radius' 			=> $this->input->post('radius'),
				'ip_user' 			=> $this->input->ip_address(),
				'os_user' 			=> $this->agent->platform(),
				'browser_user' 		=> $agent
				// 'catatan' 			=> $this->input->post('catatan')
		);

		$insert = $this->db->insert('absensi',$data);
		$idnya = $this->db->insert_id();	
		
		echo json_encode(array("status" => 'TRUE'));
	}


	public function simpan_absen()
	{

		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Data user gagal di dapatkan';
		}

		$data = array(
				'id_account' 		=> $this->encryption->decrypt($this->session->userdata('id_karyawan')),
				'tanggal_absensi' 	=> tglTime_now(),
            	'longitude_absensi' => $this->input->post('long'),
            	'latitude_absensi' 	=> $this->input->post('lat'),
				'type_absensi' 		=> $this->input->post('type_absensi'),
				'lokasi_absen' 		=> $this->input->post('lokasi'),
				'radius' 			=> $this->input->post('radius'),
				'catatan' 			=> $this->input->post('catatan'),
				'ip_user' 			=> $this->input->ip_address(),
				'os_user' 			=> $this->agent->platform(),
				'browser_user' 		=> $agent
		);

		$insert = $this->db->insert('absensi',$data);
		$idnya = $this->db->insert_id();	
		
		redirect('rekap');
	}

	public function simpan_absen_lembur()
	{
		date_default_timezone_set("Asia/Jakarta");
		$sekarang = date('Y-m-d');
		//simpan absen lembur
		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Data user gagal di dapatkan';
		}

		// cek jenis absen
		if($this->input->post('type_absensi') == 'MASUK-LEMBUR'){
			$data = array(
            	'jam_masuk' 		=> date('H:i:s'),
            	'longitude_absensi' => $this->input->post('long'),
            	'latitude_absensi' 	=> $this->input->post('lat'),
				'lokasi_absen' 		=> $this->input->post('lokasi_absen'),
				'radius' 			=> $this->input->post('radius'),
				'catatan' 			=> $this->input->post('catatan'),
				'ip_user' 			=> $this->input->ip_address(),
				'os_user' 			=> $this->agent->platform(),
				'browser_user' 		=> $agent
			);
			echo 'A';
		}else if($this->input->post('type_absensi') == 'PULANG-LEMBUR'){
			// Jika absen pulang hitung total menit lemburnya
			// cari jam masuk
			$idKaryawan = $this->encryption->decrypt($this->session->userdata('id_karyawan'));
            $query = "SELECT * FROM pengajuan_lembur l 
            LEFT JOIN pengajuan_lembur_detail d ON l.id_pengajuan=d.id_pengajuan 
            WHERE d.id_karyawan='$idKaryawan' AND l.tanggal='$sekarang'";
            $cek = $this->db->query($query)->row_array();

			$awal  = strtotime($cek['jam_masuk']);
			$akhir = strtotime(date('H:i:s'));
			$diff  = $akhir - $awal;
			$menit   = floor($diff / (60));
			// $menit = $diff - ( $jam * (60 * 60) );


			$data = array(
            	'jam_pulang' 			=> date('H:i:s'),
            	'p_longitude_absensi' 	=> $this->input->post('long'),
            	'p_latitude_absensi' 	=> $this->input->post('lat'),
				'p_lokasi_absen' 		=> $this->input->post('lokasi_absen'),
				'p_radius' 				=> $this->input->post('radius'),
				'p_catatan' 			=> $this->input->post('catatan'),
				'p_ip_user' 			=> $this->input->ip_address(),
				'p_os_user' 			=> $this->agent->platform(),
				'p_browser_user' 		=> $agent,
				'durasi_lembur' 		=> $menit
			);
			echo 'AB';
			
		}
		
		$cc = $this->db->update('pengajuan_lembur_detail', $data, [
			'id_karyawan' 		=> $this->encryption->decrypt($this->session->userdata('id_karyawan')),
			'tanggal' 			=> $this->input->post('tanggal')
		]);
		
		var_dump($cc);
		
		redirect('absen/lembur');
	}


	public function cek()
	{
		$data = array(
				'id_account' 			=> $this->encryption->decrypt($this->session->userdata('id_karyawan')),
				'tanggal_absensi like ' => tgl_now(),
				'type_absensi' 			=> $this->input->post('type_absensi')
		);
		$cari = $this->db->get_where('absensi',$data)->num_rows();


		if($cari > 0){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
		
	}

}
