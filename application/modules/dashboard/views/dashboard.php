<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <!-- <h1 class="m-0 text-dark">Dashboard
          <?php echo $this->encryption->decrypt($this->session->userdata('id')); ?>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->

      <div class="row">
        <div class="col-lg-12 col-12">

        <div class="row">
        <div class="col-lg-12 col-12">
          <!-- Username :<?php echo $this->encryption->decrypt($this->session->userdata('username'));?>  <br>
          ID User : <?php echo $this->encryption->decrypt($this->session->userdata('id'));?> <br> -->
          <?php 
          // $idLokasi = $this->encryption->decrypt($this->session->userdata('id_lokasi'));
          // $lokasi = $this->db->query("SELECT * FROM lokasi WHERE id_lokasi='$idLokasi'")->row_array(); 
          // if($idLokasi == '0'){
          //   $namaLokasi = 'Kantor Pusat';
          // }else{
          //   $namaLokasi = $lokasi['nama_lokasi'];
          // }
          
          ?>
          <!-- ID Lokasi : <?php echo $idLokasi.' # '.$namaLokasi;?> <br> -->
          <a href="<?=base_url('dashboardabsen/detail');?>" target="_blank" class="btn btn-info btn-sm">Detail Data Absen Hari ini</a>
          <hr>
          <br>

        </div>
        </div>
          


  

<!-- ================================================================================================== -->
<?php 
if($this->encryption->decrypt($this->session->userdata('id_lokasi')) == '0'){
$where = '';
$whereAnd = '';
}else{
  $where = " WHERE id_lokasi='$idLokasi'";
  $whereAnd = " AND id_lokasi='$idLokasi'";
} ?>
<div class="row">
          <div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2 shadow-sm">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-warning">
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="./assets/G.jpg" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Statistik Karyawan</h3>
                <h5 class="widget-user-desc">Berdasarkan Usia</h5>
              </div>
              <?php 
              $karyawan = $this->db->query("SELECT COUNT(id_karyawan) as jumKaryawan FROM karyawan $where")->row_array();
              $Laki = $this->db->query("SELECT COUNT(id_karyawan) as jumKaryawan FROM karyawan WHERE jenis_kelamin='Laki-laki' $whereAnd")->row_array();
              $perempuan = $this->db->query("SELECT COUNT(id_karyawan) as jumKaryawan FROM karyawan WHERE jenis_kelamin='Perempuan' $whereAnd")->row_array();
              $umur = $this->db->query("SELECT
COUNT(IF(umur < 25,1,NULL)) AS 'satu',
COUNT(IF(umur BETWEEN 25 and 29,1,NULL)) AS 'dua',
COUNT(IF(umur BETWEEN 30 and 34,1,NULL)) AS 'tiga',
COUNT(IF(umur BETWEEN 35 and 39,1,NULL)) AS 'empat',
COUNT(IF(umur BETWEEN 40 and 49,1,NULL)) AS 'lima',
COUNT(IF(umur >= 50,1,NULL)) AS 'manula' 
FROM (SELECT no_reg, nama_lengkap, tanggal_lahir, TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS umur FROM karyawan $where)  as dummy_table")->row_array();
              ?>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Jumlah Karyawan <span class="float-right badge bg-primary"><?=$karyawan['jumKaryawan'];?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Laki-Laki <span class="float-right badge bg-warning"><?=$Laki['jumKaryawan'];?></span>
                    </a>
                  </li>  
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Perempuan <span class="float-right badge bg-success"><?=$perempuan['jumKaryawan'];?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Usia < 25 Tahun <span class="float-right badge bg-info"><?=$umur['satu'];?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Usia 26 - 30 Tahun <span class="float-right badge bg-info"><?=$umur['dua'];?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Usia 31 - 35 Tahun <span class="float-right badge bg-info"><?=$umur['tiga'];?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Usia 36 - 40 Tahun <span class="float-right badge bg-info"><?=$umur['empat'];?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Usia 41 - 50 Tahun <span class="float-right badge bg-info"><?=$umur['lima'];?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Usia > 50 Tahun <span class="float-right badge bg-info"><?=$umur['manula'];?></span>
                    </a>
                  </li>
                  
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->
          




          <div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2 shadow-sm">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="./assets/G.jpg" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Statistik Karyawan</h3>
                <h5 class="widget-user-desc">Department</h5>
              </div>
              
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                <?php 
              $depart = $this->db->query("SELECT *, (SELECT COUNT(id_karyawan) FROM karyawan WHERE id_department = department.id_department $whereAnd) as jum FROM department")->result();
              foreach($depart as $dpt){
              ?>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                    <?=$dpt->nama_department?> <span class="float-right badge bg-secondary"><?=$dpt->jum?></span>
                    </a>
                  </li>
                  
                  <?php } ?>
                  
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->




          <div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2 shadow-sm">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-success">
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="./assets/G.jpg" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Statistik Karyawan</h3>
                <h5 class="widget-user-desc">Lokasi</h5>
              </div>
              
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                <?php 
              $lokasi = $this->db->query("SELECT *, (SELECT COUNT(id_karyawan) FROM karyawan WHERE id_lokasi = lokasi.id_lokasi $whereAnd) as jum FROM lokasi $where")->result();
              foreach($lokasi as $lks){
              ?>
                  <li class="nav-item">
                  <a href="<?=base_url('lokasi/detail/'.$lks->id_lokasi);?>" class="nav-link">
                    <?=$lks->nama_lokasi?> <span class="float-right badge bg-secondary"><?=$lks->jum?></span>
                    </a>
                  </li>
                  
                  <?php } 
                  
                  $query = "SELECT COUNT(id_karyawan) as jj FROM karyawan WHERE id_lokasi = '0'";
                  $Nonlokasi = $this->db->query($query)->row_array();

                  if($this->encryption->decrypt($this->session->userdata('id_lokasi')) == '0'){

                  ?>

                  <li class="nav-item">
                    <a href="<?=base_url('lokasi/detail/0');?>" class="nav-link">
                      Non Lokasi <span class="float-right badge bg-secondary"><?=$Nonlokasi['jj'];?></span>
                    </a>
                  </li>
                  <?php } ?>
                  
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->





        </div>
      </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


