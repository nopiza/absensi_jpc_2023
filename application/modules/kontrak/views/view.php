  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Kontrak Karyawan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Cuti</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Kontrak Karyawan</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-info btn-sm" onclick="add()"><i class="fa fa-plus"></i> Tambah Kontrak Baru</a>&nbsp;
                <button class="btn btn-default btn-sm" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="20%">Nama</th>
                        <th width="15%">Tgl Kontrak</th>
                        <th width="15%">Tgl Berakhir</th>
                        <th width="20%">Department</th>
                        <th width="10%">Count Down</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

    
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Karyawan</label>
                            <div class="col-md-5">
                            <input name="karyawan" placeholder="" class="form-control" type="text" id="karyawan">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. KTP</label>
                            <div class="col-md-5">
                                <input name="no_ktp" id="no_ktp" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Lokasi</label>
                            <div class="col-md-5">
                                <input name="lokasi" id="lokasi" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Department</label>
                            <div class="col-md-5">
                                <input name="department" id="department" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-5">
                                <input name="jabatan" id="jabatan" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Kontrak</label>
                            <div class="col-md-3">
                                <input name="tanggal_awal" id="tanggal_awal" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Berakhir Kontrak</label>
                            <div class="col-md-3">
                                <input name="tanggal_akhir" id="tanggal_akhir" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tempat Penerimaan</label>
                            <div class="col-md-5">
                                <input name="tempat_penerimaan" id="lokasi" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Department</label>
                            <div class="col-md-5">
                                <select name="department" id="department" class="form-control">
                                <option value="">-- Pilih --</option>
                                <?php 
                                foreach($department as $dpt){
                                    echo '<option value="'.$dpt->id_department.'">'.$dpt->nama_department.'</option>';
                                }
                                ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-5">
                            <select name="jabatan" id="jabatan" class="form-control">
                                <option value="">-- Pilih --</option>
                                <?php 
                                foreach($jabatan as $dpt){
                                    echo '<option value="'.$dpt->id_jabatan.'">'.$dpt->nama_jabatan.'</option>';
                                }
                                ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Lokasi</label>
                            <div class="col-md-5">
                            <select name="lokasi" id="lokasi" class="form-control">
                                <option value="">-- Pilih --</option>
                                <?php 
                                foreach($lokasi as $dpt){
                                    echo '<option value="'.$dpt->id_lokasi.'">'.$dpt->nama_lokasi.'</option>';
                                }
                                ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label class="control-label col-md-3">Gaji Pokok</label>
                            <div class="col-md-2">
                                <input name="gaji_pokok" id="gaji_pokok" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Site</label>
                            <div class="col-md-2">
                                <input name="tunjangan_site" id="tunjangan_site" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Jabatan</label>
                            <div class="col-md-2">
                                <input name="tunjangan_jabatan" id="tunjangan_jabatan" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">insentif</label>
                            <div class="col-md-2">
                                <input name="insentif" id="insentif" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Lain-lain</label>
                            <div class="col-md-2">
                                <input name="tunjangan_other" id="tunjangan_other" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jumlah Upah</label>
                            <div class="col-md-2">
                                <input name="jumlah_upah" id="jumlah_upah" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">File Lampiran Kontrak</label>
                            <div class="col-md-7">
                                <input name="lampiran" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->




<div class="modal fade bd-example-modal-xl" id="detail_cuti" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog modal-xl ">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="modalLabel">Upload Berkas</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      </div>

		  <div class="modal-body" id="lampiran">


	    </div>
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-xl" id="history_kontrak" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog modal-xl ">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="modalLabel">-</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      </div>

		  <div class="modal-body" id="history_ktrk">


	    </div>
    </div>
  </div>
</div>


</body>
</html>





<?php  $this->load->view('template/footer'); ?>

<script src="<?php echo base_url('assets/admin/plugins/select2/select2.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2-bootstrap.css') ?>">

<script type="text/javascript">

var url_apps = "<?=base_url();?>"

$(document).ready(function () {
//----->
//Ambil semua data customer untuk select 2
  $("#karyawan").select2({
    ajax: {
      url: url_apps+'cuti/ajax_select_karyawan',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params, // search term
        };
      },
      results: function (data, params) {
        console.log(data);
        return {
            results: $.map(data, function (item) {
                return {
                    text: item.nama_lengkap,
                    id: item.id_karyawan
                }
            })
        };
      },
      cache: true
    },
    minimumInputLength: 1,
  });  


  $('#karyawan').on('change', function() {
  var idSiswa = $(this).val();
  $.ajax({
    url: url_apps + 'cuti/get/' + $(this).val(),
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    //alert(data.ALAMAT);
    $('#no_ktp').val(data.no_ktp);
    $('#department').val(data.nama_department);
    $('#lokasi').val(data.nama_lokasi);
    $('#jabatan').val(data.nama_jabatan);
    
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
});



});  
</script>







<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });


      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });

   function add()
   {
       save_method = 'add';
       $('#form')[0].reset(); // reset form on modals
       $('#karyawan').select2('data', {id: '', text:''});
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
       $('#modal_form').modal('show'); // show bootstrap modal
       $('.modal-title').text('Input Data Kontrak Karyawan'); // Set Title to Bootstrap modal title
       $('#photo-preview').hide(); // hide photo preview modal
        // $('#label-photo').text('Upload Photo'); // label photo upload
   }

   function edit(id)
   {
       save_method = 'update';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
    
       //Ajax Load data from ajax
       $.ajax({
           url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_edit/')?>/" + id,
           type: "GET",
           dataType: "JSON",
           success: function(data)
           {
               $('[name="id"]').val(data.id_kontrak);
               $('#karyawan').select2('data', {id: data.id_karyawan, text: data.nama_lengkap});
               $('[name="tanggal_awal"]').val(data.tgl_kontrak);
               $('[name="tanggal_akhir"]').val(data.tgl_habis_kontrak);
               $('[name="tempat_penerimaan"]').val(data.tempat_penerimaan);
               $('[name="department"]').val(data.id_department);
               $('[name="jabatan"]').val(data.id_jabatan);
               $('[name="lokasi"]').val(data.id_lokasi);
            //    document.getElementById("department").value = data.id_department;
               $('[name="jabatan"]').val(data.id_jabatan);
               $('[name="lokasi"]').val(data.id_lokasi);

               $('[name="gaji_pokok"]').val(formatRupiah(data.gaji_pokok));
               $('[name="tunjangan_site"]').val(formatRupiah(data.tunjangan_site));
               $('[name="tunjangan_jabatan"]').val(formatRupiah(data.tunjangan_jabatan));
               $('[name="insentif"]').val(formatRupiah(data.insentif));
               $('[name="tunjangan_other"]').val(formatRupiah(data.tunjangan_other));
               $('[name="jumlah_upah"]').val(formatRupiah(data.jum_upah));

               $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
               $('.modal-title').text('Edit Data Kontrak'); // Set title to Bootstrap modal title
              
              $('#photo-preview').show(); // show photo preview modal
              if(data.foto)
              {
                  $('#label-photo').text('Change Photo'); // label photo upload
                  $('#photo-preview div').html('<img src="'+url+'assets/images/'+data.foto+'" class="img-responsive" width="50">'); // show photo
                  $('#photo-preview div').append('<label><input type="checkbox" name="remove_photo" value="'+data.foto+'"/> Hapus foto ketika di simpan</label>'); // remove photo
   
              }
              else
              {
                  $('#label-photo').text('Upload Photo'); // label photo upload
                  $('#photo-preview div').text('(No photo)');
              }
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error get data from ajax');
           }
       });
   }

   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
   }

   function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
       }
    
       // ajax adding data to database
       var formData = new FormData($('#form')[0]);
       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   $('#modal_form').modal('hide');
                   reload_table();
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }

   function hapus(id){
    $.confirm({
      title: 'Confirm!',
      content: 'Apakah anda yakin menghapus data ini ?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_delete/" + id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  reload_table();
                  Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Dihapus'
                   });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }

</script>


<script type="text/javascript">

    var gaji_pokok = document.getElementById('gaji_pokok');
        gaji_pokok.addEventListener('keyup', function(e) {
        gaji_pokok.value = formatRupiah(this.value);
    });

    var tunjangan_site = document.getElementById('tunjangan_site');
        tunjangan_site.addEventListener('keyup', function(e) {
        tunjangan_site.value = formatRupiah(this.value);
    });

    var tunjangan_jabatan = document.getElementById('tunjangan_jabatan');
        tunjangan_jabatan.addEventListener('keyup', function(e) {
        tunjangan_jabatan.value = formatRupiah(this.value);
    });

    var insentif = document.getElementById('insentif');
        insentif.addEventListener('keyup', function(e) {
        insentif.value = formatRupiah(this.value);
    });

    var tunjangan_other = document.getElementById('tunjangan_other');
        tunjangan_other.addEventListener('keyup', function(e) {
        tunjangan_other.value = formatRupiah(this.value);
    });

    var jumlah_upah = document.getElementById('jumlah_upah');
        jumlah_upah.addEventListener('keyup', function(e) {
        jumlah_upah.value = formatRupiah(this.value);
    });


    gaji_pokok.addEventListener('keydown', function(event) { limitCharacter(event); });
    tunjangan_site.addEventListener('keydown', function(event) { limitCharacter(event); });
    tunjangan_jabatan.addEventListener('keydown', function(event) { limitCharacter(event); });
    insentif.addEventListener('keydown', function(event) { limitCharacter(event); });
    tunjangan_other.addEventListener('keydown', function(event) { limitCharacter(event); });
    jumlah_upah.addEventListener('keydown', function(event) { limitCharacter(event); });
    



/* Fungsi */
function formatRupiah(bilangan, prefix) {
    var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function limitCharacter(event) {
key = event.which || event.keyCode;
if (key != 188 // Comma
  &&
  key != 8 // Backspace
  &&
  key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
  &&
  (key < 48 || key > 57) // Non digit
  // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
) {
  event.preventDefault();
  return false;
}
}

var url = "<?=base_url();?>";
  function history(id){
      $('#history_ktrk').load(url + 'kontrak/detailkontrak/'+id );
      $('#history_kontrak').modal('show'); // show bootstrap modal
      $('.modal-title').text('History Kontrak Karyawan'); // Set Title to Bootstrap modal title

   }
   
</script>