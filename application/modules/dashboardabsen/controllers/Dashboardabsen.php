<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboardabsen extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('customade');
		$this->load->library(array('form_validation'));	
		check_login();

	}
	
	public function index()
	{
		$data=array();
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		    );
		$data=array('csrf'=>$csrf);	

		$this->load->view('template/header',$data);
		$this->load->view('dashboard',$data);
		$this->load->view('template/footer',$data);
	}

	public function detail()
	{
		$data=array();
		$this->load->view('template/header_kosong',$data);
		$this->load->view('harian_kosong',$data);
		$this->load->view('template/footer',$data);
	}

}