<section>
<div class="row">
        <div class="col-lg-12 col-12">
          <center>
            <h3>Dashboard Absensi Hari Ini</h3>
            <?php 
            date_default_timezone_set("Asia/Jakarta");
            echo $tanggal = tgl_indo(date('Y-m-d'));
            $tgl = date('Y-m-d');
            ?>
          </center>
          
          <hr>
          <br>

        </div>
        </div>
          


<!-- ================================================================================================== -->
<?php 
if($this->encryption->decrypt($this->session->userdata('id_lokasi')) == '0'){
$where = '';
$whereAnd = '';
}else{
  $where = " WHERE id_lokasi='$idLokasi'";
  $whereAnd = " AND id_lokasi='$idLokasi'";
} ?>
<div class="row">
          <div class="col-md-12">
            <!-- Widget: user widget style 2 -->
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th width="25%">Nama Karyawan</th>
                  <th width="10%">Lokasi</th>
                  <th width="10%">Jadwal</th>
                  <th width="10%">Masuk</th>
                  <th width="10%">Pulang</th>
                  <th width="10%">M Lembur</th>
                  <th width="10%">P Lembur</th>
                  <th width="10%">Keterangan</td>
                </tr>
              </thead>
              <tbody>
                <?php 
                $no = 1;
                $tabelJemKer = 'jam_kerja_'.date('m');
                $kolom = 'tgl_'.(date('d') * 1);
                $query = "SELECT * FROM karyawan k 
                LEFT JOIN lokasi l ON k.id_lokasi=l.id_lokasi 
                LEFT JOIN jabatan jb ON k.id_jabatan = jb.id_jabatan 
                LEFT JOIN $tabelJemKer j ON j.id_karyawan = k.id_karyawan 
                LEFT JOIN jam_kerja jm ON jm.id_jamker = j.$kolom 
                ORDER BY k.id_karyawan ASC";
                $kary = $this->db->query($query)->result();
                foreach($kary as $ky){ 
                echo '<tr>
                  <td>'.$no++.'</td>
                  <td>'.$ky->nama_lengkap.'<br><span class="badge badge-success">'.$ky->nama_jabatan.'</span></td>
                  <td align="center">'.$ky->nama_lokasi.'<br><span class="badge badge-warning">'.$ky->no_reg.'</span></td>
                  <td align="center"><span class="badge badge-info">'.$ky->nama_jamker.'</span><br>'.$ky->senin_m.' - '.$ky->senin_p.'</td>
                  <td align="center">'.absenMasuk_jpc($tgl, $ky->id_karyawan).'</td>
                  <td align="center">'.absenPulang_jpc($tgl, $ky->id_karyawan).'</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>';
                  }
                  ?>
              </tbody>
              
            </table>


            <!-- /.widget-user -->
          </div>
          <!-- /.col -->
          




         

