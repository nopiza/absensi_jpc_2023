<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <!-- <h1 class="m-0 text-dark">Dashboard -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->

      <div class="row">
        <div class="col-lg-12 col-12">

        <div class="row">
        <div class="col-lg-12 col-12">
          Dashboard Presensi Kehadiran<br>
          <a href="<?=base_url('dashboardabsen/detail');?>" target="_blank" class="btn btn-info btn-sm">Detail Data Absen Hari ini</a>
          <hr>
          <br>

        </div>
        </div>
          


  

<!-- ================================================================================================== -->
<?php 
if($this->encryption->decrypt($this->session->userdata('id_lokasi')) == '0'){
$where = '';
$whereAnd = '';
}else{
  $where = " WHERE id_lokasi='$idLokasi'";
  $whereAnd = " AND id_lokasi='$idLokasi'";
} ?>
<div class="row">     


          <div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2 shadow-sm">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-success">
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="./assets/G.jpg" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Statistik Kehadiran</h3>
                <h5 class="widget-user-desc">Lokasi</h5>
              </div>
              
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                <?php 
              $lokasi = $this->db->query("SELECT *, (SELECT COUNT(id_karyawan) FROM karyawan WHERE id_lokasi = lokasi.id_lokasi $whereAnd) as jum FROM lokasi $where")->result();
              foreach($lokasi as $lks){
              ?>
                  <li class="nav-item">
                  <a href="<?=base_url('lokasi/detail/'.$lks->id_lokasi);?>" class="nav-link">
                    <?=$lks->nama_lokasi?> <span class="float-right badge bg-secondary"><?=$lks->jum?></span>
                    </a>
                  </li>
                  
                  <?php } 
                  
                  $query = "SELECT COUNT(id_karyawan) as jj FROM karyawan WHERE id_lokasi = '0'";
                  $Nonlokasi = $this->db->query($query)->row_array();

                  if($this->encryption->decrypt($this->session->userdata('id_lokasi')) == '0'){

                  ?>

                  <li class="nav-item">
                    <a href="<?=base_url('lokasi/detail/0');?>" class="nav-link">
                      Non Lokasi <span class="float-right badge bg-secondary"><?=$Nonlokasi['jj'];?></span>
                    </a>
                  </li>
                  <?php } ?>
                  
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->





        </div>
      </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


