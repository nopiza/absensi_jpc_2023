<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Potongan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'potongan');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Potongan_model','potongan');
		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
	}

	public function detail($id)
	{

      	$user_data['data_ref'] = $this->data_ref;
		$query = "SELECT * FROM potongan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_potongan ='$id'";
      	$user_data['potongan'] = $this->db->query($query)->row_array();
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('detail',$user_data);

	}

	public function ajax_list()
	{
		$list = $this->potongan->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = ' <a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_potongan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_potongan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
			$row[] = tgl_indo($post->tanggal);
			$row[] = $post->nama_lengkap;
			$row[] = $post->nama_jabatan;
			$row[] = rupiah($post->zakat);
			$row[] = rupiah($post->koperasi);
			$row[] = rupiah($post->kasbon);
			$row[] = rupiah($post->kas);
			$row[] = rupiah($post->lain_lain);
			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->potongan->count_all(),
						"recordsFiltered" => $this->potongan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$this->db->join('karyawan', 'potongan.id_karyawan = karyawan.id_karyawan');
		$data = $this->db->get_where('potongan', ['id_potongan' => $id])->row_array();
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'no_reg' 				=> $this->input->post('noreg'),
			'no_ktp' 				=> $this->input->post('no_ktp'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			// 'tempat_lahir' 		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'alamat' 			=> $this->input->post('alamat'),
			// 'no_hp' 			=> $this->input->post('no_hp'),
			// 'agama' 			=> $this->input->post('agama'),
			// 'status_perkawinan' 	=> $this->input->post('status_perkawinan'),
			'no_bpjs_kes' 		=> $this->input->post('no_bpjs_kes'),
			'no_bpjs_tk' 		=> $this->input->post('no_bpjs_tk'),
			'gaji_pokok' 				=> $this->input->post('gaji_pokok'),
			'tunjangan' 	=> $this->input->post('tunjangan'),
			't_kerajinan' 		=> $this->input->post('t_kerajinan'),
			't_meal' 		=> $this->input->post('t_kerajinan'),
			't_transport' 		=> $this->input->post('t_kerajinan'),
			'tanggal_masuk' 		=> $this->input->post('tanggal_masuk'),
			'stt_admin' 			=> $this->input->post('is_admin'),
			'stt_potongan' 			=> $this->input->post('stt_potongan'),
			'username' 			=> $this->input->post('username'),
			'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT)
			// 'range_mcu' 			=> $this->input->post('range_mcu'),
			// 'id_department' 			=> $this->input->post('department'),
			// 'id_jabatan' 			=> $this->input->post('range_mcu'),
			// 'id_lokasi' 			=> $this->input->post('range_mcu')
		);

		$this->potongan->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{

		$param = [
			'tanggal'			=> $this->input->post('tanggal'),
			'zakat'				=> $this->input->post('zakat'),
			'koperasi'			=> $this->input->post('koperasi'),
			'kasbon'			=> $this->input->post('kasbon'),
			'kas'				=> $this->input->post('kas') ,
			'lain_lain'			=> $this->input->post('lain_lain')
		]; 
   
		$this->potongan->update(array('id_potongan' => $this->input->post('id')), $param);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('potongan',array('id_potongan'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function download()
	{
		$data['karyawan'] = $this->db->query("SELECT *, karyawan.id_karyawan as idnya FROM karyawan 
		LEFT JOIN potongan ON potongan.id_karyawan = karyawan.id_karyawan
		LEFT JOIN lokasi ON karyawan.id_lokasi = lokasi.id_lokasi 
		LEFT JOIN jabatan ON karyawan.id_jabatan = jabatan.id_jabatan 
		ORDER BY karyawan.id_karyawan ASC")->result();
		$this->load->view('download_potongan', $data);
	}


	public function updatedata(){
		$user_data['data_ref'] = $this->data_ref;
		$this->load->view('template/header',$user_data);
		$this->load->view('update_data',$user_data);
	}


	public function proses_upload()
	{
		$idKavling = $this->input->post('id');
		$file_mimes = array('application/octet-stream', 
		'application/vnd.ms-excel', 
		'application/x-csv', 
		'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		// echo $_FILES['datakavling']['name'];
 
			if(isset($_FILES['datakavling']['name'])) {
				$arr_file = explode('.', $_FILES['datakavling']['name']);
				$extension = end($arr_file);
				if('csv' == $extension) {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
				} else {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				}
				$spreadsheet = $reader->load($_FILES['datakavling']['tmp_name']);
				// hapus dulu
				$this->db->delete('potongan', "MONTH(tanggal) = '01'");
				$sheetData = $spreadsheet->getActiveSheet()->toArray();
				for($i = 1;$i < count($sheetData);$i++){
					$id_karyawan           	= $sheetData[$i]['1'];
					$tanggal           		= $sheetData[$i]['2'];
					$zakat           		= str_replace('.','', str_replace(',','', $sheetData[$i]['6']));
					$koperasi          		= str_replace('.','', str_replace(',','', $sheetData[$i]['7']));
					$kasbon        			= str_replace('.','', str_replace(',','', $sheetData[$i]['8']));
					$kas      				= str_replace('.','', str_replace(',','', $sheetData[$i]['9']));
					$lain_lain      		= str_replace('.','', str_replace(',','', $sheetData[$i]['10']));
			
					$param = [
						'id_karyawan'		=> $id_karyawan,
						'tanggal'			=> $tanggal,
						'zakat'				=> $zakat,
						'koperasi'			=> $koperasi,
						'kasbon'			=> $kasbon,
						'kas'				=> $kas ,
						'lain_lain'			=> $lain_lain
					];
					// cek data karyawan di potongan

					$cek = $this->db->query("SELECT * FROM potongan WHERE id_karyawan='$id_karyawan'")->num_rows();
					if($cek){
						$this->db->update('potongan', $param, ['id_karyawan' => $id_karyawan]);
					}else{
						$this->db->insert('potongan', $param);
					}
					

				}
				// header("Location: index.php"); 
			}else{
				echo 'Tidak Proses';
			}
		redirect('potongan');
	}


}
