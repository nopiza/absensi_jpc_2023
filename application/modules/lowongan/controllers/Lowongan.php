<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowongan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'lowongan');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Lowongan_model','lowongan');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->lowongan->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_lowongan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_lowongan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = tgl_indo($post->tanggal_mulai).'<br>'.tgl_indo($post->tanggal_selesai);
         	$row[] = $post->judul_lowongan;
         	$row[] = $post->nama_lokasi;
         	$row[] = $post->jumlah_kebutuhan;

			if($post->stt_lowongan == '0'){
				$row[] = '<span class="badge badge-danger">Pending</span>';
			}else if($post->stt_lowongan == '1'){
				$row[] = '<span class="badge badge-info">Publish</span>';
			}else if($post->stt_lowongan == '2'){
				$row[] = '<span class="badge badge-warning">Proses Seleksi</span>';
			}else if($post->stt_lowongan == '3'){
				$row[] = '<span class="badge badge-success">Selesai</span>';
			}
         	

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->lowongan->count_all(),
						"recordsFiltered" => $this->lowongan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->lowongan->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'tanggal_mulai' 		=> $this->input->post('tgl_mulai'),
			'tanggal_selesai' 		=> $this->input->post('tgl_selesai'),
			'judul_lowongan' 		=> $this->input->post('judul'),
			'deskripsi' 			=> $this->input->post('deskripsi'),
			'lokasi_penempatan' 	=> $this->input->post('lokasi'),
			'jumlah_kebutuhan' 		=> $this->input->post('jumlah_kebutuhan'),
			'umur_min' 				=> $this->input->post('umur_min'),
			'umur_max' 				=> $this->input->post('umur_max')
		);
		$this->lowongan->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'tanggal_mulai' 		=> $this->input->post('tgl_mulai'),
			'tanggal_selesai' 		=> $this->input->post('tgl_selesai'),
			'judul_lowongan' 		=> $this->input->post('judul'),
			'deskripsi' 			=> $this->input->post('deskripsi'),
			'lokasi_penempatan' 	=> $this->input->post('lokasi'),
			'jumlah_kebutuhan' 		=> $this->input->post('jumlah_kebutuhan'),
			'umur_min' 				=> $this->input->post('umur_min'),
			'umur_max' 				=> $this->input->post('umur_max'),
			'stt_lowongan' 			=> $this->input->post('stt_lowongan')
		);
		$this->lowongan->update(array('id_lowongan' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('lowongan',array('id_lowongan'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	
}
