<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=data_potongan.xls");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
<table border="1">
    <tr>
        <td>No</td>
        <td>Id Karyawan</td>
        <td>Tanggal</td>
        <td>Nama Lengkap</td>
        <td>Lokasi</td>
        <td>Jabatan</td>
        <td>Zakat</td>
        <td>Koperasi</td>
        <td>Kasbon</td>
        <td>Kas</td>
        <td>Lain-lain</td>
    </tr>
    <?php 
    $no = 1;
    foreach($karyawan as $ky){
        echo '<tr>
            <td>'.$no++.'</td>
            <td>'.$ky->idnya.'</td>
            <td>'.$ky->tanggal.'</td>
            <td>'.$ky->nama_lengkap.'</td>
            <td>'.$ky->nama_lokasi.'</td>
            <td>'.$ky->nama_jabatan.'</td>
            <td align="right">'.rupiah($ky->zakat).'</td>
            <td align="right">'.rupiah($ky->koperasi).'</td>
            <td align="right">'.rupiah($ky->kasbon).'</td>
            <td align="right">'.rupiah($ky->kas).'</td>
            <td align="right">'.rupiah($ky->lain_lain).'</td>
        </tr>';
    }

    ?>
</table>