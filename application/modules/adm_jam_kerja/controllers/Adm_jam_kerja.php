<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_jam_kerja extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'adm_laporan');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Adm_laporan_model','laporan');
		// $this->load->model('Group/Group_model','group');
		// check_login_opd();
	}

	public function index()
	{
		$user_data['data_ref'] = $this->data_ref;
		$user_data['pegawai'] = $this->db->get('karyawan')->result();
     	
     	$this->load->view('template/header-kosong');
		$this->load->view('view',$user_data);

	}


    public function get($nip){

        $item=$this->db->query("SELECT * FROM pegawai WHERE nip ='$nip' ")->row_array();

        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


}
