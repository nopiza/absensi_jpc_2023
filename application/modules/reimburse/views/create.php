  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Reimburse</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Reimburse</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Form Reimburse</h3>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

        <section id="services" class="services">



<div class="row">
          <div class="col-lg-8 col-md-8 kotak">
                <form action="<?=base_url('reimburse/proses');?>" method="POST" id="form" class="form-horizontal" >

                        <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal Pengajuan</label>
                            <div class="col-md-6">
                                <input name="tanggal_pengajuan" value="<?=$tanggal_pengajuan;?>" class="form-control" type="date" id="tanggal_pengajuan">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal Kwitansi</label>
                            <div class="col-md-6">
                                <input name="tanggal_kwitansi" value="<?=$tanggal_kwitansi;?>" class="form-control" type="date" id="tanggal_kwitansi">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Nominal Kwitansi</label>
                            <div class="col-md-6">
                                <input name="nominal" value="<?=$nominal;?>" class="form-control" type="text" id="nominal">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Keterangan</label>
                            <div class="col-md-6">
                                <input name="keterangan" value="<?=$keterangan;?>" class="form-control" type="text" id="keterangan">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div id="form_upload">
                        <div class="form-group row">
                            <label class="control-label col-md-2">Lampiran 1</label>
                            <div class="col-md-9">
                              <?php
                              $foto = $this->db->query("SELECT * FROM reimburse WHERE id_reimburse='$id_reimburse'")->row_array();
                              if(@$foto['lampiran_1'] != '' ){
                                echo '<span class="btn btn-success btn-sm">data terkirim</span> 
                                <a href="'.base_url('reimburse/hapuslampiran/'.$id_reimburse.'/1').'" class="btn btn-danger btn-sm" onclick="return confirm(\'Yakin ingin menghapus lampiran?\')"><i class="fas fa-trash"></i></a>';
                              }else{
                                echo '<input name="foto_1" id="foto_1" onchange="dua()" type="file">';
                              }
                              ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Lampiran 2</label>
                            <div class="col-md-9">
                              <?php
                             if(@$foto['lampiran_2'] != '' ){
                              echo '<span class="btn btn-success btn-sm">data terkirim</span> 
                              <a href="'.base_url('reimburse/hapuslampiran/'.$id_reimburse.'/2').'" class="btn btn-danger btn-sm" onclick="return confirm(\'Yakin ingin menghapus lampiran?\')"><i class="fas fa-trash"></i></a>';
                              }else{
                                echo '<input name="foto_2" id="foto_2" onchange="dua()" type="file">';
                              }
                              ?>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Lampiran 3</label>
                            <div class="col-md-9">
                            <?php
                                if(@$foto['lampiran_3'] != '' ){
                                  echo '<span class="btn btn-success btn-sm">data terkirim</span> 
                                  <a href="'.base_url('reimburse/hapuslampiran/'.$id_reimburse.'/3').'" class="btn btn-danger btn-sm" onclick="return confirm(\'Yakin ingin menghapus lampiran?\')"><i class="fas fa-trash"></i></a>';
                                }else{
                                    echo '<input name="foto_3" id="foto_3" onchange="dua()" type="file">';
                                }
                            ?>
                            </div>
                        </div>


                    <div align="left">
                            <?php
                            if(@$foto['lampiran_1'] == '' AND @$foto['lampiran_2'] == '' AND @$foto['lampiran_3'] == ''){ 
                            ?>
                                <a href="#" class="btn btn-danger btn-md" >Silahkan melengkapi data terlebih dahulu</a>
                            <?php }else{ ?>
                                <!-- <button type="submit" class="btn btn-success btn-md">Kirim Reimburse</button> -->
                                <a href="<?=base_url('reimburse/simpan/'.$id_reimburse);?>" class="btn btn-primary btn-md">Kirim Reimburse</a>
                                <a href="<?=base_url('reimburse/batalkan/'.$id_reimburse);?>" class="btn btn-danger btn-md" onclick="return confirm('Anda akan membatalkan proses?')">Batalkan Progres</a>
                                
                            <?php } ?>
                            </div>
                        </div>

                        </div>
</div>

                </form>

                


          </div>

        </div>


          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>



</body>
</html>


<?php  $this->load->view('template/footer'); ?>


<link rel="stylesheet" href="<?=base_url('assets/loading/css/jquery.loadingModal.css');?>">
<!-- <script src="http://code.jquery.com/jquery-3.1.1.slim.min.js"></script> -->
<script src="<?=base_url('assets/loading/js/jquery.loadingModal.js');?>"></script>


<script src="<?php echo base_url('assets/admin/plugins/select2/select2.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2-bootstrap.css') ?>">



<script type="text/javascript">
var idProses = <?=$id_reimburse?>;
var sttProses = <?=$sttProses?>;

// $(document).ready(function() {
//         alert("I am an alert box!");
//     });

// $(document).ready(function() {
//     // alert('xaaaxx');
//     if(sttProses == '0'){
//         $('#form_upload').hide();     
//         // alert('xxx');
//     }else if(sttProses == '1'){
//         $('#form_upload').show();     
//         // $('#kodeKavling').select2('data', {id: idKavling, text: ''});    
//         // alert('as');
//     }else{
//         alert('Cek : ' + idProses);
//     }
// });
 

function dua()
   {
    var idProgres = <?=$id_reimburse?>;
        $('body').loadingModal({text: 'Tunggu sesaat, <br> Sedang mengirim data...'});
       var url = "<?php echo base_url('reimburse/ajax_add/')?>";
       // ajax adding data to database
       var formData = new FormData($('#form')[0]);
       $.ajax({
            url : url + idProgres,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                //    alert('Berhasil');
                   location.reload(); 
                   $('body').loadingModal('hide');
                    $('body').loadingModal('destroy');
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
            //   alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }


  
   var url_apps = "<?=base_url();?>"


$('#tanggal_kwitansi').on('blur', function() {
        // $('#form_upload').show();      
        
        // var tanggal_pengajuan = $(this).val();
        var idProgres = <?=$id_reimburse?>;
        $.ajax({
            url: url_apps + 'reimburse/tanggal_kwitansi/' + $(this).val() +'/'+ idProgres,
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
          location.reload(); 
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });


    $('#nominal').on('blur', function() {
        // $('#form_upload').show();      
        
        // var tanggal_pengajuan = $(this).val();
        var idProgres = <?=$id_reimburse?>;
        $.ajax({
            url: url_apps + 'reimburse/nominal/' + $(this).val() +'/'+ idProgres,
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
          location.reload(); 
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });


    $('#keterangan').on('blur', function() {
        // $('#form_upload').show();      
        
        // var tanggal_pengajuan = $(this).val();
        var idProgres = <?=$id_reimburse?>;
        $.ajax({
            url: url_apps + 'reimburse/keterangan/' + $(this).val() +'/'+ idProgres,
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
          location.reload(); 
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });


</script>