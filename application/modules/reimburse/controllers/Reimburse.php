<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reimburse extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'reimburse');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Reimburse_model','reimburse');
		// $this->load->model('Group/Group_model','group');

		// check_login();
	}

	public function index()
	{
    	$user_data['data_ref'] = $this->data_ref;
	    $this->load->view('template/header_marketing',$user_data);
		$this->load->view('view',$user_data);
	}

	public function input()
	{
		$user_data['data_ref'] = $this->data_ref;
		// $idUser = $this->encryption->decrypt($this->session->userdata('id'));
		// $user_data['id_user'] = $idUser;
		//cek apakah ada row yang belum selesai upload
		$idUser = $this->encryption->decrypt($this->session->userdata('id_karyawan'));
		$query = "SELECT * FROM reimburse 
		LEFT JOIN karyawan ON reimburse.id_karyawan = karyawan.id_karyawan 
		WHERE karyawan.id_karyawan = '$idUser' AND stt_upload < '2'";
		$cek = $this->db->query($query)->num_rows();
		if($cek){
			// Sudah ada data ------------>
			$data = $this->db->query($query)->row_array();
			if($data['id_karyawan'] == NULL){
				$user_data['id_karyawan'] = '0';
			}else{
				$user_data['id_karyawan'] = $data['id_karyawan'];
				$user_data['tanggal_pengajuan'] = $data['tanggal_pengajuan'];
				$user_data['tanggal_kwitansi'] = $data['tanggal_kwitansi'];
				$user_data['nominal'] = $data['nominal'];
				$user_data['keterangan'] = $data['keterangan'];
			}
			
			// $user_data['kodeKavling'] = $data['kode_kavling'];
			$user_data['id_reimburse'] = $data['id_reimburse'];
			$user_data['sttProses'] = $data['stt_upload'];
		}else{
			
			// buat data row baru untuk menampung foto yang diupload
			$param = [
				'tanggal_pengajuan'		=> date('Y-m-d'),
				'id_karyawan'		=> $idUser
			];
			$this->db->insert('reimburse', $param);
			redirect('reimburse/input');
		}
    	
	    $this->load->view('template/header_marketing',$user_data);
		$this->load->view('create',$user_data);
	}

	public function ajax_list()
	{

		$list = $this->reimburse->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$no++;
			$row = array();
         	$row[] = tgl_indo($post->tanggal_pengajuan);	
			$row[] = rupiah($post->nominal);	
			if($post->stt_upload <= 2){
				$row[] = '<span class="badge badge-pill badge-warning">Pengajuan</span>';	
			}else if($post->stt_upload == 3){
				$row[] = '<span class="badge badge-pill badge-success">Disetujui</span>';	
			}else if($post->stt_upload == 4){
				$row[] = '<span class="badge badge-pill badge-primary">Pengajuan</span>';	
			}

			$ft = '';
			if($post->lampiran_1 <> ''){
				$ft .= '<a href="'.base_url('./lampiran_reimburse/'.$post->lampiran_1).'" target="_blank" class="btn btn-xs btn-info">Lampiran 1</a> ';
			}else{
				$ft .= '';
			}	

			if($post->lampiran_2 <> ''){
				$ft .= ' <a href="'.base_url('./lampiran_reimburse/'.$post->lampiran_2).'" target="_blank" class="btn btn-xs btn-info">Lampiran 2</a> ';
			}else{
				$ft .= '';
			}	

			if($post->lampiran_3 <> ''){
				$ft .= ' <a href="'.base_url('./lampiran_reimburse/'.$post->lampiran_3).'" target="_blank" class="btn btn-xs btn-info">Lampiran 3</a> ';
			}else{
				$ft .= '';
			}	

			$row[] = $ft;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->reimburse->count_all(),
						"recordsFiltered" => $this->reimburse->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}



	function upload($token="")
    {
        //cek apakah token valid
        $cektoken = $this->db->query("SELECT * FROM formulir WHERE token='$token'")->num_rows();
        if($cektoken > 0){
            //cek apakah sudah upload 6 berkas
            // $cekBerkas = $this->db->query("SELECT * FROM lampiran WHERE token='$token'")->num_rows();
            $cekStatus = $this->db->query("SELECT * FROM formulir WHERE token='$token'")->row_array();

			$cekStatus          = $this->db->query("SELECT * FROM formulir WHERE token='$token'")->row_array();
			$data['pendaftar']  = $cekStatus['nama_lengkap'];
			$data['noregist']   = $cekStatus['no_registrasi'];
			$data['token']      = $token;
			$data['title']      = "Form Registrasi";
			$this->load->view('registrasi/upload', $data);

        }else{
            redirect('home');
        }
        
    }


    public function ajax_add($idreimburse)
    {

        if(!empty($_FILES['foto_1']['name']))
        {
            $upload = $this->_do_upload_1();
            $data['nama_file'] = $upload;
            $param = array(
                'lampiran_1'         => $_FILES['foto_1']['name']
            );

            $this->db->update('reimburse', $param, ['id_reimburse' => $idreimburse]);
			echo json_encode(array("status" => TRUE));
        }

		if(!empty($_FILES['foto_2']['name']))
        {
            $upload = $this->_do_upload_2();
            $data['nama_file'] = $upload;
            $param = array(
                'lampiran_2'         => $_FILES['foto_2']['name']
            );

            $this->db->update('reimburse', $param, ['id_reimburse' => $idreimburse]);
			echo json_encode(array("status" => TRUE));
        }

		if(!empty($_FILES['foto_3']['name']))
        {
            $upload = $this->_do_upload_3();
            $data['nama_file'] = $upload;
            $param = array(
                'lampiran_3'         => $_FILES['foto_3']['name']
            );

            $this->db->update('reimburse', $param, ['id_reimburse' => $idreimburse]);
			echo json_encode(array("status" => TRUE));
        }
        
    }


	private function _do_upload_1(){

		$config['upload_path']          = './lampiran_reimburse/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']             = 0; //set max size allowed in Kilobyte
		$config['max_width']            = 0; // set max width image allowed
		$config['max_height']           = 0; // set max height allowed
		// $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
   
		  $this->load->library('upload', $config);
		  $this->upload->initialize($config);
		  if(!$this->upload->do_upload('foto_1')) //upload and validate
		  {
			  $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			  $data['status']         = FALSE;
			  echo json_encode($data);
			  exit();
		  }
		  return $this->upload->data('file_name');
	 }


	 private function _do_upload_2(){

		$config['upload_path']          = './lampiran_reimburse/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']             = 0; //set max size allowed in Kilobyte
		$config['max_width']            = 0; // set max width image allowed
		$config['max_height']           = 0; // set max height allowed
		// $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
   
		  $this->load->library('upload', $config);
		  $this->upload->initialize($config);
		  if(!$this->upload->do_upload('foto_2')) //upload and validate
		  {
			  $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			  $data['status']         = FALSE;
			  echo json_encode($data);
			  exit();
		  }
		  return $this->upload->data('file_name');
	 }

	 private function _do_upload_3(){

		$config['upload_path']          = './lampiran_reimburse/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']             = 0; //set max size allowed in Kilobyte
		$config['max_width']            = 0; // set max width image allowed
		$config['max_height']           = 0; // set max height allowed
		// $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
   
		  $this->load->library('upload', $config);
		  $this->upload->initialize($config);
		  if(!$this->upload->do_upload('foto_3')) //upload and validate
		  {
			  $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			  $data['status']         = FALSE;
			  echo json_encode($data);
			  exit();
		  }
		  return $this->upload->data('file_name');
	 }



	public function ajax_delete($id)
	{
		$this->db->delete('lokasi_kavling',array('id_lokasi'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function hapuslampiran($id, $nolampiran){
		$this->db->update('reimburse',['lampiran_'.$nolampiran=> ''], array('id_reimburse'=>$id));
		// echo json_encode(array("status" => TRUE));
		redirect('reimburse/input');
	}


	// public function ajax_select(){
    //     $this->db->select('id_kavling,kode_kavling');
    //     $this->db->like('kode_kavling',$this->input->get('q'),'both');
    //     $this->db->limit(20);
    //     $items=$this->db->get('kavling_peta')->result_array();
    //     //output to json format
    //     echo json_encode($items);
    // }

	// public function isi_id($idKavling, $idreimburse){
    //     $this->db->update('reimburse' ,['id_kavling' => $idKavling, 'stt_upload' => '1'], ['id_reimburse' => $idreimburse]);
    //     echo json_encode(array("status" => TRUE));
    // }

	public function simpan($idreimburse){
        $this->db->update('reimburse' ,['stt_upload' => '2'], ['id_reimburse' => $idreimburse]);
        redirect('reimburse');
    }

	public function batalkan($idreimburse){
        $this->db->delete('reimburse', ['id_reimburse' => $idreimburse]);
        redirect('reimburse');
    }


	public function tanggal_kwitansi($tgl, $idRem){
        $this->db->update('reimburse' ,['tanggal_kwitansi' => $tgl], ['id_reimburse' => $idRem]);
        redirect('reimburse/keterangan');
    }

	public function nominal($nominal, $idRem){
        $this->db->update('reimburse' ,['nominal' => $nominal], ['id_reimburse' => $idRem]);
        redirect('reimburse/keterangan');
    }

	public function keterangan($keterangan, $idRem){
        $this->db->update('reimburse' ,['keterangan' => $keterangan], ['id_reimburse' => $idRem]);
        redirect('reimburse/keterangan');
    }


}
