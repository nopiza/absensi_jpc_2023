<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_regu_anggota_model extends CI_Model {

	var $table = 'regu_anggota';
	var $column_order = array('nama_regu','stt_regu',null); //set column field database for datatable orderable
	var $column_search = array('nama_regu','stt_regu'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('id_anggota' => 'asc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($id)
	{
		// $id = '2';
		// $id = $this->uri->segment(3);
		$this->db->from($this->table);
		$this->db->join('regu', 'regu.id_regu = regu_anggota.id_regu','left');
		$this->db->join('karyawan', 'regu_anggota.id_karyawan = karyawan.id_karyawan','left');
		$this->db->where('regu_anggota.id_regu', $id);


		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($id)
	{
		$this->_get_datatables_query($id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);

		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($id)
	{
		$this->_get_datatables_query($id);

		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($id)
	{

		$this->db->from($this->table);
		$this->db->join('regu', 'regu.id_regu = regu_anggota.id_regu','left');
		$this->db->join('karyawan', 'regu_anggota.id_karyawan = karyawan.id_karyawan','left');
		$this->db->where('regu_anggota.id_regu', $id);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id_anggota  ',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id_anggota', $id);
		$this->db->delete($this->table);
	}

}
