<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_regu_anggota extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'adm_regu_anggota');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Adm_regu_anggota_model','regu_anggota');

	}

	public function index($idRegu)
	{
		$user_data['data_ref'] = $this->data_ref;
		$user_data['id_regu'] = $idRegu;

     	$this->load->view('template/header');
		$this->load->view('view',$user_data);

	}

	public function ajax_list($id)
	{
		// $id = $this->uri->segment(3);
		$list = $this->regu_anggota->get_datatables($id);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_anggota."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
				
			$no++;
			$row = array();
         	$row[] = $no;
			$row[] = $post->nama_regu;
         	$row[] = $post->nama_lengkap;
			$row[] = $link_hapus;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->regu_anggota->count_all($id),
						"recordsFiltered" => $this->regu_anggota->count_filtered($id),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_add()
	{
		$data = array(
			'id_regu' 			=> $this->input->post('id_regu'),
			'id_karyawan' 		=> $this->input->post('id_karyawan'),
			'no_reg' 				=> $this->input->post('no_reg')
		);
		
		$this->regu_anggota->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->regu_anggota->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}
