<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'registrasi');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Registrasi_model','registrasi');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->registrasi->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_reg."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_reg."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap.'<br><b>NIK : </b>'.$post->no_ktp;
         	$row[] = $post->tempat_lahir;
         	$row[] = tgl_indo($post->tanggal_lahir);
         	$row[] = $post->no_wa;
         	$row[] = $post->judul_lowongan;

         	

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->registrasi->count_all(),
						"recordsFiltered" => $this->registrasi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->registrasi->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'tanggal_mulai' 		=> $this->input->post('tgl_mulai'),
			'tanggal_selesai' 		=> $this->input->post('tgl_selesai'),
			'judul_registrasi' 		=> $this->input->post('judul'),
			'deskripsi' 			=> $this->input->post('deskripsi'),
			'lokasi_penempatan' 	=> $this->input->post('lokasi'),
			'jumlah_kebutuhan' 		=> $this->input->post('jumlah_kebutuhan'),
			'umur_min' 				=> $this->input->post('umur_min'),
			'umur_max' 				=> $this->input->post('umur_max')
		);
		$this->registrasi->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'tanggal_mulai' 		=> $this->input->post('tgl_mulai'),
			'tanggal_selesai' 		=> $this->input->post('tgl_selesai'),
			'judul_registrasi' 		=> $this->input->post('judul'),
			'deskripsi' 			=> $this->input->post('deskripsi'),
			'lokasi_penempatan' 	=> $this->input->post('lokasi'),
			'jumlah_kebutuhan' 		=> $this->input->post('jumlah_kebutuhan'),
			'umur_min' 				=> $this->input->post('umur_min'),
			'umur_max' 				=> $this->input->post('umur_max'),
			'stt_registrasi' 			=> $this->input->post('stt_registrasi')
		);
		$this->registrasi->update(array('id_registrasi' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('registrasi',array('id_reg'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	
}
