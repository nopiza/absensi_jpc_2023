<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Izin extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'izin');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kordinator_model','kordinator');
		check_login_karyawan();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;    	
      	$this->load->view('template/header_marketing',$user_data);
		$this->load->view('izin',$user_data);
	}

	public function kordinator()
	{
      	$user_data['data_ref'] = $this->data_ref;    	
      	$this->load->view('template/header_marketing',$user_data);
		$this->load->view('view_kordinator',$user_data);
	}

	public function pengajuan()
	{
		$user_data['data_ref'] = $this->data_ref;    	
		$this->load->view('template/header_marketing',$user_data);
	  	$this->load->view('form-izin',$user_data);
	}

	public function verifikasi($id)
	{
		$user_data['data_ref'] = $this->data_ref;    	
		$user_data['ijin'] = $this->db->query("SELECT * FROM pengajuan_izin 
		LEFT JOIN karyawan ON pengajuan_izin.id_karyawan = karyawan.id_karyawan 
		WHERE id_izin='$id'")->row_array();    	
		$this->load->view('template/header_marketing',$user_data);
	  	$this->load->view('form-izin-verif',$user_data);
	}

	public function ajax_list_kordinator()
	{

		$list = $this->kordinator->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_setujui = ' <a class="btn btn-xs btn-info" href="'.base_url('izin/verifikasi/'.$post->id_izin).'")">Detail</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->jenis_izin;

			if($post->status_izin == '0'){
				$row[] = '<span class="badge badge-warning">Menunggu Persetujuan</span>';
			}else if($post->status_izin == '1'){
				$row[] = '<span class="badge badge-success">Disetujui Atasan</span>';
			}else if($post->status_izin == '2'){
				$row[] = '<span class="badge badge-danger">Ditolah Kordinator</span>';
			}else if($post->status_izin == '2'){
				$row[] = '<span class="badge badge-info">Disetujui HRD</span>';
			}
         	

			//add html for action
			$row[] = $link_setujui;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->kordinator->count_all(),
						"recordsFiltered" => $this->kordinator->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function simpan()
	{

		$data = array(
			'id_karyawan' 		=> $this->input->post('id_karyawan'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'jenis_izin' 			=> $this->input->post('jenis'),
			'deskripsi' 			=> $this->input->post('deskripsi'),
			'status_izin' 			=> '0'
		);

		$this->db->insert('pengajuan_izin',$data);
		redirect('izin');
	}


	public function simpan_verifikasi()
	{
		$param = array(
			'catatan_kordinator' 		=> $this->input->post('catatan'),
			'status_izin' 		=> $this->input->post('hasil_verifikasi')
		);
		$this->db->update('pengajuan_izin',$param, ['id_izin' => $this->input->post('id_izin')]);
		redirect('izin/kordinator');
	}




}
