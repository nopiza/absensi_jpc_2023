  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pengajuan Izin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboardkaryawan'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('izin'); ?>">Izin</a></li>
              <li class="breadcrumb-item active">Pengajuan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-body">

        <form action="<?=base_url('izin/simpan');?>" id="form" class="form-horizontal" method="POST">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

      <?php 
      $ID = $this->encryption->decrypt($this->session->userdata('id_karyawan'));
      $karyawan = $this->db->query("SELECT * FROM karyawan WHERE id_karyawan='$ID'")->row_array();
      ?>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Karyawan</label>
                            <div class="col-md-6">
                                <input name="nama_lengkap" value="<?=$karyawan['nama_lengkap'];?>" class="form-control" type="text" readonly>
                                <input name="id_karyawan" value="<?=$karyawan['id_karyawan'];?>" class="form-control" type="hidden">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. REG</label>
                            <div class="col-md-6">
                                <input name="no_reg"  value="<?=$karyawan['no_reg'];?>"  class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-6">
                                <input name="tanggal" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Durasi Izin</label>
                            <div class="col-md-2">
                                <input name="durasi" placeholder="" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jenis Izin</label>
                            <div class="col-md-6">
                                <select name="jenis" placeholder="" class="form-control">
                                    <option value="Cuti">Cuti</option>
                                    <option value="Sakit">Sakit</option>
                                    <option value="Izin">Izin</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Deskripsi Izin</label>
                            <div class="col-md-6">
                                <textarea name="deskripsi" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-6">
                                <button name="btn" class="btn btn-primary btn-sm" type="submit">Kirim Pengajuan</button>
                                <span class="help-block"></span>
                            </div>
                        </div>


                    </div>
                </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
               
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
</body>
</html>


<?php  $this->load->view('template/footer'); ?>