  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Verifikasi Izin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboardkaryawan'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('izin'); ?>">Izin</a></li>
              <li class="breadcrumb-item active">Verifikasi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-body">

        <form action="<?=base_url('izin/simpan_verifikasi');?>" id="form" class="form-horizontal" method="POST">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Karyawan</label>
                            <div class="col-md-6">
                                <input name="nama_lengkap" value="<?=$ijin['nama_lengkap'];?>" class="form-control" type="text" readonly>
                                <input name="id_karyawan" value="<?=$ijin['id_karyawan'];?>" class="form-control" type="hidden">
                                <input name="id_izin" value="<?=$ijin['id_izin'];?>" class="form-control" type="hidden">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. REG</label>
                            <div class="col-md-6">
                                <input name="no_reg"  value="<?=$ijin['no_reg'];?>"  class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-6">
                            <input name="nama_lengkap" value="<?=$ijin['tanggal'];?>" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label class="control-label col-md-3">Durasi Izin</label>
                            <div class="col-md-2">
                            <input name="nama_lengkap" value="<?=$ijin['durasi'];?>" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jenis Izin</label>
                            <div class="col-md-6">
                            <input name="nama_lengkap" value="<?=$ijin['jenis_izin'];?>" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Deskripsi Izin</label>
                            <div class="col-md-6">
                                <textarea name="deskripsi" class="form-control" readonly><?=$ijin['deskripsi'];?></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Persetujuan</label>
                            <div class="col-md-6">
                                <select name="hasil_verifikasi" placeholder="" class="form-control">
                                    <option value="0">Pending</option>
                                    <option value="1">Disetujui</option>
                                    <option value="2">Ditolak</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Catatan Kordinator</label>
                            <div class="col-md-6">
                                <textarea name="catatan" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-6">
                                <button name="btn" class="btn btn-primary btn-sm" type="submit">Proses Verifikasi</button>
                                <span class="help-block"></span>
                            </div>
                        </div>



                    </div>
                </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
               
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
</body>
</html>


<?php  $this->load->view('template/footer'); ?>