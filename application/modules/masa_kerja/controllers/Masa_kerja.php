<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masa_kerja extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'masa_kerja');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Masa_kerja_model','masa_kerja');
		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
	}


	public function ajax_list_masa_kerja()
	{
		$list = $this->masa_kerja->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = ' <a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit Tanggal Kerja</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
			if($post->foto == ''){
				if($post->jenis_kelamin == 'Laki-laki'){
					$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/pria.jpg').'"><br>'.$post->no_reg;
				}else if($post->jenis_kelamin == 'Perempuan'){
					$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/wanita.jpg').'"><br>'.$post->no_reg;
				}
			}else{
				$row[] = '<img class="direct-chat-img" src="'.base_url('/assets/images/'.$post->foto).'"><br>'.$post->no_reg;
			}
			 
			 if($post->jenis_kelamin == 'Perempuan'){
				$jk = '<span class="badge badge-pill badge-warning">Perempuan</span>';
			 }else{
				$jk = '<span class="badge badge-pill badge-info">Laki-laki</span>';
			 }
         	$row[] = $post->nama_lengkap.'<br>'.$jk;

			$row[] = $post->nama_jabatan.'<br><span class="badge badge-pill badge-success">'.$post->golongan.'</span> ';
			$row[] = tgl_indo($post->tanggal_mulai_kerja);

			// menghitung masa kerja
			$diff  = date_diff( date_create($post->tanggal_mulai_kerja), date_create() );
			$a = $diff->format('%a'); 
			$tahun = floor($a / 365);
			$hari = $a % 365;

			$row[] = $tahun.' Tahun, '.$hari.' hari';

			//add html for action
			$row[] = $link_edit;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->masa_kerja->count_all(),
						"recordsFiltered" => $this->masa_kerja->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_update_masa_kerja()
	{
		$data = array(
			'nama_lengkap' 				=> $this->input->post('nama_lengkap'),
			'no_reg' 					=> $this->input->post('noreg'),
			'tanggal_mulai_kerja' 		=> $this->input->post('tanggal_masuk')
		);

		$this->masa_kerja->update(array('id_karyawan' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{
		$this->db->delete('masa_kerja',array('id_masa_kerja'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function download()
	{
		$data['masa_kerja_pkwt'] = $this->db->query("SELECT * FROM masa_kerja 
		LEFT JOIN lokasi ON masa_kerja.id_lokasi = lokasi.id_lokasi 
		LEFT JOIN jabatan ON masa_kerja.id_jabatan = jabatan.id_jabatan 
		WHERE sistem_penggajian = 'PWT | PWTT'")->result();
		$this->load->view('download_pkwt', $data);
	}


	public function updatedata($id){
		$user_data['jenis'] = $id;     	
		$this->load->view('template/header',$user_data);
		$this->load->view('update_data',$user_data);
	}


	public function proses_upload()
	{
		$idKavling = $this->input->post('id');
		$file_mimes = array('application/octet-stream', 
		'application/vnd.ms-excel', 
		'application/x-csv', 
		'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		// echo $_FILES['datakavling']['name'];
 
			if(isset($_FILES['datakavling']['name'])) {
				$arr_file = explode('.', $_FILES['datakavling']['name']);
				$extension = end($arr_file);
				if('csv' == $extension) {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
				} else {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				}
				$spreadsheet = $reader->load($_FILES['datakavling']['tmp_name']);
				
				$sheetData = $spreadsheet->getActiveSheet()->toArray();
				for($i = 1;$i < count($sheetData);$i++){
					$id_masa_kerja           	= $sheetData[$i]['1'];
					$gaji_pokok           	= str_replace('.','', str_replace(',','', $sheetData[$i]['6']));
					$tunjangan          	= str_replace('.','', str_replace(',','', $sheetData[$i]['7']));
					$t_kerajinan        	= str_replace('.','', str_replace(',','', $sheetData[$i]['8']));
					$t_meal      			= str_replace('.','', str_replace(',','', $sheetData[$i]['9']));
					$t_transport      		= str_replace('.','', str_replace(',','', $sheetData[$i]['10']));
					$t_hp        			= str_replace('.','', str_replace(',','', $sheetData[$i]['11']));
					$t_perumahan        	= str_replace('.','', str_replace(',','', $sheetData[$i]['12']));
					$t_kehadiran        	= str_replace('.','', str_replace(',','', $sheetData[$i]['13']));
					$uang_saku        		= str_replace('.','', str_replace(',','', $sheetData[$i]['14']));
					$cuti        		= str_replace('.','', str_replace(',','', $sheetData[$i]['15']));
			
					$param = [
						'gaji_pokok'			=> $gaji_pokok,
						'tunjangan'				=> $tunjangan,
						't_kerajinan'			=> $t_kerajinan,
						't_meal'				=> $t_meal,
						't_transport'			=> $t_transport ,
						't_hp'					=> $t_hp,
						't_perumahan'			=> $t_perumahan,
						't_kehadiran'			=> $t_kehadiran,
						't_uang_saku'			=> $uang_saku,
						'cuti_tahunan'			=> $cuti
					];
					$this->db->update('masa_kerja', $param, ['id_masa_kerja' => $id_masa_kerja]);

				}
				// header("Location: index.php"); 
			}else{
				echo 'Tidak Proses';
			}
		redirect('masa_kerja');
	}


	public function ajax_edit($id)
	{
		$data = $this->masa_kerja->get_by_id($id);
		echo json_encode($data);
	}


}
