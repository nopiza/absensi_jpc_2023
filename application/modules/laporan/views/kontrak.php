    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Laporan Kontrak Yang Akan Habis Pada Periode : <br><?=tgl_indo($awal);?> sampai <?=tgl_indo($akhir);?></h3>
              <div class="card-tools">
                <!-- <a href="<?=base_url('laporan/cetak_pdf_kontrak');?>" target="_blank" class="btn btn-info btn-sm" ><i class="fa fa-plus"></i> Cetak PDF</a>&nbsp; -->
                <a href="<?=base_url('laporan/cetak_excel_kontrak/'.$awal.'/'.$akhir.'/'.$lokasi);?>" target="_blank"  class="btn btn-warning btn-sm" ><i class="fa fa-plus"></i> Cetak Excel</a>&nbsp;
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="10%">NIK</th>
                        <th width="16%">Nama Karyawan</th>
                        <th width="14%">Tanggal Mulai Kerja</th>
                        <th width="14%">Jabatan</th>
                        <th width="14%">Lokasi</th>
                        <th width="14%">Mulai Kontrak</th>
                        <th width="14%">Habis Kontrak</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no=1;
                    foreach($kontrak as $dt){
                    ?>
                    <tr>
                        <td><?=$no++;?></td>
                        <td><?=$dt->nik;?></td>
                        <td><?=$dt->nama_lengkap;?></td>
                        <td><?=tgl_indo($dt->tgl_kontrak);?></td>
                        <td><?=$dt->nama_jabatan;?></td>
                        <td><?=$dt->nama_lokasi;?></td>
                        <td><?=tgl_indo($dt->tgl_kontrak);?></td>
                        <td><?=tgl_indo($dt->tgl_habis_kontrak);?></td>
                    </tr>

                    <?php 
                    }
                    ?>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>