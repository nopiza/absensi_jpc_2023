<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_dataabsen extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'adm_dataabsen');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Adm_laporan_model','laporan');
		// $this->load->model('Group/Group_model','group');
		// check_login_opd();
	}

	public function index()
	{
		$user_data['sub_menu_active'] = 'Menu';
        $user_data['data_ref'] = $this->data_ref;
        
        $this->load->view('template/header');
        $this->load->view('view_index_individu',$user_data);

	}


	// public function opd()
	// {

	// 	$user_data['sub_menu_active'] = 'Menu';
	// 	$user_data['data_ref'] = $this->data_ref;
     	
    //  	$this->load->view('template/header_opd');
	// 	$this->load->view('view_index_opd',$user_data);

	// }

	// public function opd_semua()
	// {

	// 	$user_data['sub_menu_active'] = 'Menu';
	// 	$user_data['data_ref'] = $this->data_ref;
     	
    //  	$this->load->view('template/header_opd');
	// 	$this->load->view('view_index_opd_semua',$user_data);

	// }

    
    public function rekapglobal(){
        $user_data['data_ref'] = $this->data_ref;
        $this->load->view('template/header');
		$this->load->view('form',$user_data);
    }

    public function tampil(){
        $user_data['data_ref'] = $this->data_ref;
        $user_data['tanggalAwal'] = $this->input->post('tanggal_awal');
        $user_data['tanggalAkhir'] = $this->input->post('tanggal_akhir');
        $idLokasi = $this->input->post('lokasi');
        $lokasi = $this->db->query("SELECT * FROM lokasi WHERE id_lokasi='$idLokasi'")->row_array();
        $user_data['namalokasi'] = $lokasi['nama_lokasi'];
        $user_data['lokasi'] = $idLokasi;
        $user_data['karyawan'] = $this->db->query("SELECT * FROM karyawan WHERE id_lokasi='$idLokasi'")->result();

        $this->load->view('template/header_kosong');
		$this->load->view('tampil',$user_data);
    }

    public function tampilexcel($tglAwal, $tglAkhir, $idLokasi){
        $user_data['data_ref'] = $this->data_ref;
        $user_data['tanggalAwal'] = $tglAwal;
        $user_data['tanggalAkhir'] = $tglAkhir;
        
        $lokasi = $this->db->query("SELECT * FROM lokasi WHERE id_lokasi='$idLokasi'")->row_array();
        $user_data['namalokasi'] = $lokasi['nama_lokasi'];
        $user_data['karyawan'] = $this->db->query("SELECT * FROM karyawan WHERE id_lokasi='$idLokasi'")->result();

        $this->load->view('template/header_kosong');
		$this->load->view('tampilexcel',$user_data);
    }


	public function ajax_proses()
	{
		$data = '<table class="table table-bordered">
				<tr>
					<td>No.</td>
					<td>Nama Pegawai</td>
					<td>Tanggal</td>
				</tr>
			</table>';

			echo json_encode($data);
	}


	public function ajax_proses_individu($nip, $bulan, $tahun)
	{

		$data = '
        <a href="'.base_url('adm_dataabsen/cetakdataaja/'.$nip.'/'.$bulan.'/'.$tahun).'" class="btn btn-warning btn-sm" target="_blank"> Cetak Data Absen</a>';

        // <a href="'.base_url('adm_dataabsen/cetakdataaja/'.$nip.'/'.$bulan).'" class="btn btn-warning btn-sm" target="_blank"> Cetak Data Absen</a>;

        // <a href="'.base_url('adm_dataabsen/cetakdataaja_2/'.$nip.'/'.$bulan).'" class="btn btn-primary btn-sm" target="_blank"> Cetak Tanpa Potongan</a>
        $data .= '
        <br>
        <br>

        <table class="table table-bordered">
				<tr>
					<td width="5%">No.</td>
                    <td width="15%">Hari</td>
					<td width="20%">Tanggal</td>
					<td width="20%">Masuk</td>
					<td width="20%">Pulang</td>
                    <td width="20%">Keterangan</td>
				</tr>';

        // $bulan = date('m'); 
        // $tahun = date('Y');
        // $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
          $jumHari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 
        // $jumHari = 7;
		for ($i=1; $i <= $jumHari; $i++) { 

            $tgl = sprintf("%02d", $i);
            $tanggal    = $tahun.'-'.$bulan.'-'.$tgl;
            $namaHari = namaHari($tanggal);
            if($namaHari == 'Sabtu' || $namaHari == 'Minggu'){
                $warna = 'style="background-color:#ffa07a';
            }else{
                $warna = '';
            }
            $data  .= '<tr '.$warna.'">
                    <td>'.$i.'</td>
                    <td>'.$namaHari.'</td>
                    <td>'.tgl_indo($tanggal).'</td>
                    <td align="center">'.absenMasuk_jpc($tanggal, infoPegawai($nip)['id_karyawan']).'</td>
                    <td align="center">'.absenPulang_jpc($tanggal, infoPegawai($nip)['id_karyawan']).'</td>
                    <td></td>
                </tr>';
        }


			$data .= '</table>';

			echo json_encode($data);

	}


	public function individu()
	{

		$user_data['sub_menu_active'] = 'Menu';
		$user_data['data_ref'] = $this->data_ref;
     	
     	$this->load->view('template/header_opd');
		$this->load->view('view_index_individu',$user_data);

	}


	public function ajax_select_pegawai(){
        $this->db->select('id_karyawan, nama_lengkap');
        $this->db->like('nama_lengkap',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('karyawan')->result_array();
        //output to json format
        echo json_encode($items);
    }


    public function get($nip){
        $item=$this->db->query("SELECT * FROM karyawan WHERE id_karyawan ='$nip' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


    function cetak($nip, $bulan){

        // $nip        = $this->input->post('nama_pegawai');
        // $bulan      = $this->input->post('bulan');
        $tahun      = date('Y');
        $tanggal_awal = '01-'.$bulan.'-'.$tahun;
        $tanggal_akhir = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun).'-'.$bulan.'-'.$tahun;;

        $tglAwal = strtotime($tanggal_awal);
        $tglAkhir = strtotime($tanggal_akhir);


        // $from   = '01-'.$bulan.'-'.$tahun;
        // $to     = '29-'.$bulan.'-'.$tahun;

        $from   = convertDate($tanggal_awal);
        $to     = convertDate($tanggal_akhir);

         $bulan = substr($tanggal_awal,3,2);
         $bulan2 = substr($tanggal_akhir,3,2);


         $tahun = substr($tanggal_awal,6,4);

        if(!empty($tglAkhir)){
            if($tglAkhir < $tglAwal)die('tanggal selesai tidak boleh sebelum tanggal mulai');
        }
        

        $no     = 1;
        $totAlpa = 0;
        $totSakit = 0;
        $totDL = 0;
        $totIjin = 0;
        $totCuti = 0;
        $TotalPotongan = 0;
        $masuk  = '';
        $pulang = '';



        //Identifikasi Pegawai
        $query = "SELECT nama_pegawai, nip FROM pegawai a WHERE nip = '$nip'";
        $pegawai= $this->db->query($query)->row_array();
        $NIPPegawai = $pegawai['nip'];

       
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Absensi');
        $this->mypdf->SetFont('Arial','B',14);
        $this->mypdf->Cell(200,7,'Laporan Absensi Pegawai',0,0,'C');
        $this->mypdf->Ln();
       

        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(10,23,"Nama Pegawai");
        $this->mypdf->text(35,23," : ".$pegawai['nama_pegawai']);

        $this->mypdf->text(10,27,"NIP / NIK");
        $this->mypdf->text(35,27," : ".$pegawai['nip']);

        $this->mypdf->text(10,31,"Periode Absen");
        $this->mypdf->text(35,31," : ". convertDate($from).' sampai '.convertDate($to));


        $Dataijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL BETWEEN '$from' AND '$to' ORDER BY TANGGAL ASC");

 
        $this->mypdf->Ln();
        $this->mypdf->Ln();
        $this->mypdf->Ln();

         //jam kerja aktif sesuai bulan yang dipilih 
        //$tabel_jamker = 'jam_kerja_'.$bulanTrim;
        $tabel_jamker = 'jam_kerja_covid_'.$bulan;
        $jamKerjaPegawai = $this->db->get_where($tabel_jamker, array('nip'=>$NIPPegawai))->row_array();


        //Judul Tabel
        $this->mypdf->SetFillColor(70,130,180);
        $header = array('No','Hari','Tanggal','J Masuk','j Pulang','A Masuk','A Pulang','Pot M','Pot P','Pot Apl','Alpa','DL','TB','Sakit','Cuti','Pot');
        $w = array(6, 12, 15, 15, 15, 15, 15 ,10,10,10,10,10,10,10,10,10);
        $this->mypdf->SetFont('Arial','B',7);
        for($i=0;$i<count($header);$i++)$this->mypdf->Cell($w[$i],5,$header[$i],1,0,'C');


        $this->mypdf->Ln();
        $this->mypdf->SetFont('Arial','',7);
        $no=1;
        $this->mypdf->SetWidths($w);   
        //Master Baris

        $from = mktime(0,0,0,date('m',strtotime($from)),date('d',strtotime($from))-1,date('Y',strtotime($from)));
        $from=date('Y-m-d', $from);



            $alpa   = 0;
            $dl     = 0;
            $ijin   = 0;
            $sakit  = 0;
            $cuti   = 0;
            $potA   = 0;
            $potB   = 0;
            $jMasuk = '';
            $jPulang = '';
            $masuk = '';
            $pulang = '';
            $TotpotonganAlpa = 0;
            $subTotalPotongan = 0;



                    if($potA == 5 AND $potB == 5){
                        $potonganAlpa = 1;
                        $subTotalPotongan = 5;
                    }else{
                        $potonganAlpa = 0;
                        $subTotalPotongan = $potA + $potB;
                    }

                    

                    $TotpotonganAlpa = $TotpotonganAlpa  + $potonganAlpa;
                    $totAlpa = $totAlpa + $TotpotonganAlpa;
                    $totDL = $totDL + $dl;
                    $totIjin = $totIjin + $ijin;
                    $totSakit = $totSakit + $sakit;
                    $totCuti = $totCuti + $cuti;
                    $TotalPotongan = $TotalPotongan + $subTotalPotongan;

            $aa=($potA==0)?$potA='':'A';
            $bb=($potB==0)?$potB='':'A';
            $cc=($totAlpa==0)?$totAlpa='':'A';
            $dd=($totDL==0)?$totDL='':'A';
            $ee=($totIjin==0)?$totIjin='':'A';
            $ff=($totSakit==0)?$totSakit='':'A';
            $gg=($totCuti==0)?$totCuti='':'A';
            $hh=($TotalPotongan==0)?$TotalPotongan='':'A';
            $ii=($dl==0)?$dl='':'A';
            $jj=($ijin==0)?$ijin='':'A';
            $kk=($sakit==0)?$sakit='':'A';
            $ll=($cuti==0)?$cuti='':'A';
            $mm=($alpa==0)?$alpa='':'A';
            $pp=($TotpotonganAlpa==0)?$TotpotonganAlpa='':'A';
            $nn=($subTotalPotongan==0)?$subTotalPotongan='':'A';
            $hh=($TotalPotongan > 100 )?$TotalPotongan=100:$TotalPotongan=$TotalPotongan;

            $tang = convertDate($tanggal_awal);
            $tang_akhir = convertDate($tanggal_akhir);

            $tabel ='hitung_'.$bulan.'_'.$tahun;
            $tabel2 ='hitung_'.$bulan2.'_'.$tahun;



                    $hitung = $this->db->query("
                    SELECT * FROM $tabel WHERE nip = '$nip' AND TANGGAL BETWEEN '$tang' AND '$tang_akhir'")->result();
               
                
                
         

            
            

            foreach ($hitung as $hate) {

                $PotAlpa    = '';
                $PotSakit   = '';
                $PotDL      = '';
                $PotTB      = '';
                $PotCuti    = '';

                if(substr($hate->JMASUK,0,5) == '00:00'){ $a = '-'; }else{ $a = substr($hate->JMASUK,0,5); }
                if(substr($hate->JPULANG,0,5) == '00:00'){ $b = '-'; }else{ $b = substr($hate->JPULANG,0,5); }
                if(substr($hate->ABSNMASUK,0,5) == '00:00'){ $c = '-'; }else{ $c = substr($hate->ABSNMASUK,0,5); }
                if(substr($hate->ABSNKELUAR,0,5) == '00:00'){ $d = '-'; }else{ $d = substr($hate->ABSNKELUAR,0,5); }
                if(substr($hate->POTONGANMSK,0,5) == '0.0'){ $e = ''; }else{ $e = substr($hate->POTONGANMSK,0,5); }
                if(substr($hate->POTONGANPLNG,0,5) == '0.0'){ $f = ''; }else{ $f = substr($hate->POTONGANPLNG,0,5); }
                if(substr($hate->POT_HARIAN,0,5) == '0.0'){ $h = ''; }else{ $h = substr($hate->POT_HARIAN,0,5); }
                if($hate->POT_APEL == '0.0'){ $k = ''; }else{ $k = $hate->POT_APEL; }

                $pot_Harian = floatval($e) + floatval($f) + floatval($h) + floatval($k);
                if($pot_Harian == '0'){ $g = ''; }else{ $g = $pot_Harian; }

                if($hate->KODE_KET == 'A'){ 
                    $PotAlpa = '1'; 
                }elseif($hate->KODE_KET == 'S'){
                    $PotSakit = '1'; 
                }elseif($hate->KODE_KET == 'DL'){
                    $PotDL = '1'; 
                }elseif($hate->KODE_KET == 'TB'){
                    $PotTB = '1'; 
                }elseif($hate->KODE_KET == 'C'){
                    $PotCuti = '1'; 
                }

                // $k= '';

                 //Cetak sebagai kolom  
                $this->mypdf->setAligns(array('C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C'));      
                $this->mypdf->Row(array($no,$hate->HARI,convertDate($hate->TANGGAL),
                    $a,
                    $b,
                    $c,
                    $d,
                    $e,
                    $f,
                    $k,
                    $PotAlpa,$PotDL,$PotTB,$PotSakit,$PotCuti,$g));        
                $no++;

                $TotalPotongan = floatval($TotalPotongan) + floatval($g);
            }
           
        
        $posisi = $this->mypdf->GetY();
        
        $this->mypdf->setAligns(array('C','R','C','C','C','c','C','c','C','C','C','C','C','C','C','C','C'));
        $this->mypdf->Row(array('','','','','','','','','','',$totAlpa,$totDL,$totIjin,$totSakit,$totCuti,$TotalPotongan.' %')); 

        
        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(11,$posisi+ 15,"KETERANGAN IJIN : . ");

        $no=1;
        $y=$posisi+ 20;

        $tgl_ijin = $tahun.'-'.$bulan.'-01';
        // $ijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL > '$tgl_ijin' ORDER BY TANGGAL ASC");
        foreach($Dataijin->result() as $dt) { 
            $this->mypdf->SetFont('Arial','',8);
            $this->mypdf->text(11,$y,$no.".");
            $this->mypdf->text(17,$y,tgl_indo($dt->TANGGAL));
            $this->mypdf->text(41,$y,$dt->JENISIJIN);
            $this->mypdf->text(52,$y,$dt->KETERANGAN);
            $y=$y+4.2;
            $no++;
        }   

        $this->mypdf->text(145,$y+10," KEPALA Dinas KOMINFO" );
        $this->mypdf->text(152,$y+30,"SUTADI");
        $this->mypdf->text(145,$y+36,"196603151987011001");

        $this->mypdf->Output();
    }






    function cetakdataaja($nip, $bulan,$tahun){

        // $nip        = $this->input->post('nama_pegawai');
        // $bulan      = $this->input->post('bulan');
        // $tahun      = date('Y');
        $tanggal_awal = '01-'.$bulan.'-'.$tahun;
        $tanggal_akhir = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun).'-'.$bulan.'-'.$tahun;;

        $tglAwal = strtotime($tanggal_awal);
        $tglAkhir = strtotime($tanggal_akhir);


        // $from   = '01-'.$bulan.'-'.$tahun;
        // $to     = '29-'.$bulan.'-'.$tahun;

        $from   = convertDate($tanggal_awal);
        $to     = convertDate($tanggal_akhir);

         $bulan = substr($tanggal_awal,3,2);
         $bulan2 = substr($tanggal_akhir,3,2);


         $tahun = substr($tanggal_awal,6,4);

        if(!empty($tglAkhir)){
            if($tglAkhir < $tglAwal)die('tanggal selesai tidak boleh sebelum tanggal mulai');
        }
        

        $no     = 1;
        $totAlpa = 0;
        $totSakit = 0;
        $totDL = 0;
        $totIjin = 0;
        $totCuti = 0;
        $TotalPotongan = 0;
        $masuk  = '';
        $pulang = '';



        //Identifikasi Pegawai
        $query = "SELECT nama_lengkap, id_karyawan, no_reg FROM karyawan a WHERE id_karyawan = '$nip'";
        $pegawai= $this->db->query($query)->row_array();
        $NIPPegawai = $pegawai['id_karyawan'];

       
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Absensi');
        $this->mypdf->SetFont('Arial','B',14);
        $this->mypdf->Cell(200,7,'Rekap Absensi Karyawan',0,0,'C');
        $this->mypdf->Ln();
       

        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(10,23,"Nama Karyawan");
        $this->mypdf->text(35,23," : ".$pegawai['nama_lengkap']);

        $this->mypdf->text(10,27,"ID Karyawan");
        $this->mypdf->text(35,27," : ".$pegawai['no_reg']);

        $this->mypdf->text(10,31,"Periode Absen");
        $this->mypdf->text(35,31," : ". convertDate($from).' sampai '.convertDate($to));

        $id_karyawan = $pegawai['id_karyawan'];
        $Dataijin = $this->db->query("SELECT * FROM pengajuan_izin 
        WHERE id_karyawan='$id_karyawan' AND TANGGAL BETWEEN '$from' AND '$to' ORDER BY TANGGAL ASC");

 
        $this->mypdf->Ln();
        $this->mypdf->Ln();
        $this->mypdf->Ln();

        
        //Judul Tabel
        $this->mypdf->SetFillColor(70,130,180);
        $header = array('No','Hari','Tanggal','A Masuk','A Pulang','Keterangan');
        $w = array(6, 25, 35, 25, 25, 25);
        $this->mypdf->SetFont('Arial','B',7);
        for($i=0;$i<count($header);$i++)$this->mypdf->Cell($w[$i],5,$header[$i],1,0,'C');


        $this->mypdf->Ln();
        $this->mypdf->SetFont('Arial','',9);
        $no=1;
        $this->mypdf->SetWidths($w);   
        //Master Baris

        $from = mktime(0,0,0,date('m',strtotime($from)),date('d',strtotime($from))-1,date('Y',strtotime($from)));
        $from=date('Y-m-d', $from);


        // $tahun = date('Y');
            $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        for ($i=1; $i <= $jumHari; $i++) { 

            $tgl = sprintf("%02d", $i);
            $tanggal    = $tahun.'-'.$bulan.'-'.$tgl;
            $namaHari = namaHari($tanggal);
            if($namaHari == 'Sabtu' || $namaHari == 'Minggu'){
                $warna = 'style="background-color:#ffa07a';
            }else{
                $warna = '';
            }

            

            // $data  .= '<tr '.$warna.'">
            //         <td>'.$i.'</td>
            //         <td>'.$namaHari.'</td>
            //         <td>'.tgl_indo($tanggal).'</td>
            //         <td align="center">'.absenMasuk($tanggal, infoPegawaiByNIP($nip)['id_pegawai']).'</td>
            //         <td align="center">'.absenPulang($tanggal, infoPegawaiByNIP($nip)['id_pegawai']).'</td>
            //         <td></td>
            //     </tr>';

                $this->mypdf->setAligns(array('C','C', 'C','C','C'));
                $this->mypdf->Row(array($no++,$namaHari, tgl_indo($tanggal),
                absenMasuk_jpc($tanggal, infoPegawai($nip)['id_karyawan']),
                absenPulang_jpc($tanggal, infoPegawai($nip)['id_karyawan']),'')); 

        }

        
        $posisi = $this->mypdf->GetY();
        
        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(11,$posisi+ 15,"KETERANGAN IJIN : . ");

        $no=1;
        $y=$posisi+ 20;

        $tgl_ijin = $tahun.'-'.$bulan.'-01';
        // $ijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL > '$tgl_ijin' ORDER BY TANGGAL ASC");
        foreach($Dataijin->result() as $dt) { 
            $this->mypdf->SetFont('Arial','',8);
            $this->mypdf->text(11,$y,$no.".");
            $this->mypdf->text(17,$y,tgl_indo($dt->tanggal));
            $this->mypdf->text(41,$y,$dt->jenis_izin);
            $this->mypdf->text(52,$y,$dt->deskripsi);
            $y=$y+4.2;
            $no++;
        }   

        // $this->mypdf->text(145,$y+10," KEPALA DINAS" );
        // $this->mypdf->text(148,$y+30,"......................");
        // $this->mypdf->text(145,$y+36,"..........................");

        $this->mypdf->Output();
    }




    function cetakharian($kolok, $tanggal){


        $kolok = $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$user_data['kolok'] = $kolok;
		$user_data['tanggal'] = $this->input->post('tanggal');
		$cabang = $this->input->post('cabang');
		$queryCabang = '';

		$user_data['data_ref'] = $this->data_ref;

		$this->db->order_by('urutan', 'ASC');
		$this->db->where(['kolok' => $kolok]);
		if(!empty($cabang)){
			$this->db->where('pegawai.KOLOK="'.$cabang.'"', null, false);
		}

		$this->db->where('pegawai.nip NOT LIKE "%plt%"', null, false);
		$pegawai = $this->db->get('pegawai')->result();

       
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Absensi');
        $this->mypdf->SetFont('Arial','B',14);
        $this->mypdf->Cell(200,7,'Rekap Absensi Harian Pegawai',0,0,'C');
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->Ln();
        $this->mypdf->Cell(200,7,'Tanggal : '.tgl_indo($tanggal),0,0,'C');
        
       

        // $this->mypdf->SetFont('Arial','',8);
        // $this->mypdf->text(10,23,"Nama Pegawai");
        // $this->mypdf->text(35,23," : ".$pegawai['nama_pegawai']);

        // $this->mypdf->text(10,27,"NIP / NIK");
        // $this->mypdf->text(35,27," : ".$pegawai['nip']);

        // $this->mypdf->text(10,31,"Periode Absen");
        // $this->mypdf->text(35,31," : ". convertDate($from).' sampai '.convertDate($to));


        // $Dataijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL BETWEEN '$from' AND '$to' ORDER BY TANGGAL ASC");

 
        // $this->mypdf->Ln();
        $this->mypdf->Ln();
        $this->mypdf->Ln();

        
        //Judul Tabel
        $this->mypdf->SetFillColor(70,130,180);
        $header = array('No','Nama','Nip','A Masuk','A Pulang','Keterangan');
        $w = array(10, 60, 40, 25, 25, 25);
        $this->mypdf->SetFont('Arial','B',7);
        for($i=0;$i<count($header);$i++)$this->mypdf->Cell($w[$i],5,$header[$i],1,0,'C');


        $this->mypdf->Ln();
        $this->mypdf->SetFont('Arial','',9);
        $no=1;
        $this->mypdf->SetWidths($w);   


        foreach($pegawai as $pgw){

            // $tgl = sprintf("%02d", $i);
            // $tanggal    = $tahun.'-'.$bulan.'-'.$tgl;
            $namaHari = namaHari($tanggal);
            if($namaHari == 'Sabtu' || $namaHari == 'Minggu'){
                $warna = 'style="background-color:#ffa07a';
            }else{
                $warna = '';
            }

                $this->mypdf->setAligns(array('C','L', 'C','C','C'));
                $this->mypdf->Row(array($no++,$pgw->nama_pegawai, $pgw->nip,
                absenMasuk_new($tanggal, infoPegawaiByNIP($pgw->nip)['id_pegawai'], $pgw->nip, 'NON'),
                absenPulang_new($tanggal, infoPegawaiByNIP($pgw->nip)['id_pegawai'], $pgw->nip, 'NON'),'')); 

        }

    

        // $this->mypdf->text(145,$y+10," KEPALA DINAS" );
        // $this->mypdf->text(148,$y+30,"......................");
        // $this->mypdf->text(145,$y+36,"..........................");

        $this->mypdf->Output();
    }



    function cetakdarihitung($nip, $bulan){

        // $nip        = $this->input->post('nama_pegawai');
        // $bulan      = $this->input->post('bulan');
        $tahun      = date('Y');
        $tanggal_awal = '01-'.$bulan.'-'.$tahun;
        $tanggal_akhir = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun).'-'.$bulan.'-'.$tahun;;

        $tglAwal = strtotime($tanggal_awal);
        $tglAkhir = strtotime($tanggal_akhir);


        // $from   = '01-'.$bulan.'-'.$tahun;
        // $to     = '29-'.$bulan.'-'.$tahun;

        $from   = convertDate($tanggal_awal);
        $to     = convertDate($tanggal_akhir);

         $bulan = substr($tanggal_awal,3,2);
         $bulan2 = substr($tanggal_akhir,3,2);


         $tahun = substr($tanggal_awal,6,4);

        if(!empty($tglAkhir)){
            if($tglAkhir < $tglAwal)die('tanggal selesai tidak boleh sebelum tanggal mulai');
        }
        

        $no     = 1;
        $totAlpa = 0;
        $totSakit = 0;
        $totDL = 0;
        $totIjin = 0;
        $totCuti = 0;
        $TotalPotongan = 0;
        $masuk  = '';
        $pulang = '';



        //Identifikasi Pegawai
        $query = "SELECT nama_pegawai, nip FROM pegawai a WHERE nip = '$nip'";
        $pegawai= $this->db->query($query)->row_array();
        $NIPPegawai = $pegawai['nip'];

       
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Absensi');
        $this->mypdf->SetFont('Arial','B',14);
        $this->mypdf->Cell(200,7,'Rekap Absensi Pegawai',0,0,'C');
        $this->mypdf->Ln();
       

        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(10,23,"Nama Pegawai");
        $this->mypdf->text(35,23," : ".$pegawai['nama_pegawai']);

        $this->mypdf->text(10,27,"NIP / NIK");
        $this->mypdf->text(35,27," : ".$pegawai['nip']);

        $this->mypdf->text(10,31,"Periode Absen");
        $this->mypdf->text(35,31," : ". convertDate($from).' sampai '.convertDate($to));


        $Dataijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL BETWEEN '$from' AND '$to' ORDER BY TANGGAL ASC");

 
        $this->mypdf->Ln();
        $this->mypdf->Ln();
        $this->mypdf->Ln();

        
        //Judul Tabel
        $this->mypdf->SetFillColor(70,130,180);
        $header = array('No','Hari','Tanggal','A Masuk','A Pulang','Keterangan');
        $w = array(6, 25, 35, 25, 25, 25);
        $this->mypdf->SetFont('Arial','B',7);
        for($i=0;$i<count($header);$i++)$this->mypdf->Cell($w[$i],5,$header[$i],1,0,'C');


        $this->mypdf->Ln();
        $this->mypdf->SetFont('Arial','',9);
        $no=1;
        $this->mypdf->SetWidths($w);   
        //Master Baris

        $from = mktime(0,0,0,date('m',strtotime($from)),date('d',strtotime($from))-1,date('Y',strtotime($from)));
        $from=date('Y-m-d', $from);


           
        
        

        $tahun = date('Y');
            $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        for ($i=1; $i <= $jumHari; $i++) { 

            $tgl = sprintf("%02d", $i);
            $tanggal    = $tahun.'-'.$bulan.'-'.$tgl;
            $namaHari = namaHari($tanggal);
            if($namaHari == 'Sabtu' || $namaHari == 'Minggu'){
                $warna = 'style="background-color:#ffa07a';
            }else{
                $warna = '';
            }

            

            // $data  .= '<tr '.$warna.'">
            //         <td>'.$i.'</td>
            //         <td>'.$namaHari.'</td>
            //         <td>'.tgl_indo($tanggal).'</td>
            //         <td align="center">'.absenMasuk($tanggal, infoPegawaiByNIP($nip)['id_pegawai']).'</td>
            //         <td align="center">'.absenPulang($tanggal, infoPegawaiByNIP($nip)['id_pegawai']).'</td>
            //         <td></td>
            //     </tr>';

                $this->mypdf->setAligns(array('C','C', 'C','C','C'));
                $this->mypdf->Row(array($no++,$namaHari, tgl_indo($tanggal),cariJamMasuk_adm($tanggal, infoPegawaiByNIP($nip)['id_pegawai']),cariJamPulang_adm($tanggal, infoPegawaiByNIP($nip)['id_pegawai']),'')); 

        }

        
        $posisi = $this->mypdf->GetY();
        
        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(11,$posisi+ 15,"KETERANGAN IJIN : . ");

        $no=1;
        $y=$posisi+ 20;

        $tgl_ijin = $tahun.'-'.$bulan.'-01';
        // $ijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL > '$tgl_ijin' ORDER BY TANGGAL ASC");
        foreach($Dataijin->result() as $dt) { 
            $this->mypdf->SetFont('Arial','',8);
            $this->mypdf->text(11,$y,$no.".");
            $this->mypdf->text(17,$y,tgl_indo($dt->TANGGAL));
            $this->mypdf->text(41,$y,$dt->JENISIJIN);
            $this->mypdf->text(52,$y,$dt->KETERANGAN);
            $y=$y+4.2;
            $no++;
        }   

        $this->mypdf->text(145,$y+10," KEPALA Dinas KOMINFO" );
        $this->mypdf->text(156,$y+30,"SUTADI");
        $this->mypdf->text(145,$y+36,"196603151987011001");

        $this->mypdf->Output();
    }





    function individu_bulan(){

        $nip 		= $this->input->post('nama_pegawai');
        $bulan 		= $this->input->post('bulan');
        $tahun		= date('Y');
        $tanggal_awal = '01-'.$bulan.'-'.$tahun;
        $tanggal_akhir = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun).'-'.$bulan.'-'.$tahun;;

        $tglAwal = strtotime($tanggal_awal);
        $tglAkhir = strtotime($tanggal_akhir);


        // $from   = '01-'.$bulan.'-'.$tahun;
        // $to     = '29-'.$bulan.'-'.$tahun;

        $from   = convertDate($tanggal_awal);
        $to     = convertDate($tanggal_akhir);

         $bulan = substr($tanggal_awal,3,2);
         $bulan2 = substr($tanggal_akhir,3,2);


         $tahun = substr($tanggal_awal,6,4);

        if(!empty($tglAkhir)){
            if($tglAkhir < $tglAwal)die('tanggal selesai tidak boleh sebelum tanggal mulai');
        }
        

        $no     = 1;
        $totAlpa = 0;
        $totSakit = 0;
        $totDL = 0;
        $totIjin = 0;
        $totCuti = 0;
        $TotalPotongan = 0;
        $masuk  = '';
        $pulang = '';



        //Identifikasi Pegawai
        $query = "SELECT nama_pegawai, nip FROM pegawai a WHERE nip = '$nip'";
        $pegawai= $this->db->query($query)->row_array();
        $NIPPegawai = $pegawai['nip'];

       
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Absensi');
        $this->mypdf->SetFont('Arial','B',14);
        $this->mypdf->Cell(200,7,'Laporan Absensi Pegawai',0,0,'C');
        $this->mypdf->Ln();
       

        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(10,23,"Nama Pegawai");
        $this->mypdf->text(35,23," : ".$pegawai['nama_pegawai']);

        $this->mypdf->text(10,27,"NIP / NIK");
        $this->mypdf->text(35,27," : ".$pegawai['nip']);

        $this->mypdf->text(10,31,"Periode Absen");
        $this->mypdf->text(35,31," : ". convertDate($from).' sampai '.convertDate($to));


        $Dataijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL BETWEEN '$from' AND '$to' ORDER BY TANGGAL ASC");

 
        $this->mypdf->Ln();
        $this->mypdf->Ln();
        $this->mypdf->Ln();

         //jam kerja aktif sesuai bulan yang dipilih 
        //$tabel_jamker = 'jam_kerja_'.$bulanTrim;
        $tabel_jamker = 'jam_kerja_covid_'.$bulan;
        $jamKerjaPegawai = $this->db->get_where($tabel_jamker, array('nip'=>$NIPPegawai))->row_array();


        //Judul Tabel
        $this->mypdf->SetFillColor(70,130,180);
        $header = array('No','Hari','Tanggal','J Masuk','j Pulang','A Masuk','A Pulang','Pot M','Pot P','Pot Apl','Alpa','DL','TB','Sakit','Cuti','Pot');
        $w = array(6, 12, 15, 15, 15, 15, 15 ,10,10,10,10,10,10,10,10,10);
        $this->mypdf->SetFont('Arial','B',7);
        for($i=0;$i<count($header);$i++)$this->mypdf->Cell($w[$i],5,$header[$i],1,0,'C');


        $this->mypdf->Ln();
        $this->mypdf->SetFont('Arial','',7);
        $no=1;
        $this->mypdf->SetWidths($w);   
        //Master Baris

        $from = mktime(0,0,0,date('m',strtotime($from)),date('d',strtotime($from))-1,date('Y',strtotime($from)));
        $from=date('Y-m-d', $from);



            $alpa   = 0;
            $dl     = 0;
            $ijin   = 0;
            $sakit  = 0;
            $cuti   = 0;
            $potA   = 0;
            $potB   = 0;
            $jMasuk = '';
            $jPulang = '';
            $masuk = '';
            $pulang = '';
            $TotpotonganAlpa = 0;
            $subTotalPotongan = 0;



                    if($potA == 5 AND $potB == 5){
                        $potonganAlpa = 1;
                        $subTotalPotongan = 5;
                    }else{
                        $potonganAlpa = 0;
                        $subTotalPotongan = $potA + $potB;
                    }

                    

                    $TotpotonganAlpa = $TotpotonganAlpa  + $potonganAlpa;
                    $totAlpa = $totAlpa + $TotpotonganAlpa;
                    $totDL = $totDL + $dl;
                    $totIjin = $totIjin + $ijin;
                    $totSakit = $totSakit + $sakit;
                    $totCuti = $totCuti + $cuti;
                    $TotalPotongan = $TotalPotongan + $subTotalPotongan;

            $aa=($potA==0)?$potA='':'A';
            $bb=($potB==0)?$potB='':'A';
            $cc=($totAlpa==0)?$totAlpa='':'A';
            $dd=($totDL==0)?$totDL='':'A';
            $ee=($totIjin==0)?$totIjin='':'A';
            $ff=($totSakit==0)?$totSakit='':'A';
            $gg=($totCuti==0)?$totCuti='':'A';
            $hh=($TotalPotongan==0)?$TotalPotongan='':'A';
            $ii=($dl==0)?$dl='':'A';
            $jj=($ijin==0)?$ijin='':'A';
            $kk=($sakit==0)?$sakit='':'A';
            $ll=($cuti==0)?$cuti='':'A';
            $mm=($alpa==0)?$alpa='':'A';
            $pp=($TotpotonganAlpa==0)?$TotpotonganAlpa='':'A';
            $nn=($subTotalPotongan==0)?$subTotalPotongan='':'A';
            $hh=($TotalPotongan > 100 )?$TotalPotongan=100:$TotalPotongan=$TotalPotongan;

            $tang = convertDate($tanggal_awal);
            $tang_akhir = convertDate($tanggal_akhir);

            $tabel ='hitung_'.$bulan.'_'.$tahun;
            $tabel2 ='hitung_'.$bulan2.'_'.$tahun;



                    $hitung = $this->db->query("
                    SELECT * FROM $tabel WHERE nip = '$nip' AND TANGGAL BETWEEN '$tang' AND '$tang_akhir'")->result();
               
                
                
         

            
            

            foreach ($hitung as $hate) {

                $PotAlpa    = '';
                $PotSakit   = '';
                $PotDL      = '';
                $PotTB      = '';
                $PotCuti    = '';

                if(substr($hate->JMASUK,0,5) == '00:00'){ $a = '-'; }else{ $a = substr($hate->JMASUK,0,5); }
                if(substr($hate->JPULANG,0,5) == '00:00'){ $b = '-'; }else{ $b = substr($hate->JPULANG,0,5); }
                if(substr($hate->ABSNMASUK,0,5) == '00:00'){ $c = '-'; }else{ $c = substr($hate->ABSNMASUK,0,5); }
                if(substr($hate->ABSNKELUAR,0,5) == '00:00'){ $d = '-'; }else{ $d = substr($hate->ABSNKELUAR,0,5); }
                if(substr($hate->POTONGANMSK,0,5) == '0.0'){ $e = ''; }else{ $e = substr($hate->POTONGANMSK,0,5); }
                if(substr($hate->POTONGANPLNG,0,5) == '0.0'){ $f = ''; }else{ $f = substr($hate->POTONGANPLNG,0,5); }
                if(substr($hate->POT_HARIAN,0,5) == '0.0'){ $h = ''; }else{ $h = substr($hate->POT_HARIAN,0,5); }
                if($hate->POT_APEL == '0.0'){ $k = ''; }else{ $k = $hate->POT_APEL; }

                $pot_Harian = floatval($e) + floatval($f) + floatval($h) + floatval($k);
                if($pot_Harian == '0'){ $g = ''; }else{ $g = $pot_Harian; }

                if($hate->KODE_KET == 'A'){ 
                    $PotAlpa = '1'; 
                }elseif($hate->KODE_KET == 'S'){
                    $PotSakit = '1'; 
                }elseif($hate->KODE_KET == 'DL'){
                    $PotDL = '1'; 
                }elseif($hate->KODE_KET == 'TB'){
                    $PotTB = '1'; 
                }elseif($hate->KODE_KET == 'C'){
                    $PotCuti = '1'; 
                }

                // $k= '';

                 //Cetak sebagai kolom  
                $this->mypdf->setAligns(array('C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C'));      
                $this->mypdf->Row(array($no,$hate->HARI,convertDate($hate->TANGGAL),
                    $a,
                    $b,
                    $c,
                    $d,
                    $e,
                    $f,
                    $k,
                    $PotAlpa,$PotDL,$PotTB,$PotSakit,$PotCuti,$g));        
                $no++;

                $TotalPotongan = floatval($TotalPotongan) + floatval($g);
            }
           
        
        $posisi = $this->mypdf->GetY();
        
        $this->mypdf->setAligns(array('C','R','C','C','C','c','C','c','C','C','C','C','C','C','C','C','C'));
        $this->mypdf->Row(array('','','','','','','','','','',$totAlpa,$totDL,$totIjin,$totSakit,$totCuti,$TotalPotongan.' %')); 

        
        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(11,$posisi+ 15,"KETERANGAN IJIN : . ");

        $no=1;
        $y=$posisi+ 20;

        $tgl_ijin = $tahun.'-'.$bulan.'-01';
        // $ijin = $this->db->query("SELECT * FROM ijin_pegawai WHERE NIP='$nip' AND TANGGAL > '$tgl_ijin' ORDER BY TANGGAL ASC");
        foreach($Dataijin->result() as $dt) { 
            $this->mypdf->SetFont('Arial','',8);
            $this->mypdf->text(11,$y,$no.".");
            $this->mypdf->text(17,$y,tgl_indo($dt->TANGGAL));
            $this->mypdf->text(41,$y,$dt->JENISIJIN);
            $this->mypdf->text(52,$y,$dt->KETERANGAN);
            $y=$y+4.2;
            $no++;
        }   

        $this->mypdf->text(145,$y+10," Kepala Dinas KOMINFO" );
        $this->mypdf->text(142,$y+30,"SUTADI");
        $this->mypdf->text(145,$y+36,"196603151987011001");

        $this->mypdf->Output();
    }


    public function update_manual(){

        //Update untuk memberikan statu spada absen manual / Jenis Masuk atau Pulang
        $query = "SELECT a.id_absensi, a.id_account, a.id_absen_mesin, a.tanggal_absensi, 

(SELECT MIN(tanggal_absensi) as kecil FROM absensi_mesin 
 WHERE id_account=a.id_account AND LEFT(absensi_mesin.tanggal_absensi, 10) = LEFT(a.tanggal_absensi, 10) ) as masuk, 
 
 IF(
 (SELECT MIN(tanggal_absensi) as kecil FROM absensi_mesin 
 WHERE id_account=a.id_account 
 AND LEFT(absensi_mesin.tanggal_absensi, 10) = LEFT(a.tanggal_absensi, 10) 
 AND RIGHT(tanggal_absensi, 8) < '10:30:00' 
 ) = a.tanggal_absensi , 'MASUK' , '') as status

FROM absensi_mesin a WHERE LEFT(a.tanggal_absensi, 7) = '2021-02' AND a.id_account = a.id_account  
ORDER BY `a`.`id_absen_mesin` ASC";

    $lakukan = $this->db->query($query)->result();

    foreach ($lakukan as $ini) {
        
        if($ini->status == 'MASUK'){
            echo $ini->id_absensi.' - '.$ini->tanggal_absensi.'<br>';
            $this->db->update('absensi_mesin', ['type_absensi' => 'Masuk'], ['id_absensi' => $ini->id_absensi]);
        }
    }

    }



    public function update_manual_pulang(){

        //Update untuk memberikan statu spada absen manual / Jenis Masuk atau Pulang
        $query = "SELECT a.id_absensi, a.id_account, a.id_absen_mesin, a.tanggal_absensi, 

(SELECT MAX(tanggal_absensi) as besar FROM absensi_mesin 
 WHERE id_account=a.id_account AND LEFT(absensi_mesin.tanggal_absensi, 10) = LEFT(a.tanggal_absensi, 10) ) as masuk, 
 
 IF((SELECT MAX(tanggal_absensi) as besar FROM absensi_mesin 
 WHERE id_account=a.id_account 
 AND LEFT(absensi_mesin.tanggal_absensi, 10) = LEFT(a.tanggal_absensi, 10) 
 AND RIGHT(tanggal_absensi, 8) > '12:00:00' 
 ) = a.tanggal_absensi , 'PULANG' , '') as status

FROM absensi_mesin a WHERE LEFT(a.tanggal_absensi, 7) = '2021-02' AND a.id_account = a.id_account  
ORDER BY `a`.`id_absen_mesin` ASC";

    $lakukan = $this->db->query($query)->result();

    foreach ($lakukan as $ini) {
        
        if($ini->status == 'PULANG'){
            echo $ini->id_absensi.' - '.$ini->tanggal_absensi.'<br>';
            $this->db->update('absensi_mesin', ['type_absensi' => 'Pulang'], ['id_absensi' => $ini->id_absensi]);
        }
    }

    }




    function template(){



        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Absensi');
        $this->mypdf->SetFont('Arial','B',14);
        $this->mypdf->Cell(200,7,'Laporan Absensi Pegawai',0,0,'C');
        $this->mypdf->Ln();
       

        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(10,23,"Nama Pegawai");
        $this->mypdf->text(35,23," : ");

        $this->mypdf->text(10,27,"NIP / NIK");
        $this->mypdf->text(35,27," : ");

        $this->mypdf->text(10,31,"Periode Absen");
        $this->mypdf->text(35,31," : ");


 
        $this->mypdf->Ln();
        $this->mypdf->Ln();
        $this->mypdf->Ln();

        
        //Judul Tabel
        $this->mypdf->SetFillColor(70,130,180);
        $header = array('No','Hari','Tanggal','A Masuk','A Pulang','Keterangan');
        $w = array(6, 25, 35, 25, 25, 25, 25);
        $this->mypdf->SetFont('Arial','B',7);
        for($i=0;$i<count($header);$i++)
            $this->mypdf->Cell($w[$i],5,$header[$i],1,0,'C');


        $this->mypdf->Ln();
        $this->mypdf->SetFont('Arial','',9);
        $no=1;
        $this->mypdf->SetWidths($w);   
        //Master Baris


        for ($i=1; $i <=5; $i++) { 

            
                $this->mypdf->setAligns(array('C'));
                $this->mypdf->Row(array($i, 'as','','','','')); 


        }

        
        $posisi = $this->mypdf->GetY();
        
        $this->mypdf->SetFont('Arial','',8);
        $this->mypdf->text(11,$posisi+ 15,"KETERANGAN IJIN : . ");

        $no=1;
        $y=$posisi+ 20;




        $this->mypdf->text(145,$y+10," KEPALA Dinas KOMINFO" );
        $this->mypdf->text(156,$y+30,"SUTADI");
        $this->mypdf->text(145,$y+36,"196603151987011001");

        $this->mypdf->Output();
    }


    public function peta($id, $jenis, $tanggal){

        $absen = $this->db->query("SELECT MAX(tanggal_absensi) as jamAbsen FROM absensi
     WHERE tanggal_absensi like '$tanggal%' 
     AND id_account='$id' 
     AND type_absensi = '$jenis'")->row_array();

        $jamAbsen = $absen['jamAbsen'];
        $cekAbsen = $this->db->get_where('absensi', ['id_account'=>$id, 'tanggal_absensi' => $jamAbsen, 'type_absensi'=>$jenis])->row_array();
        echo '
        <h4>'.infoPegawai($id)['nama_pegawai'].'</h4>
        <iframe 
  width="100%" 
  height="300" 
  frameborder="0" 
  scrolling="no" 
  marginheight="0" 
  marginwidth="0" 
  src="https://maps.google.com/maps?q='.$cekAbsen['latitude_absensi'].','.$cekAbsen['longitude_absensi'].'&hl=id&z=15&amp;output=embed">
 </iframe>

        <br>
          <br>
          <br>
          Keterangan : <br>
          ID Data : '.$cekAbsen['id_absensi'].'
          <br>
          '.$cekAbsen['tanggal_absensi'].'


';

    }



    public function transfer(){
        $dariBCK = $this->db->query("SELECT * FROM att_tarik_manual")->result();

        foreach ($dariBCK as $data){
            echo "- ";
            $param = [
                'sn' 	        => 'FIO66208020151166', 
                'scan_date'     => $data->scan_date, 	
                'pin' 	        => $data->pin, 
                'verifymode' 	=> '1', 
                'inoutmode'     => $data->inoutmode, 	
                'device_ip'     => '103.145.32.214'
            ];
            $this->db->insert('att_log', $param);
        }

    }


    // public function ajax_select_pegawai(){
	// 	$kolok = $this->encryption->decrypt($this->session->userdata('KOLOK'));
    //     $this->db->select('nip,nama_pegawai');
    //     $this->db->where(['kolok'=> $kolok]);
    //     $this->db->like('nama_pegawai',$this->input->get('q'),'both');
    //     $this->db->limit(20);
    //     $items=$this->db->get('pegawai')->result_array();
    //     //output to json format
    //     echo json_encode($items);
    // }


    // public function get($nip){

    //     $item=$this->db->query("SELECT * FROM pegawai WHERE nip ='$nip' ")->row_array();

    //     return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    // }



}
