  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Kehadiran per Karyawan.</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
       <!-- /.card-header -->
        <div class="card-body">

          <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id" id="id"/>

          <div class="form-group row">
            <label class="col-sm-3 control-label">Nama Pegawai</label>
            <div class="col-md-5">
              <input name="nama_pegawai" type="text" class="form-control" id="nama_pegawai" >
              <span class="help-block"></span>
            </div>
          </div>


          <div class="form-group row">
            <label class="col-sm-3 control-label">NIP / NIK</label>
            <div class="col-md-5">
              <input name="nip_pegawai" type="text" class="form-control" id="nip_pegawai" readonly="">
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-3 control-label">Bulan</label>
            <div class="col-md-2">
              <select name="bulan" class="form-control" id="bulan" >
              <option value="01" <?php if(date('m') =='01') { echo "selected";} ?> >Januari</option>
                <option value="02" <?php if(date('m') =='02') { echo "selected";} ?> >Februari</option>
                <option value="03" <?php if(date('m') =='03') { echo "selected";} ?> >Maret</option>
                <option value="04" <?php if(date('m') =='04') { echo "selected";} ?> >April</option>
                <option value="05" <?php if(date('m') =='05') { echo "selected";} ?> >Mei</option>
                <option value="06" <?php if(date('m') =='06') { echo "selected";} ?> >Juni</option>
                <option value="07" <?php if(date('m') =='07') { echo "selected";} ?> >Juli</option>
                <option value="08" <?php if(date('m') =='08') { echo "selected";} ?> >Agustus</option>
                <option value="09" <?php if(date('m') =='09') { echo "selected";} ?> >September</option>
                <option value="10" <?php if(date('m') =='10') { echo "selected";} ?> >Oktober</option>
                <option value="11" <?php if(date('m') =='11') { echo "selected";} ?> >November</option>
                <option value="12" <?php if(date('m') =='12') { echo "selected";} ?> >Desember</option>
              </select>
              <span class="help-block"></span>
            </div>
            <div class="col-md-3">
              <!-- <a class="btn btn-primary btn-md" href="javascript:void(0)" onclick="proses()">Proses</a> -->
              <!-- <select name="bulan" class="form-control" id="bulan" >
                <option value="01">-- Pilih Mesin --</option>
                <option value="1">FingerSpot</option>
                <option value="2">Solution</option>
              </select> -->
              <span class="help-block"></span>
            </div>
          </div>



          <div class="form-group row">
            <label class="col-sm-3 control-label">Tahun</label>
            <div class="col-md-2">
              <select name="tahun" class="form-control" id="tahun" >
                <option value="2022" <?php if(date('Y') =='2022') { echo "selected";} ?> >2022</option>
              <option value="2023" <?php if(date('Y') =='2023') { echo "selected";} ?> >2023</option>
              </select>
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-3 control-label"></label>
            <div class="col-md-5">
              <a href="#" class="btn btn-primary btn-md" onclick="proses()" >Tampilkan</a>
            </div>
          </div>


                                                  
          </form>

          <div id="laporan"></div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>



<div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Large Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="peta"></div>

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">

function detail(id, jenis, tanggal) {
  // var id;
  $('#modal-xl').modal('show'); // show bootstrap modal
  // $('#peta').load('http://detik.com'); 
  $("#peta").load('<?php echo base_url('adm_dataabsen/peta/');?>' + id +'/'+ jenis+'/'+ tanggal);
  $('.modal-title').text('Detail absensi'); // Set Title to Bootstrap modal title
}


</script>



<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

function proses(){

  var idKaryawan     = document.getElementById("nama_pegawai").value;
  var bulan   = document.getElementById("bulan").value;
  var tahun   = document.getElementById("tahun").value;

  // alert(nip + ' - ' + bulan)

url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_proses_individu')?>/" + idKaryawan + '/' + bulan+ '/' + tahun;

    
       // ajax adding data to database
       $.ajax({
           url : url,
           type: "POST",
           data: $('#form').serialize(),
           dataType: "JSON",
           success: function(data)
           {
  
                   document.getElementById('laporan').innerHTML = data;
  
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
   
           }
       });
   }


</script>




<script src="<?php echo base_url('assets/admin/plugins/select2/select2.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2-bootstrap.css') ?>">

<script type="text/javascript">
  
var url_apps = '<?=base_url();?>';


$(document).ready(function () {
//----->
//Ambil semua data customer untuk select 2
  $("#nama_pegawai").select2({
    ajax: {
      url: url_apps+'adm_dataabsen/ajax_select_pegawai',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params, // search term
        };
      },
      results: function (data, params) {
        console.log(data);
        return {
            results: $.map(data, function (item) {
                return {
                    text: item.nama_lengkap,
                    id: item.id_karyawan
                }
            })
        };
      },
      cache: true
    },
    minimumInputLength: 1,
  });  


});


$('#nama_pegawai').on('change', function() {
  $.ajax({
    url: url_apps + 'adm_dataabsen/get/' + $(this).val(),
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    // alert(data);
    $('#nip_pegawai').val(data.no_reg);
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
});

</script>



