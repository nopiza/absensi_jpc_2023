<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=rekap_absen.xls");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
Periode : <?=tgl_indo($tanggalAwal);?> sampai <?=tgl_indo($tanggalAkhir);?> <br>
Lokasi Kerja : <?=$namalokasi;?> <br>

<?php 
$tglAwal2 = $tanggalAwal;
$tglAkhir2 = $tanggalAkhir;
$awal = $tanggalAwal;
?>

<table border="1">
  <thead>
      <tr>
          <td align="center" width="35px">No</td>
          <td align="center" width="350px">NAMA PEGAWAI</td>
          <?php 
            while (strtotime($tanggalAwal) <= strtotime($tanggalAkhir)) {
                //cek hari
                $hari = namaHari($tanggalAwal); //Mendapatkan Nama Hari
                $tgl = substr($tanggalAwal, 8,2);
                $bln = substr($tanggalAwal, 5,2);
                //Cek jika sabtu Ahad
                if($hari =='Minggu'){
                $warna = 'bgcolor="red"';
                }else{
                $warna = '';
                }
                echo '<td align="center" '.$warna.'>'.$tgl.'<br>'.getblnPendek($bln).'</td>';
                $tanggalAwal = date ("Y-m-d", strtotime("+1 day", strtotime($tanggalAwal)));//looping tambah 1 date
            }
            ?>
      </tr>
  </thead>
  <tbody>
    <?php 
    $no = 1;
    foreach($karyawan as $kyr){
        echo ' <tr>
        <td>'.$no++.'</td>
        <td>'.$kyr->nama_lengkap.'</td>';

            while (strtotime($tglAwal2) <= strtotime($tglAkhir2)) {
                //cek hari
                $hari = namaHari($tglAwal2); //Mendapatkan Nama Hari
                //Cek jika sabtu Ahad
                if($hari =='Minggu'){
                    $warna = 'bgcolor="#ffdede"';
                }else{
                    $warna = '';
                }
                echo '<td align="center" '.$warna.'>'
                .substr(absenMasuk_jpc($tglAwal2, infoPegawai($kyr->id_karyawan)['id_karyawan']), 0,5).'<br>'
                .substr(absenPulang_jpc($tglAwal2, infoPegawai($kyr->id_karyawan)['id_karyawan']), 0,5).'</td>';
                $tglAwal2 = date ("Y-m-d", strtotime("+1 day", strtotime($tglAwal2)));//looping tambah 1 date
            }

            $tglAwal2 = $awal;

      echo '</tr>';
    }
    ?>
  </tbody>

