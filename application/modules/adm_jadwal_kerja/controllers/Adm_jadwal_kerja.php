<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_jadwal_kerja extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'adm_jadwal_kerja');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Adm_laporan_model','jadwal_kerja');
		// $this->load->model('Group/Group_model','group');
		// check_login_opd();
	}

	public function index()
	{
		$user_data['data_ref'] = $this->data_ref;
		$user_data['pegawai'] = $this->db->get('karyawan')->result();
		// $user_data['jam_kerja'] = $this->db->get('jam_kerja')->result();
		$user_data['jam_kerja'] = $this->db->get_where('jam_kerja','status >= 0')->result();
     	
     	$this->load->view('template/header');
		$this->load->view('index',$user_data);

	}

	public function copyjadwal()
	{
		$user_data['data_ref'] = $this->data_ref;
		// $user_data['pegawai'] = $this->db->get_where('pegawai', ['kode_skpd' => $kolok])->result();
		$this->db->order_by('urutan', 'ASC');
		$this->db->where(['kode_skpd' => $kolok]);
		$user_data['pegawai'] = $this->db->get('pegawai')->result();

		$user_data['jam_kerja'] = $this->db->get('jam_kerja')->result();
     	
     	$this->load->view('template/header');
		$this->load->view('index_copy',$user_data);

	}

	public function copy_proses()
	{

		$user_data['data_ref'] = $this->data_ref;

		$user_data['bulan'] = $this->input->post('bulan_acuan');
		$namaTable = 'jam_kerja_'.$this->input->post('bulan_acuan');

		$master = $this->db->query("SELECT * FROM $namaTable WHERE kolok = '$kolok'")->result();

		foreach ($master as $mstr) {
			$param = array(
					'nip' => $mstr->nip,
					'kolok' => $kolok,
					'id_jamkerja' => $mstr->id_jamkerja,
					'tgl_1' => $mstr->tgl_1,
					'tgl_2' => $mstr->tgl_2,
					'tgl_3' => $mstr->tgl_3,
					'tgl_4' => $mstr->tgl_4,
					'tgl_5' => $mstr->tgl_5,
					'tgl_6' => $mstr->tgl_6,
					'tgl_7' => $mstr->tgl_7,
					'tgl_8' => $mstr->tgl_8,
					'tgl_9' => $mstr->tgl_9,
					'tgl_10' => $mstr->tgl_10,
					'tgl_11' => $mstr->tgl_11,
					'tgl_12' => $mstr->tgl_12,
					'tgl_13' => $mstr->tgl_13,
					'tgl_14' => $mstr->tgl_14,
					'tgl_15' => $mstr->tgl_15,
					'tgl_16' => $mstr->tgl_16,
					'tgl_17' => $mstr->tgl_17,
					'tgl_18' => $mstr->tgl_18,
					'tgl_19' => $mstr->tgl_19,
					'tgl_20' => $mstr->tgl_20,
					'tgl_21' => $mstr->tgl_21,
					'tgl_22' => $mstr->tgl_22,
					'tgl_23' => $mstr->tgl_23,
					'tgl_24' => $mstr->tgl_24,
					'tgl_25' => $mstr->tgl_25,
					'tgl_26' => $mstr->tgl_26,
					'tgl_27' => $mstr->tgl_27,
					'tgl_28' => $mstr->tgl_28,
					'tgl_29' => $mstr->tgl_29,
					'tgl_30' => $mstr->tgl_30,
					'tgl_31' => $mstr->tgl_31
				);

			$nama_tableBaru = 'jam_kerja_'.$this->input->post('bulan_baru');
			$this->db->insert($nama_tableBaru, $param);

			// echo $mstr->nip;
		}
     	
  //    	$this->load->view('template/header');
		// $this->load->view('index_copy',$user_data);

	}

	public function tampil()
	{
		$data['kolok'] = $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$this->load->view('template/header');
		$this->load->view('history', $data);
	}

	public function hapus($bulan, $tahun, $id_karyawan)
	{
		$tabel = 'jam_kerja_'.$bulan;
		$this->db->delete($tabel, ['id_karyawan' => $id_karyawan]);

		redirect('adm_jadwal_kerja/detail/'.$bulan.'/'.$tahun);
	}


	public function detail($bulan, $tahun)
	{

		$kolok = $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$user_data['kolok'] = $kolok;
		$user_data['bulan'] = $bulan;
		$user_data['tahun'] = $tahun;

		$user_data['data_ref'] = $this->data_ref;

		$cabang = $this->input->post('cabang');
		$this->db->order_by('id_karyawan', 'ASC');
		$user_data['pegawai'] = $this->db->get('karyawan')->result();
     	
     	$this->load->view('template/header_kosong');
		$this->load->view('view_tampil_detail',$user_data);

	}

	public function kelompok()
	{
		$user_data['bulan'] = $this->input->post('bulan');
		$user_data['tahun'] = $this->input->post('tahun');
		$user_data['jamKer'] = $this->input->post('jam_kerja');
		$cabang = $this->input->post('cabang');
		$regu = $this->input->post('regu');
		$user_data['data_ref'] = $this->data_ref;

		if(!empty($regu)){
			$this->db->join('regu_anggota', 'regu_anggota.id_karyawan = karyawan.id_karyawan','left');
			$this->db->where('regu_anggota.id_regu="'.$regu.'"', null, false);
		}
		$this->db->join('jabatan', 'jabatan.id_jabatan = karyawan.id_karyawan', 'left');
		$user_data['pegawai'] = $this->db->get('karyawan')->result();
     	 
     	$this->load->view('template/header');
		$this->load->view('view_kelompok',$user_data);

	}


	public function kelompok_proses()
	{
		$bulan 		= $this->input->post('bulan');
		$tahun 		= $this->input->post('tahun');
		$namaTabel 	= 'jam_kerja_'.$bulan;
		$jamKer 	= $this->input->post('jam_kerja');
// 		$jumHari 	= cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		$jumHari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 
		$tgl_awal 	= '01'; //$tahun.'-'.$bulan.'-01';
      	$tgl_akhir 	= $jumHari;

		$pegawai 	= $this->db->query("SELECT * FROM karyawan")->result();
		$que = '';
		$quer = '';
		for ($i=1; $i < 32; $i++) { 
			if($this->input->post('che_'.$i) == ''){
				$jamKer[$i] = '0';
				$que = ', tgl_'.$i.' =>'. $jamKer[1];
			}else{
				$jamKer[$i] = $this->input->post('idJamker');
				$que = ', tgl_'.$i.' =>'. $jamKer[1];
			}
			// $jamKer[$i] = $this->input->post('che_'.$i);
			// $quer .= $quer.$que;
		}

			//cari id jam kerja untuk warna - Kelompok jamkerja
			// cek dulu
			if($this->input->post('kelompok') == ''){
				$besar 		= $this->db->query("SELECT MAX(id_jamkerja) as besar FROM $namaTabel")->row_array();
				$idJemKerjaSekarang = $besar['besar']+1;
			}else{
				$idJemKerjaSekarang = $this->input->post('kelompok');
			}
	

			foreach ($pegawai as $pgw) {

				// echo ' - '.$pgw->$kolom;
			$id_karyawan = $pgw->id_karyawan;
			$cek = $this->db->query("SELECT * FROM $namaTabel WHERE id_karyawan='$id_karyawan'")->num_rows();
			if(!$cek){
				$param = array(
					'id_karyawan' => $pgw->id_karyawan,
					'id_jamkerja' => $idJemKerjaSekarang,
					'tgl_1' => $jamKer[1],
					'tgl_2' => $jamKer[2],
					'tgl_3' => $jamKer[3],
					'tgl_4' => $jamKer[4],
					'tgl_5' => $jamKer[5],
					'tgl_6' => $jamKer[6],
					'tgl_7' => $jamKer[7],
					'tgl_8' => $jamKer[8],
					'tgl_9' => $jamKer[9],
					'tgl_10' => $jamKer[10],
					'tgl_11' => $jamKer[11],
					'tgl_12' => $jamKer[12],
					'tgl_13' => $jamKer[13],
					'tgl_14' => $jamKer[14],
					'tgl_15' => $jamKer[15],
					'tgl_16' => $jamKer[16],
					'tgl_17' => $jamKer[17],
					'tgl_18' => $jamKer[18],
					'tgl_19' => $jamKer[19],
					'tgl_20' => $jamKer[20],
					'tgl_21' => $jamKer[21],
					'tgl_22' => $jamKer[22],
					'tgl_23' => $jamKer[23],
					'tgl_24' => $jamKer[24],
					'tgl_25' => $jamKer[25],
					'tgl_26' => $jamKer[26],
					'tgl_27' => $jamKer[27],
					'tgl_28' => $jamKer[28],
					'tgl_29' => $jamKer[29],
					'tgl_30' => $jamKer[30],
					'tgl_31' => $jamKer[31]
				);

				//cek apakah pegawai tercentang
				if($this->input->post('pegawai_'.$id_karyawan) != ''){
					$this->db->insert($namaTabel, $param);
				}
				
			}else{
				//update karena jadwal sudah ada di table
				if($this->input->post('pegawai_'.$id_karyawan) != ''){
					for ($i=1; $i < 32; $i++) { 
						if($this->input->post('che_'.$i) == ''){
	
						}else{
							$jamKer[$i] = $this->input->post('idJamker');
							$this->db->update($namaTabel, ["tgl_$i" => $jamKer[$i]], ['id_karyawan' => $pgw->id_karyawan]);
						}
						// $jamKer[$i] = $this->input->post('che_'.$i);
						// $quer .= $quer.$que;
					}
				}
				
			}
		}

		// echo $jamKer[5];
		// var_dump($jamKer);
		redirect('adm_jadwal_kerja/tampil');
		
	}


	public function cek_jadwal(){

		$bulan 		= $this->input->post('bulan');
		$tahun 		= $this->input->post('tahun');
		$namaTabel 	= 'jam_kerja_'.$bulan;
		$jamKer 	= $this->input->post('jam_kerja');
// 		$jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		$jumHari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 

		$tgl_awal 	= '01'; //$tahun.'-'.$bulan.'-01';
      	$tgl_akhir 	= $jumHari;

		$kolok 		= $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$pegawai 	= $this->db->query("SELECT * FROM karyawan WHERE kode_skpd='$kolok'")->result();

		//Buat jam kerja ------------------------->
		// foreach ($pegawai as $pgw) {
		// 	$nip = $pgw->nip;
		// 	$cek = $this->db->query("SELECT * FROM $namaTabel WHERE nip='$nip'")->num_rows();
		// 	if(!$cek){
		// 		$param = array(
		// 			'nip' => $pgw->nip,
		// 			'kolok' => $pgw->kode_skpd,
		// 			'id_jamkerja' => $jamKer,
		// 			'tgl_1' => $jamKer,
		// 			'tgl_2' => $jamKer,
		// 			'tgl_3' => $jamKer,
		// 			'tgl_4' => $jamKer,
		// 			'tgl_5' => $jamKer,
		// 			'tgl_6' => $jamKer,
		// 			'tgl_7' => $jamKer,
		// 			'tgl_8' => $jamKer,
		// 			'tgl_9' => $jamKer,
		// 			'tgl_10' => $jamKer,
		// 			'tgl_11' => $jamKer,
		// 			'tgl_12' => $jamKer,
		// 			'tgl_13' => $jamKer,
		// 			'tgl_14' => $jamKer,
		// 			'tgl_15' => $jamKer,
		// 			'tgl_16' => $jamKer,
		// 			'tgl_17' => $jamKer,
		// 			'tgl_18' => $jamKer,
		// 			'tgl_19' => $jamKer,
		// 			'tgl_20' => $jamKer,
		// 			'tgl_21' => $jamKer,
		// 			'tgl_22' => $jamKer,
		// 			'tgl_23' => $jamKer,
		// 			'tgl_24' => $jamKer,
		// 			'tgl_25' => $jamKer,
		// 			'tgl_26' => $jamKer,
		// 			'tgl_27' => $jamKer,
		// 			'tgl_28' => $jamKer,
		// 			'tgl_29' => $jamKer,
		// 			'tgl_30' => $jamKer,
		// 			'tgl_31' => $jamKer
		// 		);
		// 		$this->db->insert($namaTabel, $param);
		// 	}
		// }


		//Table Hitung Bulanan ------------------------->

		$detail_jamKerja = $this->db->query("SELECT * FROM jam_kerja WHERE id_jamker='$jamKer'")->row_array();

        //cek apakah tabel bulan dimaksud sudah dibuat
        //$bulan = str_replace('0','',$data['bulan']);
        $namatable 			= 'jam_kerja_'.$bulan;
        $namatable_hitung 	= 'hitung_'.$bulan.'_2021';
        $row_hitung 		= "";


            //Jika sudah ada tabel bulanan
            //Cek di tabel proses apakah bulan dimaksud sudah diisi
            $pegawai = $this->db->get_where('pegawai', array('kode_skpd'=>$kolok))->result();
            foreach ($pegawai as $pgw){
            	
                    $nip          = $pgw->nip;
                    $nama          = $pgw->nama_pegawai;

                    //Input ke tabel jam kerja bulanan
                    //$this->db->query("INSERT INTO $namatable SET nip = '$nip', kolok='$kolok'");
                    //Input ke tabel hitung bulanan


                    if($tgl_awal >= '10'){

                    }else{
                      $tgl_awal = str_replace('0', '', $tgl_awal);
                    }

                    if($tgl_akhir >= '10'){

                    }else{
                      $tgl_akhir = str_replace('0', '', $tgl_akhir);
                    }
                    
                    $kolom = "";
                    $nama_kolom = "";
                    for($i=$tgl_awal;$i <= $tgl_akhir;$i++){

                      $tgl = $tahun.'-'.$bulan.'-'.$i;
                      $hari = namaHari($tgl);
                      $hariKecil = namaHariKecil($tgl);

                      $kolom .= "'".$jamKer."', ";
                      $nama_kolom .= "tgl_".$i.", ";
                      // $kolom = $kolom.', tgl_'.$i = '$jam_kerja';

                      //Insert hitungan per hari
                      

                      $jamMasuk = $detail_jamKerja[$hariKecil.'_m'];
                      $jamBatas = $detail_jamKerja[$hariKecil.'_batas'];
                      $jamPulang = $detail_jamKerja[$hariKecil.'_p'];
                      // $this->db->query("INSERT INTO $tabel_hitung SET KOLOK='$kolok', NIP='$nip', TANGGAL='$tgl', HARI='$hari', JMASUK='$jamMasuk', JPULANG='$jamPulang'");

                      $row_hitung .= "('".$kolok."', '".$nip."', '".$nama."', '".$tgl."', '".$hari."', '".$jamMasuk."', '".$jamBatas."', '".$jamPulang."'), ";
                      
                    }

                    $kolom = rtrim($kolom, ', ');
                    $a = "'$pgw->nip','$pgw->kode_skpd',".$kolom.','.$jamKer;
                    
                    @$kolom2 .= "(".$a."), ";
                }

                $nama_kolom = rtrim($nama_kolom, ', ');
                $query = "INSERT INTO $namatable (nip, kolok, $nama_kolom, id_jamkerja) VALUES ";
                $kolom2 = rtrim($kolom2, ', ');
                $row_hitung = rtrim($row_hitung, ', ');

                //query hitung
                $query_hitung = "INSERT INTO $namatable_hitung (KOLOK, NIP, NAMA, TANGGAL, HARI, JMASUK, BATAS_ABSEN,JPULANG) VALUES ";
                $asd = $query.$kolom2;

                // echo "<br>";

                $cc = $query_hitung.$row_hitung;

                $this->db->query($cc);

                $this->db->query($asd);


		if($this->input->post('jenis') == 'Kelompok'){
			redirect('adm_jadwal_kerja/kelompok/'.$bulan.'/'.$kolok.'/'.$jamKer.'/'.$tahun);
		}elseif($this->input->post('jenis') == 'Individu'){
			redirect('adm_jadwal_kerja/detail/'.$bulan.'/'.$kolok.'/'.$jamKer.'/'.$tahun);
		}
     	

	}


	public function ajax_update($tanggal, $nip, $status, $isiUncheck, $bulan){

		//cek Statu
		$kolom = 'tgl_'.$tanggal;
		$bulan	= $bulan;
		$namaTabel = 'jam_kerja_'.$bulan;
		$cek = $this->db->query("SELECT * FROM $namaTabel WHERE nip='$nip'")->row_array();


		$nilaiDefault = $cek['id_jamkerja'];

		if($status == 'true'){
			//isi data dengan nilai default
			$query = "UPDATE $namaTabel SET $kolom = '$nilaiDefault' WHERE nip='$nip'";
			$this->db->query($query);
		}else{
			//isi data dengan nilai pilihan 
			$query = "UPDATE $namaTabel SET $kolom = '$isiUncheck' WHERE nip='$nip'";
			$this->db->query($query);
		}

        echo json_encode(array("status" => TRUE));
    }


    public function get($nip){

        $item=$this->db->query("SELECT * FROM pegawai WHERE nip ='$nip' ")->row_array();

        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }











    public function buat($bulan, $tahun){

		$namaTabel 	= 'jam_kerja_'.$bulan;

// 		$jumHari 	= cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		$jumHari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 

		$tgl_awal 	= '01'; //$tahun.'-'.$bulan.'-01';
      	$tgl_akhir 	= $jumHari;

		$kolok 		= $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$pegawai 	= $this->db->query("SELECT * FROM pegawai WHERE kode_skpd='$kolok'")->result();


		//Table Hitung Bulanan ------------------------->
		$detail_jamKerja = $this->db->query("SELECT * FROM jam_kerja WHERE id_jamker='$jamKer'")->row_array();
        //cek apakah tabel bulan dimaksud sudah dibuat
        //$bulan = str_replace('0','',$data['bulan']);
        $namatable 			= 'jam_kerja_'.$bulan;
        $namatable_hitung 	= 'hitung_'.$bulan.'_2023';
        $row_hitung 		= "";


            //Jika sudah ada tabel bulanan
            //Cek di tabel proses apakah bulan dimaksud sudah diisi
            $pegawai = $this->db->get_where('pegawai', array('kode_skpd'=>$kolok))->result();
            foreach ($pegawai as $pgw) {
                    $nip          = $pgw->nip;
                    $nama          = $pgw->nama_pegawai;

                    if($tgl_awal >= '10'){

                    }else{
                      $tgl_awal = str_replace('0', '', $tgl_awal);
                    }

                    if($tgl_akhir >= '10'){

                    }else{
                      $tgl_akhir = str_replace('0', '', $tgl_akhir);
                    }
                    
                    $kolom = "";
                    $nama_kolom = "";
                    for($i=$tgl_awal;$i <= $tgl_akhir;$i++){

                      $tgl = $tahun.'-'.$bulan.'-'.$i;
                      $hari = namaHari($tgl);
                      $hariKecil = namaHariKecil($tgl);

                      $kolom .= "'".$jamKer."', ";
                      $nama_kolom .= "tgl_".$i.", ";
                      // $kolom = $kolom.', tgl_'.$i = '$jam_kerja';

                      //Insert hitungan per hari
                      

                      $jamMasuk = $detail_jamKerja[$hariKecil.'_m'];
                      $jamBatas = $detail_jamKerja[$hariKecil.'_batas'];
                      $jamPulang = $detail_jamKerja[$hariKecil.'_p'];
                      // $this->db->query("INSERT INTO $tabel_hitung SET KOLOK='$kolok', NIP='$nip', TANGGAL='$tgl', HARI='$hari', JMASUK='$jamMasuk', JPULANG='$jamPulang'");

                      $row_hitung .= "('".$kolok."', '".$nip."', '".$nama."', '".$tgl."', '".$hari."', '".$jamMasuk."', '".$jamBatas."', '".$jamPulang."'), ";
                      
                    }

                    $kolom = rtrim($kolom, ', ');
                    $a = "'$pgw->nip','$pgw->kode_skpd',".$kolom.','.$jamKer;
                    
                    @$kolom2 .= "(".$a."), ";
                }

                $nama_kolom = rtrim($nama_kolom, ', ');
                $query = "INSERT INTO $namatable (nip, kolok, $nama_kolom, id_jamkerja) VALUES ";
                $kolom2 = rtrim($kolom2, ', ');
                $row_hitung = rtrim($row_hitung, ', ');

                //query hitung
                $query_hitung = "INSERT INTO $namatable_hitung (KOLOK, NIP, NAMA, TANGGAL, HARI, JMASUK, BATAS_ABSEN,JPULANG) VALUES ";
                $asd = $query.$kolom2;

                // echo "<br>";

                echo $cc = $query_hitung.$row_hitung;

                $this->db->query($cc);

                $this->db->query($asd);


		if($this->input->post('jenis') == 'Kelompok'){
			redirect('adm_jadwal_kerja/kelompok/'.$bulan.'/'.$kolok.'/'.$jamKer.'/'.$tahun);
		}elseif($this->input->post('jenis') == 'Individu'){
			redirect('adm_jadwal_kerja/detail/'.$bulan.'/'.$kolok.'/'.$jamKer.'/'.$tahun);
		}
     	

	}



	public function isihitung($bulan, $tahun){

// 		$jumHari 	= cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		$jumHari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 
		$tgl_awal 	= '01'; //$tahun.'-'.$bulan.'-01';
      	$tgl_akhir 	= $jumHari;

		$kolok 		= $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$pegawai 	= $this->db->query("SELECT * FROM karyawan")->result();
 
		//Table Hitung Bulanan ------------------------->
		// $detail_jamKerja = $this->db->query("SELECT * FROM jam_kerja WHERE id_jamker='$jamKer'")->row_array();
        //cek apakah tabel bulan dimaksud sudah dibuat
        //$bulan = str_replace('0','',$data['bulan']);
        $namatable 			= 'jam_kerja_'.$bulan;
        $namatable_hitung 	= 'hitung_'.$bulan.'_2023';
        $row_hitung 		= "";


            //Jika sudah ada tabel bulanan
            //Cek di tabel proses apakah bulan dimaksud sudah diisi
			$query = "SELECT * FROM $namatable a LEFT JOIN karyawan b ON a.id_karyawan = b.id_karyawan";
            // $pegawai = $this->db->get_where('pegawai', array('kode_skpd'=>$kolok))->result();
            $pegawai = $this->db->query($query)->result();
            foreach ($pegawai as $pgw) {
                    $nip          = $pgw->id_karyawan;
                    $nama         = $pgw->nama_lengkap;

                    if($tgl_awal >= '10'){

                    }else{
                      $tgl_awal = str_replace('0', '', $tgl_awal);
                    }

                    if($tgl_akhir >= '10'){

                    }else{
                      $tgl_akhir = str_replace('0', '', $tgl_akhir);
                    }

					$detail_jamKerja = $this->db->query("SELECT * FROM jam_kerja WHERE id_jamker='11'")->row_array();
					$jamKer = '11';
                    
                    $kolom = "";
                    $nama_kolom = "";
                    for($i=$tgl_awal;$i <= $tgl_akhir;$i++){

                      $tgl = $tahun.'-'.$bulan.'-'.$i;
                      $hari = namaHari($tgl);
                      $hariKecil = namaHariKecil($tgl);

                      $kolom .= "'".$jamKer."', ";
                      $nama_kolom .= "tgl_".$i.", ";
                      // $kolom = $kolom.', tgl_'.$i = '$jam_kerja';

                      //Insert hitungan per hari
                      

                      $jamMasuk = $detail_jamKerja[$hariKecil.'_m'];
                      $jamBatas = $detail_jamKerja[$hariKecil.'_batas'];
                      $jamPulang = $detail_jamKerja[$hariKecil.'_p'];
                      // $this->db->query("INSERT INTO $tabel_hitung SET KOLOK='$kolok', NIP='$nip', TANGGAL='$tgl', HARI='$hari', JMASUK='$jamMasuk', JPULANG='$jamPulang'");

                      $row_hitung .= "('".$kolok."', '".$nip."', '".$nama."', '".$tgl."', '".$hari."', '".$jamMasuk."', '".$jamBatas."', '".$jamPulang."'), ";
                      
                    }

                    $kolom = rtrim($kolom, ', ');
                    $a = "'$pgw->id_karyawan',".$kolom.','.$jamKer;
                    
                    @$kolom2 .= "(".$a."), ";
                }

                $nama_kolom = rtrim($nama_kolom, ', ');
                $query = "INSERT INTO $namatable (nip, kolok, $nama_kolom, id_jamkerja) VALUES ";
                $kolom2 = rtrim($kolom2, ', ');
                $row_hitung = rtrim($row_hitung, ', ');

                //query hitung
                $query_hitung = "INSERT INTO $namatable_hitung (KOLOK, NIP, NAMA, TANGGAL, HARI, JMASUK, BATAS_ABSEN,JPULANG) VALUES ";
                // $asd = $query.$kolom2;

                // echo "<br>";
				//aktifkan disini jika menampilkan query
                $cc = $query_hitung.$row_hitung;

                $this->db->query($cc);

                // $this->db->query($asd);

				
				redirect('adm_jadwal_kerja/tampil');
		// if($this->input->post('jenis') == 'Kelompok'){
		// 	redirect('adm_jadwal_kerja/kelompok/'.$bulan.'/'.$kolok.'/'.$jamKer.'/'.$tahun);
		// }elseif($this->input->post('jenis') == 'Individu'){
		// 	redirect('adm_jadwal_kerja/detail/'.$bulan.'/'.$kolok.'/'.$jamKer.'/'.$tahun);
		// }
     	

	}


	


	public function set_wfo($bulan)	{

		$kolok = $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$bulan          = $bulan;
        $tahun          = date('Y');
        // $batasTanggal   = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
        $batasTanggal = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 
        // $kolok           = $this->encryption->decrypt($this->session->userdata('KOLOK'));
        $kolok          = $this->encryption->decrypt($this->session->userdata('KOLOK'));
        $tabelHitung    = 'hitung_'.$bulan.'_'.$tahun;

        //Jadikan WFH semua
        $this->db->query("UPDATE $tabelHitung SET WFO='0'");

        $pegawai        = $this->db->query("SELECT * FROM pegawai WHERE kode_skpd = '$kolok' ")->result();

        foreach ($pegawai as $pgw) {


	        for($i=1; $i<=$batasTanggal; $i++){

	        	$tgl        = sprintf("%02d", $i); // mendapatkan tanggal sesuai looping
                $tanggal    = $tahun.'-'.$bulan.'-'.$tgl; // tanggal lengkap

	        	$tabel = 'jam_kerja_'.$bulan;
	        	$jk = $this->db->query("SELECT * FROM $tabel WHERE nip='$pgw->nip'")->row_array();
	        	if($jk['tgl_'.$i] == '9'){
	        		$this->db->query("UPDATE $tabelHitung SET WFO='1' WHERE nip ='$pgw->nip' AND TANGGAL='$tanggal'");
	        	}
	        	
	        }
	    }
	}

	function kosongkan($bulan){
		$tahun = date('Y');
		$nama_tabel = 'hitung_'.$bulan.'_'.$tahun;
		$this->db->query("TRUNCATE TABLE $nama_tabel");
  
		$nama_tabel_jamKerja = 'jam_kerja_'.$bulan;
		$this->db->query("TRUNCATE TABLE $nama_tabel_jamKerja");
		
		redirect('adm_jadwal_kerja/tampil');
	}


}
