
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Setting Jadwal Kerja</h1>
            Bulan : <?=$bulan;?> - <?=$tahun;?><br>
            Kolok : <?=$kolok;?> <br>
            Jam Kerja : <?=$jamKer;?> <br>
            <form action="#" id="form" class="form-horizontal">
               <p>Pilih jadwal Untuk Uncheck :</p>
               <?php
               $jadwal = $this->db->query("SELECT * FROM jam_kerja")->result();
               foreach ($jadwal as $jwl) {
                 echo '<input type="radio" id="male" name="pilihan" value="'.$jwl->id_jamker.'">
                <label for="male">'.$jwl->nama_jamker.'</label>';
               }
               ?>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /.content-header -->


<table id="table" class=" table-bordered" cellspacing="0" >
  <thead>
      <tr>
          <td align="center">No</td>
          <td align="center">NAMA PEGAWAI</td>
          <?php 
          $tahun = date('Y');
          $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
          for ($i=1; $i <= $jumHari ; $i++) { 
            //cek hari
            if($i < 10){ $tgl = '0'.$i;}else{ $tgl = $i;} 
            $tanggal = $tahun.'-'.$bulan.'-'.$tgl; //Mendapatkan tanggal
            $hari = namaHari($tahun.'-'.$bulan.'-'.$tgl); //Mendapatkan Nama Hari
            //Cek jika sabtu Ahad
            if($hari == 'Sabtu' OR $hari =='Minggu'){
              $warna = 'bgcolor="red"';
            }else{
              $warna = '';
            }

            echo '<td align="center" '.$warna.'>'.$i.'</td>';
          }
          ?>
      </tr>
  </thead>
  
  <tbody>
    <form >
    <?php
    $no = 1;
    foreach ($pegawai as $pgw) { ?>
      <tr>
          <td width="35px" align="center"><?=$no++;?></td>
          <td width="250px"><?= strtoupper($pgw->nama_pegawai);?></td>
          <?php 

          for ($i=1; $i <= $jumHari ; $i++) { 
            //cek hari
            if($i < 10){ $tgl = '0'.$i;}else{ $tgl = $i;} 
            $tanggal = $tahun.'-'.$bulan.'-'.$tgl; //Mendapatkan tanggal
            $hari = namaHari($tahun.'-'.$bulan.'-'.$tgl); //Mendapatkan Nama Hari
            //Cek jika sabtu Ahad
            if($hari == 'Sabtu' OR $hari =='Minggu'){
              $warna = 'bgcolor="#f590a3"';
              $stt = '';
              $disable = 'disabled';
            }else{
              $warna = '';
              
              $disable = '';

              // Cari jadwal pegawai pada tanggal ini 
              $namaTabel  = 'jam_kerja_covid_'.$bulan;
              $nip        = $pgw->nip;
              $kolom      = 'tgl_'.$i;
              $query = "SELECT * FROM $namaTabel WHERE nip = '$nip' AND $kolom='$jamKer'";
              $jamPerTgl = $this->db->query($query)->num_rows(); 
              //cek Default jam Kerja
              if($jamPerTgl) {

                $stt = 'checked';
              }else{
                $stt = '';
              }
            }

            echo '<td width="35px" align="center" '.$warna.'>
            <input type="checkbox" id="myCheck_'.$i.'_'.$pgw->nip.'" onclick="myFunction('.$i.',\''.$pgw->nip.'\',\''.$bulan.'\')" name="che" value="2" '.$stt.' '.$disable.'>
            </td>';
          }
          ?>
      </tr>
    <?php } ?>
  </tbody>
</table>





<?php  $this->load->view('template/footer_kosong'); ?>



<script src="<?php echo base_url('assets/admin/plugins/select2/select2.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2-bootstrap.css') ?>">



<script type="text/javascript">


   function myFunction(id, nip, bulan)
   {
      var x = document.getElementById("myCheck_"+id+'_'+nip).checked;
      // var un = document.getElementById("male").val;
       var un = document.getElementsByName('pilihan'); 
       for(i = 0; i < un.length; i++) { 
                if(un[i].checked) 
                var isi = un[i].value; 
            } 
      // var un = $('[name="pilihan"]').val;

        $.ajax({
         url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update/')?>" + id + '/' + nip + '/' + x + '/' + isi + '/' + bulan,
         type: "GET",
         dataType: "JSON",
         success: function(data)
         {
            // + $('[name="che"]').val() 
            // alert(id + nip);
            // location.reload(); 
            Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil diupdate'
                   });

            // var x = document.getElementById("myCheck_"+id+'_'+nip).checked;
            // alert(x);

         },
         error: function (jqXHR, textStatus, errorThrown)
         {
             alert('Error get data from ajax');
         }
     });
   }

   // function edit(id)
   // {
   //     save_method = 'update';
   //     $('#form')[0].reset(); // reset form on modals
   //     $('.form-group').removeClass('has-error'); // clear error class
   //     $('.help-block').empty(); // clear error string
   //     $('#btnSave').text('Simpan'); //change button text
   //     $('#btnSave').attr('disabled',false); //set button enable 
    
   //     //Ajax Load data from ajax
   //     $.ajax({
   //         url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_edit/')?>/" + id,
   //         type: "GET",
   //         dataType: "JSON",
   //         success: function(data)
   //         {
   //             $('[name="id"]').val(data.id_pegawai);
   //             $('[name="nik"]').val(data.nip);
   //             $('[name="nama_pegawai"]').val(data.nama_pegawai);
   //             $('[name="nip_atasan"]').val(data.nip_atasan);
   //             $('[name="email"]').val(data.email);
   //             $('[name="no_wa"]').val(data.no_wa);
   //             $('[name="jenis"]').val(data.nama_golongan);
   //             $('[name="urutan"]').val(data.urutan);
   //             $('#modal-xl').modal('show'); // show bootstrap modal when complete loaded
   //             $('.modal-title').text('Edit Non Pegawai'); // Set title to Bootstrap modal title
    
   //         },
   //         error: function (jqXHR, textStatus, errorThrown)
   //         {
   //             alert('Error get data from ajax');
   //         }
   //     });
   // }

   
   // function save()
   // {
   //     $('#btnSave').text('Menyimpan...'); //change button text
   //     $('#btnSave').attr('disabled',true); //set button disable 
   //     var url;
    
   //     if(save_method == 'add') {
   //         url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
   //     } else {
   //         url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
   //     }
    
   //     // ajax adding data to database
   //     $.ajax({
   //         url : url,
   //         type: "POST",
   //         data: $('#form').serialize(),
   //         dataType: "JSON",
   //         success: function(data)
   //         {
    
   //             if(data.status) //if success close modal and reload ajax table
   //             {
   //                 $('#modal-xl').modal('hide');
   //                 reload_table();
   //                 Lobibox.notify('success', {
   //                     size: 'mini',
   //                     msg: 'Data berhasil Disimpan'
   //                 });
   //             }
   //             else
   //             {
   //                 for (var i = 0; i < data.inputerror.length; i++) 
   //                 {
   //                     $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
   //                     $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
   //                 }
   //             }
   //             $('#btnSave').text('Simpan'); //change button text
   //             $('#btnSave').attr('disabled',false); //set button enable 
    
    
   //         },
   //         error: function (jqXHR, textStatus, errorThrown)
   //         {
   //             alert('Error adding / update data');
   //             $('#btnSave').text('Simpan'); //change button text
   //             $('#btnSave').attr('disabled',false); //set button enable 
    
   //         }
   //     });
   // }

   

</script>





