  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Set Jadwal Kerja</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Setting</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

            <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10%">No</th>
                    <th width="40%">Bulan</th>
                    <th width="35%">Jumlah Pegawai</th>
                    <th width="15%">Detail</th>
                </tr>
            </thead>
            <tbody>
              <?php
              for ($i=1; $i < 13; $i++) { 
                $bln = sprintf("%02s", $i);
                $table = 'jam_kerja_covid_'.$bln;
                $jum = $this->db->query("SELECT COUNT(id_jamkerja_bulan) as j FROM $table WHERE kolok= '$kolok'")->row_array();
                echo '<tr>
                  <td>'.$i.'</td>
                  <td>'.getbln($i).'</td>
                  <td>'.$jum['j'].'</td>
                  <td><a href="'.base_url('adm_jadwal_kerja/detail/'.$bln.'/2021').'" class="btn btn-primary btn-sm">Detail Pegawai</a></td>
                </tr>';
              }
              ?>
                
            </tbody>
          </table>


        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>






<?php  $this->load->view('template/footer'); ?>



