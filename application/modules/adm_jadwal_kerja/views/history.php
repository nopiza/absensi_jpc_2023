 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Set Jadwal Kerja</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Setting Jadwal Kerja</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th width="5%">NO.</th>
                          <th width="15%">Bulan</th>
                          <th width="15%">Jam Kerja</th>
                          <th width="15%">Tabel Hitung</th>
                          <th width="50%">Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php 
                        $NO = 1;
                        $tahun = date('Y');
                        for($i=1;$i<13;$i++) { ?>
                        <tr>
                          <td class="py-1">
                            <?=$NO;?>
                          </td>
                          <td>
                            <?=getbln($i);?>
                          </td>
                          <?php
                           if($i < 10){
                              $bulan = '0'.$i;
                              $tabel = 'jam_kerja_'.$bulan;
                              $tabelHitung = 'hitung_'.$bulan.'_'.$tahun;
                            }else{
                              $bulan = $i;
                              $tabel = 'jam_kerja_'.$bulan;
                              $tabelHitung = 'hitung_'.$bulan.'_'.$tahun;
                            }
                            ?>
                          <td align="center"> 
                            <?php
                            //Hitung data di tabel jam kerja
                            $pgwJamKer = $this->db->query("SELECT * FROM $tabel")->num_rows();
                            echo $pgwJamKer;
                            ?>
                          </td>
                          <td align="center">
                            <?php
                            // $kalender = CAL_GREGORIAN;
                            $tahun = date('Y');
                            // $hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                            $hari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 

                            //Hitung data di tabel Hitung bulanan
                            $pgwHitung = $this->db->query("SELECT * FROM $tabelHitung")->num_rows();
                            echo $a = $pgwHitung / $hari;
                            ?>
                          </td>
                          <td>
                            <a href="<?php echo base_url('adm_jadwal_kerja/');?>" class="btn btn-sm btn-info" >Setting Jam Kerja</a>
                            <a href="<?php echo base_url('adm_jadwal_kerja/isihitung/'.$bulan.'/'.date('Y'));?>" class="btn btn-sm btn-warning" >Isi Tabel Bulanan</a>
                            <a href="<?php echo base_url('adm_jadwal_kerja/detail/'.$bulan.'/2022');?>" target="_blank" class="btn btn-sm btn-primary" >Detail Jadwal</a>
                            <a href="<?php echo base_url('adm_jadwal_kerja/kosongkan/'.$bulan);?>" class="btn btn-sm btn-danger"  onclick="return confirm('Anda yakin akan mengosongkan data?')">Kosongkan Jam kerja</a>
                            
                          </td>

                        </tr>

                        <?php  $NO++; } ?>
                      </tbody>


                    </table>

                    </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>






<?php  $this->load->view('template/footer'); ?>