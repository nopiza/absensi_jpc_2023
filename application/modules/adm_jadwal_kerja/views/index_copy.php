  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Copy Jadwal Kerja</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Setting</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <form action="<?=base_url('adm_jadwal_kerja/copy_proses');?>" id="form" class="form-horizontal" method="POST">

          <div class="form-group row">

            <label class="col-sm-3 control-label">Bulan Acuan</label>
            <div class="col-md-3">
              <select name="bulan_acuan" class="form-control" id="bulan_acuan" >
                <option value="">-- Pilih --</option>
                <option value="01">Januari</option>
                <option value="02">Februari</option>
                <option value="03">Maret</option>
                <option value="04">April</option>
                <option value="05">Mei</option>
                <option value="06">Juni</option>
                <option value="07">Juli</option>
                <option value="08">Agustus</option>
                <option value="09">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
              </select>
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-3 control-label">Bulan Baru</label>
            <div class="col-md-3">
              <select name="bulan_baru" class="form-control" id="bulan_baru" >
                <option value="01" <?=$a = date('m') == '01'?'selected':'';?> >Januari</option>
                <option value="02" <?=$a = date('m') == '02'?'selected':'';?> >Februari</option>
                <option value="03" <?=$a = date('m') == '03'?'selected':'';?> >Maret</option>
                <option value="04" <?=$a = date('m') == '04'?'selected':'';?> >April</option>
                <option value="05" <?=$a = date('m') == '05'?'selected':'';?> >Mei</option>
                <option value="06" <?=$a = date('m') == '06'?'selected':'';?> >Juni</option>
                <option value="07" <?=$a = date('m') == '07'?'selected':'';?> >Juli</option>
                <option value="08" <?=$a = date('m') == '08'?'selected':'';?> >Agustus</option>
                <option value="09" <?=$a = date('m') == '09'?'selected':'';?> >September</option>
                <option value="10" <?=$a = date('m') == '10'?'selected':'';?> >Oktober</option>
                <option value="11" <?=$a = date('m') == '11'?'selected':'';?> >November</option>
                <option value="12" <?=$a = date('m') == '12'?'selected':'';?> >Desember</option>
              </select>
              <span class="help-block"></span>
            </div>
          </div>


          <div class="form-group row">
            <label class="col-sm-3 control-label">Tahun</label>
            <div class="col-md-3">
              <select name="tahun" class="form-control" id="tahun" >
                <option value="2021">2021</option>
              </select>
              <span class="help-block"></span>
            </div>
          </div>



                <?php
                $kolokcabang = $this->db
                                  ->query('select * from master_skpd where KOLOK_UTAMA="'.$kolok.'" ')
                                  ->result_array();

                if(!empty($kolokcabang)){
                  ?>
                  <div class="form-group row">
                    <label class="col-sm-3 control-label">Pilih Bagian/UPT/Kelurahan</label>
                    <div class="col-md-3">
                    <div class="form-group row">
                      <label for=""></label>
                      <select class="form-control" name="cabang" id="">
                        <option value=''>Pilih Bagian/UPT/Kelurahan</option>
                        <?php
                          foreach($kolokcabang as $cabang){
                            ?>
                            <option value="<?=$cabang['KOLOK']?>"><?=$cabang['LOKASI']?></option>
                            <?php
                          }
                        ?>
                      </select>
                    </div>
                    <span class="help-block"></span>
                  </div>
                </div>
                  <?php
                }
              ?>



          <div class="form-group row">
            <label class="col-sm-3 control-label"></label>
            <div class="col-md-2">
              <button class="btn btn-primary btn-md" type="submit" >Proses</button>
            </div>
          </div>




                                                  
          </form>

          <div id="laporan"></div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>






<?php  $this->load->view('template/footer'); ?>



