  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Setting Jadwal Kerja berkelompok</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Setting | Bulan : <?=$bulan;?> - <?=$tahun;?> | Jam Kerja : <?=$jamKer;?> </h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

<form action="<?=base_url('adm_jadwal_kerja/kelompok_proses');?>" id="form" class="form-horizontal" method="POST">

<input type="hidden" value="<?=$bulan;?>" name="bulan" id="bulan"/>
<input type="hidden" value="<?=$tahun;?>" name="tahun" id="tahun"/>
<input type="hidden" value="<?=$jamKer;?>" name="idJamker" id="idJamker"/>
<input type="hidden" value="<?=$this->input->post('kelompok');;?>" name="kelompok" id="kelompok"/>

<table id="table" class=" table-bordered" cellspacing="0" >
  <thead>
      <tr>

          <?php 
          echo '<td align="center" >check all</td>';
          $tahun = date('Y');
        //   $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
          $jumHari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 
          for ($i=1; $i <= $jumHari ; $i++) { 
            //cek hari
            if($i < 10){ $tgl = '0'.$i;}else{ $tgl = $i;} 
            $tanggal = $tahun.'-'.$bulan.'-'.$tgl; //Mendapatkan tanggal
            $hari = namaHari($tahun.'-'.$bulan.'-'.$tgl); //Mendapatkan Nama Hari
            //Cek jika sabtu Ahad
            if($hari == 'Sabtu' OR $hari =='Minggu'){
              $warna = 'bgcolor="red"';
            }else{
              $warna = '';
            }

            echo '<td align="center" '.$warna.'>'.$i.'</td>';
          }
          ?>
      </tr>

      <tr>
          <?php 
          echo '<td width="35px" align="center" >
            <input type="checkbox" name="pilihsemuatgl" value="" id="pilihsemuatgl">
            </td>';
          $tahun = date('Y');
        //   $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
        $jumHari = date('t', mktime(0, 0, 0, $bulan, 1, $tahun)); 
          for ($i=1; $i <= $jumHari ; $i++) { 
            //cek hari
            if($i < 10){ $tgl = '0'.$i;}else{ $tgl = $i;} 
            $tanggal = $tahun.'-'.$bulan.'-'.$tgl; //Mendapatkan tanggal
            $hari = namaHari($tahun.'-'.$bulan.'-'.$tgl); //Mendapatkan Nama Hari
            //Cek jika sabtu Ahad
            if($hari == 'Sabtu' OR $hari =='Minggu'){
              //cek apakah shift
              if($jamKer == '12' OR $jamKer == '13' OR $jamKer == '14'){
                $warna = '';
                $disable = '';
              }else{
                $warna = 'bgcolor="red"';
                // $disable = 'disabled';
                $disable = '';
              }
              
            }else{
              $warna = '';
              $disable = '';
            }

              echo '<td width="35px" align="center" '.$warna.'>
            <input type="checkbox" name="che_'.$i.'" value="'.$i.'" '.$disable.' class="pilihtgl">
            </td>';
          }
          ?>
      </tr>

  </thead>

</table>

<br>
<br>
<h3 class="card-title">Data Pegawai
<br>
<div class="row">
<table id="table" class="table table-bordered" cellspacing="0" >
  <thead>
      <tr>
        <th>No</th>
        <th><input type="checkbox" id="pilihsemua" name="semua" value="" ></th>
        <th>Nama Pegawai</th>
        <th>Jabatan</th>
      </tr>
  </thead>
  
  <tbody>
    <form >
    <?php
    $no = 1;
    foreach ($pegawai as $pgw) { ?>
      <tr>
          <td width="5%" align="center"><?=$no++;?></td>
          <?php 
            echo '<td width="35px" align="left" '.$warna.'>
            <input type="checkbox" id="pegawai_'.$pgw->id_karyawan.'" name="pegawai_'.$pgw->id_karyawan.'" class="pilih"  value="'.$pgw->id_karyawan.'">
            </td>';
          ?>
          <td width="35%"><?= strtoupper($pgw->nama_lengkap);?></td>
          <td width="60%"><?= strtoupper($pgw->nama_jabatan);?></td>

      </tr>
    <?php } ?>
  </tbody>
</table>

<button type="submit" class="btn btn-primary btn-sm">Proses Jadwal</button>

</form>


<?php  $this->load->view('template/footer'); ?>

<script type="text/javascript">


   function myFunction(id, nip, bulan)
   {
      var x = document.getElementById("myCheck_"+id+'_'+nip).checked;
      // var un = document.getElementById("male").val;
       var un = document.getElementsByName('pilihan'); 
       for(i = 0; i < un.length; i++) { 
                if(un[i].checked) 
                var isi = un[i].value; 
            } 
      // var un = $('[name="pilihan"]').val;

        $.ajax({
         url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update/')?>" + id + '/' + nip + '/' + x + '/' + isi + '/' + bulan,
         type: "GET",
         dataType: "JSON",
         success: function(data)
         {
            // + $('[name="che"]').val() 
            // alert(id + nip);
            // location.reload(); 
            Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil diupdate'
                   });

            // var x = document.getElementById("myCheck_"+id+'_'+nip).checked;
            // alert(x);

         },
         error: function (jqXHR, textStatus, errorThrown)
         {
             alert('Error get data from ajax');
         }
     });
   }


$("#pilihsemua").click(function () {
  
  $('.pilih').attr('checked', this.checked);

});

$("#pilihsemuatgl").click(function () {
  
  $('.pilihtgl').attr('checked', this.checked);

});
   

</script>







        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>









