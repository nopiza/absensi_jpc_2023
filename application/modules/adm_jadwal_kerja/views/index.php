  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Set Jadwal Kerja</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Setting Jadwal Kerja</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-lg-6" >

            <form action="<?=base_url('adm_jadwal_kerja/kelompok');?>" id="form" class="form-horizontal" method="POST">
            <input type="hidden" value="" name="id" id="id"/>

            <div class="form-group row">
              <label class="col-sm-4 control-label">Bulan</label>
              <div class="col-md-6">
                <select name="bulan" class="form-control" id="bulan" >
                  <option value="01" <?php if(date('m') =='01') { echo "selected";} ?> >Januari</option>
                  <option value="02" <?php if(date('m') =='02') { echo "selected";} ?> >Februari</option>
                  <option value="03" <?php if(date('m') =='03') { echo "selected";} ?> >Maret</option>
                  <option value="04" <?php if(date('m') =='04') { echo "selected";} ?> >April</option>
                  <option value="05" <?php if(date('m') =='05') { echo "selected";} ?> >Mei</option>
                  <option value="06" <?php if(date('m') =='06') { echo "selected";} ?> >Juni</option>
                  <option value="07" <?php if(date('m') =='07') { echo "selected";} ?> >Juli</option>
                  <option value="08" <?php if(date('m') =='08') { echo "selected";} ?> >Agustus</option>
                  <option value="09" <?php if(date('m') =='09') { echo "selected";} ?> >September</option>
                  <option value="10" <?php if(date('m') =='10') { echo "selected";} ?> >Oktober</option>
                  <option value="11" <?php if(date('m') =='11') { echo "selected";} ?> >November</option>
                  <option value="12" <?php if(date('m') =='12') { echo "selected";} ?> >Desember</option>
                </select>
                <span class="help-block"></span>
              </div>
            </div>


            <div class="form-group row">
              <label class="col-sm-4 control-label">Tahun</label>
              <div class="col-md-6">
                <select name="tahun" class="form-control" id="tahun" >
                  <option value="2022" <?php if(date('Y') =='2022') { echo "selected";} ?> >2022</option>
                  <option value="2023" <?php if(date('Y') =='2023') { echo "selected";} ?> >2023</option>
                </select>
                <span class="help-block"></span>
              </div>
            </div>


            <div class="form-group row">
              <label class="col-sm-4 control-label">Jenis Jam Kerja</label>
              <div class="col-md-6">
                <select name="jam_kerja" class="form-control" id="jam_kerja" >
                <option value="">-- Pilih --</option>
                  <?php
                  foreach ($jam_kerja as $jamker) {
                    echo '<option value="'.$jamker->id_jamker.'">'.$jamker->nama_jamker.'</option>';
                  }
                  ?>
                </select>
                <span class="help-block"></span>
              </div>
            </div>

            <!-- <div class="form-group row">
              <label class="col-sm-4 control-label">Jenis Pengisian</label>
              <div class="col-md-6">
                <select name="jenis" class="form-control" id="jenis" >
                  <option value="Kelompok">Kelompok</option>
                  <option value="Individu">Individu</option>
                </select>
                <span class="help-block"></span>
              </div>
            </div> -->

            <?php
                  $kolokcabang = $this->db
                                    ->query('SELECT * FROM regu')
                                    ->result_array();

                  if(!empty($kolokcabang)){
                    ?>
                    <div class="form-group row">
                      <label class="col-sm-4 control-label">Regu / Kelompok</label>
                      <div class="col-md-6">
                        <select class="form-control" name="regu" id="regu">
                          <option value=''>-- Pilih Regu / Kelompok --</option>
                          <?php
                            foreach($kolokcabang as $regu){
                              ?>
                              <option value="<?=$regu['id_regu']?>"><?=$regu['nama_regu']?></option>
                              <?php
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                    <?php
                  }
                ?>




           

            <div class="form-group row">
              <label class="col-sm-4 control-label"></label>
              <div class="col-md-6">
                <button class="btn btn-primary btn-md" type="submit" >Proses</button>
              </div>
            </div>                                
            </form>

          </div>
          <div class="col-md-5">
            <table class="table table-striped">
                  <tr>
                    <td>No.</td>
                    <td>Jam Kerja</td>
                    <td>Masuk</td>
                    <td>Pulang</td>
                  </tr>
                  <?php 
                  $no = 1;
                  $jk = $this->db->query("SELECT * FROM jam_kerja order by id_jamker ASC")->result();
                  foreach($jk as $j){
                    echo '
                  <tr>
                    <td>'.$no++.'</td>
                    <td>'.$j->nama_jamker.'</td>
                    <td>'.$j->senin_m.'</td>
                    <td>'.$j->senin_p.'</td>
                  </tr>';
                  } ?>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>






<?php  $this->load->view('template/footer'); ?>



