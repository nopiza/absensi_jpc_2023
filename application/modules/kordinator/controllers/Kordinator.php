<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kordinator extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'kordinator');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kordinator_model','kordinator');
		$this->load->model('kordinator_anggota_model','anggota');
		check_login();
	}

	public function index()
	{

      	$user_data['data_ref'] = $this->data_ref;
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->kordinator->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = ' <a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$anggota = ' <a class="btn btn-xs btn-warning" href="'.base_url('kordinator/anggota/'.$post->id_karyawan).'">Anggota</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->nama_lokasi;
			$jum = $this->db->query("SELECT COUNT(*) as jumlah FROM kordinator WHERE id_karyawan_kordinator='$post->id_karyawan'")->row_array();
         	$row[] = $jum['jumlah'];

			//add html for action
			$row[] = $anggota.$link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->kordinator->count_all(),
						"recordsFiltered" => $this->kordinator->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}



	public function ajax_list_anggota($id)
	{
		$idkordinator = $id;
		$list = $this->anggota->get_datatables($idkordinator);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->idang."'".')"><i class="glyphicon glyphicon-trash"></i> Delete Anggota</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nkor;
         	$row[] = $post->nama_lokasi;
         	$row[] = $post->nang;
			$row[] = $post->no_reg;

			//add html for action
			$row[] = $link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->anggota->count_all(),
						"recordsFiltered" => $this->anggota->count_filtered($idkordinator),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function anggota($id)
	{
		$user_data['data_ref'] = $this->data_ref;

		$data = $this->db->query("SELECT * FROM karyawan WHERE id_karyawan='$id'")->row_array();
		$user_data['nama_karyawan'] = $data['nama_lengkap'];
		$user_data['no_reg'] = $data['no_reg'];
		$user_data['id_karyawan'] = $data['id_karyawan'];
		$user_data['idKordinator'] = $id;

		$this->load->view('template/header',$user_data);
		$this->load->view('anggota',$user_data);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->kordinator->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'nama_kordinator' 		=> $this->input->post('kordinator'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->kordinator->save($data);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_add_anggota()
	{

		$data = array(
			'id_karyawan_kordinator' 		=> $this->input->post('id_karyawan_kordinator'),
			'id_karyawan_anggota' 		=> $this->input->post('id_karyawan')
		);

		$this->db->insert('kordinator', $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'nama_kordinator' 		=> $this->input->post('kordinator'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->kordinator->update(array('id_karyawan' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}


	public function detail($idkordinator)
	{

      	$user_data['data_ref'] = $this->data_ref;
		$user_data['kordinator'] = $this->db->query("SELECT * FROM kordinator WHERE id_karyawan='$idkordinator'")->row_array(); 
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('detail',$user_data);

	}


	public function ajax_select(){
		$q =$this->input->get('q');
        $items=$this->db->query("SELECT id_karyawan,no_reg, nama_lengkap FROM karyawan 
		WHERE nama_lengkap like '%$q%'")->result_array();
        //output to json format
        echo json_encode($items);
    }

	public function get($no_reg){
        $item=$this->db->query("SELECT * FROM karyawan WHERE id_karyawan ='$no_reg' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


	public function ajax_delete_anggota($id)
	{
		$this->db->delete('kordinator',array('id_karyawan_anggota'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	
}
