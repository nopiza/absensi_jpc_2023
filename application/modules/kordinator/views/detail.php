  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Karyawan Lokasi : <?=@$lokasi['nama_lokasi'];?></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="15%">Nama</th>
                        <th width="15%">Departemen</th>
                        <th width="15%">Jabatan</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

    
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-5">
                                <input name="nama_lengkap" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. KTP</label>
                            <div class="col-md-5">
                                <input name="no_ktp" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">NIK / Badge Number</label>
                            <div class="col-md-3">
                                <input name="nik" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jenis Kelamin</label>
                            <div class="col-md-3">
                                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                  <option value="Laki-laki">Laki-laki</option>
                                  <option value="Perempuan">Perempuan</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-3">
                                <input name="tempat_lahir" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-3">
                                <input name="tanggal_lahir" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. HP</label>
                            <div class="col-md-3">
                                <input name="no_hp" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Alamat</label>
                            <div class="col-md-7">
                                <input name="alamat" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Agama</label>
                            <div class="col-md-3">
                                <select class="form-control" name="agama" id="agama">
                                  <option value="Islam">Islam</option>
                                  <option value="Kristen">Kristen</option>
                                  <option value="Protestan">Protestan</option>
                                  <option value="Hindu">Hindu</option>
                                  <option value="Budha">Budha</option>
                                  <option value="Konghucu">Konghucu</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Status Perkawinan</label>
                            <div class="col-md-3">
                                <select class="form-control" name="status_perkawinan" id="status_perkawinan">
                                  <option value="Lajang">Lajang</option>
                                  <option value="Menikah">Menikah</option>
                                  <option value="Duda">Duda</option>
                                  <option value="Janda">Janda</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. BPJS Kesehatan</label>
                            <div class="col-md-3">
                                <input name="bpjs_kesehatan" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. BPJS Ketenagakerjaan</label>
                            <div class="col-md-3">
                                <input name="bpjs_ketenagakerjaan" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jadwal MCU</label>
                            <div class="col-md-1">
                                <input name="range_mcu" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Insentif</label>
                            <div class="col-md-2">
                                <input name="insentif" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Jabatan</label>
                            <div class="col-md-2">
                                <input name="tunjangan_jabatan" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Site</label>
                            <div class="col-md-2">
                                <input name="tunjangan_site" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <hr>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Foto KTP</label>
                            <div class="col-md-6">
                                <input name="ktp" placeholder="" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Foto KK</label>
                            <div class="col-md-6">
                                <input name="kk" placeholder="" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Foto BPJS</label>
                            <div class="col-md-6">
                                <input name="bpjs" placeholder="" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <hr>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Departmen</label>
                            <div class="col-md-3">
                                <select class="form-control" name="department" id="department">
                                    <option value="0">-- Pilih --</option>
                                    <?php 
                                    $dept = $this->db->query("SELECT * FROM department")->result();
                                    foreach($dept as $dp){
                                        echo '<option value="'.$dp->id_department.'">'.$dp->nama_department.'</option>';
                                    }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <!-- <div class="form-group row">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-3">
                                <select class="form-control" name="jabatan" id="jabatan">
                                <option value="0">-- Pilih --</option>
                                    <?php 
                                    // $dept = $this->db->query("SELECT * FROM jabatan")->result();
                                    // foreach($dept as $dp){
                                    //     echo '<option value="'.$dp->id_jabatan.'">'.$dp->nama_jabatan.'</option>';
                                    // }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div> -->

                        <div class="form-group row">
                            <label class="control-label col-md-3">Lokasi</label>
                            <div class="col-md-3">
                                <select class="form-control" name="lokasi" id="lokasi">
                                <option value="0">-- Pilih --</option>
                                    <?php 
                                    $dept = $this->db->query("SELECT * FROM lokasi")->result();
                                    foreach($dept as $dp){
                                        echo '<option value="'.$dp->id_lokasi.'">'.$dp->nama_lokasi.'</option>';
                                    }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Masuk</label>
                            <div class="col-md-3">
                                <input name="tanggal_masuk" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal PHK</label>
                            <div class="col-md-3">
                                <input name="tanggal_phk" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Status Karyawan</label>
                            <div class="col-md-3">
                                <select class="form-control" name="stt_karyawan" id="stt_karyawan">
                                  <option value="Aktif">Aktif</option>
                                  <option value="Non Aktif">Non Aktif</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>



                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
</body>
</html>





<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";
  var idLokasi = "<?=$this->uri->segment(3);?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "paging": false, 
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_list_detail/" + idLokasi,
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });


      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });


   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
   }

  

</script>


