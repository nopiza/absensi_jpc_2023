<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kordinator_anggota_model extends CI_Model {

	var $table = 'kordinator';
	var $column_order = array('nama_lengkap',null); //set column field database for datatable orderable
	var $column_search = array('nama_lengkap'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('kk.id_karyawan' => 'ASC'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($idkordinator)
	{  
		
		$this->db->select('kk.nama_lengkap as nkor, l.nama_lokasi, kk.id_lokasi, ka.nama_lengkap as nang, kk.id_karyawan, kordinator.id_karyawan_anggota as idang, 
		ka.no_reg');
		$this->db->from($this->table)->where('kk.stt_admin', '1');
		
		
		$this->db->join('karyawan kk', 'kk.id_karyawan = kordinator.id_karyawan_kordinator', 'left');
		$this->db->join('lokasi l', 'kk.id_lokasi = l.id_lokasi', 'left');
		$this->db->join('karyawan ka', 'ka.id_karyawan = kordinator.id_karyawan_anggota', 'left');
		$this->db->where("kordinator.id_karyawan_kordinator = '$idkordinator'");

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($idkordinator)
	{

		$this->_get_datatables_query($idkordinator);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);

		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($idkordinator)
	{
		$this->_get_datatables_query($idkordinator);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id_lokasi',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{

		$this->db->where('id_lokasi', $id);
		$this->db->update($this->table);
	}

}
