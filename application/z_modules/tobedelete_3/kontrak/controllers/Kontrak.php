<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'kontrak');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kontrak_model','kontrak');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
		
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['department'] = $this->db->get('department')->result();
      	$user_data['jabatan'] = $this->db->get('jabatan')->result();
      	$user_data['lokasi'] = $this->db->get('lokasi')->result();
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->kontrak->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = ' <a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_kontrak."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a> ';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_kontrak."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
			$history = ' <a class="btn btn-xs btn-success" href="javascript:void(0)" onclick="history('."'".$post->no_ktp."'".')"> History Kontrak</a> ';
			
		
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap.'<br>'.$post->nik;
         	$row[] = tgl_indo($post->tgl_kontrak);
         	$row[] = tgl_indo($post->tgl_habis_kontrak);
         	$row[] = $post->nama_department;
			$hari = countDown($post->tgl_habis_kontrak);
			if($hari > 30){
				$warna = 'badge-secondary';
			}elseif($hari < 30 AND $hari >1){
				$warna = 'badge-warning';
			}elseif($hari < 0){
				$warna = 'badge-danger';
			}
         	$row[] = '<small class="badge '.$warna.'"><i class="far fa-clock"></i> '.$hari.' Hari</small>';
			// $kontrak = 12;
			// $query = "SELECT SUM(jumlah_hari) as Jum FROM kontrak WHERE id_karyawan = '$post->id_karyawan'";
			// $kontrakTerpakai = $this->db->query($query)->row_array();
         	// $row[] = $kontrak - $kontrakTerpakai['Jum'];
			//add html for action
			$row[] = $history.$link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->kontrak->count_all(),
						"recordsFiltered" => $this->kontrak->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	private function _do_upload(){

	      $config['upload_path']          = './assets/lampiran_kontrak/';
	      $config['allowed_types']        = 'gif|jpg|png|pdf';
	      $config['max_size']             = 1000; //set max size allowed in Kilobyte
	      $config['max_width']            = 3000; // set max width image allowed
	      $config['max_height']           = 3000; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('lampiran')) //upload and validate
	        {
	            $data['inputerror'][] = 'lampiran';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	}

	public function ajax_edit($id)
	{
		$data = $this->kontrak->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'id_karyawan' 			=> $this->input->post('karyawan'),
			'no_ktp' 				=> $this->input->post('no_ktp'),
			'tgl_kontrak' 			=> $this->input->post('tanggal_awal'),
			'tgl_habis_kontrak' 	=> $this->input->post('tanggal_akhir'),
			'tempat_penerimaan' 	=> $this->input->post('tempat_penerimaan'),
			'id_department' 		=> $this->input->post('department'),
			'id_jabatan' 			=> $this->input->post('jabatan'),
			'id_lokasi' 			=> $this->input->post('lokasi'),
			'gaji_pokok' 			=> str_replace('.','', $this->input->post('gaji_pokok')),
			'tunjangan_site' 		=> str_replace('.','', $this->input->post('tunjangan_site')),
			'tunjangan_jabatan' 	=> str_replace('.','', $this->input->post('tunjangan_jabatan')),
			'insentif' 				=> str_replace('.','', $this->input->post('insentif')),
			'tunjangan_other' 		=> str_replace('.','', $this->input->post('tunjangan_other')),
			'jum_upah' 				=> str_replace('.','', $this->input->post('jumlah_upah'))
		);

		if(!empty($_FILES['lampiran_kontrak']['name']))
		{
			$upload = $this->_do_upload();
			$data['lampiran_kontrak'] = $upload;
		}

		$this->db->insert('kontrak',$data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'id_karyawan' 			=> $this->input->post('karyawan'),
			'no_ktp' 				=> $this->input->post('no_ktp'),
			'tgl_kontrak' 			=> $this->input->post('tanggal_awal'),
			'tgl_habis_kontrak' 	=> $this->input->post('tanggal_akhir'),
			'tempat_penerimaan' 	=> $this->input->post('tempat_penerimaan'),
			'id_department' 		=> $this->input->post('department'),
			'id_jabatan' 			=> $this->input->post('jabatan'),
			'id_lokasi' 			=> $this->input->post('lokasi'),
			'gaji_pokok' 			=> str_replace('.','', $this->input->post('gaji_pokok')),
			'tunjangan_site' 		=> str_replace('.','', $this->input->post('tunjangan_site')),
			'tunjangan_jabatan' 	=> str_replace('.','', $this->input->post('tunjangan_jabatan')),
			'insentif' 				=> str_replace('.','', $this->input->post('insentif')),
			'tunjangan_other' 		=> str_replace('.','', $this->input->post('tunjangan_other')),
			'jum_upah' 				=> str_replace('.','', $this->input->post('jumlah_upah'))
		);


		// if($this->input->post('ktp')) // if remove photo checked
  //     {
  //        if(file_exists('./assets/lampiran_kontrak/'.$this->input->post('ktp')) && $this->input->post('ktp'))
  //           unlink('./assets/lampiran_kontrak/'.$this->input->post('ktp'));
  //        $data['ktp'] = '';
  //     }
 
    //   if(!empty($_FILES['ktp']['name']))
    //   {
    //      $upload = $this->_do_upload();
             
    //      //delete file
    //      $kontrak = $this->kontrak->get_by_id($this->input->post('id'));
    //      // var_dump($berita);
    //      if(file_exists('./assets/lampiran_kontrak/'.$kontrak->ktp) && $kontrak->ktp)
    //          unlink('./assets/lampiran_kontrak/'.$kontrak->ktp);
 
    //      $data['ktp'] = $upload;
    //   }


		$this->kontrak->update(array('id_kontrak' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('kontrak',array('id_kontrak'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function detailkontrak($noktp)
	{
		$query = "SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.no_ktp ='$noktp'" ;
		$kyr = $this->db->query($query)->row_array();
		$a = '<div class="form-group row">
		<label class="control-label col-md-3">Nama Lengkap</label>
		<div class="col-md-5">
		<input name="karyawan" value="'.$kyr['nama_lengkap'].'" class="form-control" type="text"  readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">No. KTP</label>
		<div class="col-md-5">
			<input name="no_ktp" id="no_ktp"  value="'.$kyr['no_ktp'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Lokasi</label>
		<div class="col-md-5">
			<input name="lokasi" id="lokasi"  value="'.$kyr['nama_lokasi'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Department</label>
		<div class="col-md-5">
			<input name="department" id="department"  value="'.$kyr['nama_department'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Jabatan</label>
		<div class="col-md-5">
			<input name="jabatan" id="jabatan"  value="'.$kyr['nama_jabatan'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>
	
	<table class="table table-bordered" >
              
		<thead>
				<tr>
					<th width="5%">No</th>
					<th width="15%">Tanggal Awal</th>
					<th width="15%">Tanggal Akhir</th>
					<th width="20%">Keterangan</th>
					<th width="10%">Lampiran</th>
			</tr>
			</thead>
			<tbody>';
		$b = '';
		$no=1;
		$query = "SELECT * FROM kontrak WHERE no_ktp='$noktp'";
		$komisi = $this->db->query($query)->result();
		foreach($komisi as $kms){ 
		
		$b .= '<tr>
				
			<td>'.$no++.'</td>
			<td>'.tgl_indo($kms->tgl_kontrak).'</td>
			<td>'.tgl_indo($kms->tgl_habis_kontrak).'</td>
			<td>'.$kms->tempat_penerimaan.'</td>
			<td>'.$kms->keterangan.'</td>
			<td><a href="'.base_url('assets/lampiran_kontrak/').'" target="_blank" class="">Lampiran Kotntrak</a></td>
		</tr>';
		}

		echo $a.$b.'</tbody>
		</table>';

	}


	public function ajax_select_karyawan(){
        $this->db->select('id_karyawan,nama_lengkap');
        $this->db->like('nama_lengkap',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('karyawan')->result_array();
        //output to json format
        echo json_encode($items);
    }

	public function get($id_karyawan){
        $item=$this->db->query("SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id_karyawan' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }





}
