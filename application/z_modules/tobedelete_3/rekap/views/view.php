  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Rekap Absensi</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <?php 
          date_default_timezone_set('Asia/Makassar');
          $bulan = date('m'); 
          $tahun = date('Y');
          $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
          ?>
          <h3 class="card-title">Absensi : <?=getbln($bulan);?> <?=$tahun;?></h3>
          <div class="card-tools">
            <a href="<?=base_url('rekap/lembur');?>" class="btn btn-info btn-sm">Rekap Lembur</a>&nbsp;
          </div>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th width="40%">Tanggal</th>
                  <th width="30%">Masuk</th>
                  <th width="30%">Pulang</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $year = date('Y');
              $month = date('m');
              for($i=1; $i <= $jumHari ; $i++) { 
                if($i < 10){
                  $tgl = '0'.$i;
                }else{ $tgl = $i;} 
                $tanggal = $year.'-'.$month.'-'.$tgl;?>
                <tr>
                    <td><b><?=namaHari($year.'-'.$month.'-'.$tgl);?></br>
                    </b><?php echo $i.' '.getbln($month).' '.$year;?></td>
                    <td><?=absenMasuk_jpc($tanggal, $id_karyawan);?></td>
                    <td><?=absenPulang_jpc($tanggal, $id_karyawan);?></td>

                </tr>
              <?php } ?>
            </tbody>
          </table>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

<?php  $this->load->view('template/footer'); ?>


<div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Large Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="peta"></div>

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<script type="text/javascript">

function detail(id, jenis, tanggal) {
  // var id;
  $('#modal-xl').modal('show'); // show bootstrap modal
  // $('#peta').load('http://detik.com'); 
  $("#peta").load('<?php echo base_url('adm_monitoring/peta/');?>' + id +'/'+ jenis+'/'+ tanggal);
  $('.modal-title').text('Detail absensi'); // Set Title to Bootstrap modal title
}


</script>