<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'jadwal');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jadwal_model','jadwal');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->jadwal->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$sekarang = tgl_now();

		foreach ($list as $post) {

			$link_detail = ' <a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Detail" onclick="detail('."'".$post->id_karyawan."'".')">Detail jadwal</a>';
			$manage = ' <a class="btn btn-xs btn-info" href="javascript:void(0)" onclick="manage('."'".$post->id_karyawan."'".')">Manage</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->nama_lokasi;
         	$row[] = $post->nama_department;
         	$row[] = $post->nama_jabatan;
			$query = "SELECT jenis_jadwal FROM jadwal WHERE id_karyawan = '$post->id_karyawan' AND tanggal_awal <='$sekarang' AND tanggal_akhir >= '$sekarang' ";
			$cek = $this->db->query($query)->num_rows();
			$jadwalTerpakai = $this->db->query($query)->row_array();
			if($cek){
				if($jadwalTerpakai['jenis_jadwal'] == 'On Duty'){
					$row[] = '<span class="badge bg-primary">'.$jadwalTerpakai['jenis_jadwal'].'</span>';
				}else if($jadwalTerpakai['jenis_jadwal'] == 'Off Duty'){
					$row[] = '<span class="badge bg-danger">'.$jadwalTerpakai['jenis_jadwal'].'</span>';
				}
			}else{
				$row[] = '<span class="badge bg-secondary">Belum Ditentukan</span>';
			}
			
         	

			//add html for action
			$row[] = $manage.$link_detail;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->jadwal->count_all(),
						"recordsFiltered" => $this->jadwal->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	private function _do_upload(){

	      $config['upload_path']          = './assets/lampiran_jadwal/';
	      $config['allowed_types']        = 'gif|jpg|png|pdf';
	      $config['max_size']             = 1000; //set max size allowed in Kilobyte
	      $config['max_width']            = 3000; // set max width image allowed
	      $config['max_height']           = 3000; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('lampiran')) //upload and validate
	        {
	            $data['inputerror'][] = 'lampiran';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	}

	public function ajax_edit($id)
	{
		$data = $this->jadwal->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'id_karyawan' 		=> $this->input->post('karyawan'),
			'jenis_jadwal' 		=> $this->input->post('jenis_jadwal'),
			'tanggal_awal' 		=> $this->input->post('tanggal_awal'),
			'tanggal_akhir' 	=> $this->input->post('tanggal_akhir')
		);

		$this->db->insert('jadwal',$data);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_add_detail()
	{

		$data = array(
			'id_karyawan' 		=> $this->input->post('id_karyawan'),
			'jenis_jadwal' 		=> $this->input->post('jenis_jadwal'),
			'tanggal_awal' 		=> $this->input->post('tanggal_awal'),
			'tanggal_akhir' 	=> $this->input->post('tanggal_akhir')
		);

		$this->db->insert('jadwal',$data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'nik' 				=> $this->input->post('nik'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			'tempat_lahir' 		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'alamat' 			=> $this->input->post('alamat'),
			'no_hp' 			=> $this->input->post('no_hp'),
			'agama' 			=> $this->input->post('agama'),
			'status_perkawinan' 	=> $this->input->post('status_perkawinan'),
			'bpjs_kesehatan' 		=> $this->input->post('bpjs_kesehatan'),
			'bpjs_ketenagakerjaan' 	=> $this->input->post('bpjs_ketenagakerjaan'),
			'insentif' 				=> $this->input->post('insentif'),
			'tunjangan_jabatan' 	=> $this->input->post('tunjangan_jabatan'),
			'tunjangan_site' 		=> $this->input->post('tunjangan_site'),
			'tanggal_masuk' 		=> $this->input->post('tanggal_masuk'),
			'tanggal_phk' 			=> $this->input->post('tanggal_phk'),
			'stt_jadwal' 			=> $this->input->post('stt_jadwal')
		);


		// if($this->input->post('ktp')) // if remove photo checked
  //     {
  //        if(file_exists('./assets/lampiran_jadwal/'.$this->input->post('ktp')) && $this->input->post('ktp'))
  //           unlink('./assets/lampiran_jadwal/'.$this->input->post('ktp'));
  //        $data['ktp'] = '';
  //     }
 
      if(!empty($_FILES['ktp']['name']))
      {
         $upload = $this->_do_upload();
             
         //delete file
         $jadwal = $this->jadwal->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_jadwal/'.$jadwal->ktp) && $jadwal->ktp)
             unlink('./assets/lampiran_jadwal/'.$jadwal->ktp);
 
         $data['ktp'] = $upload;
      }


      if(!empty($_FILES['kk']['name']))
      {
         $upload = $this->_do_upload_kk();
             
         //delete file
         $jadwal = $this->jadwal->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_jadwal/'.$jadwal->kk) && $jadwal->kk)
             unlink('./assets/lampiran_jadwal/'.$jadwal->kk);
 
         $data['kk'] = $upload;
      }

		$this->jadwal->update(array('id_jadwal' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}



	public function ajax_update_detail()
	{
		$data = array(
			'jenis_jadwal' 		=> $this->input->post('jenis_jadwal'),
			'tanggal_awal' 		=> $this->input->post('tanggal_awal'),
			'tanggal_akhir' 	=> $this->input->post('tanggal_akhir')
		);

		$this->db->update('jadwal', $data, array('id_jadwal' => $this->input->post('id')));
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('jadwal',array('id_jadwal'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function detailjadwal($id)
	{
		$tahun = '2022';
		$query = "SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id'" ;
		$kyr = $this->db->query($query)->row_array();
		$a = '<div class="form-group row">
		<label class="control-label col-md-3">Nama Lengkap</label>
		<div class="col-md-5">
		<input name="karyawan" value="'.$kyr['nama_lengkap'].'" class="form-control" type="text"  readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Lokasi</label>
		<div class="col-md-5">
			<input name="lokasi" id="lokasi"  value="'.$kyr['nama_lokasi'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Department</label>
		<div class="col-md-5">
			<input name="department" id="department"  value="'.$kyr['nama_department'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Jabatan</label>
		<div class="col-md-5">
			<input name="jabatan" id="jabatan"  value="'.$kyr['nama_jabatan'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>
	<br>
	<hr>
	<br>';

	$b = '<table class="kalender" border="1">
    <tr>
        <td valign="top">'.kalender(01,$tahun, $id).'</td>
        <td valign="top">'.kalender(02,$tahun, $id).'</td>
        <td valign="top">'.kalender(03,$tahun, $id).'</td>
        <td valign="top">'.kalender(04,$tahun, $id).'</td>
    </tr>
    <tr>
        <td valign="top">'.kalender(05,$tahun, $id).'</td>
        <td valign="top">'.kalender(06,$tahun, $id).'</td>
        <td valign="top">'.kalender(07,$tahun, $id).'</td>
        <td valign="top">'.kalender(8,$tahun, $id).'</td>
    </tr>
    <tr>
        <td valign="top">'.kalender(9,$tahun, $id).'</td>
        <td valign="top">'.kalender(10,$tahun, $id).'</td>
        <td valign="top">'.kalender(11,$tahun, $id).'</td>
        <td valign="top">'.kalender(12,$tahun, $id).'</td>
    </tr>
</table>
<style>
*{margin:0; padding: 0;}

    table .kalender{padding: 10px; margin:auto; font-size: 24px}
    h1{text-align: center}
    .april{padding-top: 50px}
    .juli{padding-top: 50px;}
    .des{padding-top: 50px;}
    caption{background: black; color:white;}
    .header{
        background: gray; 
        color:white;
        font-size: 14px;
        text-align: center;
        border-width: 1px;
        border-color: #FF00EE ;
    }
    .day{
        background: #f0f0f0;
        text-align: center; 
        padding:8px;
        font-size: 12px;
    }

    .holiday, .holiday a{
        color:white;
        background: #CC0000;
        text-align: center; 
        padding:8px;
        font-size: 12px;
    }
	.sekarang{
        color:white;
        background: #40E0D0;
        text-align: center; 
        padding:8px;
        font-size: 12px;
    }
	.onduty{
        color:white;
        background: #f0c51f;
        text-align: center; 
        padding:8px;
        font-size: 12px;
    }
	.offduty{
        color:white;
        background: #b70000;
        text-align: center; 
        padding:8px;
        font-size: 12px;
    }
    table .kalender td{
        	padding: 15px;
         border-width: 1px;

    }
</style>';

		echo $a.$b.'-';

	}


	public function ajax_select_karyawan(){
        $this->db->select('id_karyawan,nama_lengkap');
        $this->db->like('nama_lengkap',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('karyawan')->result_array();
        //output to json format
        echo json_encode($items);
    }

	public function get($id_karyawan){
        $item=$this->db->query("SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id_karyawan' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


	public function detailduty($id)
	{
		$query = "SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id'" ;
		$kyr = $this->db->query($query)->row_array();
		$a = '<form id="form_detail">
		<div class="form-group row">
		<label class="control-label col-md-3">Nama Lengkap</label>
		<div class="col-md-5">
		<input name="karyawan" value="'.$kyr['nama_lengkap'].'" class="form-control" type="text"  readonly>
		<input name="id_karyawan" value="'.$kyr['id_karyawan'].'" class="form-control" type="text"  readonly>
		<input name="id" value="" class="form-control" type="text">
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Lokasi</label>
		<div class="col-md-5">
			<input name="lokasi" id="lokasi"  value="'.$kyr['nama_lokasi'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Department</label>
		<div class="col-md-5">
			<input name="department" id="department"  value="'.$kyr['nama_department'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Jabatan</label>
		<div class="col-md-5">
			<input name="jabatan" id="jabatan"  value="'.$kyr['nama_jabatan'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Jenis Jadwal</label>
		<div class="col-md-5">
			<select name="jenis_jadwal" id="" class="form-control">
				<option value="Off Duty">Off Duty</option>
				<option value="On Duty">On Duty</option>
			</select>
			<span class="help-block"></span>
		</div>
	</div>


	<div class="form-group row">
		<label class="control-label col-md-3">Tanggal Awal</label>
		<div class="col-md-3">
			<input name="tanggal_awal" id="tanggal_awal" placeholder="" class="form-control" type="date">
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Tanggal Akhir</label>
		<div class="col-md-3">
			<input name="tanggal_akhir" id="tanggal_akhir" placeholder="" class="form-control" type="date">
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3"></label>
		<div class="col-md-3">
			<button type="button" id="btnSave" onclick="simpandetail()" class="btn btn-primary">Simpan Baru</button>
			<button type="button" id="btnUpdate" onclick="updatedetail()" class="btn btn-warning">Update</button>
			<span class="help-block"></span>
		</div>
	</div>';

	$b = '';
	echo $a.$b;

	}


	public function tabelDetail($id)
	{
		$a = '<table class="table table-bordered" >
              
		<thead>
				<tr>
					<th width="5%">No</th>
					<th width="15%">Jenis Jadwal</th>
					<th width="15%">Tanggal Awal</th>
					<th width="15%">Tanggal Akhir</th>
					<th width="5%">Jumlah</th>
					<th width="5%">#</th>
			</tr>
			</thead>
			<tbody>';
		$b = '';
		$no=1;
		$query = "SELECT * FROM jadwal WHERE id_karyawan='$id'";
		$jadwal = $this->db->query($query)->result();
		foreach($jadwal as $jwl){ 
		
		$b .= '<tr>
				
			<td>'.$no++.'</td>
			<td>'.$jwl->jenis_jadwal.'</td>
			<td>'.tgl_indo($jwl->tanggal_awal).'</td>
			<td>'.tgl_indo($jwl->tanggal_akhir).'</td>
			<td>'.$jwl->jumlah_hari.'</td>
			<td>
				<a class="btn btn-xs btn-warning" href="javascript:void(0)" onclick="edit('."'".$jwl->id_karyawan."'".')">Edit</a>
				<a class="btn btn-xs btn-danger" href="javascript:void(0)" onclick="hapus('."'".$jwl->id_jadwal."'".')">Hapus</a>
			</td>
		</tr>';
		}

		echo $a.$b.'</tbody>
		</table>';


	}



}
