<?php


// echo kalender(01,2019);

?>














<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Jadwal Kerja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Jadwal Kerja</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Jadwal Kerja</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-info btn-sm" onclick="add()"><i class="fa fa-plus"></i> Tambah Jadwal</a>&nbsp;
                <button class="btn btn-default btn-sm" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              </div>
              <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="20%">Nama Karyawan</th>
                        <th width="15%">Lokasi</th>
                        <th width="20%">Departemen</th>
                        <th width="20%">Jabatan</th>
                        <th width="10%">Status Hari ini</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

    
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-5">
                            <input name="karyawan" placeholder="" class="form-control" type="text" id="karyawan">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Lokasi</label>
                            <div class="col-md-5">
                                <input name="lokasi" id="lokasi" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Department</label>
                            <div class="col-md-5">
                                <input name="department" id="department" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-5">
                                <input name="jabatan" id="jabatan" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Jenis Jadwal</label>
                            <div class="col-md-5">
                                <select name="jenis_jadwal" id="" class="form-control">
                                    <option value="Off Duty">Off Duty</option>
                                    <option value="On Duty">On Duty</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Awal</label>
                            <div class="col-md-3">
                                <input name="tanggal_awal" id="tanggal_awal" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Akhir</label>
                            <div class="col-md-3">
                                <input name="tanggal_akhir" id="tanggal_akhir" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jumlah Hari</label>
                            <div class="col-md-1">
                                <input name="jumlah_hari" id="jumlah_hari" placeholder="" class="form-control" type="text" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>



                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->






<div class="modal fade bd-example-modal-xl" id="modal_form_manage" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog modal-xl ">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="modalLabel"></h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      </div>

		  <div class="modal-body">
            <div  id="manage_duty"></div>
          <div id="tableDetail">---</div>
          </div>
    </div>
  </div>
</div>



<div class="modal fade bd-example-modal-xl" id="detail_cuti" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog modal-xl ">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="modalLabel"></h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      </div>

		  <div class="modal-body" id="lampiran">


	    </div>
    </div>
  </div>
</div>


</body>
</html>





<?php  $this->load->view('template/footer'); ?>

<script src="<?php echo base_url('assets/admin/plugins/select2/select2.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2-bootstrap.css') ?>">

<script type="text/javascript">

var url_apps = "<?=base_url();?>"

$(document).ready(function () {
    // $('#btnUpdate').button('hide');
    
//----->
//Ambil semua data customer untuk select 2
  $("#karyawan").select2({
    ajax: {
      url: url_apps+'cuti/ajax_select_karyawan',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params, // search term
        };
      },
      results: function (data, params) {
        console.log(data);
        return {
            results: $.map(data, function (item) {
                return {
                    text: item.nama_lengkap,
                    id: item.id_karyawan
                }
            })
        };
      },
      cache: true
    },
    minimumInputLength: 1,
  });  


  $('#karyawan').on('change', function() {
  var idSiswa = $(this).val();
  $.ajax({
    url: url_apps + 'cuti/get/' + $(this).val(),
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    //alert(data.ALAMAT);
    $('#department').val(data.nama_department);
    $('#lokasi').val(data.nama_lokasi);
    $('#jabatan').val(data.nama_jabatan);
    
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
});



});  
</script>







<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });


      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });

   function add()
   {
       save_method = 'add';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
       $('#modal_form').modal('show'); // show bootstrap modal
       $('.modal-title').text('Input Jadwal Kerja Karyawan'); // Set Title to Bootstrap modal title
       $('#photo-preview').hide(); // hide photo preview modal
        // $('#label-photo').text('Upload Photo'); // label photo upload
   }

   function edit(id)
   {
       save_method = 'update';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
    
       //Ajax Load data from ajax
       $.ajax({
           url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_edit/')?>/" + id,
           type: "GET",
           dataType: "JSON",
           success: function(data)
           {
               $('[name="id"]').val(data.id_jadwal);
               $('[name="jenis_jadwal"]').val(data.jenis_jadwal);
               $('[name="tanggal_awal"]').val(data.tanggal_awal);
               $('[name="tanggal_akhir"]').val(data.tanggal_akhir);

            //    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
               $('.modal-title').text('Edit Jadwal Kerja'); // Set title to Bootstrap modal title
              
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error get data from ajax');
           }
       });
   }

   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
   }

   function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
       }
    
       // ajax adding data to database
       var formData = new FormData($('#form')[0]);
       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   $('#modal_form').modal('hide');
                   reload_table();
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }









   function simpandetail()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add_detail')?>";
       var urlpanggil = "<?=base_url();?>";

    
       // ajax adding data to database
       var formData = new FormData($('#form_detail')[0]);
       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                //    $('#modal_form').modal('hide');
                   reload_table();
                $('#tableDetail').load(urlpanggil + 'jadwal/tabelDetail/'+idJadwal );
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }




   function updatedetail()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
       var urlpanggil = "<?=base_url();?>";
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update_detail')?>";
       }
    
       // ajax adding data to database
       var formData = new FormData($('#form_detail')[0]);
       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                //    $('#modal_form').modal('hide');
                //    reload_table();
                    $('#tableDetail').load(urlpanggil + 'jadwal/tabelDetail/'+idJadwal );
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }

   function hapus(id){
    var urlpanggil = "<?=base_url();?>";
    $.confirm({
      title: 'Confirm!',
      content: 'Apakah anda yakin menghapus data ini ?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_delete/" + id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  reload_table();
                $('#tableDetail').load(urlpanggil + 'jadwal/tabelDetail/'+idJadwal );
                  Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Dihapus'
                   });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }

</script>


<script type="text/javascript">
  var url = "<?=base_url();?>";

  function detail(id){
      $('#lampiran').load(url + 'jadwal/detailjadwal/'+id );
      $('#detail_cuti').modal('show'); // show bootstrap modal
      $('.modal-title').text('Detail Jadwal Kerja Tahun  : ' + <?=date('Y');?>); // Set Title to Bootstrap modal title

   }


   function manage(id){
        idJadwal = id;
        save_method = 'update';
      $('#manage_duty').load(url + 'jadwal/detailduty/'+id );
      $('#tableDetail').load(url + 'jadwal/tabelDetail/'+id );
      $('#modal_form_manage').modal('show'); // show bootstrap modal
      $('.modal-title').text('Detail Jadwal Kerja Tahun  : ' + <?=date('Y');?>); // Set Title to Bootstrap modal title
    //   document.getElementById("btnUpdate").style.display='none';
      $('#btnUpdate').hide();
      
   }


    $(document).ready(function() {
    $('#tanggal_awal, #tanggal_akhir').on('change textInput input', function () {
        if ( ($("#tanggal_awal").val() != "") && ($("#tanggal_akhir").val() != "")) {
            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            var firstDate = new Date($("#tanggal_awal").val());
            var secondDate = new Date($("#tanggal_akhir").val());
            var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) / (oneDay))); 
            $("#jumlah_hari").val(diffDays + 1);
             
        }
    });
});
   
</script>