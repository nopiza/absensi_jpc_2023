  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Rekap Slip Gaji</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <?php 

          date_default_timezone_set('Asia/Makassar');
          
          $bulan = date('m'); 
          $tahun = date('Y');
          $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

          ?>
          <h3 class="card-title">Rekap Data Slip Gaji</h3>

        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th width="40%">Periode Gaji</th>
                  <th width="30%">Lampiran</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $id = $this->encryption->decrypt($this->session->userdata('id_karyawan'));
              $slip = $this->db->query("SELECT * FROM slip_gaji WHERE id_karyawan='$id'")->result();
              foreach($slip as $sl){
                echo '<tr>
                        <td></td>
                        <td></td>
                    </tr>';
              }
              ?>
            </tbody>
          </table>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

<?php  $this->load->view('template/footer'); ?>