  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Izin Karyawan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboardkaryawan'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Riwayat Izin </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
              <div class="card-tools">
                <a href="<?=base_url('izin/pengajuan');?>" class="btn btn-info btn-sm" ><i class="fa fa-plus"></i> Ajukan Izin</a>&nbsp;
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <tbody>
                    <?php 
                    
                    $ID = $this->encryption->decrypt($this->session->userdata('id_karyawan'));
                    $ijin = $this->db->query("SELECT * FROM pengajuan_izin WHERE id_karyawan='$ID'")->result();
                    foreach($ijin as $j){
                        if($j->status_izin == '0'){
                            $status = '<span class="badge badge-danger">Menunggu Persetujuan</span>';
                        }else if($j->status_izin == '1'){
                            $status = '<span class="badge badge-warning">Disetujui Atasan</span>';
                        }else if($j->status_izin == '2'){
                            $status = '<span class="badge badge-success">Disetujui HRD</span>';
                        }
                        echo '<tr>
                        <td>
                        <b>'.tgl_indo($j->tanggal).'</b><br>
                        Jenis Pengajuan : '.$j->jenis_izin.'<br><hr>
                        '.$j->deskripsi.'<br><hr>
                        '.$status.'
                        </td>
                    </tr>';
                    }
                    ?>
                    
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

</body>
</html>





<?php  $this->load->view('template/footer'); ?>