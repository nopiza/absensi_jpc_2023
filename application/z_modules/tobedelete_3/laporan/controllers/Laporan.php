<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'transaksi');

	public function __construct()
	{
		parent::__construct();
		check_login();
	}

	public function index()
	{
      	$user_data['title'] = 'Laporan';
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('laporan_index',$user_data);
	}


	public function tampil()
	{
      	$user_data['title'] = 'Laporan';
      	$this->load->view('template/header_kosong',$user_data);
		$this->load->view('tampil_laporan',$user_data);
	}

	public function cetak_excel_kontrak($awal='', $akhir='', $lokasi='')
	{
		$q = '';
		if($lokasi != null){
			$q = "AND karyawan.id_lokasi='$lokasi' ";
		}
		
      	$user_data['title'] = 'Laporan';
		$query = "SELECT * FROM kontrak 
		LEFT JOIN karyawan ON kontrak.nik=karyawan.nik
		LEFT JOIN jabatan ON karyawan.id_jabatan=jabatan.id_jabatan 
		LEFT JOIN lokasi ON lokasi.id_lokasi=karyawan.id_lokasi 
		WHERE kontrak.tgl_habis_kontrak >= '$awal' AND kontrak.tgl_habis_kontrak <= '$akhir' AND kontrak.nik <> '' $q
      	ORDER BY kontrak.tgl_habis_kontrak ASC";
		$user_data['kontrak'] = $this->db->query($query)->result();
		$user_data['awal'] = $awal;
		$user_data['akhir'] = $akhir;
		$user_data['lokasi'] = $lokasi;
      	$this->load->view('template/header_kosong',$user_data);
		$this->load->view('cetak_excel_kontrak',$user_data);
	}


	public function cetak_excel_mcu($awal='', $akhir='', $lokasi='')
	{
		
		$q = '';
		if($lokasi != null){
		  $q = "AND karyawan.id_lokasi='$lokasi' ";
		}
		$query = "SELECT *, mcu.nik as mcunik  FROM mcu 
		LEFT JOIN karyawan ON mcu.nik=karyawan.nik
		LEFT JOIN kontrak ON kontrak.nik=karyawan.nik 
		LEFT JOIN jabatan ON karyawan.id_jabatan=jabatan.id_jabatan 
		LEFT JOIN lokasi ON lokasi.id_lokasi=karyawan.id_lokasi 
		WHERE mcu.tgl_expired_mcu >= '$awal' AND mcu.tgl_expired_mcu <= '$akhir' AND mcu.nik <> '' $q 
		ORDER BY mcu.tgl_expired_mcu ASC";
		$user_data['kontrak'] = $this->db->query($query)->result();
		$user_data['awal'] = $awal;
		$user_data['akhir'] = $akhir;
		$user_data['lokasi'] = $lokasi;
      	$this->load->view('template/header_kosong',$user_data);
		$this->load->view('cetak_excel_mcu',$user_data);
	}

	public function cetak_pdf_kontrak()
	{

		$query = "SELECT * FROM transaksi WHERE transaksi_tanggal >= '$awal' AND transaksi_tanggal <= '$akhir' AND transaksi_jenis= '$jen'";
		$data = $this->db->query($query)->result();
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Pengeluaran');
        $this->mypdf->SetFont('Arial','B',14);
		$this->mypdf->SetX(25);
		$this->mypdf->MultiCell(160,5, 'LAPORAN PENGELUARAN' ,0,'C',false);
		$this->mypdf->SetFont('Arial','',10);
		$this->mypdf->Cell(190,7,'Periode : '.tgl_indo($awal).' - '.tgl_indo($akhir),0,1,'C');
		$y = $this->mypdf->GetY() + 2;
		$this->mypdf->Line(10, $y, 200, $y);
		
		
        $this->mypdf->Ln();

		$this->mypdf->SetFont('Arial','',8);
		$this->mypdf->Cell(10,10,'NO.',1,0,'C');
		$this->mypdf->Cell(30,10,'TANGGAL',1,0,'C');
		$this->mypdf->Cell(80,10,'DESKRIPSI',1,0,'C');
		$this->mypdf->Cell(40,10,'JENIS / KETERANGAN',1,0,'C');
		$this->mypdf->Cell(30,10,'JENIS',1,1,'C');

		$no 		= 1;
		$this->mypdf->SetFont('Arial','',8);
		// $detail =$this->db->query($query)->result();
		foreach ($data as $dtl) {
			
			$bar = $this->mypdf->GetY();
			$this->mypdf->MultiCell(10,7, $no++ ,1,'C',false);
			$this->mypdf->SetY($bar);
			$this->mypdf->SetX(20);
			$this->mypdf->Cell(30,7,tgl_indo($dtl->transaksi_tanggal),1,0,'L');
			$this->mypdf->Cell(80,7,$dtl->transaksi_keterangan,1,0,'L');
			$this->mypdf->Cell(40,7,$dtl->transaksi_jenis,1,0,'L');
			$this->mypdf->Cell(30,7,rupiah($dtl->transaksi_nominal),1,1,'R');

		}
       

        $this->mypdf->Output();
    }

	}


	


	function cetak_pengeluaran(){

        $jenis = $this->input->get('jenis');
		if($jenis == '1'){
			$jen = "Pemasukan";
		}else{
			$jen = "Pengeluaran";
		}
        $awal = $this->input->get('awal');
		$akhir = $this->input->get('akhir');


		$query = "SELECT * FROM transaksi WHERE transaksi_tanggal >= '$awal' AND transaksi_tanggal <= '$akhir' AND transaksi_jenis= '$jen'";
		$data = $this->db->query($query)->result();
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Laporan Pengeluaran');
        $this->mypdf->SetFont('Arial','B',14);
		$this->mypdf->SetX(25);
		$this->mypdf->MultiCell(160,5, 'LAPORAN PENGELUARAN' ,0,'C',false);
		$this->mypdf->SetFont('Arial','',10);
		$this->mypdf->Cell(190,7,'Periode : '.tgl_indo($awal).' - '.tgl_indo($akhir),0,1,'C');
		$y = $this->mypdf->GetY() + 2;
		$this->mypdf->Line(10, $y, 200, $y);
		
		
        $this->mypdf->Ln();

		$this->mypdf->SetFont('Arial','',8);
		$this->mypdf->Cell(10,10,'NO.',1,0,'C');
		$this->mypdf->Cell(30,10,'TANGGAL',1,0,'C');
		$this->mypdf->Cell(80,10,'DESKRIPSI',1,0,'C');
		$this->mypdf->Cell(40,10,'JENIS / KETERANGAN',1,0,'C');
		$this->mypdf->Cell(30,10,'JENIS',1,1,'C');

		$no 		= 1;
		$this->mypdf->SetFont('Arial','',8);
		// $detail =$this->db->query($query)->result();
		foreach ($data as $dtl) {
			
			$bar = $this->mypdf->GetY();
			$this->mypdf->MultiCell(10,7, $no++ ,1,'C',false);
			$this->mypdf->SetY($bar);
			$this->mypdf->SetX(20);
			$this->mypdf->Cell(30,7,tgl_indo($dtl->transaksi_tanggal),1,0,'L');
			$this->mypdf->Cell(80,7,$dtl->transaksi_keterangan,1,0,'L');
			$this->mypdf->Cell(40,7,$dtl->transaksi_jenis,1,0,'L');
			$this->mypdf->Cell(30,7,rupiah($dtl->transaksi_nominal),1,1,'R');

		}
       

        $this->mypdf->Output();
    }


	
	


