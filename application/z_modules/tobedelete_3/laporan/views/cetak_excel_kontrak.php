<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=namafile.xls");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
<table border='1' width="100%">
            <tr>
                <td colspan="8"><h3 class="card-title">Laporan Kontrak Yang Akan Habis Pada Periode : <?=tgl_indo($awal);?> sampai <?=tgl_indo($akhir);?></h3></td>
            </tr>
                    <tr>
                        <th width="5%">No</th>
                        <th width="10%">NIK</th>
                        <th width="16%">Nama Karyawan</th>
                        <th width="14%">Tanggal Mulai Kerja</th>
                        <th width="14%">Jabatan</th>
                        <th width="14%">Lokasi</th>
                        <th width="14%">Mulai Kontrak</th>
                        <th width="14%">Habis Kontrak</th>
                    </tr>

                    <?php 
                    $no=1;
                    foreach($kontrak as $dt){
                    ?>
                    <tr>
                        <td><?=$no++;?></td>
                        <td><?=$dt->nik;?></td>
                        <td><?=$dt->nama_lengkap;?></td>
                        <td><?=tgl_indo($dt->tgl_kontrak);?></td>
                        <td><?=$dt->nama_jabatan;?></td>
                        <td><?=$dt->nama_lokasi;?></td>
                        <td><?=tgl_indo($dt->tgl_kontrak);?></td>
                        <td><?=tgl_indo($dt->tgl_habis_kontrak);?></td>
                    </tr>

                    <?php 
                    }
                    ?>

            </table>
