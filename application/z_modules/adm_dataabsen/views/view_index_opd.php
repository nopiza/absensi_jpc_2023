  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Laporan Absensi Per OPD</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Laporan</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id" id="id"/>

         <!--  <div class="form-group row">
            <label class="col-sm-3 control-label">Nama Atasan</label>
            <div class="col-md-5">
              <input name="nama_atasan" type="text" class="form-control" id="nama_atasan" >
              <span class="help-block"></span>
            </div>
          </div>


          <div class="form-group row">
            <label class="col-sm-3 control-label">NIP Atasan</label>
            <div class="col-md-5">
              <input name="nip_atasan" type="text" class="form-control" id="nip_atasan" readonly="">
              <span class="help-block"></span>
            </div>
          </div> -->

          <div class="form-group row">
            <label class="col-sm-3 control-label">Tanggal Mulai</label>
            <div class="col-md-3">
              <input name="tanggal_awal" type="date" class="form-control" id="tanggal_awal" >
              <span class="help-block"></span>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-3 control-label">Tanggal Akhir </label>
            <div class="col-md-3">
              <input name="tanggal_akhir" type="date" class="form-control" id="tanggal_akhir" value="">
              <span class="help-block"></span>
            </div>
          </div>


          <!-- <div class="form-group row">
            <label class="col-sm-3 control-label"></label>
            <div class="col-md-2">
              <button class="btn btn-primary btn-md" type="submit">Proses</button>
            </div>
          </div> -->

          <div class="form-group row">
            <label class="col-sm-3 control-label"></label>
            <div class="col-md-2">
              <a class="btn btn-primary btn-md" href="javascript:void(0)" onclick="proses()">Proses</a>
            </div>
          </div>




                                                  
          </form>

          <div id="laporan"></div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>






<?php  $this->load->view('template/footer'); ?>




<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";



function proses(){

  // document.getElementById('laporan').innerHTML = 'kosong';

  url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_proses')?>";

    
       // ajax adding data to database
       $.ajax({
           url : url,
           type: "POST",
           data: $('#form').serialize(),
           dataType: "JSON",
           success: function(data)
           {
  
                   document.getElementById('laporan').innerHTML = data;
  
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }


</script>





