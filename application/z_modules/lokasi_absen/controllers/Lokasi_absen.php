<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi_absen extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'lokasi_absen');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Lokasi_absen_model','lokasi_absen');
		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
     	
     	 $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
	}

	public function ajax_list()
	{

		$list = $this->lokasi_absen->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_lokasi."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_lokasi."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_tempat;
         	$row[] = $post->radius_absen;
         	$row[] = $post->catatan;
			$row[] = '<a href="#" class="btn btn-info btn-xs" onclick="peta('."'".$post->id_lokasi."'".')">View Map</a>';
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->lokasi_absen->count_all(),
						"recordsFiltered" => $this->lokasi_absen->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->lokasi_absen->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
		$data = array(
			'nama_tempat' 		=> $this->input->post('lokasi'),
			'radius_absen' 		=> $this->input->post('radius_absen'),
			'catatan' 		=> $this->input->post('catatan')
		);
		$this->lokasi_absen->update(array('id_lokasi' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('lokasi_absen',array('id_lokasi'=>$id));
		echo json_encode(array("status" => TRUE));
	}


	public function tampil($id)
	{
		$peta = $this->db->get_where('lokasi_absen',array('id_lokasi'=>$id))->row_array();
		echo '<iframe 
		width="100%" 
		height="600" 
		frameborder="0" 
		scrolling="no" 
		marginheight="0" 
		marginwidth="0" 
		src="https://maps.google.com/maps?q='.$peta['latitude'].','.$peta['longitude'].'&hl=id&z=15&amp;output=embed">
	   </iframe>';
	}

}
