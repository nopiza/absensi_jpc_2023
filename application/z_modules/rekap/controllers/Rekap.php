<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'rekap');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Rekap_model','rekap');
		check_login_karyawan();
	}

	public function index()
	{

		$user_data['data_ref']  	= $this->data_ref;
		$user_data['id_karyawan']		= $this->encryption->decrypt($this->session->userdata('id_karyawan'));
		$id = $user_data['id_karyawan'];
		$pegawai = $this->db->query("SELECT * FROM karyawan WHERE id_karyawan='$id'")->row_array();
		$user_data['no_reg'] 		= $pegawai['no_reg'];

     	$this->load->view('template/header_marketing');
		$this->load->view('view',$user_data);

	}

	public function lembur()
	{

		$user_data['data_ref']  	= $this->data_ref;
		$user_data['id_karyawan']		= $this->encryption->decrypt($this->session->userdata('id_karyawan'));
		$id = $user_data['id_karyawan'];
		$pegawai = $this->db->query("SELECT * FROM karyawan WHERE id_karyawan='$id'")->row_array();
		$user_data['no_reg'] 		= $pegawai['no_reg'];

     	$this->load->view('template/header_marketing');
		$this->load->view('lembur',$user_data);

	}
	
}
