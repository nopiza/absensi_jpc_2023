  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Proses Payroll</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    




    <!-- Main content -->
    <section class="content">
      <div class="card">


        <!-- /.card-header -->
        <div class="card-body">

        <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

    
                        <div class="form-group row">
                            <label class="control-label col-md-2">Jenis Karyawan</label>
                            <div class="col-md-3">
                                <select name="jenis_karyawan" id="jenis_karyawan" class="form-control" >
                                    <option value="0">-- Pilih --</option>
                                    <option value="1">PKWT - PKWTT</option>
                                    <option value="2">PKHL</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Lokasi</label>
                            <div class="col-md-3">
                                <select class="form-control" name="lokasi" id="lokasi">
                                <option value="0">-- Pilih --</option>
                                    <?php 
                                    $dept = $this->db->query("SELECT * FROM lokasi")->result();
                                    foreach($dept as $dp){
                                        echo '<option value="'.$dp->id_lokasi.'">'.$dp->nama_lokasi.'</option>';
                                    }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-3">
                            <button type="button" id="proses" onclick="add()" class="btn btn-primary">Proses</button>
                            </div>
                        </div>


                    </div>
                </form>

                <div id="detail"></div>

         

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

</body>
</html>





<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">


function add()
   {
        var jenis = document.getElementById("jenis_karyawan").value;
        var lokasi = document.getElementById("lokasi").value;
        url = "<?=base_url();?>";
        trx = '123';
        $('#detail').load(url + "penggajian/proses/" + jenis + '/' + lokasi);
          
   }

// function add()
//    {
//        alert('asd');
//        url = "<?php echo site_url($data_ref['uri_controllers'].'/proses')?>";
    
//        // ajax adding data to database
//        var formData = new FormData($('#form')[0]);
//        $.ajax({
//             url : url,
//             type: "POST",
//             data: formData,
//             contentType: false,
//             processData: false,
//             dataType: "JSON",
//            success: function(data)
//            {
//             $('#detail').load(url + "transaksi/detailtrx/" + trx);
//            },
//            error: function (jqXHR, textStatus, errorThrown)
//            {
//                alert('Error adding / update data');
//                $('#btnSave').text('Simpan'); //change button text
//                $('#btnSave').attr('disabled',false); //set button enable 
    
//            }
//        });
//    }




</script>


