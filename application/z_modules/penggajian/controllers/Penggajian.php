<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggajian extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'penggajian');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jabatan_model','jabatan');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
		$user_data['data_ref'] = $this->data_ref;   	
		$this->load->view('template/header',$user_data);
		$this->load->view('form',$user_data);
	}

	public function proses($jenis, $lokasi)
	{
		if($jenis == '1'){
			$jenisKaryawan = 'PWT | PWTT';
		}else{
			$jenisKaryawan = 'PKHL';
		}
		$no = 1;
		$query = "SELECT * FROM karyawan WHERE id_lokasi='$lokasi' AND sistem_penggajian='$jenisKaryawan'";
		$pay = $this->db->query($query)->result();
		$a = '<table class="table table-bordered">
					<thead>
					<th width="5%">No</th>
					<th width="25%">Nama Karyawan</th>
					<th width="8%">Pokok</th>
					<th width="8%">Tunjangan</th>
					<th width="8%">Kerajinan</th>
					<th width="8%" align="center">Meal</th>
					<th width="8%">Transport</th>
					<th width="8%">Lembur</th>
					<th width="8%">Total</th>
					<th width="8%"></th>
					<th width="8%"></th>
					</thead>
				<tbody>';
		foreach($pay as $p){
			$a .= '<tr>
				<td>'.$no++.'</td>
				<td>'.$p->nama_lengkap.'</td>
				<td align="right">'.rupiah($p->gaji_pokok).'</td>
				<td align="right">'.rupiah($p->tunjangan).'</td>
				<td align="right">'.rupiah($p->t_kerajinan).'</td>
				<td align="right">'.rupiah($p->t_meal).'</td>
				<td align="right">'.rupiah($p->t_transport).'</td>
				<td></td>
				<td></td>
				<td></td>
				<td><a href="'.base_url('penggajian/cetak/'.$p->id_karyawan).'" target="_blank" class="btn btn-success btn-sm">Cetak</a></td>
			</tr>';
		}

		$a .= '</tbody>
		</table>';

		echo $a;
	}



	function cetak($id_karyawan){

        $tahun = date('Y');
        $bulan = date('m');
		
		$kry = $this->db->query("SELECT * FROM karyawan WHERE id_karyawan = '$id_karyawan'")->row_array();

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Slip Gaji');
        $this->mypdf->SetFont('Arial','B',10);

		//Master Desain background Kwitansi Pembayaran
		// $this->mypdf->Image(base_url().'assets/aplikasi/kwitansi.jpg',10,10,190);
		// LOgo Kavling
		// $this->mypdf->Image(base_url().'assets/aplikasi/'.$konfig['logo'],18,10,15);
		$this->mypdf->Cell(190,4,'PT. JAKARTA PRIMA CRANES ',0,1,'L');
		$this->mypdf->Cell(190,4,'Balikpapan ',0,1,'L');
		$this->mypdf->SetFont('Times','B',10);  
		$this->mypdf->SetTextColor(218,0,0);
		$this->mypdf->Cell(190,6,'PAYMENT SLIP',0,1,'C');
		$this->mypdf->SetTextColor(0,0,0);

		$this->mypdf->Line(10,24,190,24);		
		$this->mypdf->SetLineWidth(0.1);
		$this->mypdf->SetFont('Times','',9);  


		$this->mypdf->Cell(21,5,'Nama ',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, $kry['nama_lengkap'],0,0,'L');

		$this->mypdf->Cell(21,5,'Departemen',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, '',0,0,'L');

		$this->mypdf->Cell(15,5,'Bulan',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(25,5, bulan($bulan),0,1,'L');

		// ===============================================================

		$this->mypdf->Cell(21,5,'Jabatan',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, $kry['id_jabatan'],0,0,'L');

		$this->mypdf->Cell(21,5,'Masuk Kerja',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(45,5, '',0,0,'L');

		$this->mypdf->Cell(15,5,'Tahun',0,0,'L');
		$this->mypdf->Cell(3,5,':',0,0,'L');
		$this->mypdf->Cell(25,5, $tahun,0,1,'L');

		// ===============================================================

		// $this->mypdf->Line(10,47,190,47);	
		$this->mypdf->Ln(3);    
		$this->mypdf->SetFont('Times','B',9);  
		$this->mypdf->setFillColor(204,255,204); 
		$this->mypdf->Cell(60,5,'Gaji Tetap : ',1,0,'L', 1);
		$this->mypdf->setFillColor(204,255,255);
		$this->mypdf->Cell(60,5,'Tunjangan Tidak Tetap',1,0,'L',1);
		$this->mypdf->setFillColor(255,204,229); 
		$this->mypdf->Cell(60,5, 'Pemotongan',1,1,'L',1);

		$this->mypdf->SetFont('Times','',9);  
		$this->mypdf->Ln(1);    

		$this->mypdf->Line(70,42,70,83);	
		$this->mypdf->Line(130,42,130,83);	
		$this->mypdf->Line(10,83,190,83);			

		$this->mypdf->Cell(30,5,'Gaji ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['gaji_pokok']),0,0,'R');
		$this->mypdf->Cell(30,5,'Overtime / Lembur : ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, 'Pinjaman',0,0,'L');
		$this->mypdf->Cell(30,5, '0',0,1,'R');

		$this->mypdf->Cell(30,5,'Tunj. Operasional  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['tunjangan']),0,0,'R');
		$this->mypdf->Cell(30,5,'Meal  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, 'Kasbon  ',0,0,'L');
		$this->mypdf->Cell(30,5, '0',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Kerajinan  ',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_kerajinan']),0,0,'R');
		$this->mypdf->Cell(30,5, 'BPJS',0,0,'L');
		$this->mypdf->Cell(30,5, '0',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Transport',0,0,'L');
		$this->mypdf->Cell(30,5,rupiah($kry['t_transport']),0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Lembur  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. Jabatan  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. HP ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		$this->mypdf->Cell(30,5,'',0,0,'L');
		$this->mypdf->Cell(30,5,'',0,0,'R');
		$this->mypdf->Cell(30,5,'Tunj. Kehadiran  ',0,0,'L');
		$this->mypdf->Cell(30,5,'0',0,0,'R');
		$this->mypdf->Cell(30,5, '',0,0,'L');
		$this->mypdf->Cell(30,5, '',0,1,'R');

		// $this->mypdf->Ln(1);    
		$this->mypdf->setFillColor(204,255,204); 
		$this->mypdf->Cell(30,5,'Total : ',0,0,'R');
		$this->mypdf->Cell(30,5,rupiah($kry['gaji_pokok'] + $kry['tunjangan']),1,0,'R', 1);

		$this->mypdf->setFillColor(204,255,255); 
		$this->mypdf->Cell(30,5,'Total : ',0,0,'R');
		$this->mypdf->Cell(30,5, rupiah($kry['t_kerajinan'] + $kry['t_transport']) ,1,0,'R', 1);

		$this->mypdf->setFillColor(255,204,229); 
		$this->mypdf->Cell(30,5, 'Total :',0,0,'R');
		$this->mypdf->Cell(30,5, '',1,1,'R', 1);

		$this->mypdf->Ln(1);    
		$this->mypdf->setFillColor(224,224,224); 
		$this->mypdf->Cell(120,5, '',0,0,'R');
		$this->mypdf->Cell(30,5, 'Gaji Bersih:',0,0,'R');
		$this->mypdf->Cell(30,5, '',1,1,'R', 1);




		
		

		//tabel
		$this->mypdf->Ln(3);    
		$this->mypdf->SetFont('Times','B',7);  
		$this->mypdf->setFillColor(211,236,230); 
		$this->mypdf->Ln(5);    
		$this->mypdf->Cell(5,5,'No.',1,0,'C', 1);
		$this->mypdf->Cell(35,5,'Tanggal',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Masuk',1,0,'C', 1);
		$this->mypdf->Cell(20,5, 'Pulang',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Jam Kerja',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Jam Kantor',1,0,'C', 1);
		$this->mypdf->Cell(20,5,'Lama Lembur',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'1,5',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'2',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'3',1,0,'C', 1);
		$this->mypdf->Cell(10,5,'4',1,1,'C', 1);

		$no =1;
		$this->mypdf->setFillColor(255,255,255); 
		$this->mypdf->SetFont('Times','',7);  
		for($i=1 ; $i <= 31; $i++){
			$tgl = $tahun.'-'.$bulan.'-'.$i;
			$this->mypdf->Cell(5,5,$no++,1,0,'C', 1);
			$this->mypdf->Cell(35,5, longdate_indo($tgl),1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5, '',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(20,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,0,'C', 1);
			$this->mypdf->Cell(10,5,'',1,1,'C', 1);
		}

		
        
        $this->mypdf->SetFont('Times','B',16);
        // $this->mypdf->text(37,74, rupiah($kavling['jumlah_bayar']));



        $this->mypdf->SetFont('Times','',11);

		$this->mypdf->SetY(70);
		$this->mypdf->Cell(120,4,'',0,0,'C');
        // $this->mypdf->Cell(60,4,'Palangka Raya, '.tgl_indo($kavling['tanggal']),0,0,'C');
		$this->mypdf->ln(21);
		$this->mypdf->Cell(120,4,'',0,0,'C');
        // $this->mypdf->Cell(60,4,$konfig['nama_penandatangan'],0,0,'C');

		//Tanda Tangan
		// $this->mypdf->Image(base_url().'assets/aplikasi/'.$konfig['file_ttd'],135,57,50);



		// $namaFile = str_replace('/','-', '0123/KNG/2022');
		// $noFile = explode('-', $namaFile);
		

		// if(file_exists('./kwitansi/'.$namaFile.'.pdf')){
		// 	echo '';
		// }else{
		// 	$this->mypdf->Output('F', './kwitansi/kwitansi-'.$noFile[0].'.pdf', true);
		// }
        

        $this->mypdf->Output();
    }


	
}
