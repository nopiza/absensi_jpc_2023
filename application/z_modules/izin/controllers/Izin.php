<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Izin extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'department');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Department_model','department');
		check_login_karyawan();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;    	
      	$this->load->view('template/header_marketing',$user_data);
		$this->load->view('izin',$user_data);
	}

	public function pengajuan()
	{
		$user_data['data_ref'] = $this->data_ref;    	
		$this->load->view('template/header_marketing',$user_data);
	  	$this->load->view('form-izin',$user_data);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->department->get_by_id($id);
		echo json_encode($data);
	}

	public function simpan()
	{

		$data = array(
			'id_karyawan' 		=> $this->input->post('id_karyawan'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'jenis_izin' 			=> $this->input->post('jenis'),
			'deskripsi' 			=> $this->input->post('deskripsi'),
			'status_izin' 			=> '0'
		);

		$this->db->insert('pengajuan_izin',$data);
		redirect('izin');
	}

	public function ajax_update()
	{
		$data = array(
			'nama_department' 		=> $this->input->post('department'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->department->update(array('id_department' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('department',array('id_department'=>$id));
		echo json_encode(array("status" => TRUE));
	}



}
