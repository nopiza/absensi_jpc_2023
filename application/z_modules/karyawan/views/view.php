  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Karyawan PKWT - PKWTT</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Karyawan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Karyawan</h3>
              <div class="card-tools">
                <a href="#" class="btn btn-info btn-sm" onclick="add()"><i class="fa fa-plus"></i> Tambah Karyawan</a>&nbsp;
                <button class="btn btn-default btn-sm" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="10%">No REG</th>
                        <th width="15%">Nama Lengkap</th>
                        <th width="15%">Jabatan</th>
                        <th width="15%">Lokasi</th>
                        <th width="15%">Alamat / No Telp</th>
                        <th width="10%">Tgl Masuk</th>
                        <th width="5%">Status</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

    
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-5">
                                <input name="nama_lengkap" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. Reg / Badge Number</label>
                            <div class="col-md-3">
                                <input name="noreg" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. KTP</label>
                            <div class="col-md-5">
                                <input name="no_ktp" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Jenis Kelamin</label>
                            <div class="col-md-3">
                                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                  <option value="Laki-laki">Laki-laki</option>
                                  <option value="Perempuan">Perempuan</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-3">
                                <input name="tempat_lahir" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-3">
                                <input name="tanggal_lahir" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. HP</label>
                            <div class="col-md-3">
                                <input name="no_hp" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Alamat</label>
                            <div class="col-md-7">
                                <input name="alamat" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Agama</label>
                            <div class="col-md-3">
                                <select class="form-control" name="agama" id="agama">
                                  <option value="Islam">Islam</option>
                                  <option value="Kristen">Kristen</option>
                                  <option value="Protestan">Protestan</option>
                                  <option value="Hindu">Hindu</option>
                                  <option value="Budha">Budha</option>
                                  <option value="Konghucu">Konghucu</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Status Perkawinan</label>
                            <div class="col-md-3">
                                <select class="form-control" name="status_perkawinan" id="status_perkawinan">
                                  <option value="Lajang">Lajang</option>
                                  <option value="Menikah">Menikah</option>
                                  <option value="Duda">Duda</option>
                                  <option value="Janda">Janda</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. BPJS Kesehatan</label>
                            <div class="col-md-3">
                                <input name="bpjs_kesehatan" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. BPJS Ketenagakerjaan</label>
                            <div class="col-md-3">
                                <input name="bpjs_ketenagakerjaan" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">No. BPJS JHT</label>
                            <div class="col-md-3">
                                <input name="bpjs_ketenagakerjaan" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- <div class="form-group row">
                            <label class="control-label col-md-3">Jadwal MCU</label>
                            <div class="col-md-1">
                                <input name="range_mcu" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div> -->

                        <hr>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Gaji Pokok</label>
                            <div class="col-md-2">
                                <input name="gaji_pokok" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Kerajinan</label>
                            <div class="col-md-2">
                                <input name="tunjangan_kerajinan" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Meal</label>
                            <div class="col-md-2">
                                <input name="tunjangan_meal" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tunjangan Transport</label>
                            <div class="col-md-2">
                                <input name="tunjangan_transport" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <hr>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Foto KTP</label>
                            <div class="col-md-6">
                                <input name="ktp" placeholder="" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Foto KK</label>
                            <div class="col-md-6">
                                <input name="kk" placeholder="" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Foto BPJS</label>
                            <div class="col-md-6">
                                <input name="bpjs" placeholder="" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <hr>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Departmen</label>
                            <div class="col-md-3">
                                <select class="form-control" name="department" id="department">
                                    <option value="0">-- Pilih --</option>
                                    <?php 
                                    $dept = $this->db->query("SELECT * FROM department")->result();
                                    foreach($dept as $dp){
                                        echo '<option value="'.$dp->id_department.'">'.$dp->nama_department.'</option>';
                                    }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <!-- <div class="form-group row">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-3">
                                <select class="form-control" name="jabatan" id="jabatan">
                                <option value="0">-- Pilih --</option>
                                    <?php 
                                    // $dept = $this->db->query("SELECT * FROM jabatan")->result();
                                    // foreach($dept as $dp){
                                    //     echo '<option value="'.$dp->id_jabatan.'">'.$dp->nama_jabatan.'</option>';
                                    // }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div> -->

                        <div class="form-group row">
                            <label class="control-label col-md-3">Lokasi</label>
                            <div class="col-md-3">
                                <select class="form-control" name="lokasi" id="lokasi">
                                <option value="0">-- Pilih --</option>
                                    <?php 
                                    $dept = $this->db->query("SELECT * FROM lokasi")->result();
                                    foreach($dept as $dp){
                                        echo '<option value="'.$dp->id_lokasi.'">'.$dp->nama_lokasi.'</option>';
                                    }
                                    ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Masuk</label>
                            <div class="col-md-3">
                                <input name="tanggal_masuk" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal PHK</label>
                            <div class="col-md-3">
                                <input name="tanggal_phk" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Status Karyawan</label>
                            <div class="col-md-3">
                                <select class="form-control" name="stt_karyawan" id="stt_karyawan">
                                  <option value="Aktif">Aktif</option>
                                  <option value="Non Aktif">Non Aktif</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Status Admin</label>
                            <div class="col-md-3">
                                <select class="form-control" name="is_admin" id="is_admin">
                                  <option value="1">Admin</option>
                                  <option value="0">Non Admin</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>



                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
</body>
</html>





<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });


      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });

   function add()
   {
       save_method = 'add';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
       $('#modal_form').modal('show'); // show bootstrap modal
       $('.modal-title').text('Tambah Data Karyawan'); // Set Title to Bootstrap modal title
       $('#photo-preview').hide(); // hide photo preview modal
        // $('#label-photo').text('Upload Photo'); // label photo upload
   }

   function edit(id)
   {
       save_method = 'update';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
    
       //Ajax Load data from ajax
       $.ajax({
           url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_edit/')?>/" + id,
           type: "GET",
           dataType: "JSON",
           success: function(data)
           {
               $('[name="id"]').val(data.id_karyawan);
               $('[name="nama_lengkap"]').val(data.nama_lengkap);
               $('[name="noreg"]').val(data.no_reg );
               $('[name="no_ktp"]').val(data.no_ktp);
               $('[name="alamat"]').val(data.alamat);
               $('[name="tempat_lahir"]').val(data.tempat_lahir);
               $('[name="tanggal_lahir"]').val(data.tanggal_lahir);
               $('[name="jenis_kelamin"]').val(data.jenis_kelamin);
               $('[name="no_hp"]').val(data.no_hp);
               $('[name="agama"]').val(data.agama);
               $('[name="status_perkawinan"]').val(data.status_perkawinan);
               $('[name="bpjs_kesehatan"]').val(data.no_bpjs_kes);
               $('[name="bpjs_ketenagakerjaan"]').val(data.no_bpjs_tk);
               $('[name="bpjs_jht"]').val(data.no_bpjs_jht);
               $('[name="gaji_pokok"]').val(data.gaji_pokok);
               $('[name="tunjangan_kerajinan"]').val(data.t_kerajinan);
               $('[name="tunjangan_meal"]').val(data.t_meal);
               $('[name="tunjangan_transport"]').val(data.t_transport);
               $('[name="tanggal_masuk"]').val(data.tanggal_masuk);
               $('[name="tanggal_phk"]').val(data.tanggal_phk);
               $('[name="stt_karyawan"]').val(data.stt_karyawan);
               $('[name="is_admin"]').val(data.stt_admin);
               $('[name="department"]').val(data.id_department);
               $('[name="jabatan"]').val(data.id_jabatan);
               $('[name="lokasi"]').val(data.id_lokasi);
               $('[name="range_mcu"]').val(data.range_mcu);

               $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
               $('.modal-title').text('Edit Data Karyawan'); // Set title to Bootstrap modal title
              
              $('#photo-preview').show(); // show photo preview modal
              if(data.foto)
              {
                  $('#label-photo').text('Change Photo'); // label photo upload
                  $('#photo-preview div').html('<img src="'+url+'assets/images/'+data.foto+'" class="img-responsive" width="50">'); // show photo
                  $('#photo-preview div').append('<label><input type="checkbox" name="remove_photo" value="'+data.foto+'"/> Hapus foto ketika di simpan</label>'); // remove photo
   
              }
              else
              {
                  $('#label-photo').text('Upload Photo'); // label photo upload
                  $('#photo-preview div').text('(No photo)');
              }
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error get data from ajax');
           }
       });
   }

   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
   }

   function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
       }
    
       // ajax adding data to database
       var formData = new FormData($('#form')[0]);
       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   $('#modal_form').modal('hide');
                   reload_table();
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }

   function hapus(id){
    $.confirm({
      title: 'Confirm!',
      content: 'Apakah anda yakin menghapus data ini ?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_delete/" + id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  reload_table();
                  Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Dihapus'
                   });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }

</script>


