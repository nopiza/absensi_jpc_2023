<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Absen Harian</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="card">

        <!-- /.card-header -->
        <div class="card-body">
            
          <div class="form-group row">
              <label class="col-sm-2 control-label"></label>
              <div class="col-sm-6" id="petani"></div>
          </div>

          <hr>

  <div id="boleh">
      <form action="<?=base_url('absen/simpan_absen');?>" id="form" method="POST" class="form-horizontal">

          <div class="form-group row">
              <label class="col-sm-2 control-label">Nama Karyawan </label>
              <div class="col-md-5">
                  <input type="text" class="form-control" name="id_customer" value="<?=$this->encryption->decrypt($this->session->userdata('nama_lengkap'));?>" readonly>
              </div>
          </div>

          <div class="form-group row">
              <label class="col-sm-2 control-label">No. REG</label>
              <div class="col-md-5">
              <input type="text" class="form-control" name="no_telp" value="<?=$this->encryption->decrypt($this->session->userdata('no_reg'));?>" readonly>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-2 control-label">Tanggal</label>
              <div class="col-md-2">
              <input type="text" class="form-control" name="tanggal" value="<?=tgl_now();?>" readonly>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-2 control-label">Lokasi</label>
              <div class="col-md-5">
              <input type="text" class="form-control" name="lat_long" value="" id="lat_long" readonly>
              <input type="hidden" class="form-control" name="long" value="" id="long">
              <input type="hidden" class="form-control" name="lat" value="" id="lat">
              </div>
          </div>
          
          <div class="form-group row">
              <label class="col-sm-2 control-label">Jenis Absen</label>
              <div class="col-md-2">
                <select name="type_absensi" id="type_absensi" class="form-control">
                  <option value="">-- Pilih --</option>
                  <option value="MASUK">Masuk</option>
                  <option value="PULANG">Pulang</option>
                  <option value="MASUK LEMBUR">Masuk Lembur</option>
                  <option value="PULANG LEMBUR">Pulang Lembur</option>
                </select>
              </div>
          </div>

          <div class="form-group row">
              <label class="col-sm-2 control-label">Catatan</label>
              <div class="col-md-8">
              <input type="text" class="form-control" name="catatan" value="" id="catatan">
              </div>
          </div>

          <div class="form-group row">
              <label class="col-sm-2 control-label"></label>
              <div class="col-md-1">
                  <button onclick="cek()" class="btn btn-block btn-primary" id="tombol" >Kirim Absensi</button>
              </div>
          </div>
          

      </form>
</div>   

<div id="tidakboleh">
<div class="alert alert-danger text-center" role="alert">Anda tidak diijinkan untuk melakukan absen di kolasi ini. Silahkan mendekat ke lokasi titik absen yang telah ditentukan</div>
</div>

</div>
<!-- /.card-body -->
</div>
<!-- /.card -->
</div>


<?php  $this->load->view('template/footer'); ?>
<script src="<?=base_url('assets/js/geo-min.js');?>"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#boleh').hide();
  $('#tidakboleh').hide();
});


        if(geo_position_js.init()){
            geo_position_js.getCurrentPosition(success_callback, error_callback, {enableHighAccuracy:true});
        }
        else{
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ="Tidak ada fungsi geolocation";
        }
 
        function success_callback(p)
        {
            var url = '<?=base_url();?>';
            latitude=p.coords.latitude ;
            longitude=p.coords.longitude;
            // var long = document.getElementById('long'); 
            // var lat = document.getElementById('lat'); 
            // var lat_long = document.getElementById('lat_long'); 
            long.value = longitude;
            lat.value = latitude;
            lat_long.value = latitude+','+longitude;
            document.getElementById("petani").innerHTML = '<iframe width="100%" height="300" frameborder="0"   scrolling="no"   marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+latitude+','+longitude+'&hl=id&z=15&amp;output=embed" ></iframe> <br>Panduan untuk setting akurasi lokasi absen klik <a href="https://support.google.com/maps/answer/2839911?co=GENIE.Platform%3DAndroid&hl=id" target="_blank">disini </a><br>';

            $.ajax({
              url : url + "absen/cek_lokasi/" + latitude + '/' + longitude,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                //   if(parseInt(data.distance) < parseInt(data.radius_absen)){
                    if(data.distance < data.radius_absen){
                    // alert('Boleh Absen' + data.distance + ' - ' + data.radius_absen + ' - ' + data.jarakPHP);
                    $('#boleh').show();
                    $('#tidakboleh').hide();
                  }else{
                    // alert('Gak Boleh Absen');
                    // alert('Tidak Boleh Absen <br>' + data.distance + ' - ' + data.radius_absen + ' - ' + data.jarakPHP);
                    $('#tidakboleh').show();
                    $('#boleh').hide();
                  }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Proses gagal...');
              }
          });
        }
        
        function error_callback(p)
        {
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ='error='+p.message;
        }        
    </script>