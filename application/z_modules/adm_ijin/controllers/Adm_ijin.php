<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_ijin extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'adm_ijin');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Adm_ijin_model','ijin');
		check_login();
	}

	public function index()
	{
		$user_data['tambah_view'] = 1;
		$user_data['lihat_view'] = 1;
		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';
		$user_data['menu_active'] = 'Data Referensi';
		$user_data['sub_menu_active'] = 'Menu';
     	$this->load->view('template/header');
		$this->load->view('view',$user_data);
	}

	public function ajax_list()
	{
		$kolok = $this->encryption->decrypt($this->session->userdata('KOLOK'));
		$list = $this->ijin->get_datatables($kolok);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_izin."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_izin."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = tgl_indo($post->tanggal);
			$row[] = '<b>'.$post->nama_lengkap.'</b><br>'.$post->no_reg;
         	$row[] = $post->jenis_izin;
         	$row[] = $post->deskripsi;
			//add html for action
			$row[] = $post->status_izin;
			$row[] = $link_edit.$link_hapus;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->ijin->count_all($kolok),
						"recordsFiltered" => $this->ijin->count_filtered($kolok),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->ijin->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add(){

        $tanggal = $this->input->post('tanggal');
        $tanggal2 = $this->input->post('tanggal2');

		if(empty($tanggal2)){
			$data = array(
				'id_karyawan' 	=> $this->input->post('nama_lengkap'),
				'tanggal' 		=> $this->input->post('tanggal'),
            	'jenis_izin' 	=> $this->input->post('jenis_izin'),
				'durasi' 		=> '1',
				'deskripsi' 	=> $this->input->post('deskripsi')
			);
			$this->ijin->save($data);

		}else{
			while (strtotime($tanggal) <= strtotime($tanggal2)) {
				$data = array(
					'id_karyawan' 	=> $this->input->post('id_karyawan'),
					'tanggal' 		=> $this->input->post('tanggal'),
					'jenis_izin' 	=> $this->input->post('jenis_izin'),
					'durasi' 		=> '1',
					'deskripsi' 	=> $this->input->post('deskripsi')
				);
				$this->ijin->save($data);
				$tanggal = date ("Y-m-d", strtotime("+1 days", strtotime($tanggal)));
			}
		}
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
        $tanggal = $this->input->post('tanggal');
        $tanggal2 = $this->input->post('tanggal2');

		$data = array(
			'id_karyawan' 	=> $this->input->post('id_karyawan'),
			'tanggal' 		=> $this->input->post('tanggal'),
			'jenis_izin' 	=> $this->input->post('jenis_izin'),
			'durasi' 		=> '1',
			'deskripsi' 	=> $this->input->post('deskripsi')
		);
		$this->ijin->update(array('IDIJIN' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->ijin->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_select(){
		$q =$this->input->get('q');
        $items=$this->db->query("SELECT id_karyawan, no_reg, nama_lengkap FROM karyawan WHERE nama_lengkap like '%$q%' OR no_reg like '%$q%'")->result_array();
        echo json_encode($items);
    }


    public function get($no_reg){
        $item=$this->db->query("SELECT * FROM karyawan WHERE no_reg ='$no_reg' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


	



}
