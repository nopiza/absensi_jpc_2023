<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'absen');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Absen_model','absen');
		$this->load->library('user_agent');
		check_login_karyawan();
	}

	function distance($lat1, $lon1, $lat2, $lon2) { 
        $pi80 = M_PI / 180; 
        $lat1 *= $pi80; 
        $lon1 *= $pi80; 
        $lat2 *= $pi80; 
        $lon2 *= $pi80; 
        $r = 6372000.797; // radius of Earth in km 6371
        $dlat = $lat2 - $lat1; 
        $dlon = $lon2 - $lon1; 
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2); 
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a)); 
        $km = $r * $c; 
        return round($km); 
    }
	

	function cek_jarak(){
		echo $this-> distance('-7.8744146', '112.5244688', '-7.8750102', '112.5244586');
	}

	public function index()
	{
		$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header_marketing');
		$this->load->view('view',$user_data);
	}

	public function form()
	{
		$user_data['data_ref'] = $this->data_ref;
     	$this->load->view('template/header_marketing');
		$this->load->view('form',$user_data);
	}	

	public function simpan_lokasi(){
		$data = array(
			'longitude' 		=> $this->input->post('long'),
			'latitude' 			=> $this->input->post('lat'),
			'radius_absen' 		=> $this->input->post('radius_absen'),
			'nama_tempat' 		=> $this->input->post('nama_tempat'),
			'catatan' 			=> $this->input->post('catatan')
		);
		$this->db->insert('lokasi_absen',$data);	
		redirect('setting');
	}


	public function peta($id){
		$cekAbsen = $this->db->get_where('lokasi_absen', ['id_lokasi'=>$id])->row_array();
		echo '
		<h4>LOKASI</h4>
		<iframe 
		width="100%" 
		height="300" 
		frameborder="0" 
		scrolling="no" 
		marginheight="0" 
		marginwidth="0" 
		src="https://maps.google.com/maps?q='.$cekAbsen['latitude'].','.$cekAbsen['longitude'].'&hl=id&z=15&amp;output=embed">
		</iframe>
		<br>
		<br>
		Keterangan : <br>
		ID Data : '.$cekAbsen['nama_tempat'].'';
	}

}
