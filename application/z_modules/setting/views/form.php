  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Setting Titik Lokasi Absen</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

        <!-- /.card-header -->
        <div class="card-body">
          <?php // echo validation_errors(); ?>
        <form action="<?=base_url('setting/simpan_lokasi');?>" method="POST" id="form" class="form-horizontal">
            <div class="form-group row">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-6" id="petani"></div>
            </div>
        <hr>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Nama Lokasi</label>
            <div class="col-md-5">
            <input type="text" class="form-control" name="nama_tempat" value="">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Lokasi</label>
            <div class="col-md-5">
            <input type="text" class="form-control" name="lat_long" value="" id="lat_long" readonly>
            <input type="hidden" class="form-control" name="long" value="" id="long">
            <input type="hidden" class="form-control" name="lat" value="" id="lat">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 control-label">Radius Absen</label>
            <div class="col-md-5">
            <input type="text" class="form-control" name="radius_absen" value="">
            </div>
        </div>
                
        <div class="form-group row">
            <label class="col-sm-2 control-label">Catatan</label>
            <div class="col-md-5">
            <input type="text" name="catatan" id="catatan" value="" class="form-control"  autocomplete="off"/>
            </div>
        </div>
                                                        

            <div class="form-group row">
                <label class="col-sm-2 control-label"></label>
                <div>
                    <button class="btn btn-block btn-primary" type="submit" id="submit">Simpan Lokasi</button>
                </div>
            </div>

            </form>                 

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


<?php  $this->load->view('template/footer'); ?>





<script src="<?=base_url('assets/js/geo-min.js');?>"></script>

    <script>
        if(geo_position_js.init()){
            geo_position_js.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
        }
        else{
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ="Tidak ada fungsi geolocation";
        }
 
        function success_callback(p)
        {
            latitude=p.coords.latitude ;
            longitude=p.coords.longitude;
            // pesan='posisi:'+latitude+','+longitude;
            // pesan = pesan + "<br/>";
            // pesan = pesan + '<img src="https://maps.googleapis.com/maps/api/staticmap?size=400×400&amp;zoom=13&amp;markers=color:red%7Clabel:C%7C'+latitude +','+longitude+'"/>';
            // div_isi=document.getElementById("div_isi");
            //alert(pesan);
            // div_isi.innerHTML =pesan;
            var long = document.getElementById('long'); 
            var lat = document.getElementById('lat'); 
            var lat_long = document.getElementById('lat_long'); 
            long.value = longitude;
            lat.value = latitude;
            lat_long.value = latitude+','+longitude;
            document.getElementById("petani").innerHTML = '<iframe width="100%" height="300" frameborder="0"   scrolling="no"   marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+latitude+','+longitude+'&hl=id&z=15&amp;output=embed" ></iframe>';
        }
        
        function error_callback(p)
        {
            div_isi=document.getElementById("div_isi");
            div_isi.innerHTML ='error='+p.message;
        }        
    </script>


 <script type="text/javascript">

$(document).ready(function() {
    $("#pertanyaan").hide();
});


function myFunction() {

  var isinya = document.getElementById("type_absensi").value;
  // alert(isinya);
  if(isinya == 'MASUK'){
    $("#pertanyaan").show();
    document.getElementById("tombol").disabled = false; 
  }else if(isinya == 'PULANG'){
    $("#pertanyaan").hide();
    document.getElementById("tombol").disabled = false; 
  }else{
    $("#pertanyaan").hide();
    document.getElementById("tombol").disabled = true; 
  }
  
} 


</script>

<link rel="stylesheet" href="<?=base_url('assets/loading/css/jquery.loadingModal.css');?>">
<!-- <script src="http://code.jquery.com/jquery-3.1.1.slim.min.js"></script> -->
<script src="<?=base_url('assets/loading/js/jquery.loadingModal.js');?>"></script>