  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Titik Lokasi Absen </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><a href="<?=base_url('setting/form');?>" class="btn btn-primary btn-sm">Tambah Lokasi</a></h3>

        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th width="5%">No.</th>
                  <th width="70%">Lokasi</th>
                  <th width="25%">Detail</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $no=1;
              $lokasi = $this->db->query("SELECT * FROM lokasi_absen")->result();
              foreach($lokasi as $lok){
                    echo '<tr>
                    <td width="5%">'.$no++.'</td>
                    <td width="70%">'.$lok->nama_tempat.'</td>
                    <td width="25%"><button class="btn btn-primary btn-xs"  onclick="detail('.$lok->id_lokasi.')">Lihat Lokasi</button></td>
                  </tr>';
               } ?>
            </tbody>
          </table>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

<?php  $this->load->view('template/footer'); ?>


<div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Large Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="peta"></div>

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<script type="text/javascript">

function detail(id) {
  // var id;
  $('#modal-xl').modal('show'); // show bootstrap modal
  // $('#peta').load('http://detik.com'); 
  $("#peta").load('<?php echo base_url('setting/peta/');?>' + id);
  $('.modal-title').text('Lokasi Absen'); // Set Title to Bootstrap modal title
}


</script>