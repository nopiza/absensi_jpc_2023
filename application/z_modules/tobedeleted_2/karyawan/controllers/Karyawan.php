<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'karyawan');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Karyawan_model','karyawan');
		$this->load->model('Keluarga_model','keluarga');
		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}


	public function detail($id)
	{

      	$user_data['data_ref'] = $this->data_ref;
		$query = "SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id'";
      	$user_data['karyawan'] = $this->db->query($query)->row_array();
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('detail',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->karyawan->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

				$link_edit = ' <a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_karyawan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
				$link_detail = ' <a class="btn btn-xs btn-warning" href="'.base_url('karyawan/detail/'.$post->id_karyawan).'" title="Detail" >Detail</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap.'<br>'.$post->no_ktp;
         	$row[] = $post->nama_department;
         	$row[] = $post->nama_jabatan;
			$row[] = $post->alamat;
			$row[] = $post->no_hp;
			if($post->stt_karyawan == 'Aktif'){
				$row[] = '<span class="btn btn-xs btn-info">Aktif</span>';
			}else{
				$row[] = '<span class="btn btn-xs btn-secondary">Non Aktif</span>';
			}


			//add html for action
			$row[] = $link_detail.$link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->karyawan->count_all(),
						"recordsFiltered" => $this->karyawan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_keluarga()
	{

		$list = $this->keluarga->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

				$link_edit = ' <a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_keluarga."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_keluarga."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
		
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->jenis_kelamin;
			//  $row[] = $post->no_hp;
			$row[] = $post->hubungan_keluarga;
			// $row[] = $post->no_hp;
			

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->keluarga->count_all(),
						"recordsFiltered" => $this->keluarga->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	private function _do_upload(){

	      $config['upload_path']          = './assets/lampiran_karyawan/';
	      $config['allowed_types']        = 'gif|jpg|png|pdf';
	      $config['max_size']             = 1000; //set max size allowed in Kilobyte
	      $config['max_width']            = 3000; // set max width image allowed
	      $config['max_height']           = 3000; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('ktp')) //upload and validate
	        {
	            $data['inputerror'][] = 'ktp';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	}


	private function _do_upload_kk(){

	      $config['upload_path']          = './assets/lampiran_karyawan/';
	      $config['allowed_types']        = 'gif|jpg|png|pdf';
	      $config['max_size']             = 1000; //set max size allowed in Kilobyte
	      $config['max_width']            = 3000; // set max width image allowed
	      $config['max_height']           = 3000; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('kk')) //upload and validate
	        {
	            $data['inputerror'][] = 'kk';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	}

	public function ajax_edit($id)
	{
		$data = $this->karyawan->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'nik' 				=> $this->input->post('nik'),
			'no_ktp' 				=> $this->input->post('no_ktp'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			'tempat_lahir' 		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'alamat' 			=> $this->input->post('alamat'),
			'no_hp' 			=> $this->input->post('no_hp'),
			'agama' 			=> $this->input->post('agama'),
			'status_perkawinan' 	=> $this->input->post('status_perkawinan'),
			'bpjs_kesehatan' 		=> $this->input->post('bpjs_kesehatan'),
			'bpjs_ketenagakerjaan' 	=> $this->input->post('bpjs_ketenagakerjaan'),
			'insentif' 				=> $this->input->post('insentif'),
			'tunjangan_jabatan' 	=> $this->input->post('tunjangan_jabatan'),
			'tunjangan_site' 		=> $this->input->post('tunjangan_site'),
			'tanggal_masuk' 		=> $this->input->post('tanggal_masuk'),
			'tanggal_phk' 			=> $this->input->post('tanggal_phk'),
			'stt_karyawan' 			=> $this->input->post('stt_karyawan'),
			'range_mcu' 			=> $this->input->post('range_mcu'),
			'id_department' 			=> $this->input->post('department'),
			'id_jabatan' 			=> $this->input->post('range_mcu'),
			'id_lokasi' 			=> $this->input->post('range_mcu')
		);

		if(!empty($_FILES['ktp']['name']))
		{
			$upload = $this->_do_upload();
			$data['ktp'] = $upload;
		}


		if(!empty($_FILES['kk']['name']))
		{
			$upload = $this->_do_upload_kk();
			$data['kk'] = $upload;
		}

		$this->karyawan->save($data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_add_keluarga()
	{
		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'nik' 				=> $this->input->post('nik'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'no_hp' 			=> $this->input->post('no_hp'),
			'hubungan_keluarga' => $this->input->post('hubungan_keluarga')
		);

		$this->db->insert('keluarga', $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'nama_lengkap' 		=> $this->input->post('nama_lengkap'),
			'nik' 				=> $this->input->post('nik'),
			'no_ktp' 				=> $this->input->post('no_ktp'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			'tempat_lahir' 		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'),
			'alamat' 			=> $this->input->post('alamat'),
			'no_hp' 			=> $this->input->post('no_hp'),
			'agama' 			=> $this->input->post('agama'),
			'status_perkawinan' 	=> $this->input->post('status_perkawinan'),
			'bpjs_kesehatan' 		=> $this->input->post('bpjs_kesehatan'),
			'bpjs_ketenagakerjaan' 	=> $this->input->post('bpjs_ketenagakerjaan'),
			'insentif' 				=> $this->input->post('insentif'),
			'tunjangan_jabatan' 	=> $this->input->post('tunjangan_jabatan'),
			'tunjangan_site' 		=> $this->input->post('tunjangan_site'),
			'tanggal_masuk' 		=> $this->input->post('tanggal_masuk'),
			'tanggal_phk' 			=> $this->input->post('tanggal_phk'),
			'stt_karyawan' 			=> $this->input->post('stt_karyawan'),
			'range_mcu' 			=> $this->input->post('range_mcu'),
			'id_department' 		=> $this->input->post('department'),
			'id_jabatan' 			=> $this->input->post('jabatan'),
			'id_lokasi' 			=> $this->input->post('lokasi')
		);


		// if($this->input->post('ktp')) // if remove photo checked
  //     {
  //        if(file_exists('./assets/lampiran_karyawan/'.$this->input->post('ktp')) && $this->input->post('ktp'))
  //           unlink('./assets/lampiran_karyawan/'.$this->input->post('ktp'));
  //        $data['ktp'] = '';
  //     }
 
      if(!empty($_FILES['ktp']['name']))
      {
         $upload = $this->_do_upload();
             
         //delete file
         $karyawan = $this->karyawan->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_karyawan/'.$karyawan->ktp) && $karyawan->ktp)
             unlink('./assets/lampiran_karyawan/'.$karyawan->ktp);
 
         $data['ktp'] = $upload;
      }


      if(!empty($_FILES['kk']['name']))
      {
         $upload = $this->_do_upload_kk();
             
         //delete file
         $karyawan = $this->karyawan->get_by_id($this->input->post('id'));
         // var_dump($berita);
         if(file_exists('./assets/lampiran_karyawan/'.$karyawan->kk) && $karyawan->kk)
             unlink('./assets/lampiran_karyawan/'.$karyawan->kk);
 
         $data['kk'] = $upload;
      }

		$this->karyawan->update(array('id_karyawan' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('karyawan',array('id_karyawan'=>$id));
		echo json_encode(array("status" => TRUE));
	}


}
