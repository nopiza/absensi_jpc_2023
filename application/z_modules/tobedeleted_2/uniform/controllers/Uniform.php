<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class uniform extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'uniform');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Uniform_model','uniform');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
		
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['department'] = $this->db->get('department')->result();
      	$user_data['jabatan'] = $this->db->get('jabatan')->result();
      	$user_data['lokasi'] = $this->db->get('lokasi')->result();
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->uniform->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Detail" onclick="edit('."'".$post->id_pembagian."'".')">Detail</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_pembagian."'".')"> Delete</a>';
			
		
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->nama_department;
         	$row[] = $post->nama_jabatan;
         	$row[] = $post->nama_uniform;
         	$row[] = $post->jumlah;
         	$row[] = tgl_indo($post->tanggal_pembagian);
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->uniform->count_all(),
						"recordsFiltered" => $this->uniform->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->uniform->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'id_karyawan' 			=> $this->input->post('karyawan'),
			'id_uniform' 			=> $this->input->post('uniform'),
			'tanggal_pembagian' 	=> $this->input->post('tanggal'),
			'jumlah' 				=> $this->input->post('jumlah'),
			'catatan' 				=> $this->input->post('catatan')
		);

		$this->db->insert('pembagian_uniform',$data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'id_karyawan' 			=> $this->input->post('karyawan'),
			'id_uniform' 			=> $this->input->post('uniform'),
			'tanggal_pembagian' 	=> $this->input->post('tanggal'),
			'jumlah' 				=> $this->input->post('jumlah'),
			'catatan' 				=> $this->input->post('catatan')
		);

		$this->uniform->update(array('id_pembagian' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('pembagian_uniform',array('id_pembagian'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function detailuniform($id)
	{
		$query = "SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id'" ;
		$kyr = $this->db->query($query)->row_array();
		$a = '<div class="form-group row">
		<label class="control-label col-md-3">Nama Lengkap</label>
		<div class="col-md-5">
		<input name="karyawan" value="'.$kyr['nama_lengkap'].'" class="form-control" type="text"  readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Lokasi</label>
		<div class="col-md-5">
			<input name="lokasi" id="lokasi"  value="'.$kyr['nama_lokasi'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Department</label>
		<div class="col-md-5">
			<input name="department" id="department"  value="'.$kyr['nama_department'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-3">Jabatan</label>
		<div class="col-md-5">
			<input name="jabatan" id="jabatan"  value="'.$kyr['nama_jabatan'].'" class="form-control" type="text" readonly>
			<span class="help-block"></span>
		</div>
	</div>
	
	<table class="table table-bordered" >
              
		<thead>
				<tr>
					<th width="5%">No</th>
					<th width="15%">Tanggal Awal</th>
					<th width="15%">Tanggal Akhir</th>
					<th width="5%">Jumlah</th>
					<th width="30%">Alamat uniform</th>
					<th width="20%">Keterangan</th>
					<th width="10%">Lampiran</th>
			</tr>
			</thead>
			<tbody>';
		$b = '';
		$no=1;
		$query = "SELECT * FROM uniform WHERE id_karyawan='$id'";
		$komisi = $this->db->query($query)->result();
		foreach($komisi as $kms){ 
		
		$b .= '<tr>
				
			<td>'.$no++.'</td>
			<td>'.tgl_indo($kms->tanggal_awal).'</td>
			<td>'.tgl_indo($kms->tanggal_akhir).'</td>
			<td>'.$kms->jumlah_hari.'</td>
			<td>'.$kms->alamat_uniform.'</td>
			<td>'.$kms->keterangan.'</td>
			<td><a href="'.base_url('assets/lampiran_uniform/'.$kms->lampiran).'" target="_blank" class="">Lampiran</a></td>
		</tr>';
		}

		echo $a.$b.'</tbody>
		</table>';

	}


	public function ajax_select_karyawan(){
        $this->db->select('id_karyawan,nama_lengkap');
        $this->db->like('nama_lengkap',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('karyawan')->result_array();
        //output to json format
        echo json_encode($items);
    }

	public function ajax_select_uniform(){
        $this->db->select('id_datauniform,nama_uniform');
        $this->db->like('nama_uniform',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('data_uniform')->result_array();
        //output to json format
        echo json_encode($items);
    }

	public function get($id_karyawan){
        $item=$this->db->query("SELECT * FROM karyawan k 
		LEFT JOIN department d ON k.id_department = d.id_department 
		LEFT JOIN lokasi l ON l.id_lokasi = k.id_lokasi 
		LEFT JOIN jabatan j ON j.id_jabatan = k.id_jabatan 
		WHERE k.id_karyawan ='$id_karyawan' ")->row_array();
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }



}
