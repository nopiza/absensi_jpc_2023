<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datauniform extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'datauniform');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Datauniform_model','datauniform');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
		
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['department'] = $this->db->get('department')->result();
      	$user_data['jabatan'] = $this->db->get('jabatan')->result();
      	$user_data['lokasi'] = $this->db->get('lokasi')->result();
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->datauniform->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_datauniform."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_datauniform."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_uniform;
         	$row[] = $post->keterangan;
         	$row[] = rupiah($post->stok);
         	
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->datauniform->count_all(),
						"recordsFiltered" => $this->datauniform->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	public function ajax_edit($id)
	{
		$data = $this->datauniform->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
			'nama_uniform' 		=> $this->input->post('nama_uniform'),
			'keterangan' 		=> $this->input->post('keterangan'),
			'stok' 				=> $this->input->post('stok')
		);

		$this->db->insert('data_uniform',$data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{

		$data = array(
			'nama_uniform' 		=> $this->input->post('nama_uniform'),
			'keterangan' 		=> $this->input->post('keterangan'),
			'stok' 				=> $this->input->post('stok')
		);


	

		$this->datauniform->update(array('id_datauniform' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('data_uniform',array('id_datauniform'=>$id));
		echo json_encode(array("status" => TRUE));
	}


}
