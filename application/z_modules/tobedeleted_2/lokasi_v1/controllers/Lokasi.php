<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'lokasi');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Lokasi_model','lokasi');
		$this->load->model('Lokasidetail_model','lokasi_detail');
		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->lokasi->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_lokasi."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_lokasi."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lokasi;
         	$row[] = $post->keterangan;

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->lokasi->count_all(),
						"recordsFiltered" => $this->lokasi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->lokasi->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'nama_lokasi' 		=> $this->input->post('lokasi'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->lokasi->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'nama_lokasi' 		=> $this->input->post('lokasi'),
			'keterangan' 		=> $this->input->post('keterangan')
		);

		$this->lokasi->update(array('id_lokasi' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('lokasi',array('id_lokasi'=>$id));
		echo json_encode(array("status" => TRUE));
	}


	public function detail($idLokasi)
	{

      	$user_data['data_ref'] = $this->data_ref;
     	
      	$this->load->view('template/header',$user_data);
		$this->load->view('detail',$user_data);

	}


	public function ajax_list_detail()
	{

		$list = $this->lokasi_detail->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_lokasi."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_lokasi."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_lengkap;
         	$row[] = $post->nama_department;
         	$row[] = $post->nama_jabatan;

			if($post->tgl_habis_kontrak == ''){
				$row[] = '<small class="badge badge-warning">Belum ada data</small>';
			}else{
				$hari = countDown($post->tgl_habis_kontrak);
				if($hari > 30){
					$warna = 'badge-secondary';
				}elseif($hari < 30 AND $hari >1){
					$warna = 'badge-warning';
				}elseif($hari < 0){
					$warna = 'badge-danger';
				}

				if($hari < 0){
					$row[] = '<small class="badge '.$warna.'"><i class="far fa-clock"></i> lewat '.str_replace('-','',$hari) .' Hari</small>';
				}else{
					$row[] = '<small class="badge '.$warna.'"><i class="far fa-clock"></i> '.$hari.' Hari</small>';
				}
				
			}
			 

			if($post->next_mcu == ''){
				$row[] = '<small class="badge badge-warning">Belum ada data</small>';
			}else{
				$hari = countDown($post->next_mcu);
				if($hari >= 30){
					$warna = 'badge-secondary';
				}elseif($hari < 30 AND $hari >1){
					$warna = 'badge-warning';
				}elseif($hari <= 0){
					$warna = 'badge-danger';
				}
				$row[] = tgl_indo($post->next_mcu).'<br>'.'<small class="badge '.$warna.'"><i class="far fa-clock"></i> '.$hari.' Hari</small>';
			}
         	
         	$row[] = '';

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->lokasi_detail->count_all(),
						"recordsFiltered" => $this->lokasi_detail->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	
}
