<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'statistik');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Statistik_model','statistik');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list_statistik()
	{

		$list = $this->statistik->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			
			$link_edit = '<a class="btn btn-xs btn-danger" href="'.base_url('statistik/cetak/'.$post->id_pembayaran).'" target="_blank"> Cetak</a>';
			$no++;
			$row = array();
         	$row[] = $no;

         	if($post->status == '0'){
				$stt = 'Kosong';
			}elseif($post->status == '1'){
				$stt = 'Booking';
			}elseif($post->status == '2'){
				$stt = '<button class="btn btn-primary btn-xs">Cash</button>';
			}elseif($post->status == '3'){
				$stt = '<button class="btn btn-warning btn-xs">Kredit</button>';
			}else{
				$stt = 'Pengurus';
			}

         	
			$row[] = tgl_indo($post->tanggal);
			$row[] = $post->kode_kavling.' / '.$stt.'<br>'.$post->nama_lengkap ;
			$row[] = rupiah($post->jumlah_bayar);
			if($post->pembayaran_ke == '0'){
				if($post->jenis_pembelian == '2'){
					$row[] = 'Pembayaran Pembelian Cash';
				}else{
					$row[] = 'Pembayaran DP';
				}
			}else{
				$row[] = 'Pembayaran Cicilan ke- '.$post->pembayaran_ke;
			}
			
			$row[] = $link_edit;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->statistik->count_all(),
						"recordsFiltered" => $this->statistik->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

		private function _do_upload()
	   {
	      $config['upload_path']          = './assets/berita/';
	      $config['allowed_types']        = 'gif|jpg|png';
	      $config['max_size']             = 0; //set max size allowed in Kilobyte
	      $config['max_width']            = 0; // set max width image allowed
	      $config['max_height']           = 0; // set max height allowed
	      $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
	 
	      $this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	        if(!$this->upload->do_upload('logo')) //upload and validate
	        {
	            $data['inputerror'][] = 'logo';
	            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        return $this->upload->data('file_name');
	   }

	public function ajax_edit($id)
	{
		$data = $this->kavling->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		// $this->_validate();
		$post_date = time();
		$post_date_format = date('Y-m-d h:i:s', $post_date);
      // $user = $this->ion_auth->user()->row();
		$data = array(
				'tanggal' 	=> date('Y-m-d'),
				'judul' 	=> $this->input->post('judul'),
				'isi_agenda' 	=> $this->input->post('judul'),
				'status' 	=> $this->input->post('status_agenda'),
            	'is_trash' 	=> 0
		);

		
		$insert = $this->kavling->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'kode_kavling'	=> $this->input->post('kode_kavling'),
			'luas_tanah'	=> $this->input->post('luas_tanah'),
			'hrg_meter'		=> $this->input->post('harga_per_meter'),
			'hrg_jual'		=> $this->input->post('harga_jual'),
			'jenis_map'		=> $this->input->post('jenis_map'),
			'map'			=> $this->input->post('map')

		);



		$this->db->update('kavling', $data , array('id_kavling' => $this->input->post('id')));
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->kavling->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul') == '')
		{
			$data['inputerror'][] = 'judul';
			$data['error_string'][] = 'Judul harus diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('isi_konten') == '')
		{
			$data['inputerror'][] = 'isi_konten';
			$data['error_string'][] = 'Konten harus diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}





	public function cetak_v1($id){

		
		// $this->db->from('kavling_peta a');
		$this->db->join('transaksi b', 'a.id_kavling = b.id_kavling', 'left');
		$this->db->join('customer c', 'a.id_customer = c.id_customer', 'left');
		$kavling = $this->db->get_where('kavling_peta a', array('a.id_kavling'=>$id))->row_array();
		// $kavling = $this->db->query("SELECT * FROM  WHERE id_kavling = $id")->row_array();

        $file=VIEWPATH."template\kwitansi.pdf";
        $config=array('file'=>$file);
        $config['jasi']=$config;


        $this->load->library('MyPDFI',$config);
        $this->mypdfi->AddPage();
        $this->mypdfi->SetFont('Arial','',6);

		$tanggal        = date("Y-m-d");

		
        $this->mypdfi->SetFont('Arial','B',9);


        //Data AK 1
        $this->mypdfi->text(163,28, "005/KBV/VIII/2020");
        $this->mypdfi->SetFont('Arial','',6.5);


        // $this->mypdfi->text(90,28,$siswa['kode_kelas']);
        $this->mypdfi->SetFont('Times','',12);

        //Data Diri
        $this->mypdfi->text(60,33, $kavling['nama_lengkap']);
        $this->mypdfi->text(60,41, ucwords(penyebut($kavling['jumlah_dp'])).'Rupiah');
        
        if($kavling['jenis_pembelian'] == '3'){
        	$this->mypdfi->text(60,49, "Pembayaran DP Pembelian Kredit Kavling MINAS Blok ".$kavling['kode_kavling']);
        }elseif($kavling['jenis_pembelian'] == '2'){
        	$this->mypdfi->text(60,49, "Pembayaran Awal Pembelian CASH Kavling MINAS Blok ".$kavling['kode_kavling']);
        }
        
        $this->mypdfi->SetFont('Times','B',16);
        $this->mypdfi->text(35,69.5, rupiah($kavling['jumlah_dp']));



        $this->mypdfi->SetFont('Times','',11);
        $this->mypdfi->text(140,69,"Balikpapan, ".tgl_indo($tanggal));
        $this->mypdfi->text(152,85, 'Faisal Damanik');


       // 
        $this->mypdfi->Output();       

        // $pdfFilePath = FCPATH."/kartu_pdf/kartu_pelajar_".$siswa['id_siswa'].".pdf";

        // $this->mypdfi->Output($pdfFilePath, 'F');    

        // redirect('kartu/index/'.$siswa['id_siswa'],'refresh');     
	}



	function cetak($id){

        
		$this->db->join('kavling_peta b', 'a.id_kavling = b.id_kavling', 'left');
		$this->db->join('customer c', 'a.id_customer = c.id_customer', 'left');
		$kavling = $this->db->get_where('pembayaran a', array('a.id_pembayaran'=>$id))->row_array();
		$konfig = $this->db->get_where('konfigurasi a', array('a.id'=>'1'))->row_array();
        

        $config=array('orientation'=>'P','size'=>'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->SetFont('Arial','B',10);
        $this->mypdf->SetLeftMargin(10);
        $this->mypdf->addPage();
        $this->mypdf->setTitle('Kwintasi Pembayaran');
        $this->mypdf->SetFont('Arial','B',14);

		//Master Desain background Kwitansi Pembayaran
		$this->mypdf->Image(base_url().'assets/aplikasi/kwitansi.jpg',10,10,190);
		// LOgo Kavling
		$this->mypdf->Image(base_url().'assets/aplikasi/'.$konfig['logo'],18,15,15);


		// Nomor Pembayaran_model $this->mypdfi->SetTextColor(229,8,8);
        $this->mypdf->SetFont('Arial','B',8);
		$this->mypdf->SetTextColor(229,8,8);
        $this->mypdf->text(158,33.7, $kavling['no_pembayaran']);
        $this->mypdf->SetTextColor(0,0,0);

		//Data Diri
		$this->mypdf->SetFont('Times','',12);
        $this->mypdf->text(65,39, $kavling['nama_lengkap']);
        $this->mypdf->text(65,47, ucwords(penyebut($kavling['jumlah_bayar'])).'Rupiah');
		if($kavling['pembayaran_ke']== '0'){
			if($kavling['jenis_pembelian']== '2'){
				$this->mypdf->text(65,55, "Pembayaran Pembelian ".$konfig['nama_kavling']." Blok ".$kavling['kode_kavling']);
			}else if($kavling['jenis_pembelian']== '3'){
				$this->mypdf->text(65,55, "Pembayaran DP Pembelian ".$konfig['nama_kavling']." Blok ".$kavling['kode_kavling']);
			}
		}else{
			$this->mypdf->text(65,55, "Pembayaran Cicilan kex ".$kavling['pembayaran_ke']." ".$konfig['nama_kavling']." Blok ".$kavling['kode_kavling']);
		}

        
        $this->mypdf->SetFont('Times','B',16);
        $this->mypdf->text(37,74, rupiah($kavling['jumlah_bayar']));



        $this->mypdf->SetFont('Times','',11);

		$this->mypdf->SetY(70);
		$this->mypdf->Cell(120,4,'',0,0,'C');
        $this->mypdf->Cell(60,4,'Balikpapan, '.tgl_indo($kavling['tanggal']),0,0,'C');
		$this->mypdf->ln(21);
		$this->mypdf->Cell(120,4,'',0,0,'C');
        $this->mypdf->Cell(60,4,$konfig['nama_penandatangan'],0,0,'C');

		//Tanda Tangan
		$this->mypdf->Image(base_url().'assets/aplikasi/'.$konfig['file_ttd'],135,57,50);


		// Rekening Pembayaran
		$this->mypdf->SetFont('Times','',9);
		$this->mypdf->text(22,82,'Rekening Pembayaran : ');
		$this->mypdf->text(22,85,$konfig['nama_bank']);
		$this->mypdf->SetFont('Times','B',11);
		$this->mypdf->text(22,89,$konfig['no_rekening']);
		$this->mypdf->SetFont('Times','',10);
		$this->mypdf->text(22,92,$konfig['nama_pemilik_rek']);
        

        
        // $posisi = $this->mypdf->GetY();
        
        // $this->mypdf->SetFont('Arial','',8);
        // $this->mypdf->text(11,$posisi+ 15,"KETERANGAN IJIN : . ");

        // $no=1;
        // $y=$posisi+ 20;



        // $this->mypdf->text(145,$y+10," KEPALA DINAS" );
        // $this->mypdf->text(148,$y+30,"......................");
        // $this->mypdf->text(145,$y+36,"..........................");

        $this->mypdf->Output();
    }



}
