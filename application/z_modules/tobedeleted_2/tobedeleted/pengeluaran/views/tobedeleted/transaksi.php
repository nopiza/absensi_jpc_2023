<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Permohonan Pelayanan Jasa Kapal</h3>

              <div class="card-tools">
                <form onsubmit="return confirm('Apakah data inputan sudah benar?');"
                action="<?=base_url('permohonan/baru');?>" id="form_master" class="form-horizontal" method="POST" enctype="multipart/form-data">
                
                <button class="btn btn-warning btn-sm" type="submit"><i class="fa fa-plus"></i> Proses Permohonan</button>
                <a href="<?=base_url('permohonan/tabel');?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Data Permohonan</a>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

                
        <div class="row col-lg-12">
            
        <div class="col-lg-6">
        <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                       
                    <div class="form-group row">
                    <label class="control-label col-md-4">No. Permohonan</label>
                        <div class="col-md-6">
                            <input name="no_permohonan" class="form-control" type="text" id="no_spb" readonly="" value="<?=$no_registrasi;?>">
                            <span class="help-block"></span>
                        </div>
                    </div>


                        <div class="form-group row">
                            <label class="control-label col-md-4">Tgl Permintaan</label>
                            <div class="col-md-6">
                                <input name="tanggal" placeholder="" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Nama Kapal</label>
                            <div class="col-md-6">
                                <input name="nama_kapal" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Call Sign</label>
                            <div class="col-md-6">
                                <input name="call_sign" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Bendera</label>
                            <div class="col-md-6">
                                <input name="bendera" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Keagenan</label>
                            <div class="col-md-6">
                            <input name="keagenan" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Jenis Kapal</label>
                            <div class="col-md-6">
                                <input name="jenis_kapal" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Sifat Kunjung</label>
                            <div class="col-md-6">
                                <input name="sifat_kunjungan" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">No. Trayek</label>
                            <div class="col-md-6">
                                <input name="no_trayek" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Jenis Pelayaran</label>
                            <div class="col-md-6">
                                <input name="jenis_pelayaran" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Jenis Trayek</label>
                            <div class="col-md-6">
                                <input name="jenis_trayek" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Jenis Kemasan</label>
                            <div class="col-md-6">
                                <input name="jenis_kemasan" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- <div class="form-group row">
                            <label class="control-label col-md-4">Pergerakan Dari</label>
                            <div class="col-md-6">
                                <input name="gerakan_dari" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-4">Pergerakan Ke</label>
                            <div class="col-md-6">
                                <input name="gerakan_ke" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div> -->


                    </div>
            </div>

        <div class="col-lg-6">  

                    <div class="form-group row">
                    <label class="control-label col-md-4">Jenis Kapal</label>
                        <div class="col-md-6">
                            <select class="form-control" name="jenis_kapal">
                            <option value="0">-</option>  
                            <option value="1">Kapal Asing</option>  
                            <option value="2">Kapal Lokal</option>   
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                            <label class="control-label col-md-4">Jam Permintaan</label>
                            <div class="col-md-6">
                                <input name="jam" placeholder="" class="form-control" type="time">
                                <span class="help-block"></span>
                            </div>
                        </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Berat Kotor</label>
                    <div class="col-md-6">
                        <input name="berat_kotor" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="control-label col-md-4">Panjang (LOA)</label>
                    <div class="col-md-6">
                        <input name="panjang_loa" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Bobot Mati</label>
                    <div class="col-md-6">
                        <input name="bobot_mati" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Draf Muka / Belakang</label>
                    <div class="col-md-6">
                        <input name="draf_muka_belakang" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Jenis Barang</label>
                    <div class="col-md-6">
                        <input name="jenis_barang" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Rencana Bongkar</label>
                    <div class="col-md-6">
                        <input name="rencana_bongkar" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Rencana Muat</label>
                    <div class="col-md-6">
                        <input name="rencana_muat" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Pelabuhan Muat</label>
                    <div class="col-md-6">
                        <input name="pelabuhan_muat" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Pelabuhan Asal</label>
                    <div class="col-md-6">
                        <input name="pelabuhan_asal" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Pelabuhan Tujuan</label>
                    <div class="col-md-6">
                        <input name="pelabuhan_tujuan" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-4">Pelabuhan Tujuan Akhir</label>
                    <div class="col-md-6">
                        <input name="pelabuhan_tujuan_akhir" placeholder="" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>
        </div>
        </div>

                    
           
                    

                    </div>
                </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>
