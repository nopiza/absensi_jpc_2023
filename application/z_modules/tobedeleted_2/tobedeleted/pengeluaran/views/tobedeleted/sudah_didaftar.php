  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Pendaftaran Pasien Berhasil</h3>

              <div class="card-tools">
                <?php
                if($this->uri->segment(2) == 'sudah_didaftar'){ ?>
                <a href="<?=base_url('pemeriksaan/');?>" class="btn btn-info btn-sm" ><i class="fa fa-plus"></i> Input Hasil Pemeriksaan</a>
            <?php } ?>
                <a href="<?=base_url('transaksi');?>" class="btn btn-primary btn-sm" type="submit"><i class="fa fa-plus"></i> Pendaftaran Baru</a>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                       <div class="form-group row">
                            <label class="control-label col-md-2">No. Pendaftaran</label>
                            <div class="col-md-3">
                                <input name="no_spb" class="form-control" type="text" id="no_spb" readonly="" value="<?=$registrasi['no_registrasi'];?>">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal</label>
                            <div class="col-md-3">
                                <input name="tanggal" class="form-control" type="date" value="<?=date('Y-m-d');?>" readonly="">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-2">NIK</label>
                            <div class="col-md-3">
                                <input name="nik" placeholder="" class="form-control" type="text" value="<?=$registrasi['nik'];?>" readonly="">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Nama Pasien</label>
                            <div class="col-md-5">
                                <input name="nama_pasien" placeholder="" class="form-control" type="text" value="<?=$registrasi['nama_pasien'];?>" readonly="">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Jenis Kelamin</label>
                            <div class="col-md-5">
                                <input name="jenis_kelamin" placeholder="" class="form-control" type="text" value="<?=$registrasi['jenis_kelamin'];?>" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Tempat Lahir</label>
                            <div class="col-md-5">
                                <input name="tempat_lahir" placeholder="" class="form-control" type="text" value="<?=$registrasi['tempat_lahir'];?>" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal Lahir</label>
                            <div class="col-md-3">
                                <input name="tgl_lahir" placeholder="" class="form-control" type="date" value="<?=$registrasi['tgl_lahir'];?>" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Alamat</label>
                            <div class="col-md-5">
                                <input name="alamat" placeholder="" class="form-control" type="text" value="<?=$registrasi['alamat'];?>" readonly="">
                                <span class="help-block"></span>
                            </div>
                        </div>


                    </div>


          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>
