  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Edit Data Pasien</h3>

              <div class="card-tools">
                <form onsubmit="return confirm('Apakah data inputan sudah benar?');"
                action="<?=base_url('transaksi/baru');?>" id="form_master" class="form-horizontal" method="POST" enctype="multipart/form-data">
                
                <button class="btn btn-warning btn-sm" type="submit"><i class="fa fa-plus"></i> Simpan Perubahan</button>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                       <div class="form-group row">
                            <label class="control-label col-md-2">No. Registrasi</label>
                            <div class="col-md-3">
                                <input name="no_spb" class="form-control" type="text" id="no_spb" readonly="" value="<?=$editrans['no_registrasi'];?>">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal</label>
                            <div class="col-md-3">
                                <input name="tanggal" class="form-control" type="date" value="<?=date('Y-m-d');?>">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-2">NIK</label>
                            <div class="col-md-3">
                                <input name="nik" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Nama Pasien</label>
                            <div class="col-md-5">
                                <input name="nama_pasien" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Jenis Kelamin</label>
                            <div class="col-md-3">
                                <select name="jenis_kelamin" class="form-control" >
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>

                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Tempat Lahir</label>
                            <div class="col-md-5">
                                <input name="tempat_lahir" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal Lahir</label>
                            <div class="col-md-3">
                                <input name="tgl_lahir" placeholder="" class="form-control" type="date" >
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Alamat</label>
                            <div class="col-md-5">
                                <input name="alamat" placeholder="" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>


                    </div>
                </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>
