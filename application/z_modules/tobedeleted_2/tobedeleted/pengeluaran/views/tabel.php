  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Catatan Pengeluaran Operasional</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Pengeluaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Pengeluaran</h3>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">
     
        <form action="<?=base_url('pengeluaran/pengeluaran_proses');?>" method="post" enctype="multipart/form-data">

        <div class="form-group row">
        <label class="control-label col-md-2">Tanggal</label>
            <div class="col-md-6">
                <input name="tanggal" class="form-control" type="date" >
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group row">
        <label class="control-label col-md-2">Deskripsi</label>
            <div class="col-md-6">
                <input name="deskripsi" class="form-control" type="text" >
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group row">
        <label class="control-label col-md-2">Jumlah</label>
            <div class="col-md-1">
                <input name="pcs" class="form-control" type="text" >
                <span class="help-block"></span>
            </div>
            <label class="control-label col-md-2">Harga Satuan</label>
            <div class="col-md-3">
                <input name="harga_satuan" class="form-control" type="text" >
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group row">
        <label class="control-label col-md-2">Keterangan</label>
            <div class="col-md-6">
                <input name="keterangan" class="form-control" type="text" >
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group row">
        <label class="control-label col-md-2">Bukti Pembayaran</label>
            <div class="col-md-6">
                <input name="fupload"  type="file" >
                <span class="help-block"></span>
            </div>
        </div>

        

        <div class="form-group row">
        <label class="control-label col-md-2"></label>
            <div class="col-md-6">
                <button class="btn btn-primary">Simpan</button>
                <span class="help-block"></span>
            </div>
        </div>
    </form>


        <hr>


           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="3%">No</th>
                        <th width="20%">Tanggal</th>
                        <th width="35%">Uraian</th>
                        <th width="5%">Pcs</th>
                        <th width="10%">Hrg Satuan</th>
                        <th width="8%">Jumlah</th>
                        <th width="8%">Lampiran</th>
                        <th width="12%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>





<div class="modal fade" id="modal-lg">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Large Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="detail">
        
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->





</body>
</html>





<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });


      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });

  

   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
   }

   function hapus(id){
    $.confirm({
      title: 'Confirm!',
      content: 'Apakah anda yakin menghapus data ini ?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "<?php echo $data_ref['uri_controllers']; ?>/pengeluaran/ajax_delete/" + id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  reload_table();
                  Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Dihapus'
                   });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }


  function bukti(id)
   {

       $('#modal-lg').modal('show'); // show bootstrap modal
       $('.modal-title').text('Bukti Pengeluaran No. #' + id); // Set Title to Bootstrap modal title
       $("#detail").load('<?php echo base_url('pengeluaran/lampiran/');?>' + id);
   }
  

</script>


