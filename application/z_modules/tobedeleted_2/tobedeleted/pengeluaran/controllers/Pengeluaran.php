<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'pengeluaran');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pengeluaran_model','pengeluaran');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

    public function index()
	{

        $user_data['data_ref'] = $this->data_ref;

        $this->load->view('template/header',$user_data);
        $this->load->view('tabel',$user_data);
        // $this->load->view('template/footer',$user_data);

	}


    


    public function ajax_list()
	{

		$list = $this->pengeluaran->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {


			$link_edit = ' <a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_pengeluaran."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_pengeluaran."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			
			$no++;
			$row = array();
         	$row[] = $no;
            $row[] = tgl_indo($post->tanggal);
         	$row[] = $post->keterangan;
			$row[] = $post->pcs;
			$row[] = rupiah($post->harga_satuan);
			$row[] = rupiah($post->harga_satuan * $post->pcs);
			$row[] = ' <a class="btn btn-xs btn-info" href="javascript:void(0)" title="Edit" onclick="bukti('."'".$post->id_pengeluaran."'".')"><i class="glyphicon glyphicon-pencil"></i> Bukti</a>';
			//add html for action
			$row[] = $link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->pengeluaran->count_all(),
						"recordsFiltered" => $this->pengeluaran->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}




    public function ajax_delete($id)
	{
		$this->db->delete('pengeluaran',array('id_pengeluaran'=>$id));
		echo json_encode(array("status" => TRUE));
	}


    public function caritrans($id){
        $query = "SELECT * FROM permohonan WHERE id_permohonan ='$id'";
        $item = $this->db->query($query)->row_array();

        //Cari master data kurs dan tarif tetap
        $kurs = $this->db->query("SELECT * FROM kurs_mata_uang WHERE id_kurs='1'")->row_array();

        if($item['jenis_pelayaran_kapal'] == 'LOKAL'){
            $item['kurs'] = '1';
            $item['tarif_tetap'] = rupiah($kurs['kapal_lokal'], 2);
            $item['tarif_variabel'] = rupiah($kurs['variabel_lokal'], 2);
            $jumlah = (int)$kurs['kapal_lokal'] + ((int)$item['berat_kotor'] * (int)$kurs['variabel_lokal']);
            $item['jumlah'] = rupiah($jumlah, 2);
            $item['idr'] = rupiah($jumlah, 2);
        }else{
            //ASING -------------------------------------
            $item['kurs'] = rupiah($kurs['nilai'], 2);
            $item['tarif_tetap'] = rupiah($kurs['kapal_asing'], 2);
            $item['tarif_variabel'] = rupiah($kurs['variabel_asing'], 2);
            $jumlah = (int)$kurs['kapal_asing'] + ((int)$item['berat_kotor'] * (12/100));
            $idr = (int)$kurs['kapal_asing'] + ((int)$item['berat_kotor'] * (12/100)) * (int)$kurs['nilai'];
            $item['jumlah'] = rupiah($jumlah, 2);
            $item['idr'] = rupiah($idr, 2);
        }

        $item['berat_kotor'] = rupiah($item['berat_kotor']);
        
        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }



    public function edit($id)
    {

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';

      $query = "SELECT *, transaksi.keterangan as ket FROM transaksi 
  LEFT JOIN dokter ON transaksi.id_dokter = dokter.id_dokter 
  LEFT JOIN keperluan ON transaksi.keperluan = keperluan.id_keperluan 
  WHERE Transaksi.id_transaksi";

      $user_data['editrans'] = $this->db->query($query)->row_array();

        
        $this->load->view('template/header',$user_data);
        $this->load->view('transaksi_edit',$user_data);
        // $this->load->view('template/footer',$user_data);

    }


    public function detail($id)
    {

      $user_data['data_ref'] = $this->data_ref;
      $user_data['trans'] = $this->db->query("SELECT * FROM transaksi a 
        LEFT JOIN vendor b ON a.id_vendor = b.id_vendor 
        WHERE a.id_transaksi = '$id'")->row_array();


        
        $this->load->view('template/header',$user_data);
        $this->load->view('transaksi_detail',$user_data);
        // $this->load->view('template/footer',$user_data);

    }

	
	public function baru()
	{

    	$data = array(
            'tanggal'               => $this->input->post('tanggal'),
            'jam'               => $this->input->post('jam'),
            'no_permohonan'         => $this->input->post('no_permohonan'),
            'nama_kapal'            => $this->input->post('nama_kapal'),
            'call_sign'             => $this->input->post('call_sign'),
            'bendera'               => $this->input->post('bendera'),
            'keagenan'              => $this->input->post('keagenan'),
            'jenis_kapal'           => $this->input->post('jenis_kapal'),
            'sifat_kunjungan'       => $this->input->post('sifat_kunjungan'),
            'no_trayek'             => $this->input->post('no_trayek'),
            'jenis_pelayaran'       => $this->input->post('jenis_pelayaran'),
            'jenis_trayek'          => $this->input->post('jenis_trayek'),
            'jenis_kemasan'         => $this->input->post('jenis_kemasan'),
            'berat_kotor'           => $this->input->post('berat_kotor'),
            'panjang_loa'           => $this->input->post('panjang_loa'),
            'bobot_mati'            => $this->input->post('bobot_mati'),
            'draf_muka_belakang'    => $this->input->post('draf_muka_belakang'),
            'jenis_barang'          => $this->input->post('jenis_barang'),
            'rencana_bongkar'       => $this->input->post('rencana_bongkar'),
            'rencana_muat'          => $this->input->post('rencana_muat'),
            'pelabuhan_asal'        => $this->input->post('pelabuhan_asal'),
            'pelabuhan_tujuan'      => $this->input->post('pelabuhan_tujuan'),
            'pergerakan'            => $this->input->post('gerakan'),
            'pelabuhan_muat'            => $this->input->post('pelabuhan_muat'),
            'pelabuhan_tujuan_akhir'            => $this->input->post('pelabuhan_tujuan_akhir')
		);

		$this->db->insert('permohonan', $data);
        $id = $this->db->insert_id();
		redirect('permohonan/tabel');
	}

    public function pengeluaran_proses(){
        $nama_file   = $_FILES['fupload']['name'];           
        
        if (!empty($nama_file)){
                
                $folder = 'file_pengeluaran/';
                $file_upload = $folder .$nama_file;

                move_uploaded_file($_FILES['fupload']['tmp_name'],$file_upload);
                        
                $data=array(
                'tanggal'  	        => $this->input->post('tanggal'),
                'deskripsi' 	   	=> $this->input->post('deskripsi'),
                'keterangan' 	   	=> $this->input->post('keterangan'),
                'pcs'     	        => $this->input->post('pcs'),
                'harga_satuan'      => $this->input->post('harga_satuan'),
                'sub_total'         => $this->input->post('pcs') * $this->input->post('harga_satuan'),
                'file_lampiran'     => $nama_file,
                'id_user'           => '1',
                'surname'           => 'asd'
                );
        
                $this->db->insert("pengeluaran",$data);
                redirect('pengeluaran');

        }else{

                $data=array(
                'tanggal'  	        => $this->input->post('tanggal'),
                'deskripsi' 	   	=> $this->input->post('deskripsi'),
                'keterangan' 	   	=> $this->input->post('keterangan'),
                'pcs'     	        => $this->input->post('pcs'),
                'harga_satuan'      => $this->input->post('harga_satuan'),
                'sub_total'         => $this->input->post('pcs') * $this->input->post('harga_satuan'),
                'id_user'           => '1'
                );
        
                $this->db->insert("pengeluaran",$data);
                redirect('pengeluaran');
        }
    }

    public function lampiran($id){
        $lampiran = $this->db->query("SELECT * FROM pengeluaran WHERE id_pengeluaran='$id'")->row_array();
        echo '<image src="'.base_url('file_pengeluaran/'.$lampiran['file_lampiran']).'" width="100%">';
    }



    public function sudah_didaftar($id){

        $user_data['data_ref'] = $this->data_ref;
        $user_data['registrasi'] = $this->db->query("SELECT * FROM transaksi WHERE id_transaksi='$id'")->row_array();


        $this->load->view('template/header',$user_data);
        $this->load->view('sudah_didaftar',$user_data);

    }



    public function ajax_add()
    {


        $data = array(
            'description'       => $this->input->post('description'),
            'part_number'       => $this->input->post('part_number'),
            'coli'              => $this->input->post('coli'),
            'items'             => $this->input->post('items'),
            'reference_spb'     => $this->input->post('reference'),
            'remark'            => $this->input->post('remark'),
            'id_transaksi'      => $this->input->post('id_transaksi')
        );

        
        $insert = $this->db->insert('transaksi_detail', $data);
        echo json_encode(array("status" => TRUE));

    }



	public function ajax_select_customer(){
        $this->db->select('id_vendor, nama_vendor');
        $this->db->like('nama_vendor',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('vendor')->result_array();
        //output to json format
        echo json_encode($items);
    }

    public function ajax_select_part(){
        $this->db->select('part_name, part_number');
        $this->db->like('part_number',$this->input->get('q'),'both');
        $this->db->limit(20);
        $items=$this->db->get('part_number')->result_array();
        //output to json format
        echo json_encode($items);
    }



    function no_transaksi(){
           
            $queryNoTrans = "SELECT MAX(no_permohonan) as besar FROM permohonan";
            $dataNoTrans = $this->db->query($queryNoTrans)->row_array();
            $numqNoTrans = $this->db->query($queryNoTrans)->num_rows();

            echo $numqNoTrans;
            if($numqNoTrans != NULL){
                $start = substr($dataNoTrans['besar'],4,5);
                @$next = $start + 1;
            }
            
            // echo $start;
            // echo '<br>';
            $tempNo = strlen($next);

            // echo $tempNo;
            // echo '<br>';
            
            if ($tempNo == '0')
            {
                $qNo = "00000";
            }
            elseif ($tempNo == 1)
            {
                $qNo = "0000";
            }
            elseif ($tempNo == 2)
            {
                $qNo = "000";
            }
            elseif ($tempNo == 3)
            {
                $qNo = "00";
            }
            elseif ($tempNo == 4)
            {
                $qNo = "0";
            }
            elseif ($tempNo == 5)
            {
                $qNo = "";
            }
            
            $tahun = date('Y');
            $qoNo = 'REG/'.$qNo.$next.'/LIB/'.$tahun;

            return $qoNo;
    }


    public function get($vendor){

        $item=$this->db->query("SELECT * FROM vendor WHERE id_vendor ='$vendor' ")->row_array();

        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }


    public function get_part($pname){
        $pn = str_replace('%20', ' ', $pname);
        $query = "SELECT * FROM part_number WHERE part_number ='$pn'";
        $item=$this->db->query($query)->row_array();

        return $this->output->set_content_type('application/json')->set_output(json_encode($item));        
    }

    public function isitem()
    {

        $data = '';
        $no = 1;
        $id = $this->uri->segment(3);
        $iteme = $this->db->query("SELECT * FROM transaksi_detail WHERE id_transaksi='$id'")->result();
        
        foreach ($iteme as $itm) {
            $pecah = explode('#', $itm->description);
            $des = implode("<br>",$pecah);
            $data .= ' 
            <tr>
                <td>'.$no++.'</td>
                <td>'.$des.'</td>
                <td>'.$itm->coli.'</td>
                <td>'.$itm->items.'</td>
                <td>'.$itm->reference_spb.'</td>
                <td>'.$itm->remark.'</td>
                <td><a href="'.base_url('transaksi/hapusdetail/'.$itm->id_detail.'/'.$itm->id_transaksi).'" 
                onclick="return confirm(\'Item akan dihapus?\')"
                class="btn btn-danger btn-xs"> Hapus </a></td>
            </tr>';
        }

        echo $data;
        
    }


    public function hapusdetail($id, $id_transaksi){

        $item = $this->db->query("DELETE FROM transaksi_detail WHERE id_detail ='$id'");

        redirect('transaksi/detail/'.$id_transaksi);        
    }


    private function _do_upload()
    {
        $config['upload_path']          = './lampiran/';
        $config['allowed_types']        = 'gif|jpg|png|docx|xlsx|zip|rar';
        $config['max_size']             = 0; //set max size allowed in Kilobyte
        $config['max_width']            = 0; // set max width image allowed
        $config['max_height']           = 0; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);
            $this->upload->initialize($config);
        if(!$this->upload->do_upload('lampiran')) //upload and validate
        {
            $data['inputerror'][] = 'lampiran';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }

	


}
