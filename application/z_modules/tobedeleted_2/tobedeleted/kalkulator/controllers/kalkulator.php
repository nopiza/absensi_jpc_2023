<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulator extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('customade');
		$this->load->library(array('form_validation'));	
		check_login();

	}
	
	public function index()
	{
		$data=array();
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		    );
		$data=array('csrf'=>$csrf);	

		$query = "SELECT * FROM kavling_peta";
		$data['kavling'] = $this->db->query($query)->result();


		$this->load->view('template/header',$data);
		$this->load->view('kalkulator',$data);
		$this->load->view('template/footer',$data);
	}


	public function get($id)
	{
		$query = "SELECT * FROM kavling_peta WHERE id_kavling='$id' ";
		$dataKavling = $this->db->query($query)->row_array();
		echo json_encode($dataKavling);
	}

	

	

}