  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Pembayaran</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">

        <!-- /.card-header -->
        <div class="card-body">

           <div class="row">
            <div class="col-sm-6">
              <table class="table table-bordered">
                <tr>
                  <td colspan="2" width="65%"><b><?=$cust['nama_lengkap'];?></b></td>
                </tr>
                <tr>
                  <td>Alamat</td>
                  <td><?=$cust['alamat'];?></td>
                </tr>
                <tr>
                  <td>No. Telp</td>
                  <td><?=$cust['no_telp'];?></td>
                </tr>
                <tr>
                  <td>Lokasi Kavling</td>
                  <td><?=$cust['kode_kavling'];?></td>
                  <input type="hidden" name="id_kavling" id="id_kavling" value="<?=$cust['id_kavling'];?>">
                </tr>
                <tr>
                  <td>Jenis Pembelian</td>
                  <td><?php if($cust['status'] == '2'){
                    echo 'Pembelian Cash';
                  }elseif($cust['status'] == '3'){
                    echo 'Pembelian Kredit';
                  };?>
                    
                  </td>
                </tr>
                <tr>
                  <td>Harga Jual</td>
                  <td align="right"><?=rupiah($cust['harga_jual']);?></td>
                </tr>
                <tr>
                  <td>Uang Muka</td>
                  <td align="right"><?=rupiah($cust['jumlah_dp']);?></td>
                </tr>
                <tr>
                  <td>Lama Cicilan</td>
                  <td align="right"><?=$cust['lama_cicilan'];?></td>
                </tr>
                <tr>
                  <td>Besar Cicilan</td>
                  <td align="right"><?=rupiah($cust['cicilan_per_bulan']);?></td>
                </tr>
                <tr>
                  <td>Sudah Bayar</td>
                  <td align="right"><b></b></td>
                </tr>
                <tr>
                  <td>Sisa Hutang</td>
                  <td align="right"><b></b></td>
                </tr>
              </table>
            </div>



            <div class="col-sm-6">
              <table class="table table-bordered">
                <tr>
                  <th width="23%">Cicilan </th>
                  <th width="30%">Tanggal Bayar</th>
                  <th width="30%">Jumlah Bayar</th>
                  <th width="18%">Action</th>
                </tr>
                <tr>
                  <td>Uang Muka</td>
                  <td>
                    <?php
                    //cek pembayaran DP di tabel pembayaran (cicilan ke 0)
                    $idCust = $cust['id_customer'];
                    $cekDP = $this->db->query("SELECT * FROM pembayaran WHERE id_kavling='$id_kavling' AND pembayaran_ke='0'")->row_array();
                      echo tgl_indo($cekDP['tanggal']);
                    ?>
                  </td>
                  <td align="right"><?=rupiah($cekDP['jumlah_bayar']);?></td>
                </tr>
                <?php for ($i=1; $i <= $cust['lama_cicilan']; $i++) { 

                  //Cek apakah ciclan bulan $i sudah terbayar
                  $query = "SELECT * FROM pembayaran WHERE pembayaran_ke='$i' AND id_kavling='$id_kavling'";
                  $cekk = $this->db->query($query)->num_rows();
                  if($cekk){
                    $bayar = $this->db->query($query)->row_array();
                    echo '<tr>
                      <td>Cicilan ke '.$i.'</td>
                      <td>'.tgl_indo($bayar['tanggal']).'</td>
                      <td align="right">'.rupiah($bayar['jumlah_bayar']).'</td>
                      <td><a class="btn btn-primary btn-xs" href="'.base_url('pembayaran/cetak/'.$bayar['id_pembayaran']).'" target="_blank">Cetak</a></td>
                    </tr>';
                  }else{
                    echo '<tr>
                      <td>Cicilan ke '.$i.'</td>
                      <td></td>
                      <td></td>
                      <td><button class="btn btn-danger btn-xs" id="bayar" onclick="bayar('.$i.')">Bayar</button></td>
                    </tr>';
                  }
                  
                }
                ?>
            </table>
           </div>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>





<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">

  var save_method; //for save method string
  var idKavling = document.getElementById("id_kavling").value;
  var url       = "<?php echo site_url(); ?>";

   function bayar(cicilanke)
   {

      url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_bayar')?>/" + cicilanke + '/' + idKavling;

    
       // ajax adding data to database
       var formData = new FormData($('#form')[0]);
    $.confirm({
      title: 'Peringatan!',
      content: 'Apakah melakukan proses pembayaran?',
      buttons: {
        Bayar: function () {

       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
                   location.reload(); 
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   },
        Tidak: function () {
          
        }
      }
    });
  }


</script>


