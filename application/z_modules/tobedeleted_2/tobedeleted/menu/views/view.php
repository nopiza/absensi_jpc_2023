  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Konten</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Konten</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Inbox</h3>

              <div class="card-tools">
                <?php if($tambah_view > 0){ ?>
                <a href="#" class="btn btn-info btn-sm" onclick="add()"><i class="fa fa-plus"></i> Tambah Konten</a>&nbsp;
                <?php } ?>
            <button class="btn btn-default btn-sm" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="3%">No</th>
                    <th width="5%">Induk</th>
                    <th width="20%">Menu</th>
                    <th width="20%">Jenis</th>
                    <th width="10%">Konten</th>
                    <th width="12%">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>



<link href="<?=base_url('waris/datatables/css/jquery.dataTables.min.css');?>" rel="stylesheet">

<?php  $this->load->view('template/footer'); ?>


<link rel="stylesheet" href="<?php echo site_url('theme/dist/css/Lobibox.min.css');?>"/>
<script src="<?php echo site_url('theme/dist/js/Lobibox.js');?>"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>theme/dist/css/jquery-confirm.min.css">
<script src="<?php echo base_url(); ?>theme/dist/js/jquery-confirm.min.js"></script>


<script src="<?=base_url('waris/datatables/js/jquery.dataTables.min.js')?>"></script>

<script src="<?=base_url('assets/admin/');?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


<div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Extra Large Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id" id="id"/> 


<div class="form-group row">
  <label class="col-sm-2 control-label">Parent Menu</label>
  <div class="col-md-3">
    <select name="parent_id" class="form-control" id="parent_id">
          <option value="0"> - Parent</option>  
    <?php if(isset($_GET['id'])){ 
      $paren = $this->db->query("SELECT * FROM menu WHERE id='$_GET[id]'")->result();
      foreach ($paren as $prn) {
      ?>
        <option value="<?= $prn->id; ?>"><?=$prn->title; ?></option>

<?php 
  }
}else{

   $paren = $this->db->query("SELECT * FROM menu")->result();
    foreach ($paren as $prn) {
    ?>
    <option value="<?= $prn->id; ?>"><?=$prn->title; ?></option>

 <?php
}
}
?>
</select>
<span class="help-block"></span>
</div>
</div>



<div class="form-group row">
  <label class="col-sm-2 control-label">Menu Title </label>
  <div class="col-md-3">
    <input name="title" type="text" class="form-control" id="title" value="<?php if(isset($_GET['id'])){ echo $em['title']; } ?>">
    <span class="help-block"></span>
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 control-label">Menu Order</label>
  <div class="col-md-1">
    <input name="menu_order" type="text" class="form-control" id="menu_order">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 control-label">Jenis Halaman</label>
  <div class="col-md-2">
    <select name="jenis" class="form-control" id="jenis">
      <option>- pilih -</option>
      <option value="modulle">Modulle</option>
      <option value="teks">Teks</option>
      <option value="pdf">PDF</option>
      <option value="url">Url</option>
    </select>
  </div>
  <div class="col-md-3">
    <input name="url" type="text" class="form-control" id="url" >
  </div>
</div>

<!-- input -->
<div class="form-group row">
  <label class="col-sm-2 control-label">Halaman Front</label>
    <div class="col-sm-2">
    <select name="status_front" class="form-control" id="status_front">
      <option>- pilih -</option>
      <option value="Aktif">Aktif</option>
      <option value="Non Aktif">Non Aktif</option>
    </select>
  </div>
</div>


<!-- input -->
<div class="form-group row">
  <label class="col-sm-2 control-label">Halaman Admin</label>
  <div class="col-sm-2">
    <select name="status_admin" class="form-control" id="status_admin">
      <option>- pilih -</option>
      <option value="Aktif">Aktif</option>
      <option value="Non Aktif">Non Aktif</option>
    </select>
  </div>
</div>
                                        
</form>

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->





<script src="<?php echo site_url('theme/plugins/datepicker/bootstrap-datepicker.js')?>"></script>
<script type="text/javascript">

  var save_method; //for save method string
  var table;
  var url = "<?php echo site_url(); ?>";

  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });

      //datepicker
      $('.datepicker').datepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
          todayHighlight: true,
          orientation: "top auto",
          todayBtn: true,
          todayHighlight: true,  
      });
      //set input/textarea/select event when change value, remove class error and remove text help block
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("select").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });

  });

   function add()
   {
      
      // alert("asd");
       save_method = 'add';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
       $('#modal-xl').modal('show'); // show bootstrap modal
       $('.modal-title').text('Tambah Menu'); // Set Title to Bootstrap modal title
   }

   function edit(id)
   {
       save_method = 'update';
       $('#form')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string
    
       //Ajax Load data from ajax
       $.ajax({
           url : "<?php echo site_url($data_ref['uri_controllers'].'/ajax_edit/')?>/" + id,
           type: "GET",
           dataType: "JSON",
           success: function(data)
           {
               $('[name="parent_id"]').val(data.parent_id);
               $('[name="id"]').val(data.id);
               $('[name="title"]').val(data.title);
               $('[name="menu_order"]').val(data.menu_order);
               $('[name="url"]').val(data.url);
               $('[name="jenis"]').val(data.jenis);
               $('[name="status_front"]').val(data.status_front);
               $('[name="status_admin"]').val(data.status_admin);
               $('#modal-xl').modal('show'); // show bootstrap modal when complete loaded
               $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error get data from ajax');
           }
       });
   }

   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
   }

   function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
       }
    
       // ajax adding data to database
       $.ajax({
           url : url,
           type: "POST",
           data: $('#form').serialize(),
           dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   $('#modal-xl').modal('hide');
                   reload_table();
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }

   function hapus(id){
    $.confirm({
      title: 'Confirm!',
      content: 'Apakah anda yakin menghapus data ini ?',
      buttons: {
        confirm: function () {
           $.ajax({
              url : url + "<?php echo $data_ref['uri_controllers']; ?>/ajax_delete/" + id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  reload_table();
                  Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Dihapus'
                   });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });
        },
        cancel: function () {
          
        }
      }
    });
  }

</script>





