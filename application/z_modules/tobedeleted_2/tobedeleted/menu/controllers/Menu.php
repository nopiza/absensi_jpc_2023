<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'menu');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Menu_model','menu');
		// $this->load->model('Group/Group_model','group');
		check_login();
	}

	public function index()
	{
		// if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
  //     {
  //        redirect('auth', 'refresh');
  //     }

      // $user = $this->ion_auth->user()->row();
      // $user_groups = $this->ion_auth->get_users_groups($user->id)->row();

      // $akses_menu = array('id_menu' => 14, 'tambah' => 1, 'lihat' => 3);
      // $user_data['tambah_view'] = $this->group->get_akses($user_groups->id,$akses_menu['id_menu'],$akses_menu['tambah'])->num_rows();
      // $user_data['lihat_view'] = $this->group->get_akses($user_groups->id,$akses_menu['id_menu'],$akses_menu['lihat'])->num_rows();

		$user_data['tambah_view'] = 1;
		$user_data['lihat_view'] = 1;

		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';
		$user_data['menu_active'] = 'Data Referensi';
		$user_data['sub_menu_active'] = 'Menu';
     	
      	// $this->load->view('header',$user_data);
     	$this->load->view('template/header');
		$this->load->view('view',$user_data);
		// $this->load->view('template/footer');
	}

	public function ajax_list()
	{

		$list = $this->menu->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {


				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';

				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

	


			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->id;
			$row[] = $post->title;
         	$row[] = $post->url;
         	$row[] = '<a class="btn btn-xs btn-primary" href="'.base_url('page/edit').'"> Isi Konten</a>';
			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->menu->count_all(),
						"recordsFiltered" => $this->menu->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->menu->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$this->_validate();
		$post_date = time();
		$post_date_format = date('Y-m-d h:i:s', $post_date);
      // $user = $this->ion_auth->user()->row();
		$data = array(
				'parent_id' 		=> $this->input->post('parent_id'),
				'title' 			=> $this->input->post('title'),
            	'url' 				=> $this->input->post('url'),
            	'jenis' 			=> $this->input->post('jenis'),
				'menu_order' 		=> $this->input->post('menu_order'),
				'status_front' 		=> $this->input->post('status_front'),
				'status_admin' 		=> $this->input->post('status_admin'),
            	'is_trash' 			=> 0
		);

		$insert = $this->menu->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$post_date = time();
		$post_date_format = date('Y-m-d h:i:s', $post_date);
		$data = array(
				'parent_id' 		=> $this->input->post('parent_id'),
				'title' 			=> $this->input->post('title'),
            	'url' 				=> $this->input->post('url'),
            	'jenis' 			=> $this->input->post('jenis'),
				'menu_order' 		=> $this->input->post('menu_order'),
				'status_front' 		=> $this->input->post('status_front'),
				'status_admin' 		=> $this->input->post('status_admin'),
            	'is_trash' 			=> 0
		);
		$this->menu->update(array('id' => $this->input->post('id')), $data);

		//Buat data di table content
		//cek apakah sudah ada
		$cek = $this->db->get_where('content', array('id_menu'=>$this->input->post('id')))->num_rows();
		if($cek == 0){
			$param = array(
				'id_menu' 	=> $this->input->post('id'),
				'menu'		=> $this->input->post('title')
			);

			$this->db->insert('content', $param);
		}
		
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->menu->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('parent_id') == '')
		{
			$data['inputerror'][] = 'parent';
			$data['error_string'][] = 'Parent harus diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('title') == '')
		{
			$data['inputerror'][] = 'title';
			$data['error_string'][] = 'Title Menu harus diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}



}
