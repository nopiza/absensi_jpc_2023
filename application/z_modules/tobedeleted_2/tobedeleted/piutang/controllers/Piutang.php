<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'piutang');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Piutang_model','piutang');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{

      $user_data['data_ref'] = $this->data_ref;
      $user_data['title'] = 'Sekolah';
      $user_data['menu_active'] = 'Data Referensi';
      $user_data['sub_menu_active'] = 'Sekolah';
     	
      $this->load->view('template/header',$user_data);
		$this->load->view('view',$user_data);
      // $this->load->view('template/footer',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->piutang->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {

				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->piutang_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->piutang_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
			$row[] = 'HTG-'.$post->piutang_id;
         	$row[] = tgl_indo($post->piutang_tanggal);
			$row[] = $post->piutang_keterangan;
			$row[] = rupiah($post->piutang_nominal);

			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->piutang->count_all(),
						"recordsFiltered" => $this->piutang->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

	public function ajax_edit($id)
	{
		$data = $this->piutang->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'piutang_tanggal' 		=> $this->input->post('piutang_tanggal'),
			'piutang_nominal' 		=> $this->input->post('piutang_nominal'),
			'piutang_keterangan' 		=> $this->input->post('piutang_keterangan')
		);

		$insert = $this->piutang->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'piutang_tanggal' 		=> $this->input->post('piutang_tanggal'),
			'piutang_nominal' 		=> $this->input->post('piutang_nominal'),
			'piutang_keterangan' 		=> $this->input->post('piutang_keterangan')
		);

		$this->piutang->update(array('piutang_id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('piutang',array('piutang_id'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul') == '')
		{
			$data['inputerror'][] = 'judul';
			$data['error_string'][] = 'Judul harus diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('isi_konten') == '')
		{
			$data['inputerror'][] = 'isi_konten';
			$data['error_string'][] = 'Konten harus diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}



}
