  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pengolahan Laporan IKM</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">

        <!-- /.card-header -->
        <div class="card-body">


    <div class="row">
        
        <table class="table table-bordered">
            <tr>
                <td width="9%" rowspan="2" align="center"> <b>NO. RESP</b> </td>
                <td colspan="9" align="center"> <b>NILAI UNSUR PELAYANAN </b></td>
                <td width="9%">  </td>

            </tr>
            <tr>
                <td width="9%" align="center"><b> U1 </b></td>
                <td width="9%" align="center"><b> U2 </b></td>
                <td width="9%" align="center"><b> U3 </b></td>
                <td width="9%" align="center"><b> U4 </b></td>
                <td width="9%" align="center"><b> U5 </b></td>
                <td width="9%" align="center"><b> U6 </b></td>
                <td width="9%" align="center"><b> U7 </b></td>
                <td width="9%" align="center"><b> U8 </b></td>
                <td width="9%" align="center"><b> U9 </b></td>
                <td width="9%">  </td>

            </tr>

            <?php
            $no = 1;
            $respond = $this->db->query("SELECT DISTINCT(id_koresponden) FROM `jawaban` WHERE id_pelayanan='$id_pelayanan'")->result();
            foreach ($respond as $rsp) { ?>

                <tr>
                <td width="9%" align="center"> <?=$no++;?> </td>
                <?php
                $id_koresponden = $rsp->id_koresponden;
                $jawaban = $this->db->query("SELECT * FROM `jawaban` WHERE id_koresponden='$id_koresponden' ORDER BY id_pertanyaan ASC")->result();
                foreach ($jawaban as $jwb) { 
                    echo '<td width="9%" align="center"> '.$jwb->jawaban.' </td>';
                } ?>
                <td width="9%" align="center"></td>
            </tr>

            <?php } ?>

            <!-- Jumlah bawah -->
            <tr>
                <td width="9%">  </td>
                <?php
                for ($per=1; $per < 10; $per++) { ?>
                <td width="9%" align="center">
                    <?php $jU[$per] = $this->db->query("SELECT SUM(jawaban) as jumlah FROM jawaban WHERE id_pertanyaan = '$per' AND id_pelayanan='$id_pelayanan'")->row_array();
                    echo $jU[$per]['jumlah'];?>
                </td>


            <?php } ?>

                <td width="9%">  </td>
            </tr>


            <!-- Jumlah bawah -->
            <tr>
                <td width="9%">  </td>
                <?php
                for ($per=1; $per < 10; $per++) { ?>
                <td width="9%" align="center">
                    <?php $jU[$per] = $this->db->query("SELECT SUM(jawaban) as jumlah FROM jawaban WHERE id_pertanyaan = '$per' AND id_pelayanan='$id_pelayanan'")->row_array();
                    echo $NRRUnsur[$per] = round(($jU[$per]['jumlah'] / $no) , 3);
                    
                    ?>
                </td>


            <?php } ?>

                <td width="9%"></td>
            </tr>


            <!-- Jumlah bawah -->
            <tr>
                <td width="9%">  </td>
                <?php
                for ($per=1; $per < 10; $per++) { ?>
                <td width="9%" align="center">
                    <?php 
                    $a[$per] = $NRRUnsur[$per] * 0.11;
                    echo round($a[$per],3);
                    @$subtot = @$subtot + $a[$per];
                    ?>
                </td>


            <?php } ?>

                <td width="9%">  <?=round($subtot,3);?>  </td>
            </tr>
            <tr>
                <td colspan="10"><b>IKM Unit Pelayanan</b></td>
                <td><b><?=round(($subtot*25),3);?></b></td>
            </tr>

        </table>
    </div>


        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>





<?php  $this->load->view('template/footer'); ?>


<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>


<?php

$usia_A = $this->db->query("SELECT COUNT(usia) as A FROM registrasi WHERE usia <= '20'")->row_array();
$usia_B = $this->db->query("SELECT COUNT(usia) as B FROM registrasi WHERE usia BETWEEN '21' AND '30' ")->row_array();
$usia_C = $this->db->query("SELECT COUNT(usia) as C FROM registrasi WHERE usia BETWEEN '31' AND '40' ")->row_array();
$usia_D = $this->db->query("SELECT COUNT(usia) as D FROM registrasi WHERE usia BETWEEN '41' AND '50' ")->row_array();
$usia_E = $this->db->query("SELECT COUNT(usia) as E FROM registrasi WHERE usia > '50'")->row_array();
?>

<script>
window.onload = function () {

var options = {
    animationEnabled: true,
    title: {
        text: "Bersadarkan Usia Responden"
    },
    axisY: {
        title: "Jumlah Dalam Kg"
    },
    axisX: {
        title: "Tahun 2020"
    },
    data: [{
        type: "column",

        dataPoints: [
            { label: "< 20 Tahun", y: <?=$usia_A['A'];?> },    
            { label: "21 - 30 Tahun", y: <?=$usia_B['B'];?> },   
            { label: "31 - 40 Tahun", y: <?=$usia_C['C'];?> },
            { label: "41 - 50 Tahun", y: <?=$usia_D['D'];?> }, 
            { label: "> 50 Tahun", y: <?=$usia_E['E'];?> }
            
        ]
    }]
};
$("#chartContainer").CanvasJSChart(options);




}
</script>