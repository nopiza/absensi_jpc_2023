<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboardkaryawan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));	
		$this->load->library('user_agent');
		check_login_karyawan();
	}
	
	public function index()
	{
		$data=array();
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		    );
		$data=array('csrf'=>$csrf);	

		$this->load->view('template/header_marketing');
		$this->load->view('dashboard');
		
	}

	public function pengaturan_awal(){

        $user_data['id_pegawai'] = $this->encryption->decrypt($this->session->userdata('id_pegawai'));
		$this->load->view('template/header_pengaturan');
		$this->load->view('pengaturan', $user_data);
		$this->load->view('template/footer');
	}

	public function ajax_edit($id)
	{
		$data = $this->db->query("SELECT * FROM kavling_peta a 
			LEFT JOIN customer b ON a.id_customer=b.id_customer 
			WHERE a.id_kavling = '$id' ")->row_array();
		echo json_encode($data);
	}



	public function absen_masuk()
	{

		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Data user gagal di dapatkan';
		}
 

		$data = array(
				'id_account' 		=> $this->encryption->decrypt($this->session->userdata('id_pegawai')),
				'tanggal_absensi' 	=> tglTime_now(),
            	'longitude_absensi' => $this->input->post('long'),
            	'latitude_absensi' 	=> $this->input->post('lat'),
				'type_absensi' 		=> 'MASUK',
				'ip_user' 			=> $this->input->ip_address(),
				'os_user' 			=> $this->agent->platform(),
				'browser_user' 		=> $agent,
				'catatan' 			=> ''
		);

		$insert = $this->db->insert('absensi',$data);
		$idnya = $this->db->insert_id();
		
		echo json_encode(array("status" => TRUE));
	}



	public function absen_pulang()
	{

		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Data user gagal di dapatkan';
		}
 

		$data = array(
				'id_account' 		=> $this->encryption->decrypt($this->session->userdata('id_pegawai')),
				'tanggal_absensi' 	=> tglTime_now(),
            	'longitude_absensi' => $this->input->post('long'),
            	'latitude_absensi' 	=> $this->input->post('lat'),
				'type_absensi' 		=> 'PULANG',
				'ip_user' 			=> $this->input->ip_address(),
				'os_user' 			=> $this->agent->platform(),
				'browser_user' 		=> $agent,
				'catatan' 			=> ''
		);

		$insert = $this->db->insert('absensi',$data);
		$idnya = $this->db->insert_id();	
		
		echo json_encode(array("status" => TRUE));
	}



	

}