<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Web</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">


        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url('assets/admin/'); ?>plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="<?= base_url('assets/admin/'); ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <!-- Toastr -->
        <link rel="stylesheet" href="<?= base_url('assets/admin/'); ?>plugins/toastr/toastr.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url('assets/admin/'); ?>dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

      
        <link href="<?=base_url('theme/plugins/datatables/jquery.dataTables.min.css');?>" rel="stylesheet">
        <link href="<?=base_url('theme/dist/css/Lobibox.min.css');?>" rel="stylesheet" />
        <link href="<?=base_url('theme/dist/css/jquery-confirm.min.css'); ?>" rel="stylesheet">





</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-blue navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>

            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="<?=base_url();?>">
                        Admin Web
                    </a>

                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="<?= base_url(); ?>" class="brand-link">
                <img src="<?= base_url('assets/admin/'); ?>dist/img/logo.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light"><strong>Admin Web</strong></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="<?= base_url('dashboard'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="<?= base_url('kalkulator'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-calculator"></i>
                                <p>
                                    Kalkulator
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?= base_url('transaksi'); ?>" class="nav-link">
                                <i class="nav-icon fa fa-cart-plus"></i>
                                <p>
                                    Transaksi Pembelian 
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?= base_url('statistik'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-file"></i>
                                <p>
                                    History Pembayaran
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?= base_url('pembayaran'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-file"></i>
                                <p>
                                    Transaksi Kavling
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                Keuangan
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('transaksi_keuangan'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Transaksi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('hutang'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Catatan Hutang</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('piutang'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Catatan Piutang</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('bank'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Rekening</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('laporan'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Laporan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pengguna'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Dashboard Keuangan</p>
                                </a>
                            </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                Master Data
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('kavling'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kavling</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('customer'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Customer</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('kategori'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kategori</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('konfigurasi'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Pengaturan Aplikasi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pengguna'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Pengaturan Pengguna</p>
                                </a>
                            </li>
                            </ul>
                        </li>


                        <li class="nav-item">
                            <a href="<?= base_url('login/logout'); ?>" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>
                                    Keluar
                                </p>
                            </a>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>